@extends('admin.layouts.app')
@push('styles')
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
@endpush
@section('content')
    <div class="content-body">
        {{-- tables section --}}
        <div class="row py-5">
            {{-- for report table --}}
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10 text-capitalize">
                        sales <i class="fas fa-chevron-right"></i> <span style="color:#2570D3;">all sales</span>
                        <h3>Welcome to Sales</h3>
                    </div>
                    <div class="col-md-2">
                        <div class="table-print text-right py-1 mb-1 pr-3">
                            <button onclick="exportFile('exampleTable','json')"><i class="fas fa-print"></i></button>
                            <button onclick="exportFile('exampleTable','pdf')"><i class="fas fa-file-pdf"></i></button>
                            <button onclick="exportFile('exampleTable','csv')"><i class="fas fa-file-excel"></i></button>
                        </div>
                    </div>
                </div>
                <div class="bg-theam-secondary table-search-head p-1">
                    <div class="row">
                        <div class="col-md-2 mr-0 pr-0">
                            <select name="size" id="size">
                                <option value="1000">1000</option>
                            </select>
                            <select name="size" id="size">
                                <option value="all branch">All Branch</option>
                            </select>
                        </div>
                        <div class="col-md-4 mx-0 px-0 mt-1">
                            <ul>
                                <li><a href="javascript:;">
                                        <i class="fas fa-arrow-left"></i>
                                    </a></li>
                                <li><a href="javascript:;">
                                        Today
                                    </a></li>
                                <li><a href="javascript:;">
                                        <i class="fas fa-arrow-right"></i>
                                    </a></li>
                            </ul>
                            <ul class="ml-1">
                                <li><a href="javascript:;">
                                        <i class="fas fa-arrow-left"></i>
                                    </a></li>
                                <li><a href="javascript:;">
                                        Month
                                    </a></li>
                                <li><a href="javascript:;">
                                        <i class="fas fa-arrow-right"></i>
                                    </a></li>
                            </ul>
                        </div>
                        <div class="col-md-6 ml-0 pl-0">
                            <input type="text" placeholder="From">
                            <input type="text" placeholder="To">
                            <input type="text" class="float-right" placeholder="Search...">
                        </div>
                    </div>
                </div>
                <div class="bg-theam-secondary table-summary my-1">
                    Total Costomer Bill: NRP 3,00,000 |
                    Total Costomer Paid: NRP 3,00,000 |
                </div>
                <table class="table table2 table-hover" id="exampleTable">
                    <thead>
                        <tr>
                            <th scope="col">S.N</th>
                            <th scope="col">Date</th>
                            <th scope="col">Bill No.</th>
                            <th scope="col">Customer Name</th>
                            <th scope="col">Bill Amount</th>
                            <th scope="col">Paid Amount</th>
                            <th scope="col">Due Amount</th>
                            <th scope="col" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>2077/10/11</td>
                            <td>121212284654</td>
                            <td>Alok Chaudhary</td>
                            <td>3,00,000</td>
                            <td>3,00,000</td>
                            <td>-</td>
                            <td class="text-center">
                                <a href="javascript:;">
                                    <span class="badge badge-pill badge-primary">
                                        <i class="fa fa-eye"></i>
                                    </span>
                                </a>
                                <a href="javascript:;">
                                    <span class="badge badge-pill badge-danger">X</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>2077/10/11</td>
                            <td>121212284654</td>
                            <td>Alok Chaudhary</td>
                            <td>3,00,000</td>
                            <td>3,00,000</td>
                            <td>-</td>
                            <td class="text-center">
                                <a href="javascript:;">
                                    <span class="badge badge-pill badge-primary">
                                        <i class="fa fa-eye"></i>
                                    </span>
                                </a>
                                <a href="javascript:;">
                                    <span class="badge badge-pill badge-danger">X</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>2077/10/11</td>
                            <td>121212284654</td>
                            <td>Alok Chaudhary</td>
                            <td>3,00,000</td>
                            <td>3,00,000</td>
                            <td>-</td>
                            <td class="text-center">
                                <a href="javascript:;">
                                    <span class="badge badge-pill badge-primary">
                                        <i class="fa fa-eye"></i>
                                    </span>
                                </a>
                                <a href="javascript:;">
                                    <span class="badge badge-pill badge-danger">X</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>2077/10/11</td>
                            <td>121212284654</td>
                            <td>Alok Chaudhary</td>
                            <td>3,00,000</td>
                            <td>3,00,000</td>
                            <td>-</td>
                            <td class="text-center">
                                <a href="javascript:;">
                                    <span class="badge badge-pill badge-primary">
                                        <i class="fa fa-eye"></i>
                                    </span>
                                </a>
                                <a href="javascript:;">
                                    <span class="badge badge-pill badge-danger">X</span>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <nav aria-label="Page navigation pagination1">
                    <ul class="pagination justify-content-end">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous"
                                style="background: #C1D6EC;border: 2px solid white;">
                                <i class="fas fa-arrow-left"></i>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#"
                                style="background: #C1D6EC;border: 2px solid white;">01</a></li>
                        <li class="page-item"><a class="page-link" href="#"
                                style="background: #C1D6EC;border: 2px solid white;">02</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next"
                                style="background: #C1D6EC;border: 2px solid white;">
                                <i class="fas fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="text-right col-md-12">
                <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"
                    data-target="#addNewExpense">New Expense</button>
                <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"
                    data-target="#addNewBranch">New Branch</button>
                <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"
                    data-target="#addNewItem">New Item</button>
            </div>
        </div>
        {{-- modals --}}
        <div class="row">
            {{-- new expense modal --}}
            <div class="col-md-12">
                <div class="modal fade modal_cust" id="addNewExpense" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal_ac_head modal-md" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalCenterTitle">add new expense</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <form action="" method="post" enctype="multipart/form-data" class="bg-theam-secondary pt-4">
                                @csrf
                                <div class="modal-body row">
                                    {{-- branch info --}}
                                    <div class="col-md-6">
                                        <div class="group">
                                            <div class="group-caption">Basic Information</div>
                                            <div class="row mb-2">
                                                <div class="col-md-4">Branch</div>
                                                <div class="col-md-8">
                                                    <select name="branch" id="branch" class="form-control">
                                                        <option value="">Select Branch</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-md-4">Date</div>
                                                <div class="col-md-8">
                                                    <input type="date" name="date" id="date" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">Account Head</div>
                                                <div class="col-md-8">
                                                    <select name="account_head" id="account_head" class="form-control">
                                                        <option value="">Account Head</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- account details --}}
                                    <div class="col-md-6">
                                        <div class="group">
                                            <div class="group-caption">Account Detail</div>
                                            <div class="row mb-2">
                                                <div class="col-md-4">Expense Amount</div>
                                                <div class="col-md-8">
                                                    <input type="text" name="expense_amount" class="form-control"
                                                        placeholder="NRP 1,20,000.00">
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-md-4">Paid Amount</div>
                                                <div class="col-md-8">
                                                    <input type="text" name="paid_amount" class="form-control"
                                                        placeholder="NRP 1,20,000.00">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">Remaining Amount</div>
                                                <div class="col-md-8">
                                                    <input type="text" name="rmaining_amount" class="form-control"
                                                        placeholder="NRP 1,20,000.00">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- payment info --}}
                                    <div class="col-md-6 mt-4">
                                        <div class="group">
                                            <div class="group-caption">Payment Information</div>
                                            <div class="row mb-2">
                                                <div class="col-md-4">Payment Method</div>
                                                <div class="col-md-8">
                                                    <select name="payment_method" id="payment_method" class="form-control">
                                                        <option value="">Select Method</option>
                                                        <option value="bank">Bank</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-md-4">Bank</div>
                                                <div class="col-md-8">
                                                    <input type="input" name="bank" id="bank" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">Transaction ID</div>
                                                <div class="col-md-8">
                                                    <input type="input" name="transaction_id" id="transaction_id"
                                                        placeholder="1547220XD" class="form-control">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-4">
                                        <div class="group">
                                            <div class="group-caption">Expense Type</div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="expense_type"
                                                            id="direct_expense" value="1" checked>
                                                        <label for="direct_expense">Direct Expense</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="expense_type"
                                                            id="indirect_expense" value="2">
                                                        <label for="indirect_expense">Indirect Expense</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="group mt-3">
                                            <div class="group-caption">Description</div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-check">
                                                        <textarea class="form-control" type="text" name="description"
                                                            id="description"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right mt-3">
                                        <button type="button" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- new branch modal --}}
            <div class="col-md-12">
                <div class="modal fade modal_cust" id="addNewBranch" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalCenterTitle">new branch</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <form action="" method="post" class="bg-theam-secondary pt-4" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body row">
                                    {{-- branch info --}}
                                    <div class="col-md-6">
                                        <div class="group">
                                            <div class="group-caption">Basic Information</div>
                                            <div class="row mb-2">
                                                <div class="col-md-4">Branch</div>
                                                <div class="col-md-8">
                                                    <input type="text" name="branch_id" id="branch_id" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-md-4">Branch Name</div>
                                                <div class="col-md-8">
                                                    <input type="text" name="branch_name" id="branch_name"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-md-4">Branch Address</div>
                                                <div class="col-md-8">
                                                    <input type="text" name="branch_address" id="branch_address"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">Phone Number</div>
                                                <div class="col-md-8">
                                                    <input type="text" name="contact" id="contact" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="group">
                                            <div class="group-caption">Description</div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-check">
                                                        <textarea class="form-control" type="text" name="description"
                                                            id="description" style="height: 134px"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right mt-3">
                                        <button type="button" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- new item modal --}}
            <div class="col-md-12">
                <div class="modal fade modal_cust" id="addNewItem" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal_ac_head modal-md" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalCenterTitle">add new expense</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <form action="" method="post" enctype="multipart/form-data" class="bg-theam-secondary pt-4">
                                @csrf
                                <div class="modal-body row">
                                    {{-- branch info --}}
                                    <div class="col-md-6">
                                        <div class="group mb-3">
                                            <div class="group-caption">Basic Information</div>
                                            <div class="row mb-2">
                                                <div class="col-md-8"></div>
                                                <div class="col-md-4">
                                                    <input type="file" name="image" id="image"
                                                        style="width: 90px;overflow: hidden;">
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-md-4">Item ID</div>
                                                <div class="col-md-8">
                                                    <input type="text" name="item_id" id="item_id" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-md-4">Item Name</div>
                                                <div class="col-md-8">
                                                    <input type="text" name="item_name" id="item_name" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">Item Group</div>
                                                <div class="col-md-8">
                                                    <select name="item_group" id="item_group" class="form-control">
                                                        <option value="">Select Group</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-md-12 text-right">
                                                    <button type="button" class="badge badge-success">Add Group</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="group mb-3">
                                            <div class="group-caption">Unit Type</div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="unit_type"
                                                            id="kg" value="kg" checked>
                                                        <label for="kg"> K.G</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="unit_type"
                                                            id="quintal" value="quintal">
                                                        <label for="quintal">Quintal</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="unit_type"
                                                            id="piece" value="piece">
                                                        <label for="piece">Piece</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="unit_type"
                                                            id="packet" value="packet">
                                                        <label for="packet">Packet</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="group mb-3">
                                            <div class="group-caption">Account Detail</div>
                                            <div class="row mb-2">
                                                <div class="col-md-4">Minimum Stock</div>
                                                <div class="col-md-8">
                                                    <input type="text" name="stock_quantity" id="stock_quantity"
                                                        placeholder="Stock Quantity" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">Expiry Date</div>
                                                <div class="col-md-8">
                                                    <input type="date" class="form-control" name="expiry_date"
                                                        id="expiry_date">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="group mb-3">
                                            <div class="group-caption">Settings</div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="settings"
                                                            id="vat_applicable" value="1" checked>
                                                        <label for="vat_applicable"> VAT Applicable</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="settings"
                                                            id="not_applicable" value="0">
                                                        <label for="not_applicable">Not Applicable</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="group">
                                            <div class="group-caption">Description</div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-check">
                                                        <textarea class="form-control" type="text" name="description"
                                                            id="description" style="min-height: 62px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right mt-3">
                                        <button type="button" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- purchase section --}}
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-9">
                        <h3>New Purchase</h3>
                    </div>
                    <div class="col-md-3 float-right text-right">
                        <select name="branch" id="branch" class="form-control"
                            style="background: #1B52BF;    color: white;    border-radius: 6px;    height: 31px">
                            <option value="">Select Branch</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3 bg-theam-secondary border-right-white py-3">
                <div class="mb-3">
                    <span>Supplier</span>
                    <span class="float-right btn btn-primary">New Supplier</span>
                </div>
                <div class="input-group py-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="supplier_name">
                            <i class="fa fa-user"></i>
                        </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Supplier Name" name="supplier_name">
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="date">Purchase Date </label>
                    </div>
                    <div class="col-md-8">
                        <input type="date" class="form-control" id="purchase_date" name="purchase_date">
                    </div>
                </div>
                <div class="row py-2">
                    <div class="col-md-4">
                        <label for="purchase_id">Purchase ID </label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="purchase_id" name="purchase_id"
                            placeholder="454667168721">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="chalan_no">Chalan No. </label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="chalan_no" name="chalan_no" placeholder="454667168721">
                    </div>
                </div>
                <div class="input-group py-2">
                    <input type="file" name="file" id="file" style="display: none;">
                    <input type="text" class="form-control" placeholder="Upload PDF / Image" readonly>
                    <label for="file" class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">Browse</span>
                    </label>
                </div>
                <div class="form-group form-inline">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="export">
                        <label class="form-check-label" for="export">
                            Export
                        </label>
                    </div>
                    <div class="form-check pl-3">
                        <input class="form-check-input" type="checkbox" id="shipping_details">
                        <label class="form-check-label" for="shipping_details">
                            Shipping Details
                        </label>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <label for="payment_method">Payment Method </label>
                    </div>
                    <div class="col-md-8">
                        <select class="form-control" id="payment_method" name="payment_method">
                            <option value="">Select Method</option>
                        </select>
                    </div>
                </div>
                <div class="row py-2">
                    <div class="col-md-4">
                        <label for="bank">Bank </label>
                    </div>
                    <div class="col-md-8">
                        <select class="form-control" id="bank" name="bank">
                            <option value="">Select Bank</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="txn_id">Txn. ID </label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="txn_id" name="txn_id" placeholder="454667168721">
                    </div>
                </div>
            </div>
            <div class="col-md-9 bg-theam-secondary border-left-white py-3">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Item</h3>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="barcode">
                                    <i class="fas fa-barcode"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" placeholder="Barcode" name="barcode">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group" style="padding-top: 2.2rem !important;">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="product">
                                    <i class="fas fa-briefcase"></i>
                                </span>
                            </div>
                            <select class="form-control" name="product">
                                <option value="">Select Product</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h5>Expiry Date</h3>
                            <div class="input-group pt-1">
                                <input type="date" class="form-control" placeholder="Supplier Name" name="expiry_date">
                            </div>
                    </div>
                </div>
                <div class="row pt-3">
                    <div class="col-md-2">
                        <h6>Quantity</h6>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="1" name="quantity">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="quantity">
                                    <i class="fas fa-puzzle-piece"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <h6>Bill Rate</h6>
                        <div class="input-group">
                            <input type="text" class="form-control" name="amount" placeholder="Amount">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h6>Purchase Rate</h6>
                        <div class="input-group">
                            <input type="text" class="form-control" name="purchase_rate" placeholder="Amount">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <h6>Sale Rate</h6>
                        <div class="input-group">
                            <input type="text" class="form-control" name="sales_rate" placeholder="Amount">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h6>Sale Discount</h6>
                        <div class="input-group">
                            <input type="text" class="form-control" name="sales_discount">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="quantity" style="min-width: 0px;">
                                    %
                                </span>
                            </div>
                            <input type="text" class="form-control" name="sales_discount">

                        </div>
                    </div>
                </div>
                <div class="row pt-3">
                    <div class="col-md-2 pt-2">
                        <div class="form-group form-inline">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="other_field">
                                <label class="form-check-label" for="other_field">
                                    Other Field
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-inline">
                            <div class="pr-1" style="font-weight: 600;"> Total </div>
                            <input type="text" name="total" placeholder="Amount" class="form-control text-right" readonly>
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="btn btn-success" type="button">Add Item</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table1 table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">S.N</th>
                                    <th scope="col">Particular</th>
                                    <th scope="col">Qty</th>
                                    <th scope="col">Rate</th>
                                    <th scope="col">Total</th>
                                    <th scope="col" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Mark</td>
                                    <td>1.5 KG</td>
                                    <td>20</td>
                                    <td>100</td>
                                    <td class="text-center">
                                        <a href="javascript:;">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="javascript:;">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Mark</td>
                                    <td>1.5 KG</td>
                                    <td>20</td>
                                    <td>100</td>
                                    <td class="text-center">
                                        <a href="javascript:;">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="javascript:;">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Mark</td>
                                    <td>1.5 KG</td>
                                    <td>20</td>
                                    <td>100</td>
                                    <td class="text-center">
                                        <a href="javascript:;">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="javascript:;">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Mark</td>
                                    <td>1.5 KG</td>
                                    <td>20</td>
                                    <td>100</td>
                                    <td class="text-center">
                                        <a href="javascript:;">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="javascript:;">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12  bg-theam-secondary py-3 mt-1">
                <div class="row">
                    <div class="col-md-3">
                        <h3>Note</h3>
                        <textarea name="note" id="note" class="form-control"></textarea>
                    </div>
                    <div class="col-md-7">
                        <div class="row pt-3">
                            <div class="col-md-3">
                                <h6>Grand Total</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="grand_total_amount" placeholder="Amount">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h6>VAT-Able Amount</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="vatable_amount" placeholder="Amount">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h6>VAT</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="vat">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="vat" style="min-width: 0px;">
                                            %
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" name="vat">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h6>Payable Amount</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="payable_amount" placeholder="Amount">
                                </div>
                            </div>
                        </div>
                        <div class="row pt-3">
                            <div class="col-md-3">
                                <h6>Paid Amount</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="paid_amount" placeholder="Amount">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h6>Due Amount</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="due_amount" placeholder="Amount">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 text-right">
                        <button type="button" class="btn btn-success" style="width:74%">Submit</button>
                        <button type="button" class="btn btn-warning my-2" style="width:74%">Reset Item</button>
                        <button type="button" class="btn btn-danger" style="width:74%">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- sales section --}}
        <div class="row py-5">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-9">
                        <h3>Sales and Billing</h3>
                    </div>
                    <div class="col-md-3 float-right text-right">
                        <select name="branch" id="branch" class="form-control"
                            style="background: #1B52BF;    color: white;    border-radius: 6px;    height: 31px">
                            <option value="">Select Branch</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3 bg-theam-secondary border-right-white py-3">
                <div class="mb-3">
                    <span>Customer</span>
                    <span class="float-right btn btn-primary">New Customer</span>
                </div>
                <div class="input-group py-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="customer_name">
                            <i class="fa fa-user"></i>
                        </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Customer Name" name="customer_name">
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="billing_date">Billing Date </label>
                    </div>
                    <div class="col-md-8">
                        <input type="date" class="form-control" id="billing_date" name="billing_date">
                    </div>
                </div>
                <div class="row py-2">
                    <div class="col-md-4">
                        <label for="bill_no">Bill Number </label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="bill_no" name="bill_no" placeholder="454667168721">
                    </div>
                </div>
                <div class="form-group form-inline">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="export">
                        <label class="form-check-label" for="export">
                            Export
                        </label>
                    </div>
                    <div class="form-check pl-3">
                        <input class="form-check-input" type="checkbox" id="shipping_details">
                        <label class="form-check-label" for="shipping_details">
                            Shipping Details
                        </label>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <label for="payment_method">Payment Method </label>
                    </div>
                    <div class="col-md-8">
                        <select class="form-control" id="payment_method" name="payment_method">
                            <option value="">Select Method</option>
                        </select>
                    </div>
                </div>
                <div class="row py-2">
                    <div class="col-md-4">
                        <label for="bank">Bank </label>
                    </div>
                    <div class="col-md-8">
                        <select class="form-control" id="bank" name="bank">
                            <option value="">Select Bank</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="txn_id">Txn. ID </label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="txn_id" name="txn_id" placeholder="454667168721">
                    </div>
                </div>
            </div>
            <div class="col-md-9 bg-theam-secondary border-left-white py-3">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Item</h3>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="barcode">
                                    <i class="fas fa-barcode"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" placeholder="Barcode" name="barcode">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group" style="padding-top: 2.2rem !important;">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="product">
                                    <i class="fas fa-briefcase"></i>
                                </span>
                            </div>
                            <select class="form-control" name="product">
                                <option value="">Select Product</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h5>Expiry Date</h3>
                            <div class="input-group pt-1">
                                <input type="date" class="form-control" placeholder="Supplier Name" name="expiry_date">
                            </div>
                    </div>
                </div>
                <div class="row pt-3">
                    <div class="col-md-3">
                        <h6>Stock</h6>
                        <div class="input-group">
                            <input type="text" class="form-control" name="stock" placeholder="Stocks...">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h6>Quantity</h6>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="1" name="quantity">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="quantity">
                                    <i class="fas fa-puzzle-piece"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h6>Rate</h6>
                        <div class="input-group">
                            <input type="text" class="form-control" name="rate" placeholder="Rate">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h6>Discount</h6>
                        <div class="input-group">
                            <input type="text" class="form-control" name="discount">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="quantity" style="min-width: 0px;">
                                    %
                                </span>
                            </div>
                            <input type="text" class="form-control" name="discount">

                        </div>
                    </div>
                </div>
                <div class="row pt-3">
                    <div class="col-md-4">
                        <div class="form-group form-inline">
                            <div class="pr-1" style="font-weight: 600;"> Total </div>
                            <input type="text" name="total" placeholder="Amount" class="form-control text-right" readonly>
                        </div>
                    </div>
                    <div class="col-md-8 text-right">
                        <div class="btn btn-success" type="button">Add Item</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table1 table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">S.N</th>
                                    <th scope="col">Particular</th>
                                    <th scope="col">Qty</th>
                                    <th scope="col">Rate</th>
                                    <th scope="col">Total</th>
                                    <th scope="col" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Mark</td>
                                    <td>1.5 KG</td>
                                    <td>20</td>
                                    <td>100</td>
                                    <td class="text-center">
                                        <a href="javascript:;">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="javascript:;">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Mark</td>
                                    <td>1.5 KG</td>
                                    <td>20</td>
                                    <td>100</td>
                                    <td class="text-center">
                                        <a href="javascript:;">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="javascript:;">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Mark</td>
                                    <td>1.5 KG</td>
                                    <td>20</td>
                                    <td>100</td>
                                    <td class="text-center">
                                        <a href="javascript:;">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="javascript:;">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Mark</td>
                                    <td>1.5 KG</td>
                                    <td>20</td>
                                    <td>100</td>
                                    <td class="text-center">
                                        <a href="javascript:;">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="javascript:;">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12  bg-theam-secondary py-3 mt-1">
                <div class="row">
                    <div class="col-md-3">
                        <h3>Note</h3>
                        <textarea name="note" id="note" class="form-control"></textarea>
                    </div>
                    <div class="col-md-7">
                        <div class="row pt-3">
                            <div class="col-md-3">
                                <h6>Grand Total</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="grand_total_amount" placeholder="Amount">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h6>Discount</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="discount">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="discount" style="min-width: 0px;">
                                            %
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" name="discount">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h6>VAT-Able Amount</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="vatable_amount" placeholder="Amount">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h6>VAT</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="vat">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="vat" style="min-width: 0px;">
                                            %
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" name="vat">
                                </div>
                            </div>
                        </div>
                        <div class="row pt-3">
                            <div class="col-md-3">
                                <h6>Extra Charges</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="extra_charge" placeholder="Amount">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h6>Payable Amount</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="payable_amount" placeholder="Amount">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h6>Paid Amount</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="paid_amount" placeholder="Amount">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h6>Due Amount</h6>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="due_amount" placeholder="Amount">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 text-right">
                        <button type="button" class="btn btn-success" style="width:74%">Submit</button>
                        <button type="button" class="btn btn-warning my-2" style="width:74%">Reset Item</button>
                        <button type="button" class="btn btn-danger" style="width:74%">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.5/jspdf.plugin.autotable.min.js"></script>
    <script src="https://www.jqueryscript.net/demo/export-table-json-csv-txt-pdf/src/tableHTMLExport.js"></script>
    <script>
        function exportFile(table, to) {
            $(`#${table}`).tableHTMLExport({
                type: to,
                filename: `${table}.${to}`
            });
        }

    </script>
@endpush
