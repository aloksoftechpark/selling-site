@extends('admin.layouts.app')
@push('styles')
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
@endpush
@section('content')
    <style>
        .bg-theam-one{
            background: #C1D6EC;
        }
        .theam-lists .nav-link{
            font-size: 16px;
            border-bottom: 2px solid #9CABC2;
        }
        .theam-lists .nav-link small{
            margin-left: 1.3rem;
        }
        .theam-lists .nav-pills .nav-link.active i,.theam-lists .nav-pills .nav-link.active span{
            color: #1451C0;
        }
        .theam-lists .nav-pills .nav-link.active {
            background-color: #C1D6EC;
        }
        .theam-lists .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
             color: black;
        }
    </style>
    <div class="content-body">
        {{-- tables section --}}
        <div class="row py-5">
            {{-- for report table --}}
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10 text-capitalize">
                        Pages <i class="fas fa-chevron-right"></i> User Pages <i class="fas fa-chevron-right"></i> <span style="color:#2570D3;">Profile Settings</span>
                        <h3>Profile Settings</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="bg-theam-one theam-lists">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link" id="general-setting-tab" data-toggle="pill" href="#v-general-setting" role="tab" aria-controls="v-general-setting" aria-selected="true">
                                    <i class="fas fa-cog"></i>
                                    <span>General Settings </span> <br>
                                    <small>about you company information</small>
                                </a>
                                <a class="nav-link" id="v-security-tab" data-toggle="pill" href="#v-security" role="tab" aria-controls="v-security" aria-selected="false">
                                    <i class="fas fa-shield-alt"></i>
                                    <span> Security </span> <br>
                                    <small>manage your security information </small>
                                </a>
                                <a class="nav-link" id="v-role-permission-tab" data-toggle="pill" href="#v-role-permission" role="tab" aria-controls="v-role-permission" aria-selected="false">
                                    <i class="fas fa-users-cog"></i>
                                    <span>  Role & Permission </span> <br>
                                    <small>create role for users </small>
                                </a>
                                <a class="nav-link active" id="v-academic-year-tab" data-toggle="pill" href="#v-academic-year" role="tab" aria-controls="v-role-permission" aria-selected="false">
                                    <i class="fas fa-calendar-alt"></i>
                                    <span>  Academic Year </span> <br>
                                    <small>update academic year </small>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade" id="v-general-setting" role="tabpanel" aria-labelledby="general-setting-tab">
                                general setting
                            </div>
                            <div class="tab-pane fade" id="v-security" role="tabpanel" aria-labelledby="v-security-tab">
                                security
                            </div>
                            <div class="tab-pane fade" id="v-role-permission" role="tabpanel" aria-labelledby="v-role-permission-tab">
                                role and permission
                            </div>
                            <div class="tab-pane fade show active" id="v-academic-year" role="tabpanel" aria-labelledby="v-academic-year-tab">
                                <form method="post" action="" class="bg-theam-one p-4">
                                    <h4>Academic Year</h4>
                                    <hr style="margin-top: 0.5rem; border-top: 2px solid #9CABC2;border-color: #9CABC2;">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label for="academic_year">Change Academic Year</label>
                                            <select id="academic_year" class="form-control mx-sm-3" style="min-width: 175px;height: 30px;">
                                                <option value="2077">2077</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button class="btn btn-success">SUBMIT</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
