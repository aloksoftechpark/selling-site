@extends('admin.layouts.app')
@section('content')
    @push('styles')
        <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    @endpush

    <div class="container">
        <table class="table" id="example">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">First</th>
                    <th scope="col">Last</th>
                    <th scope="col">Handle</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>@twitter</td>
                </tr>
            </tbody>
        </table>
        <p class="lead">
            <button id="json" class="btn btn-primary">TO JSON</button>
            <button id="csv" class="btn btn-info">TO CSV</button>
            <button id="pdf" class="btn btn-danger">TO PDF</button>
        </p>
    </div>
    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.5/jspdf.plugin.autotable.min.js"></script>
        <script src="https://www.jqueryscript.net/demo/export-table-json-csv-txt-pdf/src/tableHTMLExport.js"></script>
        <script>
            $("#json").on("click", function() {
                $("#example").tableHTMLExport({
                    type: "json",
                    filename: "sample.json",
                });
            });
            $("#csv").on("click", function() {
                $("#example").tableHTMLExport({
                    type: "csv",
                    filename: "sample.csv"
                });
            });
            $("#pdf").on("click", function() {
                $("#example").tableHTMLExport({
                    type: "pdf",
                    filename: "sample.pdf"
                });
            });

        </script>
    @endpush
@endsection
