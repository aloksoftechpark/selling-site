<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8" />

    <!--====== Title ======-->
    <title>Softechpark e-billing</title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--====== Favicon Icon ======-->
    {{--    <link rel="shortcut icon" href="assets/images/" type="image/png">--}}
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/logosmall.png')}}">
{{--    <link rel="stylesheet" href="{{asset('assets/css/changes.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('nepali.datepicker/nepali.datepicker.v2.2.min.css')}}" />--}}
    <!--====== Animate CSS ======-->
    <link rel="stylesheet" href="assets/css/animate.css">


    <!--====== Magnific Popup CSS ======-->
    <link rel="stylesheet" href="assets/css/magnific-popup.css" />

    <!--====== Slick CSS ======-->
    <link rel="stylesheet" href="assets/css/slick.css" />

    <!--====== Line Icons CSS ======-->
    <link rel="stylesheet" href="assets/css/LineIcons.css" />

    <!--====== Font Awesome CSS ======-->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />

    <!--====== Bootstrap CSS ======-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />

    <!--====== Google fonts ======-->
    <link href="https://fonts.googleapis.com/css?family=Bebas+Neue&display=swap" rel="stylesheet" />

    <!--====== Default CSS ======-->
    <link rel="stylesheet" href="assets/css/default.css" />

    <!--====== Style CSS ======-->
    <link rel="stylesheet" href="assets/css/style.css" />
</head>

<body>
<!--[if IE]>
<p class="browserupgrade">
    You are using an <strong>outdated</strong> browser. Please
    <a href="https://browsehappy.com/">upgrade your browser</a> to improve
    your experience and security.
</p>
<![endif]-->
<!--====== PRELOADER PART START ======-->
<div class="preloader">
    <div class="loader">
        <div class="ytp-spinner">
            <div class="ytp-spinner-container">
                <div class="ytp-spinner-rotator">
                    <div class="ytp-spinner-left">
                        <div class="ytp-spinner-circle"></div>
                    </div>
                    <div class="ytp-spinner-right">
                        <div class="ytp-spinner-circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--====== PRELOADER PART ENDS ======-->

<!--====== HEADER PART START ======-->

<header class="header-area">
    <div class="navbar-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand" href="index.html">
                            <img src="assets/images/logo.png" alt="Logo" />
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="toggler-icon"></span>
                            <span class="toggler-icon"></span>
                            <span class="toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                            <ul id="nav" class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a class="page-scroll" href="#home">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="page-scroll" href="#features">Features</a>
                                </li>
                                <li class="nav-item">
                                    <a class="page-scroll" href="#about">About</a>
                                </li>
                                <li class="nav-item">
                                    <a class="page-scroll" href="#facts">plans</a>
                                </li>
                                <li class="nav-item">
                                    <a class="page-scroll" href="#team">Contact</a>
                                </li>
                                <li class="nav-item navbar-btn d-none d-sm-inline-block has_children">
                                    <a class="main-btn sticky_acc" data-scroll-nav="0" href="#pricing" data-toggle="modal"
                                       data-target="#registerAccount">Account</a>
                                </li>
                                <!--  <li class="nav-item">
                                                      <a class="page-scroll" href="#blog">Blog</a>
                                                    </li> -->
                            </ul>
                        </div>
                        <!-- navbar collapse -->

                        <!--  <div class="navbar-btn d-none d-sm-inline-block">
                                          <a class="main-btn" data-scroll-nav="0" href="#pricing">Account</a>
                                        </div> -->
                    </nav>
                    <!-- navbar -->
                </div>
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </div>
    <!-- navbar area -->

    <div id="home" class="header-hero bg_cover" style="background-image: url(assets/images/banner-bg.svg);">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="header-hero-content text-center">
                        <h3 class="header-sub-title wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.2s">
                            SOFTECHPARK E-BILLING AND BUSINESS MANAGEMENT
                        </h3>
                        <!--  <h2 class="header-title wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.5s">Kickstart Your SaaS or App Site</h2> -->
                        <p class="text wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.8s">
                            One of the fastest and secure ebilling software on the market.
                        </p>
                    </div>
                    <!-- header hero content -->
                </div>
            </div>
            <!-- row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="header-hero-image wow fadeIn" data-wow-duration="1.3s" data-wow-delay="1.4s">
                        <img src="assets/images/header-hero.png" alt="hero" />
                        <div class="position_slider">
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img class="d-block w-100" src="assets/images/slider1.jpg" alt="First slide" />
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="assets/images/slider2.jpg" alt="Second slide" />
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="assets/images/slider3.jpg" alt="Third slide" />
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        @if(Auth::guard('web')->check())
                        <!-- position_slider -->
                        <div class="first_btn">
                            <a href="javascript:;" class="main-btn wow fadeInUp" data-toggle="modal" data-target="#trialModal"
                               data-wow-duration="1.3s" data-wow-delay="1.1s">Free Trial</a>
                            <a href="javascript:;" class="main-btn wow fadeInUp" data-toggle="modal" data-target="#activationModal"
                               data-wow-duration="1.3s" data-wow-delay="1.1s">Activate Now</a>
                        </div>
                        <!-- first_btn -->
                            @else
                                <div class="first_btn">
                                    <a href="#pricing" class="main-btn wow fadeInUp" data-toggle="modal" data-target="#registerAccount"
                                       data-wow-duration="1.3s" data-wow-delay="1.1s">Free Trial</a>
                                    <a href="#pricing" class="main-btn wow fadeInUp" data-toggle="modal" data-target="#registerAccount"
                                       data-wow-duration="1.3s" data-wow-delay="1.1s">Activate Now</a>
                                </div>
                        @endif
                    </div>
                    <!-- header hero image -->
                </div>
                <div class="col-lg-4">
                    <div class="single_man wow fadeIn" data-wow-duration="1.3s" data-wow-delay="1s">
                        <img src="assets/images/man.png" />
                    </div>
                    <!-- single_man -->
                </div>
                <!-- col -->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
        <div id="particles-1" class="particles"></div>
    </div>
    <!-- header hero -->
</header>
<!--====== HEADER PART ENDS ======-->

<section id="features" class="features">
    <div class="container">
        <div class="section-title">
            <div class="line"></div>
            <h3 class="title">KEY FEATURES</h3>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="features_blk wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                    <div class="features_point">
                        <ul>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point one</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point two</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point three</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point four</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point five</span>
                            </li>
                        </ul>
                    </div>
                    <!-- features_point -->
                    <div class="features_blk_overlay">
                        <h5>Sales & Purchase Management</h5>
                    </div>
                    <!-- features_blk_overlay -->
                    <div class="overlay_line_right"></div>
                    <!-- overlay_line_right -->
                    <div class="overlay_baloon"></div>
                    <!-- overlay_baloon -->
                </div>
                <!-- features_blk -->
            </div>
            <!-- col -->
            <div class="col-md-3">
                <div class="features_blk wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                    <div class="features_point">
                        <ul>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point one</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point two</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point three</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point four</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point five</span>
                            </li>
                        </ul>
                    </div>
                    <!-- features_point -->
                    <div class="features_blk_overlay">
                        <h5>Sales & Purchase Management</h5>
                    </div>
                    <!-- features_blk_overlay -->
                    <div class="overlay_line_right"></div>
                    <!-- overlay_line_right -->
                    <div class="overlay_baloon"></div>
                    <!-- overlay_baloon -->
                </div>
                <!-- features_blk -->
            </div>
            <!-- col -->
            <div class="col-md-3">
                <div class="features_blk wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                    <div class="features_point">
                        <ul>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point one</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point two</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point three</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point four</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point five</span>
                            </li>
                        </ul>
                    </div>
                    <!-- features_point -->
                    <div class="features_blk_overlay">
                        <h5>Sales & Purchase Management</h5>
                    </div>
                    <!-- features_blk_overlay -->
                    <div class="overlay_line_right"></div>
                    <!-- overlay_line_right -->
                    <div class="overlay_baloon"></div>
                    <!-- overlay_baloon -->
                </div>
                <!-- features_blk -->
            </div>
            <!-- col -->
            <div class="col-md-3">
                <div class="features_blk wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s">
                    <div class="features_point">
                        <ul>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point one</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point two</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point three</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point four</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point five</span>
                            </li>
                        </ul>
                    </div>
                    <!-- features_point -->
                    <div class="features_blk_overlay">
                        <h5>Sales & Purchase Management</h5>
                    </div>
                    <!-- features_blk_overlay -->
                    <div class="overlay_line_right"></div>
                    <!-- overlay_line_right -->
                    <div class="overlay_baloon"></div>
                    <!-- overlay_baloon -->
                </div>
                <!-- features_blk -->
            </div>
            <!-- col -->
            <div class="baloon_tray"></div>
            <!-- baloon_tray -->
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-md-3">
                <div class="features_blk wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                    <div class="features_point">
                        <ul>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point one</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point two</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point three</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point four</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point five</span>
                            </li>
                        </ul>
                    </div>
                    <!-- features_point -->
                    <div class="features_blk_overlay">
                        <h5>Sales & Purchase Management</h5>
                    </div>
                    <!-- features_blk_overlay -->
                    <div class="overlay_line_right"></div>
                    <!-- overlay_line_right -->
                    <div class="overlay_baloon"></div>
                    <!-- overlay_baloon -->
                </div>
                <!-- features_blk -->
            </div>
            <!-- col -->
            <div class="col-md-3">
                <div class="features_blk wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                    <div class="features_point">
                        <ul>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point one</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point two</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point three</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point four</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point five</span>
                            </li>
                        </ul>
                    </div>
                    <!-- features_point -->
                    <div class="features_blk_overlay">
                        <h5>Sales & Purchase Management</h5>
                    </div>
                    <!-- features_blk_overlay -->
                    <div class="overlay_line_right"></div>
                    <!-- overlay_line_right -->
                    <div class="overlay_baloon"></div>
                    <!-- overlay_baloon -->
                </div>
                <!-- features_blk -->
            </div>
            <!-- col -->
            <div class="col-md-3">
                <div class="features_blk wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                    <div class="features_point">
                        <ul>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point one</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point two</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point three</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point four</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point five</span>
                            </li>
                        </ul>
                    </div>
                    <!-- features_point -->
                    <div class="features_blk_overlay">
                        <h5>Sales & Purchase Management</h5>
                    </div>
                    <!-- features_blk_overlay -->
                    <div class="overlay_line_right"></div>
                    <!-- overlay_line_right -->
                    <div class="overlay_baloon"></div>
                    <!-- overlay_baloon -->
                </div>
                <!-- features_blk -->
            </div>
            <!-- col -->
            <div class="col-md-3">
                <div class="features_blk wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s">
                    <div class="features_point">
                        <ul>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point one</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point two</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point three</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point four</span>
                            </li>
                            <li>
                                <i class="fa fa-check" aria-hidden="true"></i><span>Point five</span>
                            </li>
                        </ul>
                    </div>
                    <!-- features_point -->
                    <div class="features_blk_overlay">
                        <h5>Sales & Purchase Management</h5>
                    </div>
                    <!-- features_blk_overlay -->
                    <div class="overlay_line_right"></div>
                    <!-- overlay_line_right -->
                    <div class="overlay_baloon"></div>
                    <!-- overlay_baloon -->
                </div>
                <!-- features_blk -->
            </div>
            <!-- col -->
            <div class="baloon_tray"></div>
            <!-- baloon_tray -->
        </div>
        <!-- row -->
    </div>
    <!-- container -->
</section>
<!-- features -->

<!--====== ABOUT PART START ======-->

<section id="about" class="about-area pt-70">
    <div class="about-shape-2">
        <img src="assets/images/about-shape-2.svg" alt="shape" />
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 order-lg-last">
                <div class="about-content mt-50 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.5s">
                    <div class="section-title">
                        <div class="line"></div>
                        <h3 class="title">
                            Refer And Earn <span> offer for the students</span>
                        </h3>
                    </div>
                    <!-- section title -->
                    <p class="text">
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, seiam
                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                        erat, sed diam voluptua. At vero eos et accusam et justo duo
                        dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
                        sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                        amet, consetetur sadipscing.
                    </p>
                    <div class="first_btn">
                    @if(Auth::guard('web')->check())

                        <a href="{{route('student-pending.create')}}" class="main-btn wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="1.1s" style="
                    visibility: visible;
                    animation-duration: 1.3s;
                    animation-delay: 1.1s;
                    animation-name: fadeInUp;
                  ">Try to earn</a>

                    @else
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter22222" class="main-btn wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="1.1s" style="
                    visibility: visible;
                    animation-duration: 1.3s;
                    animation-delay: 1.1s;
                    animation-name: fadeInUp;
                  ">Try to earn</a>
                        @endif
                    </div>
                </div>
                <!-- about content -->
            </div>







            <div class="col-lg-6 order-lg-first">
                <div class="about-image text-center mt-50 wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="0.5s">
                    <img src="assets/images/STUDENT.svg" alt="about" />
                </div>
                <!-- about image -->
            </div>
        </div>
        <!-- row -->
    </div>
    <!-- container -->
</section>
<!--====== ABOUT PART START ======-->

<section class="about-area pt-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="about-content mt-50 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.5s">
                    <div class="section-title">
                        <div class="line"></div>
                        <h3 class="title">
                            Refer And Get 1 month FREE<span>Offer for the clients</span>
                        </h3>
                    </div>
                    <!-- section title -->
                    <p class="text">
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, seiam
                        nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                        erat, sed diam voluptua. At vero eos et accusam et justo duo
                        dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
                        sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                        amet, consetetur sadipscing.
                    </p>
                    <div class="first_btn">
                        @if(Auth::guard('web')->check())
                        <a href="{{route('partner-pending.create')}}" class="main-btn wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="1.1s" style="
                    visibility: visible;
                    animation-duration: 1.3s;
                    animation-delay: 1.1s;
                    animation-name: fadeInUp;
                  ">Try to earn</a>
                        @else
                            <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter222222" class="main-btn wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="1.1s" style="
                    visibility: visible;
                    animation-duration: 1.3s;
                    animation-delay: 1.1s;
                    animation-name: fadeInUp;
                  ">Try to earn</a>
                            @endif
                    </div>
                </div>
                <!-- about content -->
            </div>
            <div class="col-lg-6">
                <div class="about-image text-center mt-50 wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="0.5s">
                    <img src="assets/images/CLIENT.svg" alt="about" />
                </div>
                <!-- about image -->
            </div>
        </div>
        <!-- row -->
    </div>
    <!-- container -->
    <div class="about-shape-1">
        <img src="assets/images/about-shape-1.svg" alt="shape" />
    </div>
</section>

<section id="facts" class="plans">
    <div class="about-shape-2">
        <img src="assets/images/CointLeft.svg" alt="shape" />
    </div>
    <div class="about-shape-custom">
        <img src="assets/images/coinRight.svg" alt="shape" />
    </div>
    <div class="container">
        <div class="section-title">
            <div class="line"></div>
            <h3 class="title">OUR PLANS<span></span></h3>
        </div>
        <div class="plans_head">
            <h4>Product Oriented</h4>
        </div>
        <!-- plans_head -->
        <div class="row">
            <div class="col-md-4">
                <div class="product_blk silver wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                    <h6>SILVER</h6>
                    <div class="product_list">
                        <ul>
                            <li>Up to 5 Branches</li>
                            <li>Up To 20 Users</li>
                        </ul>
                    </div>
                    <!-- product_list -->
                    <div class="subscribe_btn">
                        <a href="javascript:;">SUBSCRIBE NOW</a>
                    </div>
                    <!-- subscribe_btn -->
                    <div class="product_blk_overlay overlay_silver">
                        <p>NPR<span>1000</span></p>
                    </div>
                    <!-- product_blk_overlay -->
                </div>
                <!-- product_blk -->
            </div>
            <!-- col -->
            <div class="col-md-4">
                <div class="product_blk gold wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                    <h6>SILVER</h6>
                    <div class="product_list">
                        <ul>
                            <li>Up to 5 Branches</li>
                            <li>Up To 20 Users</li>
                        </ul>
                    </div>
                    <!-- product_list -->
                    <div class="subscribe_btn">
                        <a href="javascript:;">SUBSCRIBE NOW</a>
                    </div>
                    <!-- subscribe_btn -->
                    <div class="product_blk_overlay overlay_gold">
                        <p>NPR<span>1000</span></p>
                    </div>
                    <!-- product_blk_overlay -->
                </div>
                <!-- product_blk -->
            </div>
            <!-- col -->
            <div class="col-md-4">
                <div class="product_blk platinum wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                    <h6>SILVER</h6>
                    <div class="product_list">
                        <ul>
                            <li>Up to 5 Branches</li>
                            <li>Up To 20 Users</li>
                        </ul>
                    </div>
                    <!-- product_list -->
                    <div class="subscribe_btn">
                        <a href="javascript:;">SUBSCRIBE NOW</a>
                    </div>
                    <!-- subscribe_btn -->
                    <div class="product_blk_overlay overlay_platinum">
                        <p>NPR<span>1000</span></p>
                    </div>
                    <!-- product_blk_overlay -->
                </div>
                <!-- product_blk -->
            </div>
            <!-- col -->
        </div>
        <!-- row -->

        <div class="plans_head">
            <h4>Service Oriented</h4>
        </div>
        <!-- plans_head -->
        <div class="row">
            <div class="col-md-4">
                <div class="product_blk silver wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                    <h6>SILVER</h6>
                    <div class="product_list">
                        <ul>
                            <li>Up to 5 Branches</li>
                            <li>Up To 20 Users</li>
                        </ul>
                    </div>
                    <!-- product_list -->
                    <div class="subscribe_btn">
                        <a href="javascript:;">SUBSCRIBE NOW</a>
                    </div>
                    <!-- subscribe_btn -->
                    <div class="product_blk_overlay overlay_silver">
                        <p>NPR<span>1000</span></p>
                    </div>
                    <!-- product_blk_overlay -->
                </div>
                <!-- product_blk -->
            </div>
            <!-- col -->
            <div class="col-md-4">
                <div class="product_blk gold wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                    <h6>SILVER</h6>
                    <div class="product_list">
                        <ul>
                            <li>Up to 5 Branches</li>
                            <li>Up To 20 Users</li>
                        </ul>
                    </div>
                    <!-- product_list -->
                    <div class="subscribe_btn">
                        <a href="javascript:;">SUBSCRIBE NOW</a>
                    </div>
                    <!-- subscribe_btn -->
                    <div class="product_blk_overlay overlay_gold">
                        <p>NPR<span>1000</span></p>
                    </div>
                    <!-- product_blk_overlay -->
                </div>
                <!-- product_blk -->
            </div>
            <!-- col -->
            <div class="col-md-4">
                <div class="product_blk platinum wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                    <h6>SILVER</h6>
                    <div class="product_list">
                        <ul>
                            <li>Up to 5 Branches</li>
                            <li>Up To 20 Users</li>
                        </ul>
                    </div>
                    <!-- product_list -->
                    <div class="subscribe_btn">
                        <a href="javascript:;">SUBSCRIBE NOW</a>
                    </div>
                    <!-- subscribe_btn -->
                    <div class="product_blk_overlay overlay_platinum">
                        <p>NPR<span>1000</span></p>
                    </div>
                    <!-- product_blk_overlay -->
                </div>
                <!-- product_blk -->
            </div>
            <!-- col -->
        </div>
        <!-- row -->
    </div>
    <!-- container -->
</section>

<!--====== VIDEO COUNTER PART START ======-->

<section id="" class="choose_counter">
    <div class="container">
        <div class="choose_us wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.8s">
            <img src="assets/images/whyChoosUs.svg" alt="shape" />
        </div>
        <!-- choose_us -->
    </div>
    <!-- container -->
</section>

<!--====== VIDEO COUNTER PART ENDS ======-->

<!--====== BLOG PART START ======-->

<section id="blog"></section>

<!--====== BLOG PART ENDS ======-->

<!--====== FOOTER PART START ======-->

<footer id="team" class="footer-area">
    <div class="container">
        <div class="subscribe-area wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
            <div class="row">
                <div class="col-lg-6">
                    <div class="footer_form wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.5s">
                        <div class="form_head">
                            <h6>CONTACT US FOR ANY QUERY</h6>
                        </div>
                        <!-- form_head -->
                        <div class="main_form">
                            <form action="{{url('save/inquiry')}}" method="post" id="inqueryForm">
                                @csrf
                                <input type="text" name="name" placeholder="Your Full Name" />
                                <input type="text" name="email" placeholder="Your Email" />
                                <input type="text" name="contact" placeholder="Your Contact" />
                                <textarea name="message" rows="5" cols="30" placeholder="Your Message To Us ......"></textarea>
                                <div class="footer_submit">
                                    <a href="javascript:;" onclick="submitForm('inqueryForm')">Submit</a>
                                </div>
                                <!-- footer_submit -->
                            </form>
                        </div>
                        <!-- main_form -->
                    </div>
                    <!-- footer_form -->
                </div>
                <div class="col-lg-6">
                    <div class="footer_location wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="0.5s">
                        <div class="location_map">
                            <div class="mapouter">
                                <div class="gmap_canvas">
                                    <iframe width="100%" height="200" id="gmap_canvas"
                                            src="https://maps.google.com/maps?q=tinkunne&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0"
                                            scrolling="no" marginheight="0" marginwidth="0"></iframe><a
                                        href="https://www.embedgooglemap.net"></a>
                                </div>
                                <style>
                                    .mapouter {
                                        position: relative;
                                        text-align: right;
                                        height: 200px;
                                        width: 100%;
                                    }

                                    .gmap_canvas {
                                        overflow: hidden;
                                        background: none !important;
                                        height: 200px;
                                        width: 100%;
                                    }
                                </style>
                            </div>
                        </div>
                        <!-- location_map -->
                        <div class="location_text">
                            <ul>
                                <li>
                                    <i class="fa fa-phone" aria-hidden="true"></i> +977-01
                                    411992
                                </li>
                                <li>
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    info@softechpark.com
                                </li>
                                <li>
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    www.softechpark.com
                                </li>
                                <li>
                                    <i class="fa fa-map-marker"></i> Tinkune, Kathmandu, Nepal
                                </li>
                            </ul>
                        </div>
                        <!-- location_text -->
                        <div class="footer_submit">
                            <a href="javascript:;">Find Us In Google Maps </a>
                        </div>
                    </div>
                    <!-- footer_location -->
                </div>
            </div>
            <!-- row -->
        </div>
        <!-- subscribe area -->
        <div class="footer-widget pb-100">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-8">
                    <div class="footer-about mt-50 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                        <a class="logo" href="javascript:;">
                            <img src="assets/images/softechpark_logo_footer.png" alt="logo" />
                        </a>
                        <p class="text">
                            Lorem ipsum dolor sit amet consetetur sadipscing elitr,
                            sederfs diam nonumy eirmod tempor invidunt ut labore et dolore
                            magna aliquyam.
                        </p>
                    </div>
                    <!-- footer about -->
                </div>
                <div class="col-lg-5 col-md-7 col-sm-7">
                    <div class="footer-link d-flex mt-50 justify-content-md-between">
                        <div class="link-wrapper wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
                            <div class="footer-title">
                                <h4 class="title">Quick Link</h4>
                            </div>
                            <ul class="link">
                                <li><a href="javascript:;">Road Map</a></li>
                                <li><a href="javascript:;">Privacy Policy</a></li>
                                <li><a href="javascript:;">Refund Policy</a></li>
                                <li><a href="javascript:;">Terms of Service</a></li>
                                <li><a href="javascript:;">Pricing</a></li>
                            </ul>
                        </div>
                        <!-- footer wrapper -->
                        <div class="link-wrapper wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
                            <div class="footer-title">
                                <h4 class="title">Resources</h4>
                            </div>
                            <ul class="link">
                                <li><a href="javascript:;">Home</a></li>
                                <li><a href="javascript:;">Page</a></li>
                                <li><a href="javascript:;">Portfolio</a></li>
                                <li><a href="javascript:;">Blog</a></li>
                                <li><a href="javascript:;">Contact</a></li>
                            </ul>
                        </div>
                        <!-- footer wrapper -->
                    </div>
                    <!-- footer link -->
                </div>
                <div class="col-lg-3 col-md-5 col-sm-5">
                    <div class="footer-contact mt-50 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                        <div class="footer-title">
                            <h4 class="title">Contact Us</h4>
                        </div>
                        <ul class="contact">
                            <li>+809272561823</li>
                            <li>info@gmail.com</li>
                            <li>www.yourweb.com</li>
                            <li>
                                123 Stree New York City , United <br />
                                States Of America 750.
                            </li>
                        </ul>
                    </div>
                    <!-- footer contact -->
                </div>
            </div>
            <!-- row -->
            <div class="row">
                <div class="col-md-3">
                    <div class="footer-about">
                        <ul class="social">
                            <li>
                                <a href="javascript:;"><i class="lni-facebook-filled"></i></a>
                            </li>
                            <li>
                                <a href="javascript:;"><i class="lni-twitter-filled"></i></a>
                            </li>
                            <li>
                                <a href="javascript:;"><i class="lni-instagram-filled"></i></a>
                            </li>
                            <li>
                                <a href="javascript:;"><i class="lni-linkedin-original"></i></a>
                            </li>
                        </ul>
                    </div>
                    <!-- footer-about -->
                </div>
                <!-- col -->
                <div class="col-md-3">
                    <div class="copy_right">
                        <i class="fa fa-copyright" aria-hidden="true"></i>
                        <span>Powered by:&nbsp;
                <a href="javascript:;"> Softechpark Pvt. Ltd.</a></span>
                    </div>
                </div>
                <!-- col -->
                <div class="col-md-3">
                    <div class="footer_hr">
                        <hr />
                    </div>
                    <!-- footer_hr -->
                </div>
                <!-- col -->
                <div class="col-md-3">
                    <div class="reserved">
                        <span>All Right Reserved</span>
                    </div>
                    <!-- reserved -->
                </div>
                <!-- col -->
            </div>
            <!-- row -->
        </div>
        <!-- footer widget -->
    </div>
    <!-- container -->
    <div id="particles-2"></div>
</footer>

<!-- Modal -->
<div class="modal fade modal_with_tab" id="exampleModalCenter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                           aria-controls="nav-home" aria-selected="true">Log In</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                           aria-controls="nav-profile" aria-selected="false">Register</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="row">
                            <div class="col-md-6 no-pad-r">
                                <div class="tab_left_banner_lead">
                                    <h2>Refer And Earn<span>offer for the students</span></h2>
                                    <div class="tab_banner">
                                        <img src="assets/images/STUDENT.svg" alt="about" />
                                    </div>
                                    <!-- tab_banner -->
                                </div>
                                <!-- tab_left_banner_lead -->
                            </div>
                            <!-- col -->
                            <div class="col-md-6 bg_light_gr">
                                <div class="tab_form">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h3>Log In</h3>
                                    <form>
                                        <div class="form_head mt-3">
                                            Email:
                                            <input type="email" name="email" placeholder="santosh poudel@gmail.com" />
                                        </div>
                                        <!-- form_head -->
                                        <div class="form_head mt-3">
                                            Password:
                                            <input type="Password" name="Password" placeholder="****" />
                                        </div>
                                        <!-- form_head -->
                                        <div class="btn_logg">
                                            <a href="javascript:;" class="main_in">SIGN IN</a>
                                            <div class="bottom_logg_href">
                                                <a href="javascript:;">Forgot Password?</a>
                                                <a href="javascript:;">Not a Member yet?</a>
                                            </div>
                                            <!-- bottom_logg_href -->
                                        </div>
                                        <!-- btn_logg -->
                                    </form>
                                </div>
                                <!-- tab_form -->
                            </div>
                            <!-- col -->
                        </div>
                        <!-- row -->
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="row">
                            <div class="col-md-6 no-pad-r">
                                <div class="tab_left_banner_lead">
                                    <h2>Refer And Earn<span>offer for the students</span></h2>
                                    <div class="tab_banner">
                                        <img src="assets/images/STUDENT.svg" alt="about" />
                                    </div>
                                    <!-- tab_banner -->
                                </div>
                                <!-- tab_left_banner_lead -->
                            </div>
                            <!-- col -->
                            <div class="col-md-6 bg_light_gr">
                                <div class="tab_form">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h3>Register</h3>
                                    <form>
                                        <div class="form_head mt-1">
                                            Full Name:
                                            <input type="name" name="fullname" placeholder="santosh poudel@gmail.com" />
                                        </div>
                                        <!-- form_head -->
                                        <div class="form_head mt-1">
                                            Email:
                                            <input type="email" name="email" placeholder="santosh poudel@gmail.com" />
                                        </div>
                                        <!-- form_head -->
                                        <div class="form_head mt-1">
                                            Contact Number:
                                            <input type="phone" name="phone" placeholder="9841414141" />
                                        </div>
                                        <!-- form_head -->
                                        <div class="form_head mt-1">
                                            Password:
                                            <input type="Password" name="Password" placeholder="****" />
                                        </div>
                                        <!-- form_head -->
                                        <div class="form_head mt-1">
                                            Re-type Password:
                                            <input type="Password" name="Password" placeholder="****" />
                                        </div>
                                        <!-- form_head -->
                                        <div class="btn_logg">
                                            <a href="javascript:;" class="main_in">REGISTER</a>
                                        </div>
                                        <!-- btn_logg -->
                                    </form>
                                </div>
                                <!-- tab_form -->
                            </div>
                            <!-- col -->
                        </div>
                        <!-- row -->
                    </div>
                </div>
                <!--       <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade modal_with_tab" id="registerAccount" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        @if(Auth::check()==true)
                            <a class="nav-item nav-link" href="{{url('dashboard')}}">Log In</a>
                            @else
                            <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home2" role="tab"
                               aria-controls="nav-home" aria-selected="true">Log In</a>
                        @endif
                        <a class="nav-item nav-link active" id="nav-profile-tab2" data-toggle="tab" href="#nav-profile2"
                           role="tab" aria-controls="nav-profile" aria-selected="false">Register</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent2">
                    <div class="tab-pane fade" id="nav-home2" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="row">
                            <div class="col-md-6 no-pad-r">
                                <div class="tab_left_banner_lead">
                                    <h2>
                                        Refer And GET 1 MONTH FREE<span>offer for the Clients</span>
                                    </h2>
                                    <div class="tab_banner">
                                        <img src="assets/images/CLIENT.svg" alt="about" />
                                    </div>
                                    <!-- tab_banner -->
                                </div>
                                <!-- tab_left_banner_lead -->
                            </div>
                            <!-- col -->
                            <div class="col-md-6 bg_light_gr">
                                <div class="tab_form">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h3>Log In</h3>
                                        <div class="form_head mt-3">
                                            Email:
                                            <input type="email" class="login_email" name="email" placeholder="">
                                            <p id="login_email_error" style="color: red"></p>
                                        </div><!-- form_head -->
                                        <div class="form_head mt-3">
                                            Password:
                                            <input type="Password" class="login_password" name="password" placeholder="****">
                                            <p id="login_password_error" style="color: red"></p>
                                        </div><!-- form_head -->
                                    <div class="btn_logg">
                                        <a href="javascript:;" class="login_user main_in px-3 mt-3">SIGN IN</a>
                                        <div class="bottom_logg_href">
                                            <a href="javascript:;">Forgot Password?</a>
                                            <a href="javascript:;">Not a Member yet?</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- tab_form -->
                            </div>
                            <!-- col -->
                        </div>
                        <!-- row -->
                    </div>
                    <div class="tab-pane fade show active" id="nav-profile2" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="row">
                            <div class="col-md-6 no-pad-r">
                                <div class="tab_left_banner_lead">
                                    <h2>
                                        Refer And GET 1 MONTH FREE<span>offer for the Clients</span>
                                    </h2>
                                    <div class="tab_banner">
                                        <img src="assets/images/CLIENT.svg" alt="about" />
                                    </div>
                                    <!-- tab_banner -->
                                </div>
                                <!-- tab_left_banner_lead -->
                            </div>
                            <!-- col -->
                            <div class="col-md-6 bg_light_gr">
                                <div class="tab_form tab_form_register_client">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h3 class="mb-3">Register</h3>
                                    <form>
                                        <div class="form_head mt-1">
                                            Full Name:
                                            <input type="text" class="name" name="fullname">
                                            <p id="name_error" style="color: red"></p>
                                        </div><!-- form_head -->
                                        <div class="form_head mt-1">
                                            Email:
                                            <input type="email" class="email" name="email" placeholder="">
                                            <p id="email_error" style="color: red"></p>
                                        </div><!-- form_head -->
                                        <div class="form_head mt-1">
                                            Contact Number:
                                            <input type="text" class="phone" name="phone">
                                            <p id="phone_error" style="color: red"></p>
                                        </div><!-- form_head -->
                                        <div class="form_head mt-1">
                                            Password:
                                            <input type="Password" class="password" name="Password" placeholder="****">
                                            <p id="password_error" style="color: red"></p>
                                        </div><!-- form_head -->
                                        <div class="form_head mt-1">
                                            Re-type Password:
                                            <input type="Password" class="confirm_password" name="Password" placeholder="****">
                                        </div><!-- form_head -->
                                        <div class="form_head mt-1">
                                            Refral Code:
                                            <input type="text" class="refral_code" name="number" placeholder="1234">
                                        </div><!-- form_head -->
                                        <div class="form-check mt-2 terms_condition_accept text-center">
                                            <input type="checkbox" class="checkbox form-check-input" value="1" id="termsAccept">
                                            <label class="accept form-check-label c_black" for="termsAccept">I accept the terms and
                                                condition</label>
                                            <p id="accept_error" style="color: red"></p>
                                        </div>
                                        <div class="btn_logg btn_logg_register">
                                            <a href="javascript:;" class="save_user main_in">REGISTER</a>
                                        </div><!-- btn_logg -->
                                    </form>
                                </div>
                                <!-- tab_form -->
                            </div>
                            <!-- col -->
                        </div>
                        <!-- row -->
                    </div>
                </div>
                <!--       <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="trialModal" tabindex="-1" role="dialog" aria-labelledby="trialModalLabel"
     aria-hidden="true">
    <div class="modal-dialog index_pop free_trial free_trial_modal modal_center_margin" role="document">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <h5 class="modal-title" id="trialModalLabel">FREE TRIAL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form_head mt-3">
                        Company name:
                        <input type="text" class="company_name" name="lastname mt-1" placeholder="M.M Shoes Center" />
                        <p id="company_name_error" style="color: red"></p>
                    </div>
                    <!-- form_head -->
                    <div class="form_head mt-3">
                        PAN/Registration Number:
                        <input type="number" step="any" class="pan" name="lastname mt-1" placeholder="13412423234" />
                        <p id="pan_error" style="color: red"></p>
                    </div>
                    <!-- form_head -->
                    <div class="form_head mt-3">
                        Company Address:
                        <input type="text" class="company_address" name="lastname mt-1" placeholder="Koteshwor-32, Kathmandu, Nepal" />

                        <p id="company_address_error" style="color: red"></p></div>
                    <!-- form_head -->
                    <div class="form_head mt-3">
                        Company Contact Number:
                        <input type="text" class="contact_number" name="lastname mt-1" placeholder="01-4601667" />
                        <p id="contact_number_error" style="color: red"></p>
                    </div>
                    <div class="form_head form_head_info mt-3">
                        Packages
                        <select class="package" id="type">
                            <option value="service orinted">Service Orinted</option>
                            <option value="product orinted">Product Orinted</option>
                            <option value="third party">Third Party</option>

                        </select>
                        <p id="package_error" style="color: red"></p>
                        <div class="troubleshoot_popup">
                            <a href="javascript:;" data-toggle="tooltip" data-placement="right" data-original-title="" title=""><i
                                    class="fa fa-info-circle" aria-hidden="true"></i>
                            </a>
                            <div class="popup">
                                <h4>Front End</h4>
                                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nulla rem inventore aperiam, vitae
                                    quisquam officiis ex assumenda magnam! Rerum, veniam? Sunt, facere. </p>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:;" class="free-trial-demo btn_trial">APPLY FREE TRIAL</a>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- ...........Activate now model.......... -->
<div class="modal fade" id="activationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog index_pop free_trial modal_center_margin" role="document">
        <div class="modal-content activate_content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body body_activate">
                <ul class="nav nav-tabs dis_none" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active hide-show-nav" id="govt-tab" data-toggle="tab" href="#govt" role="tab"
                           aria-controls="govt" aria-selected="true">PERSONAL INFO</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="choose-tab" data-toggle="tab" href="#choose" role="tab" aria-controls="choose"
                           aria-selected="false">PARENTAL INFO</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info"
                           aria-selected="false">COMPANY INFO</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="overview-tab" data-toggle="tab" href="#overview" role="tab"
                           aria-controls="overview" aria-selected="false">COMPANY INFO</a>
                    </li>
                </ul>
                <div class="tab-content activate_content" id="myTabContent">
                    <div class="tab-pane fade show active hide-show" id="govt" role="tabpanel" aria-labelledby="govt-tab">
                        <div class="verify_tab">
                            <div class="govt_logo">
                                <img src="./assets/images/govt.png" />
                            </div>
                            <!-- govt_logo -->
                            <h5>DO YOU WANT IRD VERIFIED PRODUCT ?</h5>
                            <div class="verified_pro">
                                <a href="javascript:;" class="a_green btnNext">YES !</a>
                                <a href="javascript:;" class="a_red btnNext">NO, NOT NOW !</a>
                            </div>
                            <!-- verified_pro -->
                            <p>
                                Note : IRD verification is necessary. Its your choice
                                whether you want to verify by yourself or through us.
                            </p>
                        </div>
                        <!-- verify_tab -->
                    </div>
                    <div class="tab-pane fade" id="choose" role="tabpanel" aria-labelledby="choose-tab">
                        <div class="branch_head">
                            <h5 class="modal-title" id="exampleModalLabel2">
                                CHOOSE YOUR DESIRE PLANS
                            </h5>
                        </div>
                        <!-- branch_head -->
                        <form class="plans_blk">
                            <div class="form_head mt-1">
                                Select Plan Type
                                <select class="plan_type" id="type" readonly>
                                    <option value="quartely">Quartely</option>
                                    <option value="anually">Anually</option>
                                    <option value="half anually">Half Anually</option>
                                </select>
                                <p id="plan_type_number_error" style="color: red"></p>
                                <div class="troubleshoot_popup">
                                    <a href="javascript:;" data-toggle="tooltip" data-placement="right"><i class="fa fa-info-circle"
                                                                                                aria-hidden="true"></i>
                                    </a>
                                    <div class="popup">
                                        <h4>Front End</h4>
                                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nulla rem inventore aperiam, vitae
                                            quisquam officiis ex assumenda magnam! Rerum, veniam? Sunt, facere. </p>

                                        <h4>Front End</h4>
                                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nulla rem inventore aperiam, vitae
                                            quisquam officiis ex assumenda magnam! Rerum, veniam? Sunt, facere. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form_head mt-1">
                                Select Plans For Your Company
                                <select class="plan" id="plans">
                                    <option value="gold">Gold</option>
                                    <option value="silver">Silver</option>
                                    <option value="platinum">Platinum</option>

                                </select>
                                <p id="plan_error" style="color: red"></p>
                                <div class="troubleshoot_popup">
                                    <a href="javascript:;" data-toggle="tooltip" data-placement="right"><i class="fa fa-info-circle"
                                                                                                aria-hidden="true"></i>
                                    </a>
                                    <div class="popup">
                                        <h4>Front End</h4>
                                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nulla rem inventore aperiam, vitae
                                            quisquam officiis ex assumenda magnam! Rerum, veniam? Sunt, facere. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form_head mt-1">
                                Select Package For Your Company
                                <select class="package123"name="package_id"  id="organization">
                                    <option value="service oriented">select package</option>
                                    @foreach($package as $key => $packages)

                                    <option value="{{$packages->id}}">{{$packages->name}}</option>
{{--                                    <option value="service oriented">Service Oriented</option>--}}
{{--                                    <option value="third party">Third Party</option>--}}
                                    @endforeach
                                </select>
                                <p id="package123_error" style="color: red"></p>
                                <div class="troubleshoot_popup">
                                    <a href="javascript:;" data-toggle="tooltip" data-placement="right"><i class="fa fa-info-circle"
                                                                                                aria-hidden="true"></i>
                                    </a>
                                    <div class="popup">
                                        <h4>Front End</h4>
                                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nulla rem inventore aperiam, vitae
                                            quisquam officiis ex assumenda magnam! Rerum, veniam? Sunt, facere. </p>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="tab_nxt_prv text-right">
                            <a href="javascript:;" class="btnPrevious">
                                << Prev </a> <a href="javascript:;" class="btnNext">Next >>
                            </a>
                        </div>
                        <!-- tab_nxt_prv -->
                    </div>
                    <div class="tab-pane fade" id="info" role="tabpanel" aria-labelledby="info-tab">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="branch_head">
                                    <h5 class="modal-title" id="exampleModalLabel2">
                                        COMPANY INFORMATION
                                    </h5>
                                    <form class="browse_form_sec">
                                        <div class="form_head">
                                            Company Name:
                                            <input type="name" class="company_name123" name="company" placeholder="santosh poudel" />
                                            <p id="company_name123_error" style="color: red"></p>
                                        </div>
                                        <!-- form_head -->
                                        <div class="form_head mt-3">
                                            PAN/Registration Number:
                                            <input type="text" class="pan123" name="number mt-1" placeholder="M.M Shoes Center" />
                                            <p id="pan123_error" style="color: red"></p>
                                        </div>
                                        <!-- form_head -->
                                        <div class="form_head mt-3">
                                            Company Address:
                                            <input type="text" class="company_address123" name="lastname mt-1" placeholder="Koteshwor-32, Kathmandu, Nepal" />
                                            <p id="company_address123_error" style="color: red"></p>
                                        </div>
                                        <!-- form_head -->
                                        <div class="form_head mt-3">
                                            Company Contact Number:
                                            <input type="text" class="contact_number123" name="lastname mt-1" placeholder="01-4601667" />
                                            <p id="contact_number123_error" style="color: red"></p>
                                        </div>
                                        <!-- form_head -->
                                        <a href="javascript:;">APPLY FREE TRIAL</a>
{{--                                        <button>APPLY FREE TRIAL</button>--}}
                                    </form>
                                </div>
                            </div>
                            <!-- col -->
                        </div>
                        <!-- row -->
                        <div class="tab_nxt_prv text-right">
                            <a href="javascript:;" class="btnPrevious">
                                << Prev </a> <a href="javascript:;" class="overview btnNext">Next >>
                            </a>
                        </div>
                        <!-- tab_nxt_prv -->
                    </div>
                    <div class="tab-pane fade overview_main" id="overview" role="tabpanel" aria-labelledby="overview-tab">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="overview_blk">
                                    <div class="branch_head">
                                        <h5 class="modal-title" id="exampleModalLabel2">
                                            OVERVIEW
                                        </h5>
                                    </div>
                                    <div class="overview_img">
                                        <img src="assets/images/overview_img.jpg" />
                                    </div>
                                    <!-- overview_img -->
                                    <ul>
                                        <li>Company: <small><span class="company_name1234"></span><p id="company_name12_error" style="color: red"></p></small></li>
                                        <li>PAN: <small><span class="pan1234"></span><p id="pan12_error" style="color: red"></p></small></li>
                                        <li>Contact Number: <small><span class="contact_number1234"></span><p id="contact_number12_error" style="color: red"></p></small></li>
                                        <li>
                                            Address: <small><span class="company_address1234"></span><p id="company_address12_error" style="color: red"></p></small>
                                        </li>
                                        <li>
                                            Selected Plan Type: <small><span class="plan_type1234"></span></small>
                                        </li>
                                        <li>
                                            Selected Plans For Your Company: <small><span class="plan1234"></span></small>
                                        </li>
                                        <li>
                                            Selected Organization type: <small><span class="package1234"></span></small>
                                        </li>
                                    </ul>
                                </div>
                                <!-- overview_blk -->
                            </div>
                            <!-- col -->
                            <div class="col-md-7">
                                <div class="overview_right_full">
                                    <form class="overview_blk_right">
                                        <div class="form_head mt-1">
                                            Select Subscription Time
                                            <select name="payment_type_id" class="subscription_time" id="type">
                                                <option value=" ">Select payment type</option>
                                                @foreach($payment_type as $key => $payment_types)
                                                <option value="{{$payment_types->id}}">{{$payment_types->name}}</option>
                                                @endforeach
{{--                                                <option value="mercedes">Mercedes</option>--}}
{{--                                                <option value="audi">Audi</option>--}}
                                            </select>
                                            <p id="subscription_time_error" style="color: red"></p>
                                        </div>
                                        <div class="form_head mt-3">
                                            Have a pormo code ?
                                            <div class="flex_blk_over">
                                                <input type="text" class="promo_code" name="lastname mt-1" placeholder="Enter Pormo Code" />
                                                <p id="promo_code_error" style="color: red"></p>
                                                <a href="javascript:;">REDEEM</a>
                                            </div>
                                            <!-- flex_blk_over -->
                                        </div>
                                        <p id="promo_code_error" style="color: red"></p>
                                    </form>
                                    <input type="hidden" class="price_for_payment">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th scope="col">sn</th>
                                            <th scope="col">Particular</th>
                                            <th scope="col">Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody id="second">
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>
                                                Product Oriented with GOLD plan for Education
                                            </td>

                                            <td>
                                                <span class="price111 price135"></span>/-</td>
                                        </tr>
                                        <tr id="discount">
                                            <td colspan="2">Discount</td>
                                            <td><span class="discount111"></span>/-</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">VAT(13%)</td>
                                            <td>225/-</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Total</td>
                                            <td>1724/-</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div id="payment">
                                        <div class="payment_blk">
                                            <button class="btn">
                                                <img src="assets/images/visa-mastercard.png" alt="Logo" />
                                            </button>
                                            <button class="btn">
                                                <img src="assets/images/visa-mastercard.png" alt="Logo" />
                                            </button>
                                            <button class="btn">
                                                <img src="assets/images/visa-mastercard.png" alt="Logo" />
                                            </button>
                                            <button class="btn">
                                                <img src="assets/images/visa-mastercard.png" alt="Logo" />
                                            </button>
                                            <button class="btn">
                                                <img src="assets/images/visa-mastercard.png" alt="Logo" />
                                            </button>
                                        </div>
                                        <!-- payment_blk -->
                                    </div>
                                </div>
                                <!-- overview_right_full -->
                            </div>
                            <!-- col -->
                        </div>
                        <!-- row -->
                        <div class="tab_nxt_prv text-right mt-3">
                            <a class="btn btn-primary btnPrevious">
                                << Prev</a> <a class="activate_now btn btn-success btnSubmit">PROCEED FOR PAYMENT
                            </a>
                        </div>
                        <!-- tab_nxt_prv -->
                    </div>
                </div>
                <!--  <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_with_tab show" id="exampleModalCenter3" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-modal="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 no-pad-r">
                        <div class="tab_left_banner_lead">
                            <h2>Partner Programe<span>Join With Us Together</span></h2>
                            <div class="tab_banner">
                                <img src="assets/images/partner.png" alt="about" />
                            </div>
                            <!-- tab_banner -->
                        </div>
                        <!-- tab_left_banner_lead -->
                    </div>
                    <!-- col -->
                    <div class="col-md-6 bg_light_gr">
                        <div class="tab_form">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <h3>Register for partnership</h3>
                            <form>
                                <div class="form_head mt-1">
                                    Full Name:
                                    <input type="name" name="fullname" placeholder="santosh poudel@gmail.com" />
                                </div>
                                <!-- form_head -->
                                <div class="form_head mt-1">
                                    Email:
                                    <input type="email" name="email" placeholder="santosh poudel@gmail.com" />
                                </div>
                                <!-- form_head -->
                                <div class="form_head mt-1">
                                    Contact Number:
                                    <input type="phone" name="phone" placeholder="9841414141" />
                                </div>
                                <!-- form_head -->
                                <div class="form_head mt-1">
                                    Address:
                                    <input type="phone" name="phone" placeholder="Kathmandu, Nepal" />
                                </div>
                                <!-- form_head -->
                                <div class="form_head mt-1">
                                    Message For Us:
                                    <input type="phone" name="phone"
                                           placeholder="And i wil love you baby, Ill br there till the stars...." />
                                </div>
                                <!-- form_head -->
                                <div class="btn_logg">
                                    <a href="javascript:;" class="main_in">APPLY</a>
                                </div>
                                <!-- btn_logg -->
                            </form>
                        </div>
                        <!-- tab_form -->
                    </div>
                    <!-- col -->
                </div>
                <!-- row -->

                <!--       <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
            </div>
        </div>
    </div>
</div>




<!--====== FOOTER PART ENDS ======-->

<!--====== BACK TOP TOP PART START ======-->

<a href="javascript:;" class="back-to-top"><i class="lni-chevron-up"></i></a>

<!--====== BACK TOP TOP PART ENDS ======-->

<!--====== PART START ======-->

<!--
  <section class="">
      <div class="container">
          <div class="row">
              <div class="col-lg-"></div>
          </div>
      </div>
  </section>
-->

<!--====== PART ENDS ======-->

<!--====== Jquery js ======-->
<script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
<script src="assets/js/vendor/modernizr-3.7.1.min.js"></script>

<!--====== Bootstrap js ======-->
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<!--====== Plugins js ======-->
<script src="assets/js/plugins.js"></script>

<!--====== Slick js ======-->
<script src="assets/js/slick.min.js"></script>

<!--====== Ajax Contact js ======-->
<script src="assets/js/ajax-contact.js"></script>

<!--====== Counter Up js ======-->
<script src="assets/js/waypoints.min.js"></script>
<script src="assets/js/jquery.counterup.min.js"></script>

<!--====== Magnific Popup js ======-->
<script src="assets/js/jquery.magnific-popup.min.js"></script>

<!--====== Scrolling Nav js ======-->
<script src="assets/js/jquery.easing.min.js"></script>
<script src="assets/js/scrolling-nav.js"></script>

<!--====== wow js ======-->
<script src="assets/js/wow.min.js"></script>

<!--====== Particles js ======-->
<script src="assets/js/particles.min.js"></script>

<!--====== Main js ======-->
<script src="assets/js/main.js"></script>
{{--<script src="{{asset('assets/js/custom.js')}}"></script>--}}


@include('flash-message')
<script>



    $("    select[name='package_id']").change(function () {

        var package_id= $(this).val();
        $.ajax({

            url: "{{url('/edit-plan-pricing')}}",
            type: "POST",
            data: {
                _token : "{{csrf_token()}}",
                package_id : package_id
            },
            success: function (data) {
                console.log(data);
                console.log(data.plan_pricing123[0].price);
                // $('.price135').html(data.plan_pricing123[0].price);
                $('tbody#second').empty();

                $('.price_for_payment').append($('.price_for_payment').val(data.plan_pricing123[0].price))
                $.each(data.plan_pricing123, function(index, value) {
                    var sn = index + 1;

                    // $('.price_for_payment').append($('.price_for_payment').val(value.price))
                    $('tbody#second').append('<tr><th scope="row">'+sn+'</th><td class="text-left"><input type="hidden" name="pro" class="package_id135" value="'+value.package['id']+'">'+value.package['name']+' </td><td class="second_price"><input type="hidden" class="price135" name="sale_rate123" value="'+value['price']+'">'+value['price']+'</td></tr> <tr class="second123"></tr>');
                });

                var vat_am = data.vat_amount;
                $('tbody#second').append('<tr class="second1234" ><td colspan="2">'+'vat(13%)'+' </td><td><input type="hidden" class="vat_amount321" name="vat_amount321" value="'+vat_am+'">'+vat_am+'</td></tr>');

                var total = data.final_total;
                $('tbody#second').append('<tr class="second12345" ><td colspan="2">'+'total'+' </td><td><input type="hidden" class="total321" name="sale_rate123" value="'+total+'">'+total+'</td></tr>');
            },

            error: function (error) {

                console.log('error');

            }


        });

    });



    $("   select[name='payment_type_id']").change(function () {

        var payment_type_id= $(this).val();
        var price= $('.price_for_payment').val();
        // var vat_ams1010= $('.vat_amount321').val();
        // var totals1010= $('.total321').val();

        // alert(price);
        $.ajax({

            url: "{{url('/edit-payment-type')}}",
            type: "POST",
            data: {
                _token : "{{csrf_token()}}",
                payment_type_id : payment_type_id,
                price : price,
                // vat_amout : vat_amount,
                // total : total,
            },
            success: function (data) {
                console.log(data);
                // console.log(data.productById.sellingPrice);
                // $('tbody#second').empty();
                // $('.discount111').html(data.discount_amount);
                // $.each(data.payment_method123, function(index, value) {
                // $.each(data.discount_amount, function(index, value) {

            // <tr id="discount">
            //         <td colspan="2">Discount</td>
            //         <td><span class="discount111"></span>/-</td>
            //         </tr>

                $('td.second_price').empty();

                var price123 = data.price;
                $('td.second_price').append('<td><input type="hidden" class="price135" name="sale_rate123" value="'+price123+'">'+price123+'</td>');


                $('tr.second123').empty();
                // $('tr.second1234').empty();
                // $('tr.second12345').empty();
                if (data.dis != null)
                {
                    $('tr.second123').empty();

                    $('tr.second1234').empty();
                    $('tr.second12345').empty();

                    var value = data.discount_amount;
                    $('tr.second123').append('<td colspan="2">'+'discount'+' </td><td><input type="hidden" class="discount1012" name="sale_rate123" value="'+value+'">'+value+'</td>');
                    // });

                    var vat_am = data.vat_amount;
                    $('tr.second1234').append('<td colspan="2">'+'vat(13%)'+' </td><td><input type="hidden" class="vat_amount321"  name="sale_rate123" value="'+vat_am+'">'+vat_am+'</td>');

                    var total = data.final_total;
                    $('tr.second12345').append('<td colspan="2">'+'total'+' </td><td><input type="hidden"  class="total321"  name="sale_rate123" value="'+total+'">'+total+'</td>');

                }

                // else{
                //     $('tr.second1234').empty();
                //     $('tr.second12345').empty();
                //
                //     // var vat_ams= vat_amount1010;
                //     $('tr.second1234').append('<td colspan="2">'+'vat(13%)'+' </td><td><input type="hidden" name="sale_rate123" value="'+vat_ams1010+'">'+vat_ams1010+'</td>');
                //
                //     // var totals = total1010;
                //     $('tr.second12345').append('<td colspan="2">'+'total'+' </td><td><input type="hidden" name="sale_rate123" value="'+totals1010+'">'+totals1010+'</td>');
                // }



            },

            error: function (error) {

                console.log('error');

            }


        });

    });



    $(".promo_code").on('input',function () {

        var promo_code= $(".promo_code").val();
        var price= $('.price135').val();
        var discount= $('.discount1012').val();

        // alert(price);

        $.ajax({

            url: "{{url('/edit-offer-discount')}}",
            type: "POST",
            data: {
                _token : "{{csrf_token()}}",

                promo_code:promo_code,
                price : price,
                discount_amount : discount,
            },
            success: function (data) {
                console.log(data);

                // $('tr.second123').empty();
                // $('tr.second1234').empty();
               // if(data.success)
               // {
               //  if(data.error){
               //      // return;
               //      alert('test');
               //  }
                   $('tr.second12345').empty();
                   if (data.dis != null) {
                       $('tr.second123').empty();
                       $('tr.second1234').empty();
                       $('tr.second12345').empty();

                       var value = data.discount_amount;
                       $('tr.second123').append('<td colspan="2">' + 'discount' + ' </td><td><input type="hidden" class="discount1012" name="sale_rate123" value="'+value+'">'+value+'</td>');
                       // });

                       var vat_am = data.vat_amount;
                       $('tr.second1234').append('<td colspan="2">' + 'vat(13%)' + ' </td><td><input type="hidden" class="vat_amount321"  name="sale_rate123" value="'+vat_am+'">'+vat_am+'</td>');

                       var total = data.final_total;
                       $('tr.second12345').append('<td colspan="2">' + 'total' + ' </td><td><input type="hidden" class="total321"  name="sale_rate123" value="'+total+'">'+total+'</td>');

                   }
               // }


            },

            error: function (xhr, status, error) {
                console.log('error');
                var err = JSON.parse(xhr.responseText);
                $('#promo_code_error').append(err.errors.message);
                // if (err.errors.password) {
                //     toastr.error(err.errors.password);
                // }
            }

        });


    });











    $(".login_user").click(function(e){
        e.preventDefault()
        toastr.success("Please wait. requesting....");

        var email= $(".login_email").val();
        var password= $(".login_password").val();

        $.ajax({
            type: 'POST',
            url: "{{url('/user-login123')}}",
            data: {
                _token : "{{csrf_token()}}",
                email:email,
                password:password
            },
            success: function (data, status) {
                console.log(data);
                if(data.success){
                    toastr.success(data.success);
                    window.location.replace('/dashboard');
                    console.log(data);
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
                var err = JSON.parse(xhr.responseText);
                $('#login_email_error').html(err.errors.email);
                $('#login_password_error').html(err.errors.password);
            }
        });

    });

    $(".save_user").click(function(e){
        e.preventDefault()
        var name= $(".name").val();
        var email= $(".email").val();
        var phone= $(".phone").val();
        var password= $(".password").val();
        var confirm_password= $(".confirm_password").val();
        var refral_code= $(".refral_code").val();

        if($('.checkbox').is(":checked"))
        {
            toastr.success("Please wait. requesting....");
            $.ajax({
                type: 'POST',
                url: '{{url('/save-user')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    name : name,
                    email:email,
                    phone:phone,
                    password:password,
                    refral_code:refral_code,
                    confirm_password:confirm_password
                },
                success: function (data, status) {
                    if(data.indexOf('success')>=0){
                        toastr.success(data);
                        $('#registerAccount').modal('hide');
                        $(".name").val('');
                        $(".email").val('');
                        $(".phone").val('');
                        $(".password").val('');
                        $(".confirm_password").val('');
                        $(".refral_code").val('');
                    }else{
                        toastr.error(data);
                    }
                },
                error: function (xhr, status, error) {
                    console.log('error');
                    toastr.error('Something went wrong. please try again later.');
                }
            });
        }
        else{
            // alert('dvdv');
            if ($(".password").val()== '')
            {
                $('#password_error').html('please enter password');
            }
            if ($(".name").val()== '')
            {
                $('#name_error').html('please enter name ');
            }
            if ($(".email").val()== '')
            {
                $('#email_error').html('please enter valid email ');
            }
            // $('.accept').hide();
                $('.accept').html('please accept terms and condition'.fontcolor('red'));
        }



    });

    $(".free-trial-demo").click(function(e){
        e.preventDefault()
        toastr.success('Please wait, processing....');
        var company_name= $(".company_name").val();
        var contact_number= $(".contact_number").val();
        var company_address= $(".company_address").val();
        var pan= $(".pan").val();
        var pa= $(".package").val();
        $.ajax({
            type: 'POST',
            url: '{{url('/free-trial-demo')}}',
            data: {
                _token : "{{csrf_token()}}",
                company_name:company_name,
                contact_number:contact_number,
                company_address:company_address,
                pan:pan,
                package:pa,
            },
            success: function (data, status) {
                console.log(data);
                if(data.indexOf('success')>=0){
                    $('#trialModal').modal('hide');
                    toastr.success(data);
                    $(".company_name").val('');
                    $(".contact_number").val('');
                    $(".company_address").val('');
                    $(".pan").val('');
                    $(".package").val('');
                }else{
                    toastr.error(data);
                }
            },
            error: function (xhr, status, error) {
               console.log(error);
                toastr.error('Something went wrong. please try again later.');
            }
        });

    });

    $(".overview").click(function(e) {
        e.preventDefault()

        var company_name= $(".company_name123").val();
        var contact_number= $(".contact_number123").val();
        var company_address= $(".company_address123").val();
        var pan= $(".pan123").val();
        var sant= $(".package123").val();
        var plan= $(".plan").val();
        var plan_type= $(".plan_type").val();

        $('.company_name1234').html(company_name);
        $('.company_address1234').html(company_address);
        $('.contact_number1234').html(contact_number);
        $('.pan1234').html(pan);
        $('.package1234').html(sant);
        $('.plan1234').html(plan);
        $('.plan_type1234').html(plan_type);

        if ($(".company_name123").val() !== '')
        {
            $('#company_name12_error').empty();
        }

        if ($(".contact_number123").val() !== '')
        {
            $('#contact_number12_error').empty();
        }
        if ($(".company_address123").val() !== '')
        {
            $('#company_address12_error').empty();
        }
        if ($(".pan123").val() !== '')
        {
            $('#pan12_error').empty();
        }

    });

    $(".activate_now").click(function(e){
        e.preventDefault()
        toastr.success('Please wait. Processing....');
        var package_id= $(".package_id135").val();
        var amount= $(".price135").val();
        var discount_amount= $(".discount1012").val();
        var vat_amount= $(".vat_amount321").val();
        var total= $(".total321").val();
        var company_name= $(".company_name123").val();
        var contact_number= $(".contact_number123").val();
        var company_address= $(".company_address123").val();
        var pan= $(".pan123").val();
        var plan= $(".plan").val();
        var pa= $(".package123").val();
        var plan_type= $(".plan_type").val();
        var subscription_time= $(".subscription_time").val();
        var promo_code= $(".promo_code").val();
        $.ajax({
            type: 'POST',
            url: '{{url('/save/order')}}',
            data: {
                _token : "{{csrf_token()}}",
                package_id:package_id,
                amount:amount,
                discount_amount:discount_amount,
                vat_amount:vat_amount,
                total:total,
                company_name:company_name,
                contact_number:contact_number,
                company_address:company_address,
                pan:pan,
                plan:plan,
                package:pa,
                plan_type:plan_type,
                subscription_time:subscription_time,
                promo_code:promo_code,
            },
            success: function (data, status) {
                if(data.indexOf('success')>=0){
                    $("#activationModal").modal('hide');
                    toastr.success(data);
                    $(`#activationModal input`).val('');
                }else{
                    toastr.error(data);
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
                toastr.error('Something went wrong. please try again later.');
            }
        });
    });
function  submitForm(formId) {
    if(confirm('Are you sure ?')){
        let form= $(`#${formId}`);
        $.ajax({
            type: 'POST',
            url: form.attr("action"),
            data: form.serialize(),
            success: function (data, status) {
                if(data.indexOf('success')>=0){
                    toastr.success(data);
                    $(`#${formId} input`).val('');
                    $(`#${formId} textarea`).val('');
                }else{
                    toastr.error(data);
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
                toastr.error('Something went wrong. please try again later.');
            }
        });
    }return false;
}
</script>
</body>

</html>
