@extends('frontend.app')
@section('title', 'Blog | ')
@section('content')
    <div class="bg-default-secondary py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-title">
                    <h3 class="text-capitalize title" style="line-height: 38px;">
                        <span> an exclusive news about our product </span><br>
                        <span class="text-primary">to give you all insight</span>
                    </h3>
                    <p>give all the news you want about our product</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="p-5">
            <div class="row pb-4">
                <div class="col-md-6">
                        <img src="{{ asset('icon/blogSmall.png') }}" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <div class="section-title pb-3">
                        <div class="line"></div>
                        <h4 class="title text-capitalize">
                            new feature introduced <span> today by our company</span>
                        </h4>
                    </div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt dignissimos amet
                        quasi ducimus
                        iusto vel blanditiis sequi assumenda, libero neque accusantium. Corrupti nostrum adipisci,
                        ducimus magnam nihil odit nam quos reiciendis commodi.
                    </p>
                    <div class="text-right pt-3">
                        <a href="{{ url('blog/detail') }}" class="btn btn-primary btn-sm text-uppercase">learn more</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title pb-3">
                        <div class="line"></div>
                        <h4 class="title text-capitalize">
                            new feature introduced <span> today by our company</span>
                        </h4>
                    </div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt dignissimos amet
                        quasi ducimus
                        iusto vel blanditiis sequi assumenda, libero neque accusantium. Corrupti nostrum adipisci,
                        ducimus magnam nihil odit nam quos reiciendis commodi.
                    </p>
                    <div class="pt-3">
                        <button class="btn btn-primary btn-sm text-uppercase">learn more</button>
                    </div>
                </div>
                <div class="col-md-6">
                        <img src="{{ asset('icon/blogSmall.png') }}" class="img-fluid">
                </div>
            </div>
        </div>
    </div>

@endsection
