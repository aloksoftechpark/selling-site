@extends('frontend.app')
@section('title', 'Blog Details | ')
@section('content')
    <div class="container p-5">
        <div class="px-5">
            <div class="row">
                <div class="col-md-12 section-title">
                    <h3 class="text-capitalize title">
                        new feature introduced
                        <span>today by our company</span>
                    </h3>
                    <p>2077/10/12 - Kathmandu</p>
                </div>
            </div>
            <div class="row pt-3">
                <div class="col-md-12">
                    <img src="{{ asset('icon/blogBig.png') }}" class="img-fluid pb-3 ml-0 pl-0">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt dignissimos amet quasi ducimus
                        iusto
                        vel blanditiis sequi assumenda, libero neque accusantium. Corrupti nostrum adipisci, ducimus magnam
                        nihil odit nam quos reiciendis commodi. Doloribus eveniet ipsam earum, nobis. Lorem
                        ipsum dolor sit, amet consectetur adipisicing elit. Voluptate, quia quam molestiae eius eos quas
                        officiis? Sit fugit deleniti architecto.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt dignissimos amet quasi ducimus
                        iusto
                        vel blanditiis sequi assumenda, libero neque accusantium. Corrupti nostrum adipisci, ducimus magnam
                        nihil odit nam quos reiciendis commodi. Doloribus eveniet ipsam earum, nobis. Lorem
                        ipsum dolor sit, amet consectetur adipisicing elit. Voluptate, quia quam molestiae eius eos quas
                        officiis? Sit fugit deleniti architecto.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
