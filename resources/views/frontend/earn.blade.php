@extends('frontend.app')
@section('title', 'Earn | ')
@section('content')
    <style>
        .nav-tabs{
            border-bottom: 4px solid #1B51BE;
        }
        .nav-tabs .nav-link{
            border-radius: 10% 13% 0% 0% / 43% 44% 10% 10% !important;
            background: white;
            font-weight: 600;
            text-transform: uppercase;
            padding-left: 1.8rem;
            padding-right: 1.8rem;
        }
    </style>
    <div class="bg-default-secondary pt-5">
        <div class="px-5">
            <div class="row">
                <div class="col-md-12 section-title">
                    <h3 class="text-capitalize title" style="line-height: 38px;">
                        <span> an exclusive program for all of our customers </span><br>
                        <span class="text-primary">to earn from us</span>
                    </h3>
                    <p class="text-capitalize">earn and grow with us</p>
                </div>
            </div>
        </div>
        <ul class="nav nav-tabs pl-4 pt-5" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="partner-tab" data-toggle="tab" href="#partner" role="tab" aria-controls="partner" aria-selected="true">Partner</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="agent-tab" data-toggle="tab" href="#agent" role="tab" aria-controls="agent" aria-selected="false">Agent</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="associate-tab" data-toggle="tab" href="#associate" role="tab" aria-controls="associate" aria-selected="false">Associate</a>
            </li>
        </ul>
        <div class="tab-content bg-white">
            <div class="container">
                <div class="tab-pane fade show active" id="partner" role="tabpanel" aria-labelledby="partner-tab">
               <div class="container">
                   <div class="row">
                       <div class="col-md-5">
                           <div class="about-image text-center mt-50 wow fadeInRightBig" data-wow-duration="1s"
                                data-wow-delay="0.5s">
                               <img src="assets/images/STUDENT.svg" class="img-fluid" />
                           </div>
                       </div>
                       <div class="col-md-1"></div>
                       <div class="col-md-6">
                           <div class="about-content mt-50 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.5s">
                               <div class="section-title">
                                   <div class="line"></div>
                                   <h4 class="title">
                                       Refer And Earn <span> by collaborating with us</span>
                                   </h4>
                               </div>
                               <p class="text text-justify">
                                   Lorem ipsum dolor sit amet, consetetur sadipscing elitr, seiam
                                   nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                   erat, sed diam voluptua. At vero eos et accusam et justo duo
                                   dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
                                   sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                                   amet, consetetur sadipscing.Lorem ipsum dolor sit
                                   amet, consetetur sadipscing.no sea takimata
                                   sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                                   amet, consetetur sadipscing.Lorem ipsum dolor sit
                                   amet, consetetur sadipscing.
                               </p>
                           </div>
                       </div>
                       <div class="col-md-12">
                           <p class="text-justify">
                               Lorem ipsum dolor sit amet, consetetur sadipscing elitr, seiam
                               nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                               erat, sed diam voluptua. At vero eos et accusam et justo duo
                               dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
                               sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                               amet, consetetur sadipscing.Lorem ipsum dolor sit
                               amet, consetetur sadipscing.no sea takimata
                               sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                               amet, consetetur sadipscing.Lorem ipsum dolor sit
                               amet, consetetur sadipscing.
                           </p>
                       </div>
                       <div class="col-md-12 text-right pt-2">
                           <a href="javascript:;" class="btn btn-primary border-radius-0">Apply For Offer</a>
                       </div>
                   </div>
               </div>
            </div>
                <div class="tab-pane fade" id="agent" role="tabpanel" aria-labelledby="agent-tab">...</div>
                <div class="tab-pane fade" id="associate" role="tabpanel" aria-labelledby="associate-tab">...</div>
            </div>
        </div>
    </div>

@endsection
