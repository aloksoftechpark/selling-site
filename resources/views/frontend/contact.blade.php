@extends('frontend.app')
@section('title', 'Contact | ')
@section('content')
    <div class="bg-default-secondary">
        <div class="container py-5">
            <div class="row">
                <div class="col-md-12 section-title">
                    <h3 class="text-capitalize title" style="line-height: 38px;">
                        <span> feel free to contact us or </span><br>
                        <span class="text-primary">visit us personally</span>
                    </h3>
                    <p>we will response you as fast as possible</p>
                </div>
            </div>
            <div class="row py-5">
                <div class="col-md-5">
                    <div class="contact-detail p-5">
                        <div class="py-4">
                            <h3 class="text-center pb-4"><u>Contact Details</u></h3>
                            <ul>
                                <li>
                                    ADDRESS : <span>Tinkune, kathmandu Nepal</span>
                                </li>
                                <li>
                                    PHONE : <span>01-4111992, 980xxxxxxx</span>
                                </li>
                                <li>
                                    EMAIL : <span>support@softechpark.com</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="inquiei-form p-5 section-title">
                        <h3 class="text-center">
                            <span>if you need,</span> <span class="text-primary"> just drop a
                                line</span>
                        </h3>
                        <p class="text-center">You Should Not Wait For Long, Be Assure!</p>
                        <div class="row pt-4">
                            <div class="col-md-6">
                                <input type="text" class="form-control border-radius-0 mb-2" placeholder="Enter your name">
                                <input type="text" class="form-control border-radius-0 mb-2"
                                    placeholder="Enter email address">
                                <input type="text" class="form-control border-radius-0"
                                    placeholder="Enter your phone number">
                            </div>
                            <div class="col-md-6 text-right">
                                <textarea name="" class="form-control border-radius-0 mb-3" placeholder="Message"
                                    style="height: 76px;"></textarea>
                                <button class="btn btn-primary border-radius-0">SEND MESSAGE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="location-map">
        <h4 class="text-center py-3">Find Your Location</h4>
        <div class="px-4">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.9809993171352!2d85.34399461528646!3d27.686982132993023!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1988cd528363%3A0x82ef39f982517662!2sSoftechpark!5e0!3m2!1sen!2snp!4v1597914703463!5m2!1sen!2snp"
                style="width: 100%;height:450px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
                tabindex="0"></iframe>
        </div>
    </div>
@endsection
