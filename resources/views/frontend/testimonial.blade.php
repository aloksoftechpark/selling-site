<div class="testimonial-section py-5">
    <div class="container section-title">
        <h3 class="title text-center">
            <span> We Love to hear from</span> Our customers
        </h3>
        <p class="text-center pb-5">Wish Words From Our Users</p>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="slideshow-container">
                    <div class="mySlides">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{ asset('icon/kiran.png') }}" class="img-fluid">
                            </div>
                            <div class="col-md-9">
                                <p class="text-left">
                                    I love you the more in that I believe you had liked me for my own sake and for
                                    nothing
                                    else Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, hic?</p>
                                <p class="author text-right">- John Keats</p>
                                <p class="designation text-right text-capitalize">
                                    project manager
                                    <small class="text-uppercase"> (softechpark pvt.
                                        ltd.)</small>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="mySlides">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{ asset('icon/kiran.png') }}" class="img-fluid">
                            </div>
                            <div class="col-md-9">
                                <p class="text-left">
                                    ipsum dolor sit I love you the more in that I believe you had liked me for my
                                    own sake and for
                                    nothing
                                    else Loremamet consectetur adipisicing elit. Nostrum, hic?</p>
                                <p class="author text-right">- John Keats</p>
                                <p class="designation text-right text-capitalize">
                                    project manager
                                    <small class="text-uppercase"> (softechpark pvt.
                                        ltd.)</small>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="dot-container">
                    <span class="dot" onclick="currentSlide(1)"></span>
                    <span class="dot" onclick="currentSlide(2)"></span>
                </div>

                <script>
                    var slideIndex = 1;
                    showSlides(slideIndex);

                    function plusSlides(n) {
                        showSlides(slideIndex += n);
                    }

                    function currentSlide(n) {
                        showSlides(slideIndex = n);
                    }

                    function showSlides(n) {
                        var i;
                        var slides = document.getElementsByClassName("mySlides");
                        var dots = document.getElementsByClassName("dot");
                        if (n > slides.length) {
                            slideIndex = 1
                        }
                        if (n < 1) {
                            slideIndex = slides.length
                        }
                        for (i = 0; i < slides.length; i++) {
                            slides[i].style.display = "none";
                        }
                        for (i = 0; i < dots.length; i++) {
                            dots[i].className = dots[i].className.replace(" active", "");
                        }
                        slides[slideIndex - 1].style.display = "block";
                        dots[slideIndex - 1].className += " active";
                    }

                </script>
            </div>
        </div>
    </div>
</div>
