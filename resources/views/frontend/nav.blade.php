<div class="top-nav">
    <div class="px-5">
        <div class="row">
            <div class="col-md-7">
                <ul>
                    <li><a href="javascript:;"><i class="fa fa-location-arrow"></i> Tinkune Kathmandu Nepal</a></li>
                    <li><a href="javascript:;"><i class="fa fa-envelope-open"></i> info@softechpark.com</a></li>
                    <li><a href="javascript:;"><i class="fas fa-phone-volume"></i> (+977) 01 - 4111992</a></li>
                </ul>
            </div>
            <div class="col-md-5 text-right">
                <a href="javascript:;"><img src="{{ asset('icon/24x7.png') }}" class="img-fluid"></a>
                <button type="button" class="login-button ml-3" data-toggle="modal" data-target="#exampleModalCenter">
                    Login
                </button>
            </div>
        </div>
    </div>
</div>
<div class="px-5">
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('icon/softwarepark-logo.png') }}" alt="Logo" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="toggler-icon"></span>
            <span class="toggler-icon"></span>
            <span class="toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
            <ul id="nav" class="navbar-nav ml-2">
                <li class="nav-item @if (Request::path() == '/') active  @endif">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                </li>
                <li class="nav-item @if (Request::path() == 'plan-price') active  @endif ">
                    <a class="nav-link" href="{{ url('plan-price') }}">plans & pricing</a>
                </li>
                <li class="nav-item @if (Request::path() == 'feature') active  @endif">
                    <a class="nav-link" href="{{url('feature')}}">feature</a>
                </li>
                <li class="nav-item @if (Request::path() == 'earn') active  @endif">
                    <a class="nav-link" href="{{ url('earn') }}">Earn</a>
                </li>
                <li class="nav-item @if (Request::path() == 'about-us') active  @endif">
                    <a class="nav-link" href="{{ url('about-us') }}">about</a>
                </li>
                <li class="nav-item @if (Request::path() == 'blog') active  @endif">
                    <a class="nav-link" href="{{ url('blog') }}">blog</a>
                </li>
                <li class="nav-item @if (Request::path() == 'contact-us') active  @endif">
                    <a class="nav-link" href="{{ url('contact-us') }}">Contact</a>
                </li>
            </ul>
            <ul id="nav" class="navbar-nav ml-auto">
                <li class="navbar-btn d-none d-sm-inline-block">
                    <a class="main-btn sticky_acc" data-scroll-nav="0" href="#pricing" data-toggle="modal"
                        data-target="#registerAccount">WORKSPACE <i class="fas fa-long-arrow-alt-right"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</div>
<style>
    .login-register .nav-tabs {
        border:none;
    }
    .login-register .nav-link{
        padding: 0rem 1rem !important;
    }
    .login-register h3 {
        font-size: 28px;
        font-weight: 500;
        margin-bottom: 1rem;
    }
    .login-register .register label{
        margin-bottom: 0rem;
    }
    .login-register .register .form-group {
        margin-bottom: 0.2rem;
    }
    .login-register .form-control{
        border:none;
        border-bottom: 1px solid #ced4da;
    }

</style>
<!-- login / register modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="row">
                <div class="col-md-6 mr-0 pr-0" style="background: #1451C0;">
                    <img src="{{asset('icon/login-side.png')}}" alt="" class="img-fluid" style="height: 100%;" id="loginRegImg">
                </div>
                <div class="col-md-5 mr-0 pr-0 login-register">
                    <div class="row">
                        <div class="col-md-12 py-4">
                            <ul class="nav nav-tabs float-right" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="loginForm-tab" onclick="changeImg('{{asset('icon/login-side.png')}}')" data-toggle="tab" href="#loginForm" role="tab" aria-controls="loginForm" aria-selected="true">Log In</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="registerForm-tab" onclick="changeImg('{{asset('icon/register-side.png')}}')" data-toggle="tab" href="#registerForm" role="tab" aria-controls="registerForm" aria-selected="false">Register</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <div class="tab-content" id="myTabContent">
                                <form method="post" action="{{url('users-login')}}" class="tab-pane fade show active login" id="loginForm" role="tabpanel" aria-labelledby="loginForm-tab">
                                   @csrf
                                    <h3>Log In</h3>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="info@softechpark.com.uk">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" name="password" id="password" placeholder="password">
                                    </div>
                                    <button class="btn btn-primary border-radius-0 my-3" style="width: 100%;" type="button" onclick="loginRegister('loginForm')">SIGN IN</button>
                                    <div>
                                        <a href="javascript:;">Forgot Password ?</a>
                                        <a href="javascript:;" class="float-right">Not a Member yet ?</a>
                                    </div>
                                </form>
                                <form method="post" action="{{url('save-user')}}" class="tab-pane fade register" id="registerForm" role="tabpanel" aria-labelledby="registerForm-tab">
                                    @csrf
                                    <h3>Register</h3>
                                    <div class="form-group">
                                        <label for="full_name">Full Name</label>
                                        <input type="text" class="form-control" id="full_name" name="name" placeholder="Softechpark">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="info@softechpark.com.uk">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Contact Number</label>
                                        <input type="number" class="form-control" id="contact" name="phone" placeholder="9840680875">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="password">
                                    </div>
                                    <div class="form-group">
                                        <label for="c_password">Re-Type Password</label>
                                        <input type="password" class="form-control" id="c_password" name="confirm_password" placeholder="password">
                                    </div>
                                    <div class="form-group">
                                        <label for="referral_code">Referral Code</label>
                                        <input type="text" class="form-control" id="referral_code" name="refral_code" placeholder="123456HD">
                                    </div>
                                    <button class="btn btn-primary border-radius-0 my-3" type="button" onclick="loginRegister('registerForm')" style="width: 100%;">REGISTER</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        function changeImg(id) {
            $('#loginRegImg').attr('src',id);
        }
        function loginRegister(formId) {
            if (confirm('Are you sure ?')) {
                let form = $(`#${formId}`);
                $.ajax({
                    type: 'POST',
                    url: form.attr("action"),
                    data: form.serialize(),
                    success: function(data, status) {
                        console.log(data);
                        if (data.indexOf('success') >= 0) {
                            toastr.success(data);
                            location.reload();
                        } else {
                            toastr.error(data);
                        }
                    },
                    error: function(xhr, status, error) {
                        console.log(error);
                        toastr.error('Something went wrong. please try again later.');
                    }
                });
            }
            return false;
        }

    </script>
