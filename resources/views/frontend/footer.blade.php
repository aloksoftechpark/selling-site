<style>
    .smart-way-section {
        background: url("{{ asset('icon/footer-theam.png') }}");
        background-size: 100%;
        background-repeat: no-repeat;
        background-position: center;
    }

</style>
<div class="smart-way-section py-5">
    <div class="container section-title">
        <h3 class="title text-center text-capitalize">
            <span> Choose A Smart Way For </span> Your Business
        </h3>
        <p class="text-center text-capitalize">You Will Get The Best Service, Be Assure!</p>
        <div class="row">
            <div class="col-md-12 text-center pt-4">
                @if(Request::path() == 'contact-us')
                    <a href="javascript:;" class="btn btn-primary border-radius-0">TRY FOR FREE</a>
                    <a href="javascript:;" class="btn btn-outline-primary border-radius-0 ml-3">GET STARTED</a>
                @else
                    <a href="javascript:;" class="btn btn-primary border-radius-0">GET STARTED</a>
                    <a href="javascript:;" class="btn btn-outline-primary border-radius-0 ml-3">CONTACT US</a>
                @endif
            </div>
        </div>
    </div>
</div>
<footer id="team" class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-8">
                <div class="footer-about mt-4 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                    <a class="logo" href="javascript:;">
                        <img src="{{ asset('assets/images/softechpark_logo_footer.png') }}" alt="logo" />
                    </a>
                    <p class="text mt-3">
                        Lorem ipsum dolor sit amet consetetur sadipscing elitr,
                        sederfs diam nonumy eirmod tempor invidunt ut labore et dolore
                        magna aliquyam.
                    </p>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7">
                <div class="footer-link d-flex mt-50 justify-content-md-between">
                    <div class="link-wrapper wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
                        <div class="footer-title">
                            <h5 class="title">General</h5>
                        </div>
                        <ul class="link">
                            <li><a href="javascript:;">About Us</a></li>
                            <li><a href="javascript:;">About Product</a></li>
                            <li><a href="javascript:;">Security</a></li>
                            <li><a href="javascript:;">Offers & Discounts</a></li>
                        </ul>
                    </div>
                    <div class="link-wrapper wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
                        <div class="footer-title">
                            <h5 class="title">Product</h5>
                        </div>
                        <ul class="link">
                            <li><a href="javascript:;">Feature</a></li>
                            <li><a href="javascript:;">Plan & Pricing</a></li>
                            <li><a href="javascript:;">Privacy Policy</a></li>
                            <li><a href="javascript:;">Terms & Condition</a></li>
                        </ul>
                    </div>
                    <div class="link-wrapper wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
                        <div class="footer-title">
                            <h5 class="title">Help</h5>
                        </div>
                        <ul class="link">
                            <li><a href="javascript:;">User's Guide</a></li>
                            <li><a href="javascript:;">FAQ's</a></li>
                            <li><a href="javascript:;">Video Tutorials</a></li>
                            <li><a href="javascript:;">Contact Us</a></li>
                            <li><a href="javascript:;">Payment Method</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-5 col-sm-5">
                <div class="link-wrapper mt-50 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                    <div class="footer-title">
                        <h5 class="title text-right">Application</h5>
                    </div>
                    <ul class="link text-right">
                        <li><a href="javascript:;">Become a partner</a></li>
                        <li><a href="javascript:;">Become an Agent</a></li>
                        <li><a href="javascript:;">Become a Affiliate</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row mt-4 pb-3" style="border-top: 1px solid;">
            <div class="col-md-6">
                <a href="javascript:;" class="text-white mt-4"> Design And Developed By Softechpark</a></span>
            </div>
            <div class="col-md-6 text-right">
                <div class="footer-about">
                    <ul class="social">
                        <li>
                            <a href="javascript:;"><i class="lni-facebook-filled"></i></a>
                        </li>
                        <li>
                            <a href="javascript:;"><i class="lni-twitter-filled"></i></a>
                        </li>
                        <li>
                            <a href="javascript:;"><i class="lni-instagram-filled"></i></a>
                        </li>
                        <li>
                            <a href="javascript:;"><i class="lni-linkedin-original"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="particles-2"></div>
</footer>
