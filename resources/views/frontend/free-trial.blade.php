@extends('frontend.app')
@section('content')
    <style>
        .banner-section {
            background: url("{{ asset('icon/banner-bg.png') }}");
            background-size: 46% 100%;
            background-repeat: no-repeat;
            background-position: right 0px;
        }
        .banner-image-fluid {
            margin-left: -3rem;
        }
    </style>
    <div class="index-banner banner-section">
        <div class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-8">
                    <div class="banner-form section-title py-4">
                        <h3 class="text-center title text-primary"><u>FREE TRIAL</u></h3>
                        <form method="POST" action="{{ route('free-trial.store') }}" id="freeTrialForm" class="row p-4">
                            @csrf
                            <div class="col-md-6 pb-3">
                                <div class="row">
                                    <div class="col-md-5 text-right mr-0 pr-0">
                                        <label for="name">Full Name</label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control border-radius-0" placeholder="Your Name"
                                            name="name" id="name" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pb-3">
                                <div class="row">
                                    <div class="col-md-5 text-right mr-0 pr-0">
                                        <label for="company_address">Company Address</label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control border-radius-0"
                                            placeholder="Company Address" name="address" id="company_address" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pb-3">
                                <div class="row">
                                    <div class="col-md-5 text-right mr-0 pr-0">
                                        <label for="company_name">Company Name</label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control border-radius-0" placeholder="Company Name"
                                            name="company" id="company_name" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pb-3">
                                <div class="row">
                                    <div class="col-md-5 text-right mr-0 pr-0">
                                        <label for="company_email">Company Email</label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control border-radius-0" placeholder="Company Email"
                                            name="email" id="company_email" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pb-3">
                                <div class="row">
                                    <div class="col-md-5 text-right mr-0 pr-0">
                                        <label for="pan">PAN/Registration</label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="number" class="form-control border-radius-0"
                                            placeholder="PAN / Registration Number" name="pan" id="pan" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pb-3">
                                <div class="row">
                                    <div class="col-md-5 text-right mr-0 pr-0">
                                        <label for="contact_number">Contact Number</label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control border-radius-0" placeholder="Contact Number"
                                            name="contact" id="contact_number" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pb-3">
                                <div class="row">
                                    <div class="col-md-5 text-right mr-0 pr-0">
                                        <label for="plan">Plan Type</label>
                                    </div>
                                    <div class="col-md-7">
                                        <select class="form-control border-radius-0" name="plan_id" id="plan" required>
                                            <option value="">Select Your Plan</option>
                                            @foreach($plans as $plan)
                                                <option value="{{$plan->id}}">{{$plan->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pb-3">
                                <div class="row">
                                    <div class="col-md-5 text-right mr-0 pr-0">
                                        <label for="workspace">Workspace</label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control border-radius-0" placeholder="Workspace"
                                               name="workspace" id="workspace" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center pt-3">
                                <button type="submit" class="btn btn-primary border-radius-0">SUBMIT FOR FREE TRIAL</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <img src="{{ asset('icon/mascot.png') }}" class="img-fluid banner-image-fluid">
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#workspace").blur(function(){
                let workspace=$("#workspace").val().toLowerCase().trim().replace(/\s/g,'');
                $("#workspace").val(workspace);
                $.ajax({
                    type: 'GET',
                    url: `{{url('check/workspace')}}/${workspace}`,
                    success: function(data, status) {
                        console.log(data);
                        if (data.indexOf('exist') >= 0) {
                            toastr.error(data);
                        }
                    },
                    error: function(xhr, status, error) {
                        console.log(error);
                    }
                });
            });
            $('#freeTrialForm').submit(function(event) {
                event.preventDefault();
                if (confirm('Are you sure ?')) {
                    let form = $(`#freeTrialForm`);
                    $.ajax({
                        type: 'POST',
                        url: form.attr("action"),
                        data: form.serialize(),
                        success: function(data, status) {
                            console.log(data);
                            if (data.indexOf('success') >= 0) {
                                toastr.success(data);
                                $("#freeTrialForm input").val('');
                            } else {
                                toastr.error(data);
                            }
                        },
                        error: function(xhr, status, error) {
                            console.log(error);
                            toastr.error('Something went wrong. please try again later.');
                        }
                    });
                }
                return false;
            });

        });
    </script>
@endsection
