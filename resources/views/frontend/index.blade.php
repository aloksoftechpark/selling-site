@extends('frontend.app')
@push('scripts')
@endpush
@section('content')
    <style>
        .banner-section {
            background: url("{{ asset('icon/banner-bg.png') }}");
            background-size: 46%;
            background-repeat: no-repeat;
            background-position: right;
            padding-top: 6rem;
            padding-bottom: 3rem;
        }

    </style>
    <div class="index-banner banner-section">
        <div class="px-5">
            <div class="row">
                <div class="col-md-5">
                    <h6 class="text-uppercase text-primary">softechpark e-billing and business management</h6>
                    <h4 class="text-capitalize title py-4">control you business anytime form anywhere using our
                        software.</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt dignissimos amet quasi ducimus
                        iusto vel blanditiis sequi assumenda, libero neque accusantium. Corrupti nostrum adipisci,
                        ducimus magnam nihil odit nam quos reiciendis commodi. Doloribus eveniet ipsam earum, nobis.</p>
                    <div class="text-justify mt-3 mb-2">
                        <a href="{{ url('free-trial') }}" class="btn btn-primary text-uppercase border-radius-0">try for
                            free</a>
                        <a href="{{ url('order-placement') }}"
                            class="btn btn-outline-primary text-uppercase border-radius-0 ml-3">Get started</a>
                    </div>
                    <div class="text-capitalize">need more information ? <a href="javascript:;">more information</a>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <img src="{{ asset('icon/ebilling-feature.png') }}" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
    <div class="img-container">
        <div class="container py-5">
            <div class="row">
                <div class="col-md-12 section-title">
                    <h3 class="text-primary text-capitalize text-center title"><span>what we offer you</span></h3>
                    <p class="text-center">learn to use our software with ease</p>
                    <div class="container">
                        <center>
                            <img src="{{ asset('icon/dashboard.png') }}" class="image">
                        </center>
                        <div class="img-middle">
                            <img src="{{ asset('icon/playButton.png') }}" class="img-fluid" style="cursor: pointer;"
                                data-toggle="modal" data-target="#exampleModalCenter">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Video Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <iframe style="width: 100%;height:29rem;" src="https://www.youtube.com/embed/NHHhiqwcfRM" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="feature-section">
        <div class="container py-5  section-title">
            <h3 class="text-primary text-capitalize text-center title"><span>why choose us</span></h3>
            <p class="text-center">What Are Our Advantages</p>
            <div class="row pt-5">
                <div class="col-md-3 mb-4">
                    <div class="border px-2 py-3">
                        <center>
                            <img src="{{ asset('icon/support.png') }}" class="img-fluid primary">
                            <img src="{{ asset('icon/white/support.png') }}" class="img-fluid secondary">
                        </center>
                        <h6 class="text-center py-2">Quick & Free Support</h6>
                        <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                            dolorem
                            consequuntur
                            sapiente, excepturi.</p>
                    </div>
                </div>
                <div class="col-md-3 mb-4">
                    <div class="border px-2 py-3">
                        <center>
                            <img src="{{ asset('icon/instantActivation.png') }}" class="img-fluid primary">
                            <img src="{{ asset('icon/white/instantActivation.png') }}" class="img-fluid secondary">
                        </center>
                        <h6 class="text-center py-2">Instant Activation</h6>
                        <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                            dolorem
                            consequuntur
                            sapiente, excepturi.</p>
                    </div>
                </div>
                <div class="col-md-3 mb-4">
                    <div class="border px-2 py-3">
                        <center>
                            <img src="{{ asset('icon/fastAndSAFE.png') }}" class="img-fluid primary">
                            <img src="{{ asset('icon/white/fastAndSAFE.png') }}" class="img-fluid secondary">
                        </center>
                        <h6 class="text-center py-2">Fast, Safe & Secure</h6>
                        <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                            dolorem
                            consequuntur
                            sapiente, excepturi.</p>
                    </div>
                </div>
                <div class="col-md-3 mb-4">
                    <div class="border px-2 py-3">
                        <center>
                            <img src="{{ asset('icon/IRDvERIFIED.png') }}" class="img-fluid primary">
                            <img src="{{ asset('icon/white/IRDvERIFIED.png') }}" class="img-fluid secondary">
                        </center>
                        <h6 class="text-center py-2">IRD Varified</h6>
                        <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                            dolorem
                            consequuntur
                            sapiente, excepturi.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="border px-2 py-3">
                        <center>
                            <img src="{{ asset('icon/wellOrganized.png') }}" class="img-fluid primary">
                            <img src="{{ asset('icon/white/wellOrganized.png') }}" class="img-fluid secondary">
                        </center>
                        <h6 class="text-center py-2">Well Organized & User Friendly</h6>
                        <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                            dolorem
                            consequuntur
                            sapiente, excepturi.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="border px-2 py-3">
                        <center>
                            <img src="{{ asset('icon/budget.png') }}" class="img-fluid primary">
                            <img src="{{ asset('icon/white/budget.png') }}" class="img-fluid secondary">
                        </center>
                        <h6 class="text-center py-2">Budget Friendly</h6>
                        <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                            dolorem
                            consequuntur
                            sapiente, excepturi.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="border px-2 py-3">
                        <center>
                            <img src="{{ asset('icon/responsive.png') }}" class="img-fluid primary">
                            <img src="{{ asset('icon/white/responsive.png') }}" class="img-fluid secondary">
                        </center>
                        <h6 class="text-center py-2">Full Responsive</h6>
                        <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                            dolorem
                            consequuntur
                            sapiente, excepturi.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="border px-2 py-3">
                        <center>
                            <img src="{{ asset('icon/global.png') }}" class="img-fluid primary">
                            <img src="{{ asset('icon/white/global.png') }}" class="img-fluid secondary">
                        </center>
                        <h6 class="text-center py-2">Globally Accessible</h6>
                        <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
                            dolorem
                            consequuntur
                            sapiente, excepturi.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section id="about" class="referral-section about-area">
        <div class="about-shape-2">
            <img src="assets/images/about-shape-2.svg" alt="shape" />
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-4">
                    <div class="about-image text-center mt-50 wow fadeInRightBig" data-wow-duration="1s"
                        data-wow-delay="0.5s">
                        <img src="assets/images/STUDENT.svg" class="img-fluid" />
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6">
                    <div class="about-content mt-50 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.5s">
                        <div class="section-title">
                            <div class="line"></div>
                            <h4 class="title">
                                Refer And Earn <span> offer for the students</span>
                            </h4>
                        </div>
                        <p class="text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, seiam
                            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                            erat, sed diam voluptua. At vero eos et accusam et justo duo
                            dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
                            sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                            amet, consetetur sadipscing.
                        </p>
                        <div class="pt-2">
                            <a href="javascript:;" class="btn btn-primary border-radius-0">Try to
                                earn</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-6">
                    <div class="about-content mt-50 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.5s">
                        <div class="section-title">
                            <div class="line"></div>
                            <h4 class="title">
                                Refer And Earn <span> offer for the students</span>
                            </h4>
                        </div>
                        <p class="text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, seiam
                            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                            erat, sed diam voluptua. At vero eos et accusam et justo duo
                            dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
                            sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                            amet, consetetur sadipscing.
                        </p>
                        <div class="pt-2">
                            <a href="javascript:;" class="btn btn-primary border-radius-0">Try to
                                earn</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="about-image text-center mt-50 wow fadeInRightBig" data-wow-duration="1s"
                        data-wow-delay="0.5s">
                        <img src="assets/images/STUDENT.svg" class="img-fluid" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('frontend.testimonial')
    <div class="px-2 section-title">
        <h3 class="title text-center text-capitalize">
            <span> Some of </span>
            <span class="text-primary">our Partners</span>
        </h3>
        <p class="text-center text-capitalize">Who we work with</p>
        <div class="row">
            <div class="col-md-12">
                <section class="customer-logos slider py-4">
                    <div class="slide">
                        <img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg">
                    </div>
                    <div class="slide">
                        <img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg">
                    </div>
                    <div class="slide">
                        <img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg">
                    </div>
                    <div class="slide">
                        <img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg">
                    </div>
                    <div class="slide">
                        <img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg">
                    </div>
                    <div class="slide">
                        <img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg">
                    </div>
                    <div class="slide">
                        <img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg">
                    </div>
                    <div class="slide">
                        <img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg">
                    </div>
                    <div class="slide">
                        <img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg">
                    </div>

                </section>
                <script>
                    $(document).ready(function() {
                        $('.customer-logos').slick({
                            slidesToShow: 8,
                            slidesToScroll: 1,
                            autoplay: true,
                            autoplaySpeed: 2000,
                            arrows: false,
                            dots: false,
                            pauseOnHover: true,
                            responsive: [{
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 4
                                }
                            }, {
                                breakpoint: 520,
                                settings: {
                                    slidesToShow: 3
                                }
                            }]
                        });
                    });

                </script>
            </div>
        </div>
    </div>

@endsection
