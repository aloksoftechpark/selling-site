@extends('frontend.app')
@section('title', 'Plans | ')
@section('content')
    <style>
        .app {
            background: url("{{ asset('icon/coin.png') }}");
            background-size: 15%;
            background-repeat: no-repeat;
            background-position: right 30.1rem;
        }
    </style>
    <div class="bg-default-secondary app">
        <div class="container p-5">
            <div class="row">
                <div class="col-md-12 section-title">
                    <h3 class="text-capitalize title" style="line-height: 38px;">
                        <span> perfectly calculated plan for </span><br>
                        <span class="text-primary">your business</span>
                    </h3>
                    <p class="text-capitalize">
                        30 day free trial. <a href="javascript:;">cancel at anytime</a>
                    </p>
                </div>
            </div>
            <div class="row p-5">
                <div class="col-md-3">
                    <div class="post-slide8 basic-package">
                        <div class="post-img">
                            <div class="post-content">
                                <h3 class="post-title text-center">
                                    <a href="#">Basic</a>
                                </h3>
                                <div class="post-description mt-4">
                                    <ul>
                                        <li class="true"><i class="fas fa-check"></i> Sample Text Here</li>
                                        <li class="true"><i class="fas fa-check"></i> Other Text Here</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="post-date">
                                <span class="date">NRP 1000 Per Month</span>
                            </div>
                        </div>
                        <div class="select-package">
                            <a href="javascript:;">Select Package</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="post-slide8 regular-package">
                        <div class="post-img">
                            <div class="post-content">
                                <h3 class="post-title text-center">
                                    <a href="#">Regular</a>
                                </h3>
                                <div class="post-description mt-4">
                                    <ul>
                                        <li class="true"><i class="fas fa-check"></i> Sample Text Here</li>
                                        <li class="true"><i class="fas fa-check"></i> Other Text Here</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="post-date">
                                <span class="date">NRP 1500 Per Month</span>
                            </div>
                        </div>
                        <div class="select-package">
                            <a href="javascript:;">Select Package</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="post-slide8 premium-package">
                        <div class="post-img">
                            <div class="post-content">
                                <h3 class="post-title text-center">
                                    <a href="#">Premium</a>
                                </h3>
                                <div class="post-description mt-4">
                                    <ul>
                                        <li class="true"><i class="fas fa-check"></i> Sample Text Here</li>
                                        <li class="true"><i class="fas fa-check"></i> Other Text Here</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="post-date">
                                <span class="date">NRP 1850 Per Month</span>
                            </div>
                        </div>
                        <div class="select-package">
                            <a href="javascript:;">Select Package</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="post-slide8 ultimate-package">
                        <div class="post-img">
                            <div class="post-content">
                                <h3 class="post-title text-center">
                                    <a href="#">Ultimate</a>
                                </h3>
                                <div class="post-description mt-4">
                                    <ul>
                                        <li class="true"><i class="fas fa-check"></i> Sample Text Here</li>
                                        <li class="true"><i class="fas fa-check"></i> Other Text Here</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                        <li class="false"><i class="fas fa-times"></i> Description text</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="post-date">
                                <span class="date">NRP 2050 Per Month</span>
                            </div>
                        </div>
                        <div class="select-package">
                            <a href="javascript:;">Select Package</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 section-title">
                    <h3 class="text-center title py-4"><span>Frequently Asked </span> <span
                            class="text-primary">Questions</span>
                    </h3>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                        aria-expanded="true" aria-controls="collapseOne" class="text-capitalize">
                                        how does the 30 days trial work ?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl
                                        lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat
                                        libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum
                                        laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie
                                        eros. </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                        href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"
                                        class="text-capitalize">
                                        How often do i have renew my subscription with Softechpark E-Billing System ?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl
                                        lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat
                                        libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum
                                        laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie
                                        eros. </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                        href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"
                                        class="text-capitalize">
                                        ehat will happen to my account once the trial period is over ?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl
                                        lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat
                                        libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum
                                        laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie
                                        eros. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
