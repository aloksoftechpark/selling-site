@extends('frontend.app')
@section('content')
    <style>
        .banner-section {
            background: url("{{ asset('icon/banner-bg.png') }}");
            background-size: 46% 100%;
            background-repeat: no-repeat;
            background-position: right 0px;
        }
a .next{
    padding: 8px 15px 8px 15px;
    border: 1px solid white;
    margin: 5px 0px 5px 0px;
    margin-right: -5rem;
    font-size: 13px;
}
    </style>
    <div class="index-banner banner-section">
        <div class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-8">
                    <div class="banner-form section-title py-4">
                        <h3 class="text-center title text-primary"><u>Service Order Placement Form</u></h3>
                        <div class="p-4">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active nav-link-style-1" id="company-detail-tab" data-toggle="tab"
                                        href="#company-detail" role="tab" aria-controls="company-detail"
                                        aria-selected="true">Company
                                        Detail</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link nav-link-style-2" id="product-detail-tab" data-toggle="tab"
                                        href="#product-detail" role="tab" aria-controls="product-detail"
                                        aria-selected="false">Product
                                        Detail</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link nav-link-style-2" id="payment-detail-tab" data-toggle="tab"
                                        href="#payment-detail" role="tab" aria-controls="payment-detail"
                                        aria-selected="false">Payment Detail</a>
                                </li>
                            </ul>
                            <form method="POST" action="javascript:;" class="tab-content">
                                <div class="tab-pane fade show active" id="company-detail" role="tabpanel"
                                    aria-labelledby="company-detail-tab">
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-7 py-3">
                                            <div class="row pb-2">
                                                <div class="col-md-5 text-right mr-0 pr-0">
                                                    <label for="company_name">Company Name</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control border-radius-0"
                                                        placeholder="Company Name" name="company_name" id="company_name">
                                                </div>
                                            </div>
                                            <div class="row pb-2">
                                                <div class="col-md-5 text-right mr-0 pr-0">
                                                    <label for="pan">PAN / VAT Number</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control border-radius-0"
                                                        placeholder="PAN / VAT Number" name="pan" id="pan">
                                                </div>
                                            </div>
                                            <div class="row pb-2">
                                                <div class="col-md-5 text-right mr-0 pr-0">
                                                    <label for="company_address">Company Address</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control border-radius-0"
                                                        placeholder="Company Address" name="company_address"
                                                        id="company_address">
                                                </div>
                                            </div>
                                            <div class="row pb-2">
                                                <div class="col-md-5 text-right mr-0 pr-0">
                                                    <label for="company_email">Company Email</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control border-radius-0"
                                                        placeholder="Company Email" name="company_email" id="company_email">
                                                </div>
                                            </div>
                                            <div class="row pb-2">
                                                <div class="col-md-5 text-right mr-0 pr-0">
                                                    <label for="contact_number">Contact Number</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control border-radius-0"
                                                        placeholder="Company Name" name="contact_number"
                                                        id="contact_number">
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-right">
                                                <a href="javascript:;"><span class="badge badge-primary next">Next
                                                        &#8594;</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="product-detail" role="tabpanel"
                                    aria-labelledby="product-detail-tab">
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-7 py-3">
                                            <div class="row pb-2">
                                                <div class="col-md-12 text-justify mr-0 pr-0">
                                                    Do you want to verify from IRD?
                                                    <span class="ml-3">
                                                        <input type="radio" name="ird" id="ird-yes" value="1" checked>
                                                        <label for="ird-yes"> YES </label>
                                                        <input type="radio" class="ml-2" name="ird" id="ird-no" value="0">
                                                        <label for="ird-no"> NO </label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="row pb-2">
                                                <div class="col-md-5 text-right mr-0 pr-0">
                                                    <label for="plan">Plan</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <select class="form-control border-radius-0" name="plan" id="plan">
                                                        <option value="">Select Your Plan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row pb-2">
                                                <div class="col-md-5 text-right mr-0 pr-0">
                                                    <label for="subscription_type">Subscription Type</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <select class="form-control border-radius-0" name="subscription_type"
                                                        id="subscription_type">
                                                        <option value="">Select Subscription Type</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row pb-2">
                                                <div class="col-md-5 text-right mr-0 pr-0">
                                                    <label for="branches">Number Of Branch</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <select class="form-control border-radius-0" name="branches"
                                                        id="branches">
                                                        <option value="">Select Branch No.</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row pb-2">
                                                <div class="col-md-5 text-right mr-0 pr-0">
                                                    <label for="users">Number Of Users</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <select class="form-control border-radius-0" name="users" id="users">
                                                        <option value="">Select Users No.</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-right">
                                                <a href="javascript:;"><span class="badge badge-primary next">Next
                                                        &#8594;</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="payment-detail" role="tabpanel"
                                    aria-labelledby="payment-detail-tab">
                                    <div class="row">
                                        <div class="col-md-12 py-4">
                                            <table class="table table-responsive table-bordered payment-detail-table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">S.N.</th>
                                                        <th scope="col" style="width: 58%;">Particular</th>
                                                        <th scope="col">Qty</th>
                                                        <th scope="col">Rate</th>
                                                        <th scope="col">Amount (NRP)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">1</th>
                                                        <td>Product oriented gold plan for product oriented company</td>
                                                        <td class="text-center">1</td>
                                                        <td class="text-center">2000</td>
                                                        <td class="text-center">2000</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">2</th>
                                                        <td>Extra branched added</td>
                                                        <td class="text-center">3</td>
                                                        <td class="text-center">100</td>
                                                        <td class="text-center">300</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">3</th>
                                                        <td>Extra users added</td>
                                                        <td class="text-center">5</td>
                                                        <td class="text-center">50</td>
                                                        <td class="text-center">250</td>
                                                    </tr>
                                                    <tr class="no-border">
                                                        <th rowspan="5" colspan="2"></th>
                                                        <td colspan="2" class="text-right">Total</td>
                                                        <td class="text-center">2550</td>
                                                    </tr>
                                                    <tr class="no-border">
                                                        <td colspan="2" class="text-right">X 3 Month</td>
                                                        <td class="text-center">7650</td>
                                                    </tr>
                                                    <tr class="no-border">
                                                        <td colspan="2" class="text-right">Discount</td>
                                                        <td class="text-center">1000</td>
                                                    </tr>
                                                    <tr class="no-border">
                                                        <td colspan="2" class="text-right">Vat able Amount</td>
                                                        <td class="text-center">7650</td>
                                                    </tr>
                                                    <tr class="no-border">
                                                        <td colspan="2" class="text-right">13 % Vat</td>
                                                        <td class="text-center">864.50</td>
                                                    </tr>
                                                    <tr class="no-border">
                                                        <td colspan="2">
                                                            <div class="input-group">
                                                                <div class="input-group-append mr-2 mt-1">
                                                                    Promo Code
                                                                </div>
                                                                <input type="text" class="form-control" placeholder="Code">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">Redeem</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td colspan="2" class="text-right">Payable Amount</td>
                                                        <td class="text-center">7514.50</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <h4>Select Payment Method</h4>
                                    <div class="row pt-3 wallet">
                                        <div class="col-md-2">
                                            <img src="{{ asset('icon/wallet/esewa.jpg') }}" class="img-fluid">
                                        </div>
                                        <div class="col-md-2">
                                            <img src="{{ asset('icon/wallet/nic-asia-bank.png') }}" class="img-fluid">
                                        </div>
                                        <div class="col-md-2">
                                            <img src="{{ asset('icon/wallet/khalti-digital-wallet.png') }}"
                                                class="img-fluid">
                                        </div>
                                        <div class="col-md-2">
                                            <img src="{{ asset('icon/wallet/ime-pay.jpg') }}" class="img-fluid">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <button class="btn btn-primary border-radius-0">PLACE ORDER</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <img src="{{ asset('icon/mascot.png') }}" class="img-fluid banner-image-fluid">
                </div>
            </div>
        </div>
    </div>
@endsection
