@extends('frontend.app')
@section('title', 'About | ')
@section('content')
    <style>
        .mission-vision {
            height: 570px;
            background: url("{{ asset('icon/missionVisionGoal.png') }}");
            background-size: 80%;
            background-repeat: no-repeat;
            background-position: center 0px;
        }
        .mission-vision p{
            width: 50%;
            margin: 0rem 0rem 0rem 15rem;
            height: 134px;
            padding: 1rem 0rem 1rem 0rem;
        }
        .justify-content-center{
            justify-content: center;
        }
         .app {
             background: url("{{ asset('icon/group-7000.png') }}");
             background-size: 15%;
             background-repeat: no-repeat;
             background-position: right 76.1rem;
         }
    </style>
    <div class="app">
        <div class="container py-5">
        <div class="row">
            <div class="col-md-12 section-title">
                <h3 class="text-capitalize title" style="line-height: 38px;">
                    <span> everything you need to know about </span><br>
                    <span class="text-primary">us & our product</span>
                </h3>
                <p>get all the information about us as well as about our product</p>
            </div>
        </div>
        <div class="container pt-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title pb-3">
                        <div class="line"></div>
                        <h4 class="title">
                            About <br>
                            <span> Our Company</span>
                        </h4>
                    </div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt dignissimos amet
                        quasi ducimus
                        iusto vel blanditiis sequi assumenda, libero neque accusantium. Corrupti nostrum adipisci,
                        ducimus magnam nihil odit nam quos reiciendis commodi. Doloribus eveniet ipsam earum, nobis. Lorem
                        ipsum dolor sit, amet consectetur adipisicing elit. Voluptate, quia quam molestiae eius eos quas
                        officiis? Sit fugit deleniti architecto. Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                        Illum nulla dolorum eos tempore saepe numquam id vitae?
                    </p>
                    <div class="text-justify mt-3">
                        <a href="javascript:;" class="btn btn-primary text-uppercase border-radius-0">learn more</a>
                    </div>
                </div>
            </div>
            <div class="row pt-4">
                <div class="col-md-6">
                    <center>
                        <img src="{{ asset('icon/dashboard-1.png') }}" class="image" style="width: 100%;">
                    </center>
                    <div class="img-middle">
                        <img src="{{ asset('icon/playButton.png') }}" class="img-fluid"
                            style="cursor: pointer;width: 54%;margin-top: 1rem;" data-toggle="modal"
                            data-target="#exampleModalCenter">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="section-title pb-3">
                        <div class="line"></div>
                        <h4 class="title">
                            About <br>
                            <span> Our Product</span>
                        </h4>
                    </div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt dignissimos amet
                        quasi ducimus
                        iusto vel blanditiis sequi assumenda, libero neque accusantium. Corrupti nostrum adipisci,
                        ducimus magnam nihil odit nam quos reiciendis commodi. Doloribus eveniet ipsam earum, nobis. Lorem
                        ipsum dolor sit, amet consectetur adipisicing elit. Voluptate, quia quam
                    </p>
                </div>
                <div class="col-md-12 pt-3">
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iure rem inventore aliquid quibusdam nulla.
                        Tempora, maxime. Repellat amet aperiam eos nemo obcaecati, non hic cumque iusto magnam accusantium
                        quos enim dolore ipsum, est soluta impedit aut vitae exercitationem! Unde atque nisi expedita nobis,
                        amet enim delectus a laborum ratione velit repudiandae ad itaque neque nemo incidunt veniam. Facere
                        fuga maxime doloremque distinctio! Repellendus adipisci veritatis itaque culpa debitis dolor
                        consequatur. Quibusdam, soluta. Earum saepe accusantium quam itaque, culpa reprehenderit hic tempore
                        sequi totam odio pariatur. Repellat, minus! Natus nam architecto animi. Quos optio cumque possimus
                        totam consequatur necessitatibus eligendi aperiam!</p>
                </div>
            </div>
{{--            <div class="row pt-3">--}}
{{--                <div class="col-md-2"> </div>--}}
{{--                <div class="col-md-8 poli-1">--}}
{{--                    <div class="inside-poli">--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam quae voluptatem qui fugiat--}}
{{--                            voluptas--}}
{{--                            accusamus laudantium vero dolorum aliquam harum, vitae cum magnam culpa debitis quos praesentium--}}
{{--                            impedit ipsum voluptatibus nesciunt facilis natus laborum saepe itaque velit! Ullam deleniti,--}}
{{--                        </p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-2"> </div>--}}
{{--                <div class="col-md-2"> </div>--}}
{{--                <div class="col-md-8 poli-2">--}}
{{--                    <div class="inside-poli">--}}
{{--                        <p class="text-right">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam quae--}}
{{--                            voluptatem qui fugiat--}}
{{--                            voluptas--}}
{{--                            accusamus laudantium vero dolorum aliquam harum, vitae cum magnam culpa debitis quos praesentium--}}
{{--                            impedit ipsum voluptatibus nesciunt facilis natus laborum saepe itaque velit! Ullam deleniti,--}}
{{--                        </p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-2"> </div>--}}
{{--            </div>--}}
            <div class="mission-vision justify-content-center mt-5">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam dolore reprehenderit sed unde veniam voluptates. Aspernatur consequuntur distinctio odit? Corporis earum illo molestias nostrum repudiandae vel? Fugiat hic illo ut?
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam dolore reprehenderit sed unde veniam voluptates. Aspernatur consequuntur distinctio odit? Corporis earum illo molestias nostrum repudiandae vel? Fugiat hic illo ut?
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam dolore reprehenderit sed unde veniam voluptates. Aspernatur consequuntur distinctio odit? Corporis earum illo molestias nostrum repudiandae vel? Fugiat hic illo ut?
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam dolore reprehenderit sed unde veniam voluptates. Aspernatur consequuntur distinctio odit? Corporis earum illo molestias nostrum repudiandae vel? Fugiat hic illo ut?
                </p>
            </div>
        </div>
    </div>
    </div>

@endsection
