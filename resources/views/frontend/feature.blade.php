@extends('frontend.app')
@section('title', 'Features | ')
@section('content')
    <style>
        .feature .tab-lists li {
            margin-top: 0.5rem;
            border: 1px solid silver;
            border-radius: 3px;
            background: white;
        }
        .feature .tab-lists .active,.feature .tab-lists li:hover {
            background: #1B51BE !important;
        }
        .feature .tab-lists .active a{
            color: white !important;
        }
        .feature .tab-lists li:hover a{
            color: white !important;
        }
        .feature .tab-lists li:hover .white{
            display: inline-block;
        }
        .feature .tab-lists li:hover .black{
            display: none;
        }
        .feature .tab-lists .active .white{
            display: inline-block;
        }
        .feature .tab-lists .active .black{
            display: none;
        }
        .feature .tab-lists a {
            padding: 9px 14px 9px 14px;
            color: black;
            text-transform: uppercase;
            font-weight: 600;
            font-size: 14px;
            width: 100%;
        }
        .feature .tab-lists a img{
            height: 20px;
        }
        .feature .tab-lists a i{
            float: right;
            margin-top: 4px;
        }
        .feature .tab-lists a .white{
            display: none;
        }
        .feature .border{
            -webkit-box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.35);
            -moz-box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.35);
            box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.35);
            margin-top: 0.7rem;
        }
        .app {
            background: url("{{ asset('icon/group-7000.png') }}");
            background-size: 13%;
            background-repeat: no-repeat;
            background-position: right 40rem;
        }
    </style>
    <div class="bg-default-secondary feature app">
        <div class="p-5">
            <div class="row">
                <div class="col-md-12 section-title">
                    <h3 class="text-capitalize title" style="line-height: 38px;">
                        <span> an entire range of specification to manage </span> <br>
                        <span class="text-primary">your business</span>
                    </h3>
                    <p class="text-capitalize">
                        best you will get in an e-billing site
                    </p>
                </div>
            </div>
            <div class="px-5">
                <div class="row">
                    <div class="col-md-4 tab-lists">
                        <ul>
                            <li class="active">
                                <a href="javascript:;">
                                    <img src="{{asset('icon/salesblack.png')}}" alt="" class="black">
                                    <img src="{{asset('icon/sales.png')}}" alt="" class="white">
                                     sales & purchase mngt <i class="fas fa-angle-double-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <img src="{{asset('icon/leadblack.png')}}" alt="" class="black">
                                    <img src="{{asset('icon/lead.png')}}" alt="" class="white">
                                     lead management system <i class="fas fa-angle-double-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <img src="{{asset('icon/databackupBlack.png')}}" alt="" class="black">
                                    <img src="{{asset('icon/databackup.png')}}" alt="" class="white">
                                     data backup system <i class="fas fa-angle-double-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <img src="{{asset('icon/employeeBlack.png')}}" alt="" class="black">
                                    <img src="{{asset('icon/employee.png')}}" alt="" class="white">
                                    employee management <i class="fas fa-angle-double-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <img src="{{asset('icon/paperBlack.png')}}" alt="" class="black">
                                    <img src="{{asset('icon/paper.png')}}" alt="" class="white">
                                    stock management <i class="fas fa-angle-double-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <img src="{{asset('icon/newsBlack.png')}}" alt="" class="black">
                                    <img src="{{asset('icon/news.png')}}" alt="" class="white">
                                    reports <i class="fas fa-angle-double-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <img src="{{asset('icon/branchBlack.png')}}" alt="" class="black">
                                    <img src="{{asset('icon/branch.png')}}" alt="" class="white">
                                    multiple branch management <i class="fas fa-angle-double-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <img src="{{asset('icon/roleBlack.png')}}" alt="" class="black">
                                    <img src="{{asset('icon/role.png')}}" alt="" class="white">
                                    role and permission based <i class="fas fa-angle-double-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <img src="{{asset('icon/bookkeeperBlack.png')}}" alt="" class="black">
                                    <img src="{{asset('icon/bookkeeper.png')}}" alt="" class="white">
                                    account management <i class="fas fa-angle-double-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-8 ml-0 pl-0">
                        <div class="p-3 border">
                            <img src="{{asset('icon/magdotnet-banner-flat1.png')}}" class="img-fluid">
                            <div class="pt-3">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur cumque cupiditate eius
                                    error in quasi, veritatis. Ab accusantium amet ducimus eius est illo illum magni maiores
                                    necessitatibus nostrum officia placeat possimus, quia rerum sunt. Accusantium amet culpa
                                    excepturi fuga id impedit ipsam ipsum maiores omnis perspiciatis. Assumenda beatae commodi
                                    ex fugiat iste minima porro recusandae! Eum expedita fugit illo officia voluptas? Cumque
                                    doloribus maxime nobis odio temporibus ut vitae? Aliquam assumenda consequatur cum hic
                                    repellendus soluta! Animi beatae culpa cumque, dignissimos eaque eligendi eos error eum
                                    facere illum impedit magni minus molestiae mollitia officiis optio quisquam tempora tempore
                                    tenetur vero?</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A amet aperiam aspernatur at
                                    commodi, consequatur consequuntur corporis culpa debitis delectus ducimus eaque excepturi
                                    fugiat illum incidunt laudantium minima minus nemo neque nesciunt nobis nostrum obcaecati
                                    odit perferendis quae quia reiciendis rem repellendus, sapiente sed similique totam vero
                                    voluptates? Amet asperiores debitis eaque facilis nam. Accusantium assumenda atque debitis
                                    dignissimos dolorem dolores dolorum ipsum quod recusandae, repellat sapiente, similique!
                                    Accusantium aliquam animi aperiam consectetur doloribus eum exercitationem facere ipsa ipsum
                                    iure labore, mollitia natus neque nihil nisi nostrum obcaecati perferendis quibusdam quidem
                                    quo repellat sint temporibus tenetur ut vel velit voluptatibus!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci at beatae, cumque
                                    delectus dolor ea earum eligendi error eum ex fuga ipsam itaque iusto minus nobis numquam,
                                    officiis possimus rerum, saepe sed unde voluptas voluptatem? Magni quos, vel! Architecto
                                    exercitationem explicabo officiis repellat. Amet blanditiis debitis, dignissimos doloremque
                                    dolores incidunt minima nobis obcaecati, praesentium, quam qui quis quo soluta ullam
                                    voluptas! Adipisci aperiam culpa cum dolor eius illum libero, natus nemo, nihil nisi officia
                                    quia, sint veritatis? Accusantium blanditiis doloribus ducimus eius est quis sequi. Ad at
                                    eos, ex id itaque maiores, nam, natus officia qui quo quod suscipit.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white">
        @include('frontend.testimonial')
    </div>
@endsection
