@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Human Resources</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Salary Management</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Salary Management</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">Add Salary</button>
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head modal-dialog_add_salary" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">UPDATE SALARY MONTH</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body text-left salary_payroll master">
                            <div class="row">
                                <div class="col-md-12 mg-t-5">
                                    Month:
                                    <input type="text" style="float: right"  name="date" class="bod-pickers" placeholder="Select Date " id="month" >
{{--                                    <select class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">--}}
{{--                                        <option value="volvo">Guest</option>--}}
{{--                                        <option value="saab">Ranjan</option>--}}
{{--                                        <option value="opel">Manoj</option>--}}
{{--                                        <option value="audi">Pawan</option>--}}
{{--                                    </select>--}}
                                    <span>Note: Please Select The Month You Want To Update The Salary For. </span>
                                </div>
                            </div><!-- row -->
                            <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0">
                                <button  id="saveData1" class="btn btn-success mg-r-10">Update</button>
                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>




                        </div>

                    </div>
                </div>
            </div>

        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
                <div class="form-group">
                    <form action="{{route('salary-master.search')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row pd-l-0">
                    <div class="col-sm-2 pd-r-0">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="Select Branch">Select baranch</option>
                            @foreach($branch as $branches)
                                <option value="{{$branches->id}}">{{$branches->name}}</option>
                                {{--                                        <option value="opel">Manoj</option>--}}
                                {{--                                        <option value="audi">Pawan</option>--}}
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 pd-r-0">
{{--                        <input type="text" class="form-control form-control-sm" placeholder="Select Month">--}}
                        <input type="text" style="float: right" name="date" class="form-control form-control-sm bod-pickers" placeholder="Select Date " id="date" >
                    </div>
{{--                    <div class="col-sm-3 pd-r-0">--}}
{{--                        <input type="search" class="form-control form-control-sm" aria-controls="example2" placeholder="Search">--}}
{{--                    </div>--}}
                        <button class="btn btn-brand-02">search</button>
                    <div class="col-sm mail-navbar bd-b-0 justify-content-end mg-r-10" style="height: auto;">
                        <div class="d-none d-lg-flex">
                            <a href=""><i data-feather="printer"></i></a>
                            <a href=""><i data-feather="folder"></i></a>
                            <a href=""><i class="fas fa-download"></i></a>
                        </div>
                    </div>
                </div>
                    </form>

                </div> <!--form-group-->
            <div class="card">
                <table id="example1" class="table">
                    <thead>
                    <tr>
                        <th class="wd-5p"><input type="checkbox" aria-label="Checkbox for following text input"></th>
                        <th class="wd-10p">Employee Id</th>
                        <th class="wd-25p">Employee Name</th>
                        <th class="wd-20p">Designation</th>
                        <th class="wd-10p">Basic Salary</th>
                        <th class="wd-10p">Gross Salary</th>
                        <th class="wd-10p">Net Salary</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($monthwise as $key=> $monthwises)
                    <tr>
                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                        <td>{{++$key}}</td>
                        <td>{{$monthwises->employee->fullName}}</td>
                        <td>{{$monthwises->employee->designation}}</td>
                        <td>{{$monthwises->employee->salary}}</td>
                        <td>{{$monthwises->employee->salary + $monthwises->employee->salary_master[0]->addation}}</td>
                        <td>
                            @if($monthwises->employee->salary_master){{$monthwises->employee->salary_master[0]->netSalary}} @endif</td>
                        <td>
                            <button class="bg-warning bd rounded-5 text-primary saveData" data-toggle="modal" data-target="#exampleModalCenter1" data-id="{{$monthwises->employee->id}}">Update</button>
                            <div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head modal-dialog-management" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">UPDATE SALARY MANAGEMENT</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-left salary_mgmt">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="employee_detail">
                                                        <span id="name">Employee Name: Manoj Humagain</span>
                                                        <span>Employee ID: 1223224</span>
                                                        <span>Branch: Kathmandu</span>
                                                        <span>Month:Magh</span>
                                                        <span class="underline"> Attendance Report</span>
                                                        <small>Total Month Days: 30</small>
                                                        <small>Present Days: 26</small>
                                                        <small>Holidays: 4</small>
                                                        <small>Half Days: 0</small>
                                                        <small>Late: 0</small>
                                                        <small>Absent Days: 0</small>
                                                    </div><!-- employee_detail -->
                                                </div><!-- col -->




                                                <div class="col-md-9 border_wrap">
                                                    <div class="">
                                                        <div class="row">
                                                            <div class="col-md-4 ht-255">
                                                                <div class="mg-t-30">
                                                                    Basic Salary:
                                                                    <div class="input-group">

                                                                        <div class="input-group-prepend wd-25p">
                                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                                        </div>
                                                                        <input type="hidden" id="employee_id" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                        <input type="text" id="salary" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                    </div>
                                                                </div>
                                                                <div class="mg-t-5">
                                                                    Advance Due:
                                                                    <input id="advance_due" type="text" name="firstname" class="form-control form-control-sm wd-120" placeholder="" readonly="readonly" >
                                                                </div>
                                                                <div class="mg-t-5">
                                                                    Advance Deduction:
                                                                    <div class="input-group">

                                                                        <div class="input-group-prepend wd-25p">
                                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                                        </div>
                                                                        <input type="text" id="advance_deduction" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                    </div>
                                                                </div>
                                                                <div class="mg-t-5">
                                                                    Net Salary:
                                                                    <input type="text" id="net_salary" name="firstname" class="form-control form-control-sm wd-120" placeholder="" readonly="readonly">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 ht-255">
                                                                <label>Deduction</label>
                                                                <div class="">
                                                                    @foreach($salary_types as $key=> $salary_type)
                                                                    @if($salary_type->type == 'deduction')
                                                                    {{$salary_type->benificial}}:

                                                                            <div class="input-group">

                                                                                <div class="input-group-prepend wd-25p">
                                                                                    <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                                                </div>
                                                                                <input type="text" id="deductionn" class="form-control form-control-sm deduction" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                            </div>
                                                                    @endif

                                                                        @endforeach
                                                                </div>
{{--                                                                <div class="mg-t-5">--}}
{{--                                                                    Type 2:--}}
{{--                                                                    <div class="input-group">--}}

{{--                                                                        <div class="input-group-prepend wd-25p">--}}
{{--                                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>--}}
{{--                                                                        </div>--}}
{{--                                                                        <input type="text" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                                <div class="mg-t-5">--}}
{{--                                                                    Type 3:--}}
{{--                                                                    <div class="input-group">--}}

{{--                                                                        <div class="input-group-prepend wd-25p">--}}
{{--                                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>--}}
{{--                                                                        </div>--}}
{{--                                                                        <input type="text" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                                <div class="mg-t-5">--}}
{{--                                                                    Type 4:--}}
{{--                                                                    <div class="input-group">--}}

{{--                                                                        <div class="input-group-prepend wd-25p">--}}
{{--                                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>--}}
{{--                                                                        </div>--}}
{{--                                                                        <input type="text" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
                                                            </div><!-- col -->
                                                            <div class="col-md-4 ht-255">
                                                                <label>Addition</label>
                                                                <div class="">
                                                                    @foreach($salary_types as $key=> $salary_type)
                                                                    @if($salary_type->type=='addition')
                                                                    {{$salary_type->benificial}}
                                                                            <div class="input-group">

                                                                                <div class="input-group-prepend wd-25p">
                                                                                    <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                                                </div>
                                                                                <input type="text" id="addation" class="form-control form-control-sm addition" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                            </div>
                                                                    @endif
{{--                                                                    Type 1:--}}

                                                                        @endforeach
                                                                </div>
{{--                                                                <div class="mg-t-5">--}}
{{--                                                                    Type 2:--}}
{{--                                                                    <div class="input-group">--}}

{{--                                                                        <div class="input-group-prepend wd-25p">--}}
{{--                                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>--}}
{{--                                                                        </div>--}}
{{--                                                                        <input type="text" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                                <div class="mg-t-5">--}}
{{--                                                                    Type 3:--}}
{{--                                                                    <div class="input-group">--}}

{{--                                                                        <div class="input-group-prepend wd-25p">--}}
{{--                                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>--}}
{{--                                                                        </div>--}}
{{--                                                                        <input type="text" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                                <div class="mg-t-5">--}}
{{--                                                                    Type 4:--}}
{{--                                                                    <div class="input-group">--}}

{{--                                                                        <div class="input-group-prepend wd-25p">--}}
{{--                                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>--}}
{{--                                                                        </div>--}}
{{--                                                                        <input type="text" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
                                                            </div><!-- col -->
                                                        </div><!-- row -->
                                                        <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0">
                                                            <button id="saveData" class="btn btn-success mg-r-5p">Submit</button>
                                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                            <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                              <button type="button" class="btn btn-primary">Save changes</button> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- row -->





                                        </div>

                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
{{--                    <tr>--}}
{{--                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
{{--                        <td>000001</td>--}}
{{--                        <td>Employee Name</td>--}}
{{--                        <td>Superviser</td>--}}
{{--                        <td>20000</td>--}}
{{--                        <td>25000</td>--}}
{{--                        <td>23000</td>--}}
{{--                        <td>--}}
{{--                            <button class="bg-warning bd rounded-5 text-primary">Update</button>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
{{--                        <td>000001</td>--}}
{{--                        <td>Employee Name</td>--}}
{{--                        <td>sub-Manager</td>--}}
{{--                        <td>20000</td>--}}
{{--                        <td>25000</td>--}}
{{--                        <td>23000</td>--}}
{{--                        <td>--}}
{{--                            <button class="bg-warning bd rounded-5 text-primary">Update</button>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
                    </tbody>
                </table>
            </div>

        </div><!-- component-section -->

    </div><!-- content-body -->
{{--    <div class="content-footer">--}}
{{--        &copy; 2019. All Rights Reserved. Created by <a href="http://themepixels.me" target="_blank">ThemePixels</a>--}}
{{--    </div><!-- content-footer -->--}}
</div><!-- content -->




@endsection
@push('scripts')
    <script>


        $(".saveData").click(function(e){
            e.preventDefault()

            var employee_id = $(this).data('id');


            // alert(employee_id);
            // alert('test');

            $.ajax({
                type: 'POST',
                url: '{{url('/salary-master-edit')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    employee_id : employee_id,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // $('#show_modal').modal('hide');
                    // toastr.success(data.success);
                    // window.location.reload();
                    console.log(data);
                    var advance_due= $('#advance_due').val();

                    console.log(advance_due);

                    $('#salary').append($('#salary').val(data.salary));
                    $('#employee_id').append($('#employee_id').val(data.id));
                    $('#advance_duee').append($('#advance_due').val(data.advanceDue));
                    $('#name').append($('#name').val(data.fullName));

                    // var advance_deduction= (parseFloat(data.salary)- 0.0);
                    var advance_deduction= $('#advance_deduction').val();
                    // $('#advance_deduction').append($('#advance_deduction').val(advance_deduction));

                    // var net_salary= (parseFloat(data.salary)- parseFloat(advance_deduction));
                    // $('#net_salary').append($('#net_salary').val(net_salary));
                    $('#net_salary').append($('#net_salary').val(data.salary));
                    // $('#salary').append($('#salary').val(data.salary));

                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });

        $("#advance_deduction").on('input', function() {

            var advance_deduction= $('#advance_deduction').val();

            var salary= $('#salary').val();
            // console.log(addation);
            if(advance_deduction)
            {
                // alert('test');
                var net_salary= (parseFloat(salary) - parseFloat(advance_deduction));
            }
            else{
                // alert('sanrosh');
                var net_salary= (parseFloat(salary) - 0.0);

            }

            // var net_salary= (parseFloat(advance_deduction)-parseFloat(deduction)+parseFloat(addation));

            $('#net_salary').append($('#net_salary').val(net_salary));
        });


        $(".deduction").on('input', function() {

            var advance_deduction= $('#advance_deduction').val();
            var deduction= $('.deduction').val();
            var addation= $('.addition').val();
            var salary= $('#salary').val();
            // console.log(addation);

            if(advance_deduction){

                if(addation)
                {
                    // alert('test');
                    var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)-parseFloat(deduction)+parseFloat(addation));
                }
                else{
                    // alert('sanrosh');
                    var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)-parseFloat(deduction)+0.0);
                }

            }
            else{
                if(addation)
                {
                    // alert('test');
                    var net_salary= (parseFloat(salary) -parseFloat(deduction)+parseFloat(addation));
                }
                else{
                    // alert('sanrosh');
                    var net_salary= (parseFloat(salary) -parseFloat(deduction)+0.0);
                }
            }
            // if(addation)
            // {
            //     // alert('test');
            //     var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)-parseFloat(deduction)+parseFloat(addation));
            // }
            //    else{
            //     // alert('sanrosh');
            //     var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)-parseFloat(deduction)+0.0);
            // }

            // var net_salary= (parseFloat(advance_deduction)-parseFloat(deduction)+parseFloat(addation));

            $('#net_salary').append($('#net_salary').val(net_salary));
        });

        $(".addition").on('input',function () {

            var advance_deduction=$('#advance_deduction').val();
            var addation= $('.addition').val();
            var deduction= $('.deduction').val();
            var salary= $('#salary').val();
            // alert(addation);
            if(advance_deduction){

                if(deduction)
                {
                    // alert('test');
                    var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)-parseFloat(deduction)+parseFloat(addation));
                }
                else{
                    // alert('sanrosh');
                    var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)+parseFloat(addation)-0.0);
                }

            }
            else{
                if(deduction)
                {
                    // alert('test');
                    var net_salary= (parseFloat(salary) -parseFloat(deduction)+parseFloat(addation));
                }
                else{
                    // alert('sanrosh');
                    var net_salary= (parseFloat(salary) + parseFloat(addation) - 0.0);
                }
            }
            // var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)+parseFloat(addation)-parseFloat(deduction));
           // alert(net_salary);
            $('#net_salary').append($('#net_salary').val(net_salary));
        });

        $("#saveData").click(function(e){
            e.preventDefault()
            var basic_Salary = $('#salary').val();

            var employeeId  = $('#employee_id').val();
            // var employeeId = $(this).data('id');
            var advance_due =$('#advance_due').val();
            var advance_deduction =$('#advance_deduction').val();

            var net_salary =$('#net_salary').val();

            var deduction =$('#deductionn').val();

            var addation =$('#addation').val();





            // alert(employeeId);

            $.ajax({
                type: 'POST',
                url: '{{url('salary-master')}}',
                data: {
                    _token : "{{csrf_token()}}",

                   basic_salary : basic_Salary,
                    employee_id : employeeId,
                    advance_due : advance_due,


                   advance_deduction : advance_deduction,
                    net_salary : net_salary,
                    deduction : deduction,
                  addation : addation

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // $('#show_modal').modal('hide');
                    toastr.success(data.success);
                    // window.location.reload();
                    console.log('success');
                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    $('#category_error').append(err.errors.title);
                }
            });

        });

        $("#saveData1").click(function(e){
            e.preventDefault()


            var month=$('#month').val();





            // alert(employeeId);

            $.ajax({
                type: 'POST',
                url: '{{url('salary-master-month')}}',
                data: {
                    _token : "{{csrf_token()}}",

                    month : month,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // $('#show_modal').modal('hide');
                    toastr.success(data.success);
                    // window.location.reload();
                    console.log('success');
                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    $('#category_error').append(err.errors.title);
                }
            });

        });



        $(".bod-pickers").nepaliDatePicker({
            // dateFormat: "%D, %M %d, %y",

            dateFormat: "%y-%m",
            closeOnDateSelect: true

            // $('#input-id').val(getNepaliDate());
        });
        $("#clear-bth").on("click", function(event) {
            $(".bod-picker").val('');
        });
    </script>


@endpush
