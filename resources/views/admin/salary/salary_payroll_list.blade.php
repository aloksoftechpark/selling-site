@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Human Resources</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Salary Payroll</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">All Payment</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            @if(Auth::guard('admin')->check())
                 <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter15">New Payment</button>
            @else
            @can('salary-payroll-create')
                 <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter15">New Payment</button>
            @endcan
            @endif

            <div class="modal fade" id="exampleModalCenter15" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle15">ADD NEW EMPLOYEE PAYROLL</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body text-left salary_payroll">
{{--                            <form action="{{route('salary-payroll.store')}}" method="post" enctype="multipart/form-data">--}}
{{--                                @csrf--}}
{{--                            <form action="{{url('')}}" method="post" enctype="multipart/form-data">--}}
{{--                                @csrf--}}
{{--                                @method('put')--}}

                            <div class="row">
                                <div class="col-md-6">
                                    <input type="hidden" class="created_date bod-picker">
                                    Employee Name:
{{--                                    {{dd($employee)}}--}}
                                    <select class="form-control bd bd-gray-900" id="employee_id" name="employee_id" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <option value="saab">select employee</option>
                                        @foreach($employees as $employee)
{{--                                            --}}
                                        <option  value="{{$employee->id}}">{{$employee->fullName}}</option>
{{--                                            <input type="hidden" id="branch_id" value="@if(isset($employee->branch)){{$employee->branch->id}}@endif">--}}

                                        @endforeach
                                        @foreach($employees as $employee)
                                            {{--                                            --}}
{{--                                            <option  value="{{$employee->id}}">{{$employee->fullName}}</option>--}}
                                            <input type="hidden" id="branch_id" value="@if(isset($employee->branch)){{$employee->branch->id}}@endif">

                                        @endforeach
{{--                                        <option value="saab">Ranjan</option>--}}
{{--                                        <option value="opel">Manoj</option>--}}
{{--                                        <option value="audi">Pawan</option>--}}
                                    </select>
                                </div><!-- col -->
                                <div class="col-md-6">
                                    Total Due:
                                    <input type="text" id="total_due" name="firstname" class="form-control form-control-sm wd-120" placeholder=" "  readonly="readonly">
                                </div><!-- col -->
                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-6 mg-t-5">
                                    Employee ID:
{{--                                    <input type="text" class="form-control" placeholder="Barcode number" aria-label="Username" aria-describedby="basic-addon1">--}}
                                    <input type="text" name="employeeId" class="employeeId123 form-control {{ $errors->has('employeeId') ? 'has-error' : '' }}"  placeholder="Employee Id" readonly>
                                </div><!-- col -->
                                <div class="col-md-6 mg-t-5">
                                    Paying Amount:
                                    <div class="input-group">

                                        <div class="input-group-prepend wd-25p">
                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                        </div>
                                        <input id="paying_amount" type="text" name="paying_amount" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                    </div>
                                </div><!-- col -->
                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-6 mg-t-5">
                                    Advance Amount ( If Paying Advance ):
                                    <div class="input-group">

                                        <div class="input-group-prepend wd-25p">
                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                        </div>
                                        <input type="text" id="advance_amount" name="advance_amount" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                    </div>
                                </div>
                                <div class="col-md-6 mg-t-5">
                                    Remaining Due:
                                    <input  id="remaining_due" type="text" name="remaining_due" class="form-control form-control-sm wd-120" placeholder=" " readonly="readonly">
                                </div>
                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-6 mg-t-5">
                                    Payment Type:
                                    <select class="form-control bd bd-gray-900" id="payment_id" name="payment_method"  aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <option value=" ">Select payment method</option>
                                        @foreach($payment_method as $payment_method)
                                        <option value="{{$payment_method->id}}">{{$payment_method->title}}</option>
{{--                                        <option value="opel">Manoj</option>--}}
{{--                                        <option value="audi">Pawan</option>--}}
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 mg-t-5">
                                    Bank Name:
                                    <select class="form-control bd bd-gray-900" id="bank_name" name="bank_name" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <option value=" ">Select Bank Name</option>
                                        @foreach($bank as $bank)
                                        <option value="{{$bank->id}}">{{$bank->name}}</option>
{{--                                        <option value="opel">Manoj</option>--}}
{{--                                        <option value="audi">Pawan</option>--}}
                                            @endforeach
                                    </select>
                                </div>

                            </div><!-- row -->
                            <div class="modal-footer modal-footer_footer justify-content-center pd-b-0">
                                <button id="saveData" class="btn btn-success mg-r-10 " >Submit</button>
                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>
{{--                            </form>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <form action="{{route('salary-payroll.search')}}" method="post" enctype="multipart/form-data">
                    @csrf
{{--                <div class="row pd-l-0">--}}
{{--                    <div class="col-sm-2 pd-r-0">--}}
{{--                        <select class="form-control form-control-sm select2-no-search" >--}}
{{--                            <option label="Select Branch">Select baranch</option>--}}
{{--                            @foreach($branch as $branches)--}}
{{--                                <option value="{{$branches->id}}">{{$branches->name}}</option>--}}
{{--                                --}}{{--                                        <option value="opel">Manoj</option>--}}
{{--                                --}}{{--                                        <option value="audi">Pawan</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-3 pd-r-0">--}}
{{--                        <input type="search" id="branch_name" name="branch_name" class="form-control form-control-sm" aria-controls="example1" placeholder="Search">--}}
{{--                    </div>--}}
{{--                    <button class="btn btn-brand-02">search</button>--}}
                    <div class="form-group">
                        <div class="d-lg-flex pd-l-0">
                            <div class="pd-lg-r-20">
                                <select class="form-control form-control-sm select2-no-search">
                                    <option label="All Branch"></option>
                                    <option value="Firefox">Kathmandu</option>
                                    <option value="Chrome">Pokhhara</option>
                                    <option value="Opera">Butwal</option>
                                </select>
                            </div>
                            <div class="pd-lg-r-20">
                                <button class="btn btn-sm btn-light pd-5"><i class="fas fa-arrow-left"></i></button>
                                <label for="" class="btn btn-sm btn-light pd-5 mg-0">Today</label>
                                <button class="btn btn-sm btn-light pd-5"><i class="fas fa-arrow-right"></i></button>
                                <button class="btn btn-sm btn-light pd-5 mg-l-10"><i class="fas fa-arrow-left"></i></button>
                                <label for="" class="btn btn-sm btn-light pd-5 mg-0">This Month</label>
                                <button class="btn btn-sm btn-light pd-5"><i class="fas fa-arrow-right"></i></button>
                            </div>
                            <div class="pd-r-0">
                                <input type="text" class="form-control form-control-sm" placeholder="From">
                            </div>
                            <div class="pd-l-0 pd-r-20">
                                <input type="text" class="form-control form-control-sm" placeholder="TO">
                            </div>
                            <div class="pd-l-0 wd-20p">
                                <input type="search" class="form-control form-control-sm" placeholder="Search Employee">
                            </div>
                        </div>
                    </div> <!--form-group-->
{{--                    <div class="col-sm mail-navbar bd-b-0 justify-content-end mg-r-10" style="height: auto;">--}}
{{--                        <div class="d-none d-lg-flex">--}}
{{--                            @if(Auth::guard('admin')->check())--}}
{{--                                 <a href=""><i data-feather="printer"></i></a>--}}
{{--                            @else--}}
{{--                            @can('salary-payroll-print')--}}
{{--                                 <a href=""><i data-feather="printer"></i></a>--}}
{{--                            @endcan--}}
{{--                            @endif--}}

{{--                            <a href=""><i data-feather="folder"></i></a>--}}
{{--                            @if(Auth::guard('admin')->check())--}}
{{--                                 <a href=""><i class="fas fa-download"></i></a>--}}
{{--                            @else--}}
{{--                            @can('salary-payroll-download')--}}
{{--                                 <a href=""><i class="fas fa-download"></i></a>--}}
{{--                            @endcan--}}
{{--                            @endif--}}

{{--                        </div>--}}
{{--                    </div>--}}
                </div>
                </form>
            </div> <!--form-group-->
            <div class="card">
                <table id="example1" class="table  table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
{{--                        <th class="wd-5p"><input type="checkbox" aria-label="Checkbox for following text input"></th>--}}
                        <th class="wd-5p">SN</th>
                        <th class="wd-10p">Date</th>
                        <th class="wd-10p">Payment Id</th>
{{--                        <th class="wd-10p">Employee Id</th>--}}
                        <th class="wd-20p">Employee Name</th>
                        <th class="wd-10p">Paid Amount</th>
                        <th class="wd-10p">Due Amount</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($salary_payroll as $key=> $salary_payrolls)
                        @if($salary_payrolls->payingAmount)
                    <tr>

{{--                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
                        <td>{{++$key}}</td>
                        <td>{{$salary_payrolls->created_at}}</td>
                        <td>
{{--                            {{$salary_payrolls->paymentType}}--}}
                            {{$salary_payrolls->id}}
                        </td>
                        <td>{{$salary_payrolls->employee->fullName}}</td>
                        <td>
                            @if($salary_payrolls->payingAmount){{$salary_payrolls->payingAmount}}@endif
{{--                            @if($salary_payrolls->advanceAmount){{$salary_payrolls->advanceAmount}}@endif--}}
                        </td>
{{--                        <td> </td>--}}
                        <td>  {{$salary_payrolls->employee->salaryDue}}
{{--                        <td>  @if($salary_payrolls->advanceAmount){{$salary_payrolls->employee->salaryDue}}@endif--}}

                        </td>

                        <td class="d-md-flex">
{{--                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>--}}
{{--                            @if(Auth::guard('admin')->check())--}}
{{--                                 <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>--}}
{{--                            @else--}}
{{--                            @can('salary-payroll-edit')--}}
{{--                                 <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>--}}
{{--                            @endcan--}}
{{--                            @endif--}}

                            <input type="hidden"  id="test2" class="salary_payroll_id4321">
                            <input type="hidden" class="employee_id4321"  id="employee_id4321">
{{--                            <div class="mg-r-20" title="Delete"><a href=" " id="test" ><i class="icon ion-trash-b text-danger"></i></a></div>--}}
                            @if(Auth::guard('admin')->check())
                                 <div class="mg-r-20" title="Delete"><a href=" " class="payroll_edit" data-payroll_id="{{$salary_payrolls->id}}" data-toggle="modal"  data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                            @else
                            @can('salary-payroll-delete')
                                 <div class="mg-r-20" title="Delete"><a href=" " class="payroll_edit" data-payroll_id="{{$salary_payrolls->id}}" data-toggle="modal"  data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                            @endcan
                            @endif



                            {{--                            fot delete popup--}}

                            <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <div class="conform_del">
                                                <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                            </div><!-- conform_del -->

                                            <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                                <a class="btn btn-success mg-r-10" href=" " id="test" >Yes</a>
                                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div><!-- component-section -->

    </div><!-- content-body -->

</div><!-- content -->




@endsection
@push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>
        $(".payroll_edit").click(function(e){
            e.preventDefault()

            // var employeeId= $(this).val();
            var salary_payrollId = $(this).data('payroll_id');
            // var employee_id = $('#employee_id4321').val();




            // alert(employee_id);
            // alert(salary_payrollId);
            $.ajax({

                url: "{{url('/salary-payroll-edit2')}}",
                type: "POST",
                data: {
                    _token : "{{csrf_token()}}",
                    salary_payrollId : salary_payrollId
                },
                success: function (data) {
                    console.log(data);
                    // console.log(data.productById.sellingPrice);
                    $('.employee_id4321').append($('.employee_id4321').val(data.employee_id));
                    $('.salary_payroll_id4321').append($('.salary_payroll_id4321').val(data.id));

                },

                error: function (error) {

                    console.log('error');

                }


            });

        });
//for change status
        $("#test").click(function(e){
            e.preventDefault()

            var salary_payrollId = $('.salary_payroll_id4321').val();
            // var salary_payrollId = $(this).data('payroll_id');
            var employee_id = $('.employee_id4321').val();




            // alert(employee_id);
            // alert(salary_payrollId);

            $.ajax({
                type: 'POST',
                url: '{{url('change-status-payroll')}}',
                data: {
                    _token : "{{csrf_token()}}",


                    salary_payroll_id : salary_payrollId,
                    employee_id:employee_id,
                    status:0

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // $('#show_modal').modal('hide');
                    toastr.success(data.success);
                    window.location.reload();
                    console.log('success');
                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    $('#category_error').append(err.errors.title);
                }
            });

        });

{{--        $(".saveData").click(function(e){--}}
{{--            e.preventDefault()--}}

{{--            var employee_id = $(this).data('id');--}}

{{--            alert(employee_id);--}}
{{--            // alert('test');--}}

{{--            $.ajax({--}}
{{--                type: 'POST',--}}
{{--                url: '{{url('/salary-master-edit')}}',--}}
{{--                data: {--}}
{{--                    _token : "{{csrf_token()}}",--}}
{{--                    employee_id : employee_id,--}}

{{--                },--}}
{{--                success: function (data, status) {--}}

{{--                    if(data.error){--}}
{{--                        return;--}}
{{--                    }--}}

{{--                    // $('#show_modal').modal('hide');--}}
{{--                    // toastr.success(data.success);--}}
{{--                    // window.location.reload();--}}
{{--                    console.log(data);--}}

{{--                    $('#salary').append($('#salary').val(data.salary));--}}
{{--                    $('#name').append($('#name').val(data.fullName));--}}

{{--                },--}}
{{--                error: function (xhr, status, error) {--}}
{{--                    console.log('error');--}}
{{--                    // var err = JSON.parse(xhr.responseText);--}}
{{--                    // $('#category_error').append(err.errors.title);--}}
{{--                }--}}
{{--            });--}}

{{--        });--}}


        $("#paying_amount").on('input', function() {

            var total_due= $('#total_due').val();
            var paying_amount= $('#paying_amount').val();

            var remaining_due= (parseFloat(total_due)-parseFloat(paying_amount));

            $('#remaining_due').append($('#remaining_due').val(remaining_due));
        });


        $("#saveData").click(function(e){
            e.preventDefault()
            var advance_amount = $('#advance_amount').val();
            var employeeId = $('#employee_id').val();
            var payment_type =$('#payment_id').val();
            var bank_name =$('#bank_name').val();
            var branch_id =$('#branch_id').val();

            var total_due =$('#total_due').val();

            var paying_amount =$('#paying_amount').val();
            var created_date =$('.created_date').val();

            var remaining_due =$('#remaining_due').val();




            // alert(employeeId);

            $.ajax({
                type: 'POST',
                url: '{{url('salary-payroll')}}',
                data: {
                    _token : "{{csrf_token()}}",

                   advance_amount : advance_amount,
                    employee_id : employeeId,
                    payment_type : payment_type,
                    created_date : created_date,

                    bank_name : bank_name,
                    branch_id : branch_id,
                    total_due : total_due,
                    paying_amount : paying_amount,
                    remaining_due : remaining_due

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // $('#show_modal').modal('hide');
                    toastr.success(data.success);
                    window.location.reload();
                    console.log('success');
                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    $('#category_error').append(err.errors.title);
                }
            });

        });


$("select[name='employee_id']").change(function () {

    var employeeId= $(this).val();
    $.ajax({

        url: "{{url('/salary-payroll-edit')}}",
        type: "POST",
        data: {
            _token : "{{csrf_token()}}",
            employee_id : employeeId
        },
        success: function (data) {
            console.log(data);
            // console.log(data.productById.sellingPrice);
            $('#total_due').append($('#total_due').val(data.salaryDue));
            $('.employeeId123').append($('.employeeId123').val(data.employeeId));

        },

        error: function (error) {

            console.log('error');

        }


    });

});


$('#branch_name').on('keyup', function(){

    var brabch_name = $('#branch_name').val();

    $.ajax({

        type:"GET",
        url: '/search',
        data: {text: $('#branch_name').val()},
        success: function(data) {

            console.log(data);

        }



    });


});
    </script>



@endpush
