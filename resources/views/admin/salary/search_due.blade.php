@extends('admin.layouts.app')
@section('content')
{{--    <!DOCTYPE html>--}}
{{--<html lang="en">--}}
{{--<head>--}}

{{--    <!-- Required meta tags -->--}}
{{--    <meta charset="utf-8">--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">--}}

{{--    <!-- Meta -->--}}
{{--    <meta name="description" content="Responsive Bootstrap 4 Dashboard and Admin Template">--}}
{{--    <meta name="author" content="ThemePixels">--}}

{{--    <!-- Favicon -->--}}
{{--    <link rel="shortcut icon" type="image/x-icon" href="../assets/img/favicon.png">--}}

{{--    <title>Cassie Responsive Bootstrap 4 Dashboard and Admin Template</title>--}}

{{--    <!-- vendor css -->--}}
{{--    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">--}}
{{--    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">--}}
{{--    <link href="../lib/prismjs/themes/prism-tomorrow.css" rel="stylesheet">--}}
{{--    <link href="../lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">--}}
{{--    <link href="../lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">--}}
{{--    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">--}}

{{--    <!-- template css -->--}}
{{--    <link rel="stylesheet" href="../assets/css/cassie.css">--}}
{{--    <link rel="stylesheet" href="../assets/css/changes.css">--}}

{{--</head>--}}
{{--<body data-spy="scroll" data-target="#navSection" data-offset="100">--}}

{{--<div class="sidebar">--}}
{{--    <div class="sidebar-header">--}}
{{--        <div>--}}
{{--            <a href="../index.html" class="sidebar-logo"><span>cassie</span></a>--}}
{{--            <small class="sidebar-logo-headline">Responsive Dashboard Template</small>--}}
{{--        </div>--}}
{{--    </div><!-- sidebar-header -->--}}
{{--    <div id="dpSidebarBody" class="sidebar-body">--}}
{{--        <ul class="nav nav-sidebar">--}}
{{--            <li class="nav-label"><label class="content-label">Template Pages</label></li>--}}
{{--            <li class="nav-item show">--}}
{{--                <a href="../pages/dashboard-two.html" class="nav-link"><i data-feather="box"></i> Dashboard</a>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="layout"></i> Sales</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="app-newSales.html" class="nav-sub-link">New Sales</a>--}}
{{--                    <a href="app-allSales.html" class="nav-sub-link">All Sales</a>--}}
{{--                    <a href="#" class="nav-sub-link">Sales Return</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="lock"></i> Purchase</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="page-signin.html" class="nav-sub-link">New Purchase</a>--}}
{{--                    <a href="page-signup.html" class="nav-sub-link">All Purchase</a>--}}
{{--                    <a href="page-forgot.html" class="nav-sub-link">Purchase Return</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="user"></i> Customer</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="page-profile.html" class="nav-sub-link">New Customer</a>--}}
{{--                    <a href="page-timeline.html" class="nav-sub-link">All Customer</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="file-text"></i> Supplier</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="page-invoice.html" class="nav-sub-link">New Supplier</a>--}}
{{--                    <a href="page-pricing.html" class="nav-sub-link">All Supplier</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="x-circle"></i> Product and Services</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="page-404.html" class="nav-sub-link">Product</a>--}}
{{--                    <a href="page-404.html" class="nav-sub-link">Damage Product</a>--}}
{{--                    <a href="page-500.html" class="nav-sub-link">Catagories</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link"><i data-feather="layers"></i> Stock</a>--}}
{{--            </li>--}}
{{--            <li class="nav-label"><label class="content-label">Admin</label></li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="life-buoy"></i> Account</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/form-elements.html" class="nav-sub-link">Expense</a>--}}
{{--                    <a href="../components/form-input-group.html" class="nav-sub-link">Income</a>--}}
{{--                    <a href="../components/form-input-tags.html" class="nav-sub-link">Pay-in Receipt</a>--}}
{{--                    <a href="../components/form-input-masks.html" class="nav-sub-link">Pay-out Receipt</a>--}}
{{--                    <a href="../components/form-validation.html" class="nav-sub-link">Account Head</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="book"></i> Branch</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/con-grid.html" class="nav-sub-link">Branches</a>--}}
{{--                    <a href="../components/con-icons.html" class="nav-sub-link">Stock Transfer</a>--}}
{{--                    <a href="../components/con-images.html" class="nav-sub-link">Employee Transfer</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="layers"></i> Human Resource</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/com-accordion.html" class="nav-sub-link">Employee</a>--}}
{{--                    <a href="../components/com-alerts.html" class="nav-sub-link">Salary Payroll</a>--}}
{{--                    <a href="../components/com-avatar.html" class="nav-sub-link">Attendance</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="monitor"></i> Report</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/util-animation.html" class="nav-sub-link">Sales Report</a>--}}
{{--                    <a href="../components/util-background.html" class="nav-sub-link">Purchase Report</a>--}}
{{--                    <a href="../components/util-border.html" class="nav-sub-link">Expense Report</a>--}}
{{--                    <a href="../components/util-display.html" class="nav-sub-link">Income Report</a>--}}
{{--                    <a href="../components/util-divider.html" class="nav-sub-link">Payin Report</a>--}}
{{--                    <a href="../components/util-flex.html" class="nav-sub-link">Payout Report</a>--}}
{{--                    <a href="../components/util-height.html" class="nav-sub-link">Payment in/out</a>--}}
{{--                    <a href="../components/util-margin.html" class="nav-sub-link">Stock</a>--}}
{{--                    <a href="../components/util-padding.html" class="nav-sub-link">Customer Ledger</a>--}}
{{--                    <a href="../components/util-position.html" class="nav-sub-link">Supplier Ledger</a>--}}
{{--                    <a href="../components/util-typography.html" class="nav-sub-link">Account Ledger</a>--}}
{{--                    <a href="../components/util-width.html" class="nav-sub-link">Customer Due Report</a>--}}
{{--                    <a href="../components/util-extras.html" class="nav-sub-link">Supplier Due Report</a>--}}
{{--                    <a href="../components/util-extras.html" class="nav-sub-link">Profit/loss Report</a>--}}
{{--                    <a href="../components/util-extras.html" class="nav-sub-link">All Bills</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="pie-chart"></i> Configuration</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/chart-flot.html" class="nav-sub-link">General Setting</a>--}}
{{--                    <a href="../components/chart-chartjs.html" class="nav-sub-link">Payment Method</a>--}}
{{--                    <a href="../components/chart-peity.html" class="nav-sub-link">User</a>--}}
{{--                    <a href="../components/com-badge.html" class="nav-sub-link">Role and Premission</a>--}}
{{--                    <a href="../components/chart-peity.html" class="nav-sub-link">Bank</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--        </ul>--}}

{{--        <hr class="mg-t-30 mg-b-25">--}}

{{--        <ul class="nav nav-sidebar">--}}
{{--            <li class="nav-item"><a href="../pages/themes.html" class="nav-link"><i data-feather="aperture"></i> Themes</a></li>--}}
{{--            <li class="nav-item"><a href="../docs.html" class="nav-link"><i data-feather="help-circle"></i> Documentation</a></li>--}}
{{--        </ul>--}}


{{--    </div><!-- sidebar-body -->--}}
{{--</div><!-- sidebar -->--}}

{{--<div class="content">--}}
{{--    <div class="header">--}}
{{--        <div class="header-left">--}}
{{--            <a href="" class="burger-menu"><i data-feather="menu"></i></a>--}}

{{--            <div class="header-search">--}}
{{--                <i data-feather="search"></i>--}}
{{--                <input type="search" class="form-control" placeholder="What are you looking for?">--}}
{{--            </div><!-- header-search -->--}}
{{--        </div><!-- header-left -->--}}

{{--        <div class="header-right">--}}
{{--            <a href="" class="header-help-link"><i data-feather="help-circle"></i></a>--}}
{{--            <div class="dropdown dropdown-notification">--}}
{{--                <a href="" class="dropdown-link new" data-toggle="dropdown"><i data-feather="bell"></i></a>--}}
{{--                <div class="dropdown-menu dropdown-menu-right">--}}
{{--                    <div class="dropdown-menu-header">--}}
{{--                        <h6>Notifications</h6>--}}
{{--                        <a href=""><i data-feather="more-vertical"></i></a>--}}
{{--                    </div><!-- dropdown-menu-header -->--}}
{{--                    <div class="dropdown-menu-body">--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle text-primary bg-primary-light">s</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Socrates Itumay</strong> marked the task as completed.</p>--}}
{{--                                <span>5 hours ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-pink bg-pink-light">r</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Reynante Labares</strong> marked the task as incomplete.</p>--}}
{{--                                <span>8 hours ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-success bg-success-light">d</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Dyanne Aceron</strong> responded to your comment on this <strong>post</strong>.</p>--}}
{{--                                <span>a day ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-indigo bg-indigo-light">k</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Kirby Avendula</strong> marked the task as incomplete.</p>--}}
{{--                                <span>2 days ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div><!-- dropdown-menu-body -->--}}
{{--                    <div class="dropdown-menu-footer">--}}
{{--                        <a href="">View All Notifications</a>--}}
{{--                    </div>--}}
{{--                </div><!-- dropdown-menu -->--}}
{{--            </div>--}}
{{--            <div class="dropdown dropdown-loggeduser">--}}
{{--                <a href="" class="dropdown-link" data-toggle="dropdown">--}}
{{--                    <div class="avatar avatar-sm">--}}
{{--                        <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">--}}
{{--                    </div><!-- avatar -->--}}
{{--                </a>--}}
{{--                <div class="dropdown-menu dropdown-menu-right">--}}
{{--                    <div class="dropdown-menu-header">--}}
{{--                        <div class="media align-items-center">--}}
{{--                            <div class="avatar">--}}
{{--                                <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">--}}
{{--                            </div><!-- avatar -->--}}
{{--                            <div class="media-body mg-l-10">--}}
{{--                                <h6>Louise Kate Lumaad</h6>--}}
{{--                                <span>Administrator</span>--}}
{{--                            </div>--}}
{{--                        </div><!-- media -->--}}
{{--                    </div>--}}
{{--                    <div class="dropdown-menu-body">--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="user"></i> View Profile</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="edit-2"></i> Edit Profile</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="briefcase"></i> Account Settings</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="shield"></i> Privacy Settings</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="log-out"></i> Sign Out</a>--}}
{{--                    </div>--}}
{{--                </div><!-- dropdown-menu -->--}}
{{--            </div>--}}
{{--        </div><!-- header-right -->--}}
{{--    </div><!-- header -->--}}
    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Human Resources</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Salary Due</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Due Salary</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <form action="{{route('salary-due.search')}}" method="post" enctype="multipart/form-data">
                    @csrf
                <div class="row pd-l-0">
                    <div class="col-sm-2 pd-r-0">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="Select Branch">Select baranch</option>
                            @foreach($branch as $branches)
                                <option value="{{$branches->id}}">{{$branches->name}}</option>
                                {{--                                        <option value="opel">Manoj</option>--}}
                                {{--                                        <option value="audi">Pawan</option>--}}
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-3 pd-r-0">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                    <button class="btn btn-brand-02">search</button>
                    <div class="col-sm mail-navbar bd-b-0 justify-content-end mg-r-10" style="height: auto;">
                        <div class="d-none d-lg-flex">
                            <a href=""><i data-feather="printer"></i></a>
                            <a href=""><i data-feather="folder"></i></a>
                            <a href=""><i class="fas fa-download"></i></a>
                        </div>
                    </div>
                </div>
                </form>
            </div> <!--form-group-->
            <div class="card">
                <table id="example1" class="table">
                    <thead>
                    <tr>
                        <th class="wd-5p"><input type="checkbox" aria-label="Checkbox for following text input"></th>
                        <th class="wd-10p">Employee Id</th>
                        <th class="wd-20p">Employee Name</th>
                        <th class="wd-15p">Designation</th>
                        <th class="wd-10p">Advance</th>
                        <th class="wd-10p">Due Amount</th>
                        <th class="wd-10p">Balance</th>
                        <th class="wd-10p">Status</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employee1 as $key=> $employee)
                    <tr>
                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                        <td>{{++$key}}</td>
                        <td>{{$employee->fullName}}</td>
                        <td>{{$employee->designation}}</td>
                        <td>{{$employee->advanceDue}}</td>
                        <td>{{$employee->salaryDue}}</td>
                        <td>{{$employee->salaryDue - $employee->advanceDue}}</td>
                        <td>
                            @if($employee->salaryDUe == 0)

                            <span class="badge badge-pill badge-success wd-70">Active</span>

                            @else
                            <span class="badge badge-pill badge-danger wd-70">Deactive</span>
                            @endif
                        </td>
                        <td>
                            <button class="bg-success bd rounded-5 text-white"  data-toggle="modal" data-target="#exampleModalCenter">Pay Now</button>
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">ADD NEW EMPLOYEE PAYROLL</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-left salary_payroll">
{{--                                            <form action="{{route('salary-due.store')}}" method="post" enctype="multipart/form-data">--}}
{{--                                                @csrf--}}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    Employee Name:
                                                    <select class="form-control bd bd-gray-900" id="employee_id" name="employee_id" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                        <option value=" ">select employee</option>
                                                        @foreach($employees as $employee)
                                                            {{--                                            --}}
                                                            <option value="{{$employee->id}}">{{$employee->fullName}}</option>
                                                        @endforeach
                                                        @foreach($employees as $employee)
                                                            {{--                                            --}}
                                                            {{--                                            <option  value="{{$employee->id}}">{{$employee->fullName}}</option>--}}
                                                            <input type="hidden" id="branch_id" value="@if(isset($employee->branch)){{$employee->branch->id}}@endif">

                                                        @endforeach
                                                        {{--                                        <option value="saab">Ranjan</option>--}}
                                                        {{--                                        <option value="opel">Manoj</option>--}}
                                                        {{--                                        <option value="audi">Pawan</option>--}}
                                                    </select>
                                                </div><!-- col -->
                                                <div class="col-md-6">
                                                    Total Due:
                                                    <input id="total_due" type="text" name="total_due" class="form-control form-control-sm wd-120" placeholder=" "  readonly="readonly">
                                                </div><!-- col -->
                                            </div><!-- row -->
                                            <div class="row">
                                                <div class="col-md-6 mg-t-5">
                                                    Employee ID:
                                                    <input type="text" name="employeeId" class="form-control {{ $errors->has('employeeId') ? 'has-error' : '' }}" value="{{isset($employee1) ? $employee->employeeId+1 : 1 }}" placeholder="Employee Id" >
                                                </div><!-- col -->
                                                <div class="col-md-6 mg-t-5">
                                                    Paying Amount:
                                                    <div class="input-group">

                                                        <div class="input-group-prepend wd-25p">
                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                        </div>
                                                        <input id="paying_amount" type="text"  name="paying_amount" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                    </div>
                                                </div><!-- col -->
                                            </div><!-- row -->
                                            <div class="row">
                                                <div class="col-md-6 mg-t-5">
                                                    Payment Type:
                                                    <select class="form-control bd bd-gray-900" name="payment_method"  aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                        <option value="volvo">Select payment method</option>
                                                        @foreach($payment_method as $payment_method1)
                                                            <option value="{{$payment_method1->id}}">{{$payment_method1->title}}</option>
                                                            {{--                                        <option value="opel">Manoj</option>--}}
                                                            {{--                                        <option value="audi">Pawan</option>--}}
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6 mg-t-5">
                                                    Remaining Due:
                                                    <input id="remaining_due" type="text" name="remaining_due" class="form-control form-control-sm wd-120" placeholder=" " disabled="">
                                                </div>
                                            </div><!-- row -->
                                            <div class="row">
                                                <div class="col-md-6 mg-t-5">
                                                    Bank Name:
                                                    <select class="form-control bd bd-gray-900" name="bank_name" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                        <option value="volvo">Select Bank Name</option>
                                                        @foreach($bank as $bank1)
                                                            <option value="{{$bank1->id}}">{{$bank1->name}}</option>
                                                            {{--                                        <option value="opel">Manoj</option>--}}
                                                            {{--                                        <option value="audi">Pawan</option>--}}
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6 mg-t-5">
                                                    <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0">
                                                        <button id="saveData" class="btn btn-success mg-r-10" >Submit</button>
                                                        <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                        <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                          <button type="button" class="btn btn-primary">Save changes</button> -->
                                                    </div>
                                                </div>

                                            </div><!-- row -->
{{--                                            </form>--}}

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach

{{--                    @endforelse--}}
{{--                    <tr>--}}
{{--                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
{{--                        <td>000001</td>--}}
{{--                        <td>Employee Name</td>--}}
{{--                        <td>Manager</td>--}}
{{--                        <td>10,000</td>--}}
{{--                        <td>10,000</td>--}}
{{--                        <td>10,000</td>--}}
{{--                        <td>--}}
{{--                            <span class="badge badge-pill badge-danger wd-70">Deactive</span>--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            <button class="bg-success bd rounded-5 text-white">Pay Now</button>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
{{--                        <td>000001</td>--}}
{{--                        <td>Employee Name</td>--}}
{{--                        <td>Manager</td>--}}
{{--                        <td>10,000</td>--}}
{{--                        <td>10,000</td>--}}
{{--                        <td>10,000</td>--}}
{{--                        <td>--}}
{{--                            <span class="badge badge-pill badge-success wd-70">Active</span>--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            <button class="bg-success bd rounded-5 text-white">Pay Now</button>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
                    </tbody>
                </table>
            </div>

        </div><!-- component-section -->

    </div><!-- content-body -->
{{--    <div class="content-footer">--}}
{{--        &copy; 2019. All Rights Reserved. Created by <a href="http://themepixels.me" target="_blank">ThemePixels</a>--}}
{{--    </div><!-- content-footer -->--}}
</div><!-- content -->



{{--<script src="../lib/jquery/jquery.min.js"></script>--}}
{{--<script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>--}}
{{--<script src="../lib/feather-icons/feather.min.js"></script>--}}
{{--<script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>--}}
{{--<script src="../lib/prismjs/prism.js"></script>--}}
{{--<script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>--}}
{{--<script src="../lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>--}}
{{--<script src="../lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>--}}
{{--<script src="../lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>--}}
{{--<script src="../lib/select2/js/select2.min.js"></script>--}}
{{--<script src="../lib/js-cookie/js.cookie.js"></script>--}}

{{--<script src="../assets/js/cassie.js"></script>--}}

{{--</body>--}}
{{--</html>--}}


@endsection
@push('scripts')
    <script>


{{--        $(".saveData").click(function(e){--}}
{{--            e.preventDefault()--}}
{{--        --}}
{{--            var employee_id = $(this).data('id');--}}
{{--        --}}
{{--            // alert(employee_id);--}}
{{--            // alert('test');--}}
{{--        --}}
{{--            $.ajax({--}}
{{--                type: 'POST',--}}
{{--                url: '{{url('/salary-master-edit')}}',--}}
{{--                data: {--}}
{{--                    _token : "{{csrf_token()}}",--}}
{{--                    employee_id : employee_id,--}}
{{--        --}}
{{--                },--}}
{{--                success: function (data, status) {--}}
{{--        --}}
{{--                    if(data.error){--}}
{{--                        return;--}}
{{--                    }--}}
{{--        --}}
{{--                    // $('#show_modal').modal('hide');--}}
{{--                    // toastr.success(data.success);--}}
{{--                    // window.location.reload();--}}
{{--                    console.log(data);--}}
{{--        --}}
{{--                    $('#salary').append($('#salary').val(data.salary));--}}
{{--                    $('#name').append($('#name').val(data.fullName));--}}
{{--        --}}
{{--                },--}}
{{--                error: function (xhr, status, error) {--}}
{{--                    console.log('error');--}}
{{--                    // var err = JSON.parse(xhr.responseText);--}}
{{--                    // $('#category_error').append(err.errors.title);--}}
{{--                }--}}
{{--            });--}}
{{--        --}}
{{--        });--}}

        $("#paying_amount").on('input', function() {

            var total_due= $('#total_due').val();
            var paying_amount= $('#paying_amount').val();

            var remaining_due= (parseFloat(total_due)-parseFloat(paying_amount));

            $('#remaining_due').append($('#remaining_due').val(remaining_due));
        });



        $("select[name='employee_id']").change(function () {

            var employeeId= $(this).val();
            $.ajax({

                url: "{{url('/salary-payroll-edit')}}",
                type: "POST",
                data: {
                    _token : "{{csrf_token()}}",
                    employee_id : employeeId
                },
                success: function (data) {
                    console.log(data);
                    // console.log(data.productById.sellingPrice);
                    $('#total_due').append($('#total_due').val(data.salaryDue));

                },

                error: function (error) {

                    console.log('error');

                }


            });

        });


        $("#saveData").click(function(e){
            e.preventDefault()
            // var advance_amount = $('#advance_amount').val();
            var employeeId = $('#employee_id').val();
            var payment_type =$('#payment_id').val();
            var bank_name =$('#bank_name').val();
            var branch_id =$('#branch_id').val();

            var total_due =$('#total_due').val();

            var paying_amount =$('#paying_amount').val();

            var remaining_due =$('#remaining_due').val();




            // alert(employeeId);

            $.ajax({
                type: 'POST',
                url: '{{url('salary-payroll')}}',
                data: {
                    _token : "{{csrf_token()}}",

                    // advance_amount : advance_amount,
                    employee_id : employeeId,
                    payment_type : payment_type,

                    bank_name : bank_name,
                    branch_id : branch_id,
                    total_due : total_due,
                    paying_amount : paying_amount,
                    remaining_due : remaining_due

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // $('#show_modal').modal('hide');
                    toastr.success(data.success);
                    // window.location.reload();
                    console.log('success');
                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    $('#category_error').append(err.errors.title);
                }
            });

        });
    </script>

@endpush
