@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Human Resources</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Salary Due</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Due Salary</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <form action="{{route('salary-due.search')}}" method="post" enctype="multipart/form-data">
                    @csrf

{{--                <div class="row pd-l-0">--}}
{{--                    <div class="col-sm-2 pd-r-0">--}}
{{--                        <select class="form-control form-control-sm select2-no-search">--}}
{{--                            <option label="Select Branch">Select baranch</option>--}}
{{--                            @foreach($branch as $branches)--}}
{{--                                <option value="{{$branches->id}}">{{$branches->name}}</option>--}}
{{--                                --}}{{--                                        <option value="opel">Manoj</option>--}}
{{--                                --}}{{--                                        <option value="audi">Pawan</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-3 pd-r-0">--}}
{{--                        <input type="search" class="form-control form-control-sm" placeholder="Search">--}}
{{--                    </div>--}}
{{--                    <button class="btn btn-brand-02">search</button>--}}
                    <div class="d-lg-flex pd-l-0">
                        <div class="pd-lg-r-20">
                            <select class="form-control form-control-sm select2-no-search">
                                <option label="All Branch"></option>
                                <option value="Firefox">Kathmandu</option>
                                <option value="Chrome">Pokhhara</option>
                                <option value="Opera">Butwal</option>
                            </select>
                        </div>
                        <div class="pd-lg-r-20">
                            <select class="form-control form-control-sm select2-no-search">
                                <option label="Designation"></option>
                                <option value="Firefox">Manager</option>
                                <option value="Chrome">Seller</option>
                                <option value="Opera">Designer</option>
                            </select>
                        </div>
                        <div class="pd-lg-r-20">
                            <select class="form-control form-control-sm select2-no-search">
                                <option label="Status"></option>
                                <option value="Firefox">Active</option>
                                <option value="Chrome">Deactive</option>
                            </select>
                        </div>
                        <div class="pd-l-0 wd-20p">
                            <input type="search" class="form-control form-control-sm" placeholder="Search Employee">
                        </div>
                    </div>
            </div> <!--form-group-->
{{--                    <div class="col-sm mail-navbar bd-b-0 justify-content-end mg-r-10" style="height: auto;">--}}
{{--                        <div class="d-none d-lg-flex">--}}
{{--                            @if(Auth::guard('admin')->check())--}}
{{--                                 <a href=""><i data-feather="printer"></i></a>--}}
{{--                            @else--}}
{{--                            @can('salary-due-print')--}}
{{--                                 <a href=""><i data-feather="printer"></i></a>--}}
{{--                            @endcan--}}
{{--                            @endif--}}
{{--                            --}}
{{--                            <a href=""><i data-feather="folder"></i></a>--}}

{{--                            @if(Auth::guard('admin')->check())--}}
{{--                                 <a href=""><i class="fas fa-download"></i></a>--}}
{{--                            @else--}}
{{--                            @can('salary-due-download')--}}
{{--                                 <a href=""><i class="fas fa-download"></i></a>--}}
{{--                            @endcan--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
                </form>
            </div> <!--form-group-->
            <div class="card">
                <table id="example1" class="table  table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
{{--                        <th class="wd-5p"><input type="checkbox" aria-label="Checkbox for following text input"></th>--}}
                        <th class="wd-10p">Employee Id</th>
                        <th class="wd-20p">Employee Name</th>
                        <th class="wd-15p">Designation</th>
                        <th class="wd-10p">Advance</th>
                        <th class="wd-10p">Due Amount</th>
                        <th class="wd-10p">Balance</th>
                        <th class="wd-10p">Status</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employee1 as $key=> $employee)
                    <tr>
{{--                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
                        <td>{{++$key}}</td>
                        <td>{{$employee->fullName}}</td>
                        <td>{{$employee->designation}}</td>
                        <td>{{$employee->advanceDue}}</td>
                        <td>{{$employee->salaryDue}}</td>
                        <td>{{$employee->salaryDue - $employee->advanceDue}}</td>
                        <td>
                            @if($employee->salaryDUe == 0)

                            <span class="badge badge-pill badge-success wd-70">Active</span>

                            @else
                            <span class="badge badge-pill badge-danger wd-70">Deactive</span>
                            @endif
                        </td>
                        <td>
                            @if(Auth::guard('admin')->check())
                                 <button class="bg-success bd rounded-5 text-white"  data-toggle="modal" data-target="#exampleModalCenter">Pay Now</button>
                            @else
                            @can('salary-due-pay')
                                 <button class="bg-success bd rounded-5 text-white"  data-toggle="modal" data-target="#exampleModalCenter">Pay Now</button>
                            @endcan
                            @endif

                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">ADD NEW EMPLOYEE PAYROLL</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-left salary_payroll">
{{--                                            <form action="{{route('salary-due.store')}}" method="post" enctype="multipart/form-data">--}}
{{--                                                @csrf--}}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="hidden" class="created_date bod-picker">
                                                    Employee Name:
                                                    <select class="form-control bd bd-gray-900" id="employee_id" name="employee_id" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                        <option value=" ">select employee</option>
                                                        @foreach($employees as $employee)
                                                            {{--                                            --}}
                                                            <option value="{{$employee->id}}">{{$employee->fullName}}</option>
                                                        @endforeach
                                                        @foreach($employees as $employee)
                                                            {{--                                            --}}
                                                            {{--                                            <option  value="{{$employee->id}}">{{$employee->fullName}}</option>--}}
                                                            <input type="hidden" id="branch_id" value="@if(isset($employee->branch)){{$employee->branch->id}}@endif">

                                                        @endforeach
                                                        {{--                                        <option value="saab">Ranjan</option>--}}
                                                        {{--                                        <option value="opel">Manoj</option>--}}
                                                        {{--                                        <option value="audi">Pawan</option>--}}
                                                    </select>
                                                </div><!-- col -->
                                                <div class="col-md-6">
                                                    Total Due:
                                                    <input id="total_due" type="text" name="total_due" class="form-control form-control-sm wd-120" placeholder=" "  readonly="readonly">
                                                </div><!-- col -->
                                            </div><!-- row -->
                                            <div class="row">
                                                <div class="col-md-6 mg-t-5">
                                                    Employee ID:
                                                    <input type="text" name="employeeId" class="employeeId123 form-control {{ $errors->has('employeeId') ? 'has-error' : '' }}"  placeholder="Employee Id" readonly>
                                                </div><!-- col -->
                                                <div class="col-md-6 mg-t-5">
                                                    Paying Amount:
                                                    <div class="input-group">

                                                        <div class="input-group-prepend wd-25p">
                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                        </div>
                                                        <input id="paying_amount" type="text"  name="paying_amount" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                    </div>
                                                </div><!-- col -->
                                            </div><!-- row -->
                                            <div class="row">
                                                <div class="col-md-6 mg-t-5">
                                                    Payment Type:
                                                    <select class="form-control bd bd-gray-900" name="payment_method"  aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                        <option value="volvo">Select payment method</option>
                                                        @foreach($payment_method as $payment_method1)
                                                            <option value="{{$payment_method1->id}}">{{$payment_method1->title}}</option>
                                                            {{--                                        <option value="opel">Manoj</option>--}}
                                                            {{--                                        <option value="audi">Pawan</option>--}}
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6 mg-t-5">
                                                    Remaining Due:
                                                    <input id="remaining_due" type="text" name="remaining_due" class="form-control form-control-sm wd-120" placeholder=" " disabled="">
                                                </div>
                                            </div><!-- row -->
                                            <div class="row">
                                                <div class="col-md-6 mg-t-5">
                                                    Bank Name:
                                                    <select class="form-control bd bd-gray-900" name="bank_name" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                        <option value="volvo">Select Bank Name</option>
                                                        @foreach($bank as $bank1)
                                                            <option value="{{$bank1->id}}">{{$bank1->name}}</option>
                                                            {{--                                        <option value="opel">Manoj</option>--}}
                                                            {{--                                        <option value="audi">Pawan</option>--}}
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6 mg-t-5">
                                                    <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0">
                                                        <button id="saveData" class="btn btn-success mg-r-10" >Submit</button>
                                                        <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                        <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                          <button type="button" class="btn btn-primary">Save changes</button> -->
                                                    </div>
                                                </div>

                                            </div><!-- row -->
{{--                                            </form>--}}

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach

{{--                    @endforelse--}}
{{--                    <tr>--}}
{{--                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
{{--                        <td>000001</td>--}}
{{--                        <td>Employee Name</td>--}}
{{--                        <td>Manager</td>--}}
{{--                        <td>10,000</td>--}}
{{--                        <td>10,000</td>--}}
{{--                        <td>10,000</td>--}}
{{--                        <td>--}}
{{--                            <span class="badge badge-pill badge-danger wd-70">Deactive</span>--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            <button class="bg-success bd rounded-5 text-white">Pay Now</button>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
{{--                        <td>000001</td>--}}
{{--                        <td>Employee Name</td>--}}
{{--                        <td>Manager</td>--}}
{{--                        <td>10,000</td>--}}
{{--                        <td>10,000</td>--}}
{{--                        <td>10,000</td>--}}
{{--                        <td>--}}
{{--                            <span class="badge badge-pill badge-success wd-70">Active</span>--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            <button class="bg-success bd rounded-5 text-white">Pay Now</button>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
                    </tbody>
                </table>
            </div>

        </div><!-- component-section -->

    </div><!-- content-body -->

</div><!-- content -->





@endsection
@push('scripts')
    <script>


{{--        $(".saveData").click(function(e){--}}
{{--            e.preventDefault()--}}
{{--        --}}
{{--            var employee_id = $(this).data('id');--}}
{{--        --}}
{{--            // alert(employee_id);--}}
{{--            // alert('test');--}}
{{--        --}}
{{--            $.ajax({--}}
{{--                type: 'POST',--}}
{{--                url: '{{url('/salary-master-edit')}}',--}}
{{--                data: {--}}
{{--                    _token : "{{csrf_token()}}",--}}
{{--                    employee_id : employee_id,--}}
{{--        --}}
{{--                },--}}
{{--                success: function (data, status) {--}}
{{--        --}}
{{--                    if(data.error){--}}
{{--                        return;--}}
{{--                    }--}}
{{--        --}}
{{--                    // $('#show_modal').modal('hide');--}}
{{--                    // toastr.success(data.success);--}}
{{--                    // window.location.reload();--}}
{{--                    console.log(data);--}}
{{--        --}}
{{--                    $('#salary').append($('#salary').val(data.salary));--}}
{{--                    $('#name').append($('#name').val(data.fullName));--}}
{{--        --}}
{{--                },--}}
{{--                error: function (xhr, status, error) {--}}
{{--                    console.log('error');--}}
{{--                    // var err = JSON.parse(xhr.responseText);--}}
{{--                    // $('#category_error').append(err.errors.title);--}}
{{--                }--}}
{{--            });--}}
{{--        --}}
{{--        });--}}

        $("#paying_amount").on('input', function() {

            var total_due= $('#total_due').val();
            var paying_amount= $('#paying_amount').val();

            var remaining_due= (parseFloat(total_due)-parseFloat(paying_amount));

            $('#remaining_due').append($('#remaining_due').val(remaining_due));
        });



        $("select[name='employee_id']").change(function () {

            var employeeId= $(this).val();
            $.ajax({

                url: "{{url('/salary-payroll-edit')}}",
                type: "POST",
                data: {
                    _token : "{{csrf_token()}}",
                    employee_id : employeeId
                },
                success: function (data) {
                    console.log(data);
                    // console.log(data.productById.sellingPrice);
                    $('#total_due').append($('#total_due').val(data.salaryDue));
                    $('.employeeId123').append($('.employeeId123').val(data.employeeId));

                },

                error: function (error) {

                    console.log('error');

                }


            });

        });


        $("#saveData").click(function(e){
            e.preventDefault()
            // var advance_amount = $('#advance_amount').val();
            var employeeId = $('#employee_id').val();
            var payment_type =$('#payment_id').val();
            var bank_name =$('#bank_name').val();
            var branch_id =$('#branch_id').val();

            var total_due =$('#total_due').val();

            var paying_amount =$('#paying_amount').val();
            var created_date =$('.created_date').val();

            var remaining_due =$('#remaining_due').val();




            // alert(employeeId);

            $.ajax({
                type: 'POST',
                url: '{{url('salary-payroll')}}',
                data: {
                    _token : "{{csrf_token()}}",

                    // advance_amount : advance_amount,
                    employee_id : employeeId,
                    payment_type : payment_type,
                    created_date : created_date,

                    bank_name : bank_name,
                    branch_id : branch_id,
                    total_due : total_due,
                    paying_amount : paying_amount,
                    remaining_due : remaining_due

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // $('#show_modal').modal('hide');
                    toastr.success(data.success);
                    window.location.reload();
                    console.log('success');
                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    $('#category_error').append(err.errors.title);
                }
            });

        });
    </script>

@endpush
