@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Human Resources</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Salary Type Setup</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Salary Type</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            @if(Auth::guard('admin')->check())
                 <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">Add New</button>
            @else
            @can('salary-type-setup-create')
                 <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">Add New</button>
            @endcan
            @endif
            
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head salary_type" role="document">
                    <div class="modal-content">


                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle15">SALARY TYPE</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body text-left salary_payroll">
                            <form action="{{route('salary-type.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                            <div class="row">
                                <div class="col-md-6 mg-t-5">
                                    Benificial:
                                    <input type="text"  name="benificial" class="form-control" placeholder="Enter benificial like tax" aria-label="Username"  aria-describedby="basic-addon1">
                                </div>
                                <div class="col-md-6 mg-t-5">
                                    Type:
                                    <select class="form-control bd bd-gray-900" name="type" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <option value="addition">Addition</option>
                                        <option value="deduction">Deduction</option>
{{--                                        <option value="opel">Manoj</option>--}}
{{--                                        <option value="audi">Pawan</option>--}}
                                    </select>
                                </div>
                            </div><!-- row -->



                            <div class="modal-footer modal-footer_footer justify-content-center pd-b-0">
                                <button  class="btn btn-success mg-r-10">Update</button>
                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>
                        </form>
                        </div>


                    </div>
                </div>
            </div>
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-3 pd-r-0">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="Select Type"></option>
                            <option value="Firefox">Addition</option>
                            <option value="Chrome">Deduction</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                    <div class="col-sm mail-navbar bd-b-0 justify-content-end mg-r-10" style="height: auto;">
                        <div class="d-none d-lg-flex">
                            @if(Auth::guard('admin')->check())
                                 <a href=""><i data-feather="printer"></i></a>
                            @else
                            @can('salary-type-setup-print')
                                 <a href=""><i data-feather="printer"></i></a>
                            @endcan
                            @endif
                            
                            <a href=""><i data-feather="folder"></i></a>

                            @if(Auth::guard('admin')->check())
                                 <a href=""><i class="fas fa-download"></i></a>
                            @else
                            @can('salary-type-setup-download')
                                 <a href=""><i class="fas fa-download"></i></a>
                            @endcan
                            @endif
                            
                        </div>
                    </div>
                </div>
            </div> <!--form-group-->
            <div class="card">
                <table id="example1" class="table">
                    <thead>
                    <tr>
                        <th class="wd-5p">SN.</th>
                        <th class="wd-20p">Benificial</th>
                        <th class="wd-15p">Type</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($salary_type as $key=> $salary_type)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$salary_type->benificial}}</td>
                        <td>
                            @if($salary_type->type=='deduction')
                            <span class="badge badge-pill badge-warning wd-70"> Deduction</span>
                            @else
                            <span class="badge badge-pill badge-success wd-70">Addition</span>
                            @endif
                        </td>
                        <td class="d-md-flex">
{{--                            <div class="mg-r-20" title="View"><a href="../newPages/new-lead-detail.html"><i class="icon ion-clipboard text-success"></i></a></div>--}}
                            @if(Auth::guard('admin')->check())
                                 <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>
                            @else
                            @can('salary-type-setup-edit')
                                 <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>
                            @endcan
                            @endif

                            @if(Auth::guard('admin')->check())
                                 <div class="mg-r-20" title="Delete"><a href=" " data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                            @else
                            @can('salary-type-setup-delete')
                                 <div class="mg-r-20" title="Delete"><a href=" " data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                            @endcan
                            @endif
                            
{{--                            <div class="mg-r-20" title="Delete"><a href=""><i class="icon ion-trash-b text-danger"></i></a></div>--}}

                            


                            {{--                            fot delete popup--}}

                            <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <div class="conform_del">
                                                <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                            </div><!-- conform_del -->

                                            <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                                <a class="btn btn-success mg-r-10" href="{{route('salary-type.delete',$salary_type->id)}} ">Yes</a>
                                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div><!-- component-section -->

    </div><!-- content-body -->

</div><!-- content -->

@endsection
