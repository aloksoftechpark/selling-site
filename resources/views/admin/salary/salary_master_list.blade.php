@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Human Resources</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Salary Management</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Salary Management</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            {{-- <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">Add Salary Month</button> --}}
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head modal-dialog_add_salary" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">UPDATE SALARY MONTH</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body text-left salary_payroll master">
                            <div class="row">
                                <div class="col-md-12 mg-t-5">
                                    Month:
                                    <input type="text" style="float: right"  name="month" class="bod-pickers month" placeholder="Select Date " id="month">

                                    <span>Note: Please Select The Month You Want To Update The Salary For. </span>
                                </div>
                            </div><!-- row -->
                            <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0">
                                <button  id="saveData1" class="btn btn-success mg-r-10">Update</button>
                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>




                        </div>

                    </div>
                </div>
            </div>

        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
                <div class="form-group">
                    <form action="{{route('salary-master.search')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row pd-l-0">
                    <div class="col-sm-2 pd-r-0">
                        <select id="branch_name"  name="branch_id" class="form-control form-control-sm select2-no-search">
                            <option value="">Select branch</option>
                            @foreach($branch as $branches)
                                <option value="{{$branches->id}}" {{ (isset($selectedBranch) && $selectedBranch->id==$branches->id)?'selected': old('branch_id') }}>{{$branches->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 pd-r-0">
                        {{-- <input type="text" class="form-control form-control-sm" placeholder="Select Month"> --}}
                        {{-- <input type="text" style="float: right" name="date" class="form-control form-control-sm dod-pickers month" placeholder="Select Date " id="date" value="{{ (isset($date))? $date: old('date') }}"> --}}
                        <input type="hidden" name="academicYear" id="academicYear" value="{{$gloableAcademicYearSetup->academicYear}}">
                        <select class="form-control" name="month" id="salary-month">
                            <option value="">Select Month</option>
                            @if(isset($month) && $month)
                                <option value="{{$month}}"  selected="">{{$month}}</option>
                            @endif
                            <option value="BAISAKH">BAISAKH</option>
                            <option value="JESTHA">JESTHA</option>
                            <option value="ASHADH">ASHADH</option>
                            <option value="SAUN">SAUN</option>
                            <option value="BHADRA">BHADRA</option>
                            <option value="ASHOJ">ASHOJ</option>
                            <option value="KARTIK">KARTIK</option>
                            <option value="MANGSIR">MANGSIR</option>
                            <option value="PUSH">PUSH</option>
                            <option value="MAGH">MAGH</option>
                            <option value="FALGUN">FALGUN</option>
                            <option value="CHAITRA">CHAITRA</option>
                        </select>

                    </div>
                        <button class="btn btn-brand-02">search</button>

                    <div class="col-sm mail-navbar bd-b-0 justify-content-end mg-r-10" style="height: auto;">
                        <div class="d-none d-lg-flex">
                            @if(Auth::guard('admin')->check())
                                 <a href=""><i data-feather="printer"></i></a>
                            @else
                            @can('salary-master-create')
                                 <a href=""><i data-feather="printer"></i></a>
                            @endcan
                            @endif

                            <a href=""><i data-feather="folder"></i></a>

                            @if(Auth::guard('admin')->check())
                                 <a href=""><i class="fas fa-download"></i></a>
                            @else
                            @can('salary-master-download')
                                 <a href=""><i class="fas fa-download"></i></a>
                            @endcan
                            @endif

                        </div>
                    </div>
                </div>
                    </form>

                </div> <!--form-group-->
            <div class="card">
                <table id="example1" class="table">
                    <thead>
                    <tr>
                        <th class="wd-5p"><input type="checkbox" aria-label="Checkbox for following text input"></th>
                        <th class="wd-10p">Employee Id</th>
                        <th class="wd-25p">Employee Name</th>
                        <th class="wd-20p">Designation</th>
                        <th class="wd-10p">Basic Salary</th>
                        <th class="wd-10p">Gross Salary</th>
                        <th class="wd-10p">Net Salary</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        {{-- {{dd($salary_masters)}} --}}
                    @if(!$salary_masters->isEmpty())
                    @foreach($salary_masters as $key=>$master)
                    <tr>
                        {{-- fdsankl --}}
                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                        <td>{{++$key}}</td>
                        <td>@if($master->employee){{$master->employee->fullName}}@endif</td>
                        <td>@if($master->employee){{$master->employee->designation}}@endif</td>
                        <td>@if($master->employee){{$master->employee->salary}}@endif</td>
                        <td>@if($master->employee){{$master->employee->salary + $master->addation}}@endif</td>
                        <td>@if($master->employee){{$master->netSalary}}@endif </td>

                        <td>
                            @if(Auth::guard('admin')->check())
                                 <button class="bg-warning bd rounded-5 text-primary saveData" data-toggle="modal" data-target="#exampleModalCenter1" data-id="@if($master->employee){{$master->employee->id}}@endif" data-salary-id="{{$master->id}}">Update</button>
                            @else
                            @can('salary-master-edit')
                                 <button class="bg-warning bd rounded-5 text-primary saveData" data-toggle="modal" data-target="#exampleModalCenter1" data-id="@if($master->employee){{$master->employee->id}}@endif" data-salary-id="{{$master->id}}">Update</button>
                            @endcan
                            @endif

                            <div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head modal-dialog-management" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">UPDATE SALARY MANAGEMENT</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-left salary_mgmt">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="employee_detail">
                                                    <ul>
                                                        <li>  <span style="display: inline;">Employee Name:</span><span class="employee_name"></span></li>
                                                        <li>  <span>Employee ID: </span><span class="employeeId"></span></li>
                                                        <li>   <span>Branch: </span><span class="branch"></span></li>
                                                        <li>   <span>Month:</span><span class="month"></span></li>
                                                        <li>        <span class="underline"> Attendance Report</span><span class="attendance"></span></li>
                                                        <li> <small>Total Month Days: </small><small class="totalMonthDay"></small></li>
                                                        <li>   <small>Present Days: </small><small class="presentDay"></small></li>
                                                        <li> <small>Holidays: </small><small class="holiday"></small></li>
                                                        <li> <small>Half Days: </small><small class="halfDay"></small></li>
                                                        <li> <small>Late: </small><small class="lateDay"></small></li>
                                                        <li> <small>Absent Days: </small><small class="absentDay"></small></li>

                                                    </ul>

                                                    </div><!-- employee_detail -->
                                                </div><!-- col -->





                                                <div class="col-md-9 border_wrap">
                                                    <div class="">
                                                        <div class="row">
                                                            <div class="col-md-4 ht-255">
                                                                <div class="mg-t-30">
                                                                    Basic Salary:
                                                                    <div class="input-group">

                                                                        <div class="input-group-prepend wd-25p">
                                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                                        </div>
                                                                        <input type="hidden" name="" id="masterId">
                                                                        <input type="hidden" name="" id="selectedMonth" value="{{$gloableAcademicYearSetup->academicYear}}">
                                                                        <input type="hidden" id="employee_id" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                        <input type="text" id="salary" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                    </div>
                                                                </div>
                                                                <div class="mg-t-5">
                                                                    Advance Due:
                                                                    <input id="advance_due" type="text" name="firstname" class="form-control form-control-sm wd-120" placeholder="" readonly="readonly" >
                                                                </div>
                                                                <div class="mg-t-5">
                                                                    Advance Deduction:
                                                                    <div class="input-group">

                                                                        <div class="input-group-prepend wd-25p">
                                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                                        </div>
                                                                        <input type="text" id="advance_deduction" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                    </div>
                                                                </div>
                                                                <div class="mg-t-5">
                                                                    Net Salary:
                                                                    <input type="text" id="net_salary" name="firstname" class="form-control form-control-sm wd-120" placeholder="" readonly="readonly">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 ht-255">
                                                                <label>Deduction</label>
                                                                <div class="">
                                                                    @foreach($salary_types as $key=> $salary_type)
                                                                    @if($salary_type->type == 'deduction')
                                                                    {{$salary_type->benificial}}:

                                                                            <div class="input-group">

                                                                                <div class="input-group-prepend wd-25p">
                                                                                    <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                                                </div>
                                                                                <input type="text" id="deductionn" class="form-control form-control-sm deduction" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                            </div>
                                                                    @endif

                                                                        @endforeach

                                                                </div>

                                                            </div><!-- col -->
                                                            <div class="col-md-4 ht-255">
                                                                <label>Addition</label>
                                                                <div class="">
                                                                    @foreach($salary_types as $key=> $salary_type)
                                                                    @if($salary_type->type=='addition')
                                                                    {{$salary_type->benificial}}
                                                                            <div class="input-group">

                                                                                <div class="input-group-prepend wd-25p">
                                                                                    <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                                                </div>
                                                                                <input type="text" id="addation" class="form-control form-control-sm addition" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                            </div>
                                                                    @endif

                                                                        @endforeach
                                                                </div>

                                                            </div><!-- col -->
                                                        </div><!-- row -->
                                                        <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0">
                                                            <button id="saveData" class="btn btn-success mg-r-5p">Submit</button>
                                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- row -->

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>

                    @endforeach


                    @else

                    @foreach($employees as $key=> $employee)

                    <tr>
                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                        <td>{{++$key}}</td>
                        <td>{{$employee->fullName}}</td>
                        <td>{{$employee->designation}}</td>
                        <td>{{$employee->salary}}</td>
                        <td></td>
                        <td>
                          </td>

                        <td>
                            @if(Auth::guard('admin')->check())
                                 <button class="bg-warning bd rounded-5 text-primary saveData addData" data-toggle="modal" data-target="#exampleModalCenter1" data-id="{{$employee->id}}">Update</button>
                            @else
                            @can('salary-payroll-edit')
                                 <button class="bg-warning bd rounded-5 text-primary saveData addData" data-toggle="modal" data-target="#exampleModalCenter1" data-id="{{$employee->id}}">Update</button>
                            @endcan
                            @endif

                            <div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head modal-dialog-management" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">UPDATE SALARY MANAGEMENT</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-left salary_mgmt">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="employee_detail">
                                                    <ul>
                                                        <li>  <span style="display: inline;">Employee Name:</span><span class="employee_name"></span></li>
                                                        <li>  <span>Employee ID: </span><span class="employeeId"></span></li>
                                                        <li>   <span>Branch: </span><span class="branch"></span></li>
                                                        <li>   <span>Month:</span><span class="month"></span></li>
                                                        <li>        <span class="underline"> Attendance Report</span><span class="attendance"></span></li>
                                                        <li> <small>Total Month Days: </small><small class="totalMonthDay"></small></li>
                                                        <li>   <small>Present Days: </small><small class="presentDay"></small></li>
                                                        <li> <small>Holidays: </small><small class="holiday"></small></li>
                                                        <li> <small>Half Days: </small><small class="halfDay"></small></li>
                                                        <li> <small>Late: </small><small class="lateDay"></small></li>
                                                        <li> <small>Absent Days: </small><small class="absentDay"></small></li>

                                                    </ul>

                                                    </div><!-- employee_detail -->
                                                </div><!-- col -->




                                                <div class="col-md-9 border_wrap">
                                                    <div class="">
                                                        <div class="row">
                                                            <div class="col-md-4 ht-255">
                                                                <div class="mg-t-30">
                                                                    Basic Salary:
                                                                    <div class="input-group">

                                                                        <div class="input-group-prepend wd-25p">
                                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                                        </div>
                                                                        <input type="hidden" id="employee_id" class="form-control form-control-sm employee_id" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                        <input type="hidden" name="" id="branch_id">
                                                                        <input type="hidden" name="" id="changeMonth">
                                                                        <input type="hidden" name="" id="selectedMonth" value="{{$gloableAcademicYearSetup->academicYear}}">
                                                                        <input type="text" id="salary" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                    </div>
                                                                </div>
                                                                <div class="mg-t-5">
                                                                    Advance Due:
                                                                    <input id="advance_due" type="text" name="firstname" class="form-control form-control-sm wd-120" placeholder="" readonly="readonly" >
                                                                </div>
                                                                <div class="mg-t-5">
                                                                    Advance Deduction:
                                                                    <div class="input-group">

                                                                        <div class="input-group-prepend wd-25p">
                                                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                                        </div>
                                                                        <input type="text" id="advance_deduction" class="form-control form-control-sm" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                    </div>
                                                                </div>
                                                                <div class="mg-t-5">
                                                                    Net Salary:
                                                                    <input type="text" id="net_salary" name="firstname" class="form-control form-control-sm wd-120" placeholder="" readonly="readonly">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 ht-255">
                                                                <label>Deduction</label>
                                                                <div class="">
                                                                    @foreach($salary_types as $key=> $salary_type)
                                                                    @if($salary_type->type == 'deduction')
                                                                    {{$salary_type->benificial}}:

                                                                            <div class="input-group">

                                                                                <div class="input-group-prepend wd-25p">
                                                                                    <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                                                </div>
                                                                                <input type="text" id="deductionn" class="form-control form-control-sm deduction" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                            </div>
                                                                    @endif

                                                                        @endforeach

                                                                </div>

                                                            </div><!-- col -->
                                                            <div class="col-md-4 ht-255">
                                                                <label>Addition</label>
                                                                <div class="">
                                                                    @foreach($salary_types as $key=> $salary_type)
                                                                    @if($salary_type->type=='addition')
                                                                    {{$salary_type->benificial}}
                                                                            <div class="input-group">

                                                                                <div class="input-group-prepend wd-25p">
                                                                                    <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                                                </div>
                                                                                <input type="text" id="addation" class="form-control form-control-sm addition" placeholder="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                            </div>
                                                                    @endif

                                                                        @endforeach
                                                                </div>

                                                            </div><!-- col -->
                                                        </div><!-- row -->
                                                        <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0">
                                                            <button {{-- id="saveData" --}}  class="btn btn-success mg-r-5p addDataToSalaryMaster">Submit</button>
                                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                            <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                              <button type="button" class="btn btn-primary">Save changes</button> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- row -->
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>

                    @endforeach
                    @endif
</div><!-- content -->




@endsection
@push('scripts')
    <script>


        $(".saveData").click(function(e){
            e.preventDefault()

            var employee_id = $(this).data('id');
            var masterId = $(this).data('salary-id');
            var month = $('#month').val();
            var academicYear = $('#academicYear').val();
            // alert(employee_id);


            $.ajax({
                type: 'POST',
                url: '{{url('/salary-master-edit')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    employee_id : employee_id,
                    month : month,
                    masterId : masterId,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // $('#show_modal').modal('hide');
                    // toastr.success(data.success);
                    // window.location.reload();
                    // console.log(data);
                    var advance_due= $('#advance_due').val();

                    // console.log(data.salary_master.salary);


                    $('#salary').append($('#salary').val(data.salary_master.salary));
                    $('#employee_id').append($('#employee_id').val(data.salary_master.id));
                    $('.employee_name').empty().html(data.salary_master.fullName);
                    $('.employeeId').empty().html(data.salary_master.id);
                    $('.branch').empty().html(data.salary_master.branch_id);
                    $('#branch_id').append($('#branch_id').val(data.salary_master.branch_id));
                    $('.month').empty().html(data.month);
                    $('#changeMonth').append($('#changeMonth').val(data.month));
                    $('#masterId').append($('#masterId').val(data.masterId));
                    $('.attendance').empty().html(data.fullName);
                    $('.totalMonthDay').empty().html(data.fullName);
                    $('.presentDay').empty().html(data.fullName);
                    $('.holiday').empty().html(data.fullName);
                    $('.halfDay').empty().html(data.fullName);
                    $('.lateDay').empty().html(data.fullName);
                    $('.absentDay').empty().html(data.fullName);
                    $('#advance_duee').append($('#advance_due').val(data.salary_master.advanceDue));
                    $('#name').append($('#name').val(data.fullName));
                    var advance_deduction= $('#advance_deduction').val();
                    $('#net_salary').append($('#net_salary').val(data.salary_master.salary));

                },
                error: function (xhr, status, error) {
                    console.log('error');
                }
            });

        });

        $("#advance_deduction").on('input', function() {

            var advance_deduction= $('#advance_deduction').val();

            var salary= $('#salary').val();
            if(advance_deduction)
            {
                var net_salary= (parseFloat(salary) - parseFloat(advance_deduction));
            }
            else{
                var net_salary= (parseFloat(salary) - 0.0);
            }

            $('#net_salary').append($('#net_salary').val(net_salary));
        });


        $(".deduction").on('input', function() {

            var advance_deduction= $('#advance_deduction').val();
            var deduction= $('.deduction').val();
            var addation= $('.addition').val();
            var salary= $('#salary').val();
            // console.log(addation);

            if(advance_deduction){

                if(addation)
                {
                    // alert('test');
                    var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)-parseFloat(deduction)+parseFloat(addation));
                }
                else{
                    // alert('sanrosh');
                    var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)-parseFloat(deduction)+0.0);
                }

            }
            else{
                if(addation)
                {
                    // alert('test');
                    var net_salary= (parseFloat(salary) -parseFloat(deduction)+parseFloat(addation));
                }
                else{
                    // alert('sanrosh');
                    var net_salary= (parseFloat(salary) -parseFloat(deduction)+0.0);
                }
            }
            // if(addation)
            // {
            //     // alert('test');
            //     var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)-parseFloat(deduction)+parseFloat(addation));
            // }
            //    else{
            //     // alert('sanrosh');
            //     var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)-parseFloat(deduction)+0.0);
            // }

            // var net_salary= (parseFloat(advance_deduction)-parseFloat(deduction)+parseFloat(addation));

            $('#net_salary').append($('#net_salary').val(net_salary));
        });

        $(".addition").on('input',function () {

            var advance_deduction=$('#advance_deduction').val();
            var addation= $('.addition').val();
            var deduction= $('.deduction').val();
            var salary= $('#salary').val();
            // alert(addation);
            if(advance_deduction){

                if(deduction)
                {
                    // alert('test');
                    var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)-parseFloat(deduction)+parseFloat(addation));
                }
                else{
                    // alert('sanrosh');
                    var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)+parseFloat(addation)-0.0);
                }

            }
            else{
                if(deduction)
                {
                    // alert('test');
                    var net_salary= (parseFloat(salary) -parseFloat(deduction)+parseFloat(addation));
                }
                else{
                    // alert('sanrosh');
                    var net_salary= (parseFloat(salary) + parseFloat(addation) - 0.0);
                }
            }
            // var net_salary= (parseFloat(salary) - parseFloat(advance_deduction)+parseFloat(addation)-parseFloat(deduction));
           // alert(net_salary);
            $('#net_salary').append($('#net_salary').val(net_salary));
        });

        $("#saveData").click(function(e){
            e.preventDefault()
            var basic_Salary = $('#salary').val();

            var employeeId  = $('#employee_id').val();
            // var employeeId = $(this).data('id');
            var advance_due =$('#advance_due').val();
            var advance_deduction =$('#advance_deduction').val();

            var net_salary =$('#net_salary').val();

            var deduction =$('#deductionn').val();

            var addation =$('#addation').val();

            var salaryMasterId = $('#masterId').val();







            // alert(employeeId);

            $.ajax({
                type: 'POST',
                {{-- url: '{{url('salary-master')}}', --}}
                url: '{{url('salary-master-update')}}',
                data: {
                    _token : "{{csrf_token()}}",

                   basic_salary : basic_Salary,
                    employee_id : employeeId,
                    advance_due : advance_due,


                   advance_deduction : advance_deduction,
                    net_salary : net_salary,
                    deduction : deduction,
                  addation : addation,
                  salaryMasterId : salaryMasterId,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // $('#show_modal').modal('hide');
                    toastr.success(data.success);
                    window.location.reload();
                    console.log('success');
                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    $('#category_error').append(err.errors.title);
                }
            });

        });

        $("#saveData1").click(function(e){
            e.preventDefault()


            var month=$('#month').val();





            // alert(employeeId);

            $.ajax({
                type: 'POST',
                url: '{{url('salary-master-month')}}',
                data: {
                    _token : "{{csrf_token()}}",

                    month : month,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // $('#show_modal').modal('hide');
                    toastr.success(data.success);
                    // window.location.reload();
                    console.log('success');
                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    $('#category_error').append(err.errors.title);
                }
            });

        });



        $(".dod-pickers").nepaliDatePicker({
            // dateFormat: "%D, %M %d, %y",
            dateFormat: " %M %y",


            // dateFormat: "%y-%m",
            closeOnDateSelect: true

            // $('#input-id').val(getNepaliDate());
        });
        // $("#clear-bth").on("click", function(event) {
        //     $(".dod-picker").val('');
        // });
    </script>


@endpush

@push('scripts')
    <script type="text/javascript">
        $(".addDataToSalaryMaster").click(function(e){
            e.preventDefault();
            var basic_Salary = $('#salary').val();

            var employeeId  = $('#employee_id').val();
            // alert(employeeId);
            // var employeeId = $(this).data('id');
            var advance_due =$('#advance_due').val();
            var advance_deduction =$('#advance_deduction').val();

            var net_salary =$('#net_salary').val();

            var deduction =$('#deductionn').val();

            var addation =$('#addation').val();
            var branch_id =$('#branch_id').val();
            var month =$('#salary-month').val();
            var academicYear =$('#academicYear').val();





            // alert(employeeId);

            $.ajax({
                type: 'POST',
                url: '{{url('store_salary_master')}}',
                data: {
                    _token : "{{csrf_token()}}",

                   basic_salary : basic_Salary,
                    employee_id : employeeId,
                    advance_due : advance_due,

                    advance_deduction : advance_deduction,
                    net_salary : net_salary,
                    deduction : deduction,
                    addation : addation,
                    branch_id : branch_id,
                    month : month,
                    academicYear : academicYear,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // $('#show_modal').modal('hide');
                    toastr.success(data.success);
                    window.location.reload();
                    // console.log('success');
                },
                error: function (xhr, status, error) {
                    // console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    toastr.error(err.errors.month);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });
    </script>
@endpush
