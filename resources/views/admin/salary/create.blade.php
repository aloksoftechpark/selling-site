@extends('admin.layouts.app')
@section('content')


    {{--<!DOCTYPE html>--}}
{{--<html lang="en">--}}
{{--<head>--}}

{{--    <!-- Required meta tags -->--}}
{{--    <meta charset="utf-8">--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">--}}

{{--    <!-- Meta -->--}}
{{--    <meta name="description" content="Responsive Bootstrap 4 Dashboard and Admin Template">--}}
{{--    <meta name="author" content="ThemePixels">--}}

{{--    <!-- Favicon -->--}}
{{--    <link rel="shortcut icon" type="image/x-icon" href="../assets/img/favicon.png">--}}

{{--    <title>Cassie Responsive Bootstrap 4 Dashboard and Admin Template</title>--}}

{{--    <!-- vendor css -->--}}
{{--    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">--}}
{{--    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">--}}
{{--    <link href="../lib/fullcalendar/fullcalendar.min.css" rel="stylesheet">--}}
{{--    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">--}}

{{--    <!-- template css -->--}}
{{--    <link rel="stylesheet" href="../assets/css/cassie.css">--}}
{{--    <link rel="stylesheet" href="../assets/css/changes.css">--}}

{{--</head>--}}
{{--<body>--}}

{{--<div class="sidebar">--}}
{{--    <div class="sidebar-header">--}}
{{--        <div>--}}
{{--            <a href="../index.html" class="sidebar-logo"><span>cassie</span></a>--}}
{{--            <small class="sidebar-logo-headline">Responsive Dashboard Template</small>--}}
{{--        </div>--}}
{{--    </div><!-- sidebar-header -->--}}
{{--    <div id="dpSidebarBody" class="sidebar-body">--}}
{{--        <ul class="nav nav-sidebar">--}}
{{--            <li class="nav-label"><label class="content-label">Template Pages</label></li>--}}
{{--            <li class="nav-item show">--}}
{{--                <a href="../pages/dashboard-two.html" class="nav-link"><i data-feather="box"></i> Dashboard</a>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="layout"></i> Sales</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="app-newSales.html" class="nav-sub-link">New Sales</a>--}}
{{--                    <a href="app-allSales.html" class="nav-sub-link">All Sales</a>--}}
{{--                    <a href="#" class="nav-sub-link">Sales Return</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="lock"></i> Purchase</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="page-signin.html" class="nav-sub-link">New Purchase</a>--}}
{{--                    <a href="page-signup.html" class="nav-sub-link">All Purchase</a>--}}
{{--                    <a href="page-forgot.html" class="nav-sub-link">Purchase Return</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="user"></i> Customer</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="page-profile.html" class="nav-sub-link">New Customer</a>--}}
{{--                    <a href="page-timeline.html" class="nav-sub-link">All Customer</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="file-text"></i> Supplier</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="page-invoice.html" class="nav-sub-link">New Supplier</a>--}}
{{--                    <a href="page-pricing.html" class="nav-sub-link">All Supplier</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="x-circle"></i> Product and Services</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="page-404.html" class="nav-sub-link">Product</a>--}}
{{--                    <a href="page-404.html" class="nav-sub-link">Damage Product</a>--}}
{{--                    <a href="page-500.html" class="nav-sub-link">Catagories</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link"><i data-feather="layers"></i> Stock</a>--}}
{{--            </li>--}}
{{--            <li class="nav-label"><label class="content-label">Admin</label></li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="life-buoy"></i> Account</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/form-elements.html" class="nav-sub-link">Expense</a>--}}
{{--                    <a href="../components/form-input-group.html" class="nav-sub-link">Income</a>--}}
{{--                    <a href="../components/form-input-tags.html" class="nav-sub-link">Pay-in Receipt</a>--}}
{{--                    <a href="../components/form-input-masks.html" class="nav-sub-link">Pay-out Receipt</a>--}}
{{--                    <a href="../components/form-validation.html" class="nav-sub-link">Account Head</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="book"></i> Branch</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/con-grid.html" class="nav-sub-link">Branches</a>--}}
{{--                    <a href="../components/con-icons.html" class="nav-sub-link">Stock Transfer</a>--}}
{{--                    <a href="../components/con-images.html" class="nav-sub-link">Employee Transfer</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="layers"></i> Human Resource</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/com-accordion.html" class="nav-sub-link">Employee</a>--}}
{{--                    <a href="../components/com-alerts.html" class="nav-sub-link">Salary Payroll</a>--}}
{{--                    <a href="../components/com-avatar.html" class="nav-sub-link">Attendance</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="monitor"></i> Report</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/util-animation.html" class="nav-sub-link">Sales Report</a>--}}
{{--                    <a href="../components/util-background.html" class="nav-sub-link">Purchase Report</a>--}}
{{--                    <a href="../components/util-border.html" class="nav-sub-link">Expense Report</a>--}}
{{--                    <a href="../components/util-display.html" class="nav-sub-link">Income Report</a>--}}
{{--                    <a href="../components/util-divider.html" class="nav-sub-link">Payin Report</a>--}}
{{--                    <a href="../components/util-flex.html" class="nav-sub-link">Payout Report</a>--}}
{{--                    <a href="../components/util-height.html" class="nav-sub-link">Payment in/out</a>--}}
{{--                    <a href="../components/util-margin.html" class="nav-sub-link">Stock</a>--}}
{{--                    <a href="../components/util-padding.html" class="nav-sub-link">Customer Ledger</a>--}}
{{--                    <a href="../components/util-position.html" class="nav-sub-link">Supplier Ledger</a>--}}
{{--                    <a href="../components/util-typography.html" class="nav-sub-link">Account Ledger</a>--}}
{{--                    <a href="../components/util-width.html" class="nav-sub-link">Customer Due Report</a>--}}
{{--                    <a href="../components/util-extras.html" class="nav-sub-link">Supplier Due Report</a>--}}
{{--                    <a href="../components/util-extras.html" class="nav-sub-link">Profit/loss Report</a>--}}
{{--                    <a href="../components/util-extras.html" class="nav-sub-link">All Bills</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="pie-chart"></i> Configuration</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/chart-flot.html" class="nav-sub-link">General Setting</a>--}}
{{--                    <a href="../components/chart-chartjs.html" class="nav-sub-link">Payment Method</a>--}}
{{--                    <a href="../components/chart-peity.html" class="nav-sub-link">User</a>--}}
{{--                    <a href="../components/com-badge.html" class="nav-sub-link">Role and Premission</a>--}}
{{--                    <a href="../components/chart-peity.html" class="nav-sub-link">Bank</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--        </ul>--}}

{{--        <hr class="mg-t-30 mg-b-25">--}}

{{--        <ul class="nav nav-sidebar">--}}
{{--            <li class="nav-item"><a href="themes.html" class="nav-link"><i data-feather="aperture"></i> Themes</a></li>--}}
{{--            <li class="nav-item"><a href="../docs.html" class="nav-link"><i data-feather="help-circle"></i> Documentation</a></li>--}}
{{--        </ul>--}}


{{--    </div><!-- sidebar-body -->--}}
{{--</div><!-- sidebar -->--}}

{{--<div class="content">--}}
{{--    <div class="header header-calendar">--}}
{{--        <div class="header-left">--}}
{{--            <a href="" class="burger-menu"><i data-feather="menu"></i></a>--}}

{{--            <div class="header-search">--}}
{{--                <i data-feather="search"></i>--}}
{{--                <input type="search" class="form-control" placeholder="Search calendar">--}}
{{--            </div><!-- header-search -->--}}
{{--        </div><!-- header-left -->--}}

{{--        <div class="header-right">--}}
{{--            <a href="" class="header-help-link"><i data-feather="help-circle"></i></a>--}}
{{--            <div class="dropdown dropdown-notification">--}}
{{--                <a href="" class="dropdown-link new" data-toggle="dropdown"><i data-feather="bell"></i></a>--}}
{{--                <div class="dropdown-menu dropdown-menu-right">--}}
{{--                    <div class="dropdown-menu-header">--}}
{{--                        <h6>Notifications</h6>--}}
{{--                        <a href=""><i data-feather="more-vertical"></i></a>--}}
{{--                    </div><!-- dropdown-menu-header -->--}}
{{--                    <div class="dropdown-menu-body">--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle text-primary bg-primary-light">s</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Socrates Itumay</strong> marked the task as completed.</p>--}}
{{--                                <span>5 hours ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-pink bg-pink-light">r</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Reynante Labares</strong> marked the task as incomplete.</p>--}}
{{--                                <span>8 hours ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-success bg-success-light">d</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Dyanne Aceron</strong> responded to your comment on this <strong>post</strong>.</p>--}}
{{--                                <span>a day ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-indigo bg-indigo-light">k</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Kirby Avendula</strong> marked the task as incomplete.</p>--}}
{{--                                <span>2 days ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div><!-- dropdown-menu-body -->--}}
{{--                    <div class="dropdown-menu-footer">--}}
{{--                        <a href="">View All Notifications</a>--}}
{{--                    </div>--}}
{{--                </div><!-- dropdown-menu -->--}}
{{--            </div>--}}
{{--            <div class="dropdown dropdown-loggeduser">--}}
{{--                <a href="" class="dropdown-link" data-toggle="dropdown">--}}
{{--                    <div class="avatar avatar-sm">--}}
{{--                        <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">--}}
{{--                    </div><!-- avatar -->--}}
{{--                </a>--}}
{{--                <div class="dropdown-menu dropdown-menu-right">--}}
{{--                    <div class="dropdown-menu-header">--}}
{{--                        <div class="media align-items-center">--}}
{{--                            <div class="avatar">--}}
{{--                                <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">--}}
{{--                            </div><!-- avatar -->--}}
{{--                            <div class="media-body mg-l-10">--}}
{{--                                <h6>Louise Kate Lumaad</h6>--}}
{{--                                <span>Administrator</span>--}}
{{--                            </div>--}}
{{--                        </div><!-- media -->--}}
{{--                    </div>--}}
{{--                    <div class="dropdown-menu-body">--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="user"></i> View Profile</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="edit-2"></i> Edit Profile</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="briefcase"></i> Account Settings</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="shield"></i> Privacy Settings</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="log-out"></i> Sign Out</a>--}}
{{--                    </div>--}}
{{--                </div><!-- dropdown-menu -->--}}
{{--            </div>--}}
{{--        </div><!-- header-right -->--}}
{{--    </div><!-- header -->--}}
    <div class="content-header justify-content-between">
        <div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Human Resource</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Employee Salary</li>
                </ol>
            </nav>
            <h4 class="content-title content-title-xs">Employee Salary Payment</h4>
        </div>
    </div><!-- content-header -->

    <div class="content-body content-body-calendar">
        <div id="" class="content-calendar pd-10 ht-450">
            <div class="d-md-flex justify-content-between">
                <h5 class="mg-l-5 mg-t-5 mg-b-10">New Salary Payroll</h5>
                <select class="form-control form-control-sm select2-no-search wd-15p">
                    <option label="Select Branch"></option>
                    <option value="Firefox">Kathmandu</option>
                    <option value="Chrome">Pokhhara</option>
                    <option value="Opera">Butwal</option>
                </select>
                <!-- <input type="text" class="form-control form-control-sm wd-30p" placeholder="Date" aria-label="Username" aria-describedby="basic-addon1"> -->
            </div>
            <div class="wd-md-30p bd mg-r-10 d-inline-block" style="float: left;">
                <div class="form-group mg-10">
                    <!-- <label class="form-label">Payment Incoming/Outgoing</label>
                    <div class="input-group mg-b-10">
                       <div class="col"><i class="far fa-address-card"></i></div>
                      <div class="btn-group" role="group" aria-label="Basic example">

                        <button type="button" class="btn btn-outline-success active">Pay-In</button>
                        <button type="button" class="btn btn-outline-success">Pay-out</button>

                     </div>
                    </div>

                      <hr> -->
                    <div class="input-group mg-b-10">
                        <div class="input-group-prepend wd-30p">
                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">Date</span>
                        </div>
                        <input type="text" class="form-control form-control-sm" placeholder="Date" aria-label="Username" aria-describedby="basic-addon1">
                        <!-- <div class="input-group-prepend">
                          <span class="input-group-text form-control-sm wd-100p" id="basic-addon1"><i class="far fa-calendar"></i></span>
                        </div> -->
                        <div class="input-group-prepend">
                            <button class="btn btn-outline-light form-control-sm pd-l-10 pd-r-10 pd-t-5" type="button" id="button-addon1"><i class="far fa-calendar"></i></button>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend wd-30p">
                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">Payment Id</span>
                        </div>
                        <input type="text" class="form-control form-control-sm" placeholder="Salary Id" aria-label="Username" aria-describedby="basic-addon1" disabled>
                    </div>
                </div><!-- form-group -->
                <div class="wd-95p bd mg-l-8 mg-r-8 mg-t-0 mg-b-8 pd-10" style="float: left;">
                    <div class="wd-md-100p mg-b-10 d-md-flex">
                        <div class="input-group-prepend">
                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">Payment method</span>
                        </div>
                        <select class="form-control form-control-sm select2-no-search wd-60p">
                            <option label="Cash"></option>
                            <option value="Firefox">Bank</option>
                            <option value="Chrome">e-sewa</option>
                            <option value="Opera">Draft</option>
                        </select>
                    </div>
                    <div class="input-group mg-b-10">
                        <div class="input-group-prepend wd-25p">
                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">Bank</span>
                        </div>
                        <input type="text" class="form-control form-control-sm" placeholder="Bank Name" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend wd-25p">
                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">Txn. Id</span>
                        </div>
                        <input type="text" class="form-control form-control-sm" placeholder="Transaction Id" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                </div>
            </div>
            <!--Product and Services-->
            <div class="wd-md-65p bd mg-0 d-block" style="float: left;">
                <div class="row row-sm mg-t-5 mg-l-5 mg-r-5 mg-b-0">
                    <div class="form-group wd-80p">
                        <label>Employee</label>
                        <!-- <input type="text" class="form-control" placeholder="Firstname"> -->
                        <div class="input-group mg-b-0">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-light" type="button" id="employee-search"><i class="far fa-address-card"></i></button>
                            </div>
                            <div class="wd-30p">
                                <input type="text" name="" class="form-control" placeholder="Employee Id">
                            </div>
                            <input type="text" class="form-control wd-60p" placeholder="Search Employee Name" aria-label="Example text with button addon" aria-describedby="button-addon1">
                        </div>
                    </div><!-- col -->

                </div>

                <div class="d-md-flex mg-l-15 mg-b-15 mg-t-0">
                    <div class="form-group mg-b-0 mg-t-0">
                        <label>Total Due</label>
                        <input type="text" name="firstname" class="form-control form-control-sm wd-120" placeholder="Total Due" disabled>
                    </div><!-- form-group -->
                    <div class="form-group mg-b-0 mg-md-l-10 mg-t-0">
                        <label>Paid Amount <span class="tx-danger">*</span></label>
                        <input type="text" name="lastname" class="form-control form-control-sm wd-120" placeholder="Paid Amount" required>
                    </div><!-- form-group -->
                    <div class="form-group mg-b-0 mg-md-l-10 mg-t-0">
                        <label>Remaning Due</label>
                        <input type="text" name="lastname" class="form-control form-control-sm wd-120" placeholder="Remaning Due" disabled>
                    </div><!-- form-group -->
                </div><!-- d-flex -->
                <div class="form-group mg-15">
                    <label class="form-label">Description</label>
                    <textarea class="form-control form-control-sm" rows="3" placeholder="Payment Description"></textarea>
                </div><!-- form-group -->
                <div class="d-flex flex-row-reverse mg-b-10">
                    <button type="button" class="btn btn-sm btn-warning mg-r-10">Reset Item</button>
                    <button type="button" class="btn btn-sm btn-success mg-r-10">Subbmit</button>
                </div>



            </div>
            <!--Buttom-->

        </div>
    </div><!-- content-body -->
</div><!-- content -->

<div class="employee-list wd-md-60p" style="z-index: 999;">
    <div class="content-body-calendar wd-100p ht-500 bd bg-white">
        <div class="close closeregister" id="closeregister">+</div>
        <h4>Select Employee from List</h4>
        <div class="component-section mg-t-10">
            <table id="dtVerticalScrollExample" class="table table-hover table-striped table-bordered table-sm">
                <thead>
                <tr>
                    <th class="wd-20p">Employee Id</th>
                    <th class="wd-35p">Employee Name</th>
                    <th class="wd-25p">Address</th>
                    <th class="wd-20p">Mobile No</th>
                </tr>
                </thead>
                <tbody>
                <tr id="row-selection1">
                    <td>002</td>
                    <td>Bhagwan Das Agrahari</td>
                    <td>Tinkune, Kathmandu</td>
                    <td>9834567895</td>
                </tr>
                <tr id="row-selection2">
                    <td>000001</td>
                    <td>Bhagwan Das Agrahari</td>
                    <td>Tinkune, Kathmandu</td>
                    <td>1234567895</td>
                </tr>
                <tr id="row-selection3">
                    <td>000001</td>
                    <td>Bhagwan Das Agrahari</td>
                    <td>Tinkune, Kathmandu</td>
                    <td>1234567895</td>
                </tr>
                <tr id="row-selection3">
                    <td>000001</td>
                    <td>Bhagwan Das Agrahari</td>
                    <td>Tinkune, Kathmandu</td>
                    <td>1234567895</td>
                </tr>
                <tr id="row-selection3">
                    <td>000001</td>
                    <td>Bhagwan Das Agrahari</td>
                    <td>Tinkune, Kathmandu</td>
                    <td>1234567895</td>
                </tr>
                <tr id="row-selection3">
                    <td>000001</td>
                    <td>Bhagwan Das Agrahari</td>
                    <td>Tinkune, Kathmandu</td>
                    <td>1234567895</td>
                </tr>
                <tr id="row-selection3">
                    <td>000001</td>
                    <td>Bhagwan Das Agrahari</td>
                    <td>Tinkune, Kathmandu</td>
                    <td>1234567895</td>
                </tr>
                <tr id="row-selection3">
                    <td>000001</td>
                    <td>Bhagwan Das Agrahari</td>
                    <td>Tinkune, Kathmandu</td>
                    <td>1234567895</td>
                </tr>
                <tr id="row-selection3">
                    <td>000001</td>
                    <td>Bhagwan Das Agrahari</td>
                    <td>Tinkune, Kathmandu</td>
                    <td>1234567895</td>
                </tr>
                <tr id="row-selection3">
                    <td>000001</td>
                    <td>Bhagwan Das Agrahari</td>
                    <td>Tinkune, Kathmandu</td>
                    <td>1234567895</td>
                </tr>
                <tr id="row-selection3">
                    <td>000001</td>
                    <td>Bhagwan Das Agrahari</td>
                    <td>Tinkune, Kathmandu</td>
                    <td>1234567895</td>
                </tr>
                <tr id="row-selection3">
                    <td>000001</td>
                    <td>Bhagwan Das Agrahari</td>
                    <td>Tinkune, Kathmandu</td>
                    <td>1234567895</td>
                </tr>
                <tr id="row-selection3">
                    <td>000001</td>
                    <td>Bhagwan Das Agrahari</td>
                    <td>Tinkune, Kathmandu</td>
                    <td>1234567895</td>
                </tr>
                </tbody>
            </table>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div>
</div>



{{--<script src="../lib/jquery/jquery.min.js"></script>--}}
{{--<script src="../lib/jqueryui/jquery-ui.min.js"></script>--}}
{{--<script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>--}}
{{--<script src="../lib/feather-icons/feather.min.js"></script>--}}
{{--<script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>--}}
{{--<script src="../lib/js-cookie/js.cookie.js"></script>--}}
{{--<script src="../lib/moment/min/moment.min.js"></script>--}}
{{--<script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>--}}
{{--<script src="../lib/select2/js/select2.min.js"></script>--}}
{{--<script src="../lib/fullcalendar/fullcalendar.min.js"></script>--}}
{{--<script src="../lib/select2/js/select2.full.min.js"></script>--}}

{{--<script src="../assets/js/cassie.js"></script>--}}
{{--<script src="../assets/js/calendar-events.js"></script>--}}
{{--<script src="../assets/js/calendar.js"></script>--}}
{{--<script src="../assets/js/custom.js"></script>--}}
{{--<script>--}}
{{--    $(function(){--}}
{{--        'use strict'--}}

{{--        $('#example1').DataTable({--}}
{{--            language: {--}}
{{--                searchPlaceholder: 'Search...',--}}
{{--                sSearch: '',--}}
{{--                lengthMenu: '_MENU_ items/page',--}}
{{--            }--}}
{{--        });--}}

{{--        $('#example2').DataTable({--}}
{{--            responsive: true,--}}
{{--            language: {--}}
{{--                searchPlaceholder: 'Search...',--}}
{{--                sSearch: '',--}}
{{--                lengthMenu: '_MENU_ items/page',--}}
{{--            }--}}
{{--        });--}}

{{--        $('#example8').DataTable({--}}
{{--            responsive: true,--}}
{{--            language: {--}}
{{--                searchPlaceholder: 'Search...',--}}
{{--                sSearch: '',--}}
{{--                lengthMenu: '_MENU_ items/page',--}}
{{--            }--}}
{{--        });--}}



{{--        // Select2--}}
{{--        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });--}}

{{--    });--}}
{{--</script>--}}
{{--</body>--}}

{{--</html>--}}
@endsection
