<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Responsive Bootstrap 4 Dashboard and Admin Template">
    <meta name="author" content="ThemePixels">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/favicon.png') }}">
    <title>Sloftechpark</title>
    <link href="{{ asset('lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/cassie.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/changes.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/sweetalert.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('nepali.datepicker/nepali.datepicker.v2.2.min.css') }}" />
    @stack('styles');
</head>

<body>

    @include('admin.partials.sidebar')
    <div class="content content-page">
        @include('admin.partials.header')
        @yield('content')
        @include('admin.partials.footer')
    </div>
    <script type="text/javascript" src="{{ asset('nepali.datepicker/js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('nepali.datepicker/js/bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('nepali.datepicker/nepali.datepicker.v2.2.min.js') }}"></script>

    <script type="text/javascript">
        $(".bod-picker").nepaliDatePicker({
            // dateFormat: "%D, %M %d, %y",
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true,
            disableBefore: '12/08/2073',
            disableAfter: '12/20/2073'
        });
        $('.bod-picker').val(getNepaliDate());
        $(".bod-picker").nepaliDatePicker({
            npdMonth: true,
            npdYear: true,
            npdYearCount: 10
        });
        $("#clear-bth").on("click", function(event) {
            $(".bod-picker").val('');
        });

        $(".bod-pickers").nepaliDatePicker({
            // dateFormat: "%D, %M %d, %y",
            dateFormat: "%y-%m-%d",
            closeOnDateSelect: true,
            disableBefore: '12/08/2073',
            disableAfter: '12/20/2073'
        });
        // $('.bod-picker').val(getNepaliDate());
        $(".bod-pickers").nepaliDatePicker({
            npdMonth: true,
            npdYear: true,
            npdYearCount: 10
        });

    </script>


    <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('lib/jqueryui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('lib/feather-icons/feather.min.js') }}"></script>
    <script src="{{ asset('lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('lib/js-cookie/js.cookie.js') }}"></script>
    <script src="{{ asset('lib/jquery.flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('lib/jquery.flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('lib/jquery.flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/js/cassie.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{ asset('assets/js/flot.sampledata.js') }}"></script>
    <script src="{{ asset('dist/sweetalert.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    @include('flash-message')
    <script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script>
        $(function() {

            'use strict'

            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = window.location.search.substring(1),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[
                            1]);
                    }
                }
            };

            if (!Cookies.get('theme-skin')) {
                $('#defaultTheme').addClass('theme-selected');
            }

            $('.card-theme').on('click', function(e) {
                $('.card-theme').removeClass('theme-selected');
                $(this).addClass('theme-selected');

                var skin = $(this).attr('data-title');

                if (skin === 'default') {
                    $('#themeSkin').remove();
                    Cookies.remove('theme-skin');
                } else {

                    if ($('#themeSkin').length === 0) {
                        $('head').append('<link id="themeSkin" rel="stylesheet" href="{{ asset('
                            assets / css / skin ') }}.' + skin + '.css">')
                    } else {
                        $('#themeSkin').attr('href', '{{ asset('
                            assets / css / skin ') }}.' + skin + '.css');
                    }

                    Cookies.set('theme-skin', skin);
                }
            })

            var skinParam = getUrlParameter('skin');
            if (skinParam.length) {
                $('.card-theme').removeClass('theme-selected');
                $('.card-theme[data-title="' + skinParam + '"]').addClass('theme-selected');

                if (skinParam === 'default') {
                    $('#themeSkin').remove();
                    Cookies.remove('theme-skin');
                } else {

                    if ($('#themeSkin').length === 0) {
                        $('head').append('<link id="themeSkin" rel="stylesheet" href="{{ asset('
                            assets / css / skin ') }}.' + skinParam + '.css">')
                    } else {
                        $('#themeSkin').attr('href', '{{ asset('
                            assets / css / skin ') }}.' + skinParam + '.css');
                    }

                    Cookies.set('theme-skin', skinParam);
                }
            }

        })

    </script>
    <script type="text/javascript" src="https://unpkg.com/nepali-date-picker@2.0.0/dist/jquery.nepaliDatePicker.min.js"
        integrity="sha384-bBN6UZ/L0DswJczUYcUXb9lwIfAnJSGWjU3S0W5+IlyrjK0geKO+7chJ7RlOtrrF" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://unpkg.com/nepali-date-picker@2.0.0/dist/nepaliDatePicker.min.css"
        integrity="sha384-Fligaq3qH5qXDi+gnnhQctSqfMKJvH4U8DTA+XGemB/vv9AUHCwmlVR/B3Z4nE+q" crossorigin="anonymous">
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
    <script>
        function submitForm(formId) {
            if (confirm('Are you sure ?')) {
                toastr.success("Please wait. Processing...");
                let form = $(`#${formId}`);
                $.ajax({
                    type: 'POST',
                    url: form.attr("action"),
                    data: form.serialize(),
                    success: function(data, status) {
                        console.log(data);
                        if (data.indexOf('success') >= 0) {
                            toastr.success(data);
                            $(`#${formId} input`).val('');
                            $(`#${formId} textarea`).val('');
                        } else {
                            toastr.error(data);
                        }
                    },
                    error: function(xhr, status, error) {
                        console.log(error);
                        toastr.error('Something went wrong. please try again later.');
                    }
                });
            }
            return false;
        }

    </script>
    @stack('scripts')
</body>

</html>
