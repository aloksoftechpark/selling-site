@extends('admin.layouts.app')
@section('content')


    <div class="row">
        <div class="col-sm-5">
            <div class="content-header">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Account</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Bank</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Bank Account</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            <!--   <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10">New Bank</button> -->
            @if(Auth::guard('admin')->check())
                <button type="button" class="btn btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">
                New Bank
            </button>
            @else
            @can('bank-create')
                <button type="button" class="btn btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">
                New Bank
            </button>
            @endcan
            @endif

            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <form action="{{route('bank.store')}}" method="post" enctype="multipart/form-data">

                    @csrf
                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">ADD BANK</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">

                                        Bank ID: <input type="text"  name="bankId" id="bankId" value="{{isset($bank1) ? $bank1->bankId+1 : 1 }}" readonly><br>

                                </div><!-- col -->
                                <div class="col-md-6">

                                        Bank Name: <input type="text" name="name" value=""><br>

                                </div><!-- col -->
                            </div><!-- row -->

                            <div class="row mar_top">
                                <div class="col-md-6">

                                        Account Number: <input type="text" name="account_no" value=""><br>

                                </div><!-- col -->
                                <div class="col-md-6">

                                        Opening Amount:<br> <div class="input-group mg-b-10">
                                            <div class="wd-25p">
                                                <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                            </div>
                                            <input type="text" name="balance" value="" class="form-control form-control-sm group_right" placeholder="0.0">


                                        </div><br>

                                </div><!-- col -->
                            </div><!-- row -->
                            <div class="modal-footer modal-footer_footer">
                                <button  class="btn btn-success mg-r-10">Update</button>
                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>
                        </div>

                    </div>
                </div>
                </form>
            </div>



        </div><!-- col -->
    </div><!-- row -->


    <div class="content-body" style="margin-top: -30px;">
        <div class="component-section">

            <table id=" " class="table">
                <thead>
                <tr>
                    <th class="wd-5p"><input type="checkbox" aria-label="Checkbox for following text input"></th>
                    <th class="wd-15p">Bank Id</th>
                    <th class="wd-25p">Bank Name</th>
                    <th class="wd-25p">Account No</th>
                    <th class="wd-15p">Balance</th>
                    <th class="wd-15p">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($bank as $key =>$bank )
                    <tr>
                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                        <td>{{++$key}}</td>
                        <td>{{$bank->name}}</td>
                        <td>{{$bank->account_no}}</td>
                        <td>{{$bank->balance}}</td>
                        <td class="d-md-flex">
                            @if(Auth::guard('admin')->check())
                                <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>
                            @else
                            @can('bank-view')
                                <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>
                            @endcan
                            @endif

                            @if(Auth::guard('admin')->check())
                                <div class="mg-r-20" title="Edit"><a href=" " class="edit"  data-toggle="modal" data-id="{{$bank->id}}" data-target="#exampleModalCenter2"><i class="far fa-edit text-warning"></i></a></div>
                            @else
                            @can('bank-edit')
                                <div class="mg-r-20" title="Edit"><a href=" " class="edit"  data-toggle="modal" data-id="{{$bank->id}}" data-target="#exampleModalCenter2"><i class="far fa-edit text-warning"></i></a></div>
                            @endcan
                            @endif

                            @if(Auth::guard('admin')->check())
                                <div class="mg-r-20" title="Delete"><a href="{{route('bank.destroy',$bank->id)}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                            @else
                            @can('bank-delete')
                                <div class="mg-r-20" title="Delete"><a href="{{route('bank.destroy',$bank->id)}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                            @endcan
                            @endif



{{--                            <div class="mg-r-20" title="Delete"><a href="{{route('bank.destroy',$bank->id)}}"><i class="icon ion-trash-b text-danger"></i></a></div>--}}




                            {{--                            fot delete popup--}}

                            <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <div class="conform_del">
                                                <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                            </div><!-- conform_del -->

                                            <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                                <a class="btn btn-success mg-r-10" href="{{route('bank.destroy',$bank->id)}}">Yes</a>
                                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                          {{--  //for edit popup--}}
                            <div class="col-sm-0 tx-right col-lg-7">
                                <!--   <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10">New Bank</button> -->
{{--                                <button type="button" class="btn btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">--}}
{{--                                    New Bank--}}
{{--                                </button>--}}
                                <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
{{--                                    <form action="{{route('bank.store')}}" method="post" enctype="multipart/form-data">--}}

{{--                                        @csrf--}}
                                        <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalCenterTitle">ADD BANK</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">

                                                            Bank ID: <input type="text" class="bankId" name="bankId" id="bankId" readonly><br>
                                                            <input type="hidden" class="bank_id123">

                                                        </div><!-- col -->
                                                        <div class="col-md-6">

                                                            Bank Name: <input type="text" class="bank_name" name="name" value=""><br>

                                                        </div><!-- col -->
                                                    </div><!-- row -->

                                                    <div class="row mar_top">
                                                        <div class="col-md-6">

                                                            Account Number: <input type="text" class="account_no" name="account_no" value=""><br>

                                                        </div><!-- col -->
                                                        <div class="col-md-6">

                                                            Opening Amount:<br> <div class="input-group mg-b-10">
                                                                <div class="wd-25p">
                                                                    <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                                </div>
                                                                <input type="text" name="balance"  value="" class="balance form-control form-control-sm group_right" placeholder="0.0">


                                                            </div><br>

                                                        </div><!-- col -->
                                                    </div><!-- row -->
                                                    <div class="modal-footer modal-footer_footer">
                                                        <button  class="update btn btn-success mg-r-10">Update</button>
                                                        <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                        <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                          <button type="button" class="btn btn-primary">Save changes</button> -->
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
{{--                                    </form>--}}
                                </div>



                            </div><!-- col -->

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- component-section -->

    </div><!-- content-body -->

    </div><!-- content -->



@endsection
@push('scripts')

    <script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>

    <script>

        $(".edit").click(function(e){
            e.preventDefault()
            var bank_id = $(this).data('id');




            // alert(contact_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/bank-edit')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    bank_id : bank_id,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // toastr.success(data.success);
                    // window.location.reload();
                    console.log(data);
                    // var advance_due= $('#advance_due').val();


                    // $('.bod-picker').append($('.bod-picker').val(data.category.date));
                    $('.bankId').append($('.bankId').val(data.bank.bankId));
                    $('.bank_id123').append($('.bank_id123').val(data.bank.id));
                    $('.bank_name').append($('.bank_name').val(data.bank.name));
                    $('.balance').append($('.balance').val(data.bank.balance));
                    $('.account_no').append($('.account_no').val(data.bank.account_no));


                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });


        $(".update").click(function(e){
            e.preventDefault()

            var bank_id= $(".bank_id123").val();
            var name= $(".bank_name").val();
            var account_no= $(".account_no").val();
            var balance= $(".balance").val();
            var bankId= $(".bankId").val();

            // alert(category_id1234);

            $.ajax({
                type: 'POST',
                url: '{{url('/bank-update')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    bank_id : bank_id,
                    name:name,
                    balance:balance,
                    account_no:account_no,
                    bankId:bankId

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);



                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });


    </script>
    <script>
        $(function(){
            'use strict'

            $('#example1').DataTable({
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            $('#example2').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            $('#example8').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });



            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>
@endpush
