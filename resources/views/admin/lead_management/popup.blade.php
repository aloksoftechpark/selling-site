<div class="popup-container wp-100p" style="z-index: 999;">
    <div class="wd-sm-30p mg-t-auto mg-b-auto">
        <div class="card">
            <div class="card-header tx-medium pd-10">Featured</div>
            <div class="close closeregister" id="popup-close">+</div>
            <div class="card-body">
                <form action="">
                    <h6 class="tx-uppercase tx-12 tx-gray-900 mg-b-10">Assigned To: Kiran Kandel</h6>
                    <label class="content-label tx-12 tx-bold mg-b-5">Change assignment</label>
                    <select class="form-control select2 mg-b-10">
                        @forelse($user as $user)
                            <option value="{{$user->id}}">{{$user->userName}}</option>
                        @empty
                        @endforelse
                    </select>
                    <div class="form-group">
                        <button class="btn btn-sm btn-danger float-right">Cancle</button>
                        <button class="btn btn-sm btn-info float-right mg-r-10">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
