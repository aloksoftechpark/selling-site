@extends('admin.layouts.app')
@section('content')


    <link rel="stylesheet" href="{{asset('assets/css/changes.css')}}">

    <div class="content-header justify-content-between">
        <div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Lead</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Leads</li>
                </ol>
            </nav>
            <h4 class="content-title content-title-xs">Lead Assignment</h4>
        </div>
    </div><!-- content-header -->
    <div class="content-body content-body-calendar" style="margin-top: -50px;">
        <hr>
        <form action="{{route('lead-assignment.search')}}" method="post" enctype="multipart/form-data">
            @csrf
        <div class="row">

            <div class="col-3 form-group">
                <select class="form-control form-control-sm select2-no-search" name="lead">
                    <option  label="select lead"></option>
                    <option value="un" >Un-assigned</option>
                    <option value="as">Assigned</option>
                </select>
            </div><!-- form-group -->
            <div class="col-3 form-group">
                <select class="form-control form-control-sm select2-no-search" name="user_id">
                    <option label="Select User"></option>
                    {{--                    <option value="Firefox">User-1</option>--}}
                    {{--                    <option value="Chrome">User-2</option>--}}
                    {{--                    <option >Select branch</option>--}}
{{--                    {{dd($user)}}--}}
                    @forelse($user as $user)
                        <option value="{{$user->id}}">{{$user->userName}}</option>
                    @empty
                    @endforelse
                </select>
            </div><!-- form-group -->
                <button class="btn btn-brand-02">search</button>
        </div>
        </form>
        <hr class="mg-0">
        <div class="row pd-sm-r-15">
            @foreach($lead_management as $key =>$lead_management)
                <div class="col-md-6 col-xl-4 mg-t-15 mg-sm-t-20 pd-sm-r-0">
                    <div class="card card-hover card-project-two">
                        <div class="card-header bg-transparent">
                            <div>
                                <h6 class="mg-b-5"><a href="#" id="popup-container" class="tx-black ">{{$lead_management->name}}</a></h6>
                                <span>Uplode: {{$lead_management->created_at}} <br> Follow: {{$lead_management->followUpDate}}</span>
                                <span>Assigned to:

{{--                                    @foreach($lead_assign as $lead_assign)--}}
                                        @if(isset($lead_management->users))
{{--                                        {{dd($lead_assign->users->userName)}}--}}
                                    {{$lead_management->users->userName}}
{{--                                    {{$user->name}}--}}
                                        @endif
{{--                                    @endforeach--}}
                                </span>


                            </div>
                            <nav class="nav nav-card-icon">
                                <a href=""><i data-feather="check-square" class="svg-14"></i> 21</a>
                                <a href=""><i data-feather="message-square" class="svg-14"></i> 85</a>
                            </nav>
                        </div><!-- card-header -->
                        <div class="card-body pd-t-10">
                            <p class="tx-13 tx-gray-700"><strong class="tx-bold">To Do:</strong> {{$lead_management->todo}}</p>
                            <div class="card-progress">
                                <label class="content-label mg-b-0">Priority</label>
                                <div class="progress">
                                    <div class="progress-bar bg-danger wd-25p" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <label class="content-label mg-b-0">25%</label>
                            </div>

                        </div>
                    </div><!-- card -->

                </div><!-- col -->
            @endforeach
{{--            <div class="col-md-6 col-xl-4 mg-t-15 mg-sm-t-20 pd-sm-r-0">--}}
{{--                <div class="card card-hover card-project-two">--}}
{{--                    <div class="card-header bg-transparent">--}}
{{--                        <div>--}}
{{--                            <h6 class="mg-b-5"><a href="#"   class="tx-black">Softechpark Pvt. Ltd.</a></h6>--}}
{{--                            <span>Uplode: Yesterday 10:15am | Follow: Today</span>--}}
{{--                            <span>Assigned to: Kiran Kandel</span>--}}
{{--                        </div>--}}
{{--                        <nav class="nav nav-card-icon">--}}
{{--                            <a href=""><i data-feather="check-square" class="svg-14"></i> 21</a>--}}
{{--                            <a href=""><i data-feather="message-square" class="svg-14"></i> 85</a>--}}
{{--                        </nav>--}}
{{--                    </div><!-- card-header -->--}}
{{--                    <div class="card-body pd-t-10">--}}
{{--                        <p class="tx-13 tx-gray-700"><strong class="tx-bold">To Do:</strong> Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deser.</p>--}}
{{--                        <div class="card-progress">--}}
{{--                            <label class="content-label mg-b-0">Priority</label>--}}
{{--                            <div class="progress">--}}
{{--                                <div class="progress-bar bg-warning wd-60p" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                            </div>--}}
{{--                            <label class="content-label mg-b-0">25%</label>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div><!-- card -->--}}
{{--            </div><!-- col -->--}}
{{--            <div class="col-md-6 col-xl-4 mg-t-15 mg-sm-t-20 pd-sm-r-0">--}}
{{--                <div class="card card-hover card-project-two">--}}
{{--                    <div class="card-header bg-transparent">--}}
{{--                        <div>--}}
{{--                            <h6 class="mg-b-5"><a href="#"  class="tx-black">Softechpark Pvt. Ltd.</a></h6>--}}
{{--                            <span>Uplode: Yesterday 10:15am | Follow: Today</span>--}}
{{--                            <span>Assigned to: Kiran Kandel</span>--}}
{{--                        </div>--}}
{{--                        <nav class="nav nav-card-icon">--}}
{{--                            <a href=""><i data-feather="check-square" class="svg-14"></i> 21</a>--}}
{{--                            <a href=""><i data-feather="message-square" class="svg-14"></i> 85</a>--}}
{{--                        </nav>--}}
{{--                    </div><!-- card-header -->--}}
{{--                    <div class="card-body pd-t-10">--}}
{{--                        <p class="tx-13 tx-gray-700"><strong class="tx-bold">To Do:</strong> Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deser.</p>--}}
{{--                        <div class="card-progress">--}}
{{--                            <label class="content-label mg-b-0">Priority</label>--}}
{{--                            <div class="progress">--}}
{{--                                <div class="progress-bar bg-green wd-70p" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                            </div>--}}
{{--                            <label class="content-label mg-b-0">70%</label>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div><!-- card -->--}}
{{--            </div><!-- col -->--}}
        </div><!--row-->
    </div>
{{--</div><!-- content -->--}}
    <div class="popup-container wp-100p" style="z-index: 999;">
        <div class="wd-sm-30p mg-t-auto mg-b-auto">
            <div class="card">
                <div class="card-header tx-medium pd-10">Featured</div>
                <div class="close closeregister" id="popup-close">+</div>
                <div class="card-body">
                    <form action="{{route('lead-management.lead_assignment_store',$lead_management->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <h6 class="tx-uppercase tx-12 tx-gray-900 mg-b-10">Assigned To:
                            @if(isset($lead_management->users))
                                {{-- {{dd($lead_assign->users->userName)}}--}}
                                {{$lead_management->users->userName}}
                                {{--{{$user->name}}--}}
                            @endif
                        </h6>
                        <label class="content-label tx-12 tx-bold mg-b-5">Change assignment</label>
                        <select class="form-control select2 mg-b-10" name="user_id">
                            <option label="Choose one"></option>
{{--                            <option value="Firefox">Firefox</option>--}}
{{--                            <option value="Chrome">Chrome</option>--}}
{{--                            <option value="Safari">Safari</option>--}}
{{--                            <option value="Opera">Opera</option>--}}
{{--                            <option value="Internet Explorer">Internet Explorer</option>--}}
{{--                            {{dd('iii')}}--}}
                        @forelse($users as $users)
                                <option value="{{$users->id}}">{{$users->userName}}</option>
                            @empty
                            @endforelse
                        </select>
                        <div class="form-group">
                            <button class="btn btn-sm btn-danger float-right">Cancle</button>
                            <button class="btn btn-sm btn-info float-right mg-r-10">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('assets/js/customes.js')}}"></script>

{{--    @push('scripts')--}}
{{--    <script>--}}
{{--        $('#popup-container').on('click', function (e) {--}}
{{--            e.preventDefault();--}}
{{--            $.noConflict();--}}
{{--            alert('dsjkl');--}}
{{--            $('.popup-container').show('slow');--}}

{{--        })--}}
{{--    </script>--}}
{{--    @endpush--}}

@endsection
