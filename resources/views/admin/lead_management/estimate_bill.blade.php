@extends('admin.layouts.app')
@section('content')

    <style type="text/css">
        <!--

        @media print
        {
            .noprint {display:none;}
        }

        @media screen
        {

        }

        -->
    </style>
    <div class="content-header noprint">
        <div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Pages</a></li>
                    <li class="breadcrumb-item"><a href="#">Extras</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Invoice Page</li>
                </ol>
            </nav>
            <h4 class="content-title content-title-sm">Invoice Page</h4>
        </div>
    </div><!-- content-header -->
    <div class="content-body">
        <div class="card card-invoice">
            {{--//for header--}}
            <div class="card card-invoice noprint">
                <div class="card-header noprint">
                    <div>
                        <h5 class="mg-b-3">Invoice #DF032AZ{{$billing}}</h5>
                        <span class="tx-sm text-muted">Due on {{$estimate->estimate_date}}</span>
                    </div>
                    <div class="btn-group-invoice print_bill noprint">
                        <button onclick="myFunction()"><i data-feather="printer"></i>Print </button>
                        {{--                    <a href="" class="btn btn-white btn-sm btn-uppercase"><i data-feather="printer"></i> Print</a>--}}
                    </div>
                </div><!-- card-header -->
            </div>

            <div class="card-header flex_center">
                <div class="text-center">
                    @if($generalSetting)
                        <h6 class="tx-26 mg-b-5">{{$generalSetting->name}}</h6>
                        <p class="mg-b-0">{{$generalSetting->location}}</p>
                        <p class="mg-b-0">Tel No: {{$generalSetting->phoneNumber}} / {{$generalSetting->mailId}}</p>
                        <p class="mg-b-0">PAN: {{$generalSetting->panNumber}}</p>
                    @endif
                </div><!-- col -->
{{--                <div class="btn-group-invoice print_bill">--}}
{{--                    <a href="" class="btn btn-white btn-sm btn-uppercase"><i data-feather="printer"></i> Print</a>--}}
{{--                </div>--}}
            </div><!-- card-header -->
            <div class="card-body bill_body">
                <div class="row bill_head_detail">

                    <div class="col-sm-6 col-lg-8 mg-t-40 mg-sm-t-0">
                        <label class="content-label">ESTIMATE TO</label>
                        <!--    <h6 class="tx-15 mg-b-10">Juan Dela Cruz</h6> -->
                        @if($estimate->lead)
                        <p class="mg-b-0">NAME : {{$estimate->lead->name}}</p>
                        <p class="mg-b-0">ADDRESS : {{$estimate->lead->address}}</p>
                        <p class="mg-b-0">PAN : </p>
                            @endif
                    </div><!-- col -->
                    <div class="col-sm-6 col-lg-4 mg-t-40">
                        <p class="mg-b-0 text-right">ESTIMATE NUMBER : DF032AZ{{$billing}}</p>
                        <p class="mg-b-0 text-right">ISSUE DATE : {{$estimate->estimate_date}}</p>

                    </div><!-- col -->
                </div><!-- row -->

                <div class="table-responsive mg-t-25">
                    <table class="table table-invoice bd-b">
                        <thead>
                        <tr>
                            <th class="tx-left">SN.</th>
                            <th class="wd-40p d-none d-sm-table-cell">Description</th>
                            <th class="tx-center">QNTY</th>
                            <th class="tx-right">Unit Price</th>
                            <th class="tx-right">Discount</th>
                            <th class="tx-right">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($estimate_product as $key => $estimate_products)
                        <tr>
                            <td class="tx-left">{{++$key}}</td>
                            <td class="d-none d-sm-table-cell tx-color-03">
                                <h5 class="table_lead_head">{{$estimate_products->item}}</h5>
                              {{$estimate_products->description}}</td>
                            <td class="tx-center">{{$estimate_products->quantity}}</td>
                            <td class="tx-right">{{$estimate_products->rate}}</td>
                            <td class="tx-right">{{$estimate_products->discount_amount}}</td>
                            <td class="tx-right">{{$estimate_products->total}}</td>
                        </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>

                <div class="row justify-content-end mg-t-25">

                    <div class="col-sm-6 col-lg-4 order-1 order-sm-0">
                        <ul class="list-unstyled lh-7 pd-r-10">
                            <li class="d-flex justify-content-between">
                                <span>Sub-Total</span>
                                <span>{{$sub_total}}</span>
                            </li>

                            <li class="d-flex justify-content-between">
                                <span>Other Charges</span>
                                <span>{{$additional_charges}}</span>
                            </li>

                            </li>
                            <li class="d-flex justify-content-between">
                                <strong>Total</strong>
                                <strong>{{$grand_total}}</strong>
                            </li>

                        </ul>
                    </div><!-- col -->
                </div>
                @if((isset($globleSaleBillSetup)) && ($globleSaleBillSetup->description))
                <div class="footer_note">
                    <h6>NOTES :</h6>
                    {{$globleSaleBillSetup->description}}
                </div><!-- footer_note -->
                @endif

                <div class="footer_signature_estimate">

                    <p>SIGNATURE</p>
                </div><!-- footer_signature_estimate -->

            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- content-body -->
</div><!-- content -->


@endsection
@push('scripts')
    <script>
        var v1=0;
        function myFunction() {
            // window.print();
            if (v1==0) {

                window.print();
            }
            else {

                $('#s1').text('Duplicate');
                window.print();
            }
            v1++;


        }
    </script>
    <script type="text/javascript">

        function codespeedy(){
            var print_div = document.getElementById("hello");
            var print_area = window.open();
            print_area.document.write(print_div.innerHTML);
            print_area.document.close();
            print_area.focus();
            print_area.print();
            print_area.close();
// This is the code print a particular div element
        }
    </script>
@endpush
