@extends('admin.layouts.app')
@section('content')


    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Lead</a></li>
                            <li class="breadcrumb-item active" aria-current="page">All Leads</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Leads</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            @if(Auth::guard('admin')->check())
                <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10"  data-toggle="modal" data-target="#exampleModalCenter1">Add Lead</button>
            @else
            @can('lead-create')
                <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10"  data-toggle="modal" data-target="#exampleModalCenter1">Add Lead</button>
            @endcan
            @endif

            <div class="modal fade modal_cust lead_list" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">NEW LEAD</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body body_width">
                            <form action="{{route('lead-management.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
{{--                                @method('put')--}}
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="upload_type">
                                        <img id="blah" src="http://placehold.it/180" alt="your image">

                                    </div><!-- upload_type -->
                                </div><!-- col -->
                                <div class="col-md-9">
                                    <div class="mg-t-5">
                                        Company Name: <input type="text" name="name" class="form-control "  placeholder="Enter Company Name" required>
                                    </div>
                                    <div class="mg-t-5">


                                        Company Address:    <input type="text" name="address" class="form-control " placeholder="Enter Customer Address" value="">
                                    </div>


                                </div><!-- col -->
                                <div class="col-md-12">
                                    <div class="mg-t-5 if_function_input">
                                        <input type="file" name="image" onchange="readURL(this);">
                                    </div>
                                </div><!-- col -->
                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-6 mg-t-5">

                                    Status: <select name="status" class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <option value="select">Select Status</option>
                                        @foreach($lead_setups as $key=> $lead_setup)
                                            @if($lead_setup->status1)
                                        <option value="{{$lead_setup->status1}}">{{$lead_setup->status1}}</option>
                                        @endif
                                            @endforeach
                                    </select>

                                </div><!-- col -->
                                <div class="col-md-6 mg-t-5">

                                    Priority:  <select name="priority" class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <option label="Select Priority"></option>
                                        <option value="h">Hign</option>
                                        <option value="m">Mid</option>
                                        <option value="l">Low</option>
                                    </select>

                                </div><!-- col -->
                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-6 mg-t-5">

                                    Contact Number: <input type="text" name="phone_no" class="form-control " placeholder="Contact No" value="">

                                </div><!-- col -->
                                <div class="col-md-6 mg-t-5">

                                    Email:  <input type="text" name="email" class="form-control " placeholder="Enter Customer Email" value="">

                                </div><!-- col -->

                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-6 mg-t-5">

                                    Source: <select  class="form-control bd bd-gray-900" name="source" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <option value="select">select source</option>
                                        <option value="facebook">Facebook</option>
                                        <option value="other">Other</option>
{{--                                        <option value="audi">Pawan</option>--}}
                                    </select>

                                </div><!-- col -->
                                <div class="col-md-6 mg-t-5">

                                    Assigned To: <select name="added_by" class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <option value="select">Select User</option>
                                        @foreach($user as $user)
                                        <option value="{{$user->id}}"> {{$user->name}}</option>
                                        @endforeach
{{--                                        <option value="opel">Manoj</option>--}}
{{--                                        <option value="audi">Pawan</option>--}}
                                    </select>

                                </div><!-- col -->

                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-6 mg-t-5">

                                    Company Website: <input type="text" name="website" class="form-control " placeholder="https://softechpark.com/" >

                                </div><!-- col -->

                                <div class="col-md-6 mg-t-5">

                                    Follow up Date : <input type="text" name="followUpDate" class="bod-picker form-control">

                                </div><!-- col -->

                                <div class="col-md-12 mg-t-5">
                                    Description: <textarea rows="3" name="description" placeholder="Write something about supplier" class="text_area_custom"></textarea>

                                </div><!-- col -->
                            </div><!-- row -->

                            <div class="modal-footer modal-footer_footer modal-footer-right">
                                <button  class="btn btn-success mg-r-10">Update</button>
                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-2 pd-r-0">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="All Status"></option>
                            <option value="Firefox">New</option>
                            <option value="Chrome">Running</option>
                            <option value="Opera">Lost</option>
                            <option value="Opera">Client</option>
                        </select>
                    </div>
                    <div class="col-sm-2 pd-r-0">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="Assigned To"></option>
                            <option value="Firefox">Employee 1</option>
                            <option value="Chrome">Employee 2</option>
                            <option value="Opera">Employee 3</option>
                            <option value="Opera">Employee 4</option>
                        </select>
                    </div>
                    <div class="col-sm-2 pd-r-0">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="Added By"></option>
                            <option value="Firefox">Employee 1</option>
                            <option value="Chrome">Employee 2</option>
                            <option value="Opera">Employee 3</option>
                            <option value="Opera">Employee 4</option>
                        </select>
                    </div>
                    <div class="col-sm-2 pd-r-0">
                        <input type="text" class="form-control form-control-sm" placeholder="By Follow Up Date">
                    </div>
                    <div class="col-sm-2 pd-r-0">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                    <div class="col-sm mail-navbar bd-b-0 justify-content-end mg-r-10" style="height: auto;">
                        <div class="d-none d-lg-flex">
                            @if(Auth::guard('admin')->check())
                                <a href=""><i data-feather="printer"></i></a>
                            @else
                            @can('lead-print')
                                <a href=""><i data-feather="printer"></i></a>
                            @endcan
                            @endif

                          <a href=""><i data-feather="folder"></i></a>

                          @if(Auth::guard('admin')->check())
                                <a href=""><i class="fas fa-download"></i></a>
                            @else
                            @can('lead-download')
                                <a href=""><i class="fas fa-download"></i></a>
                            @endcan
                            @endif
                        </div>
                    </div>
                </div>
            </div> <!--form-group-->
            <div class="card">
                <table id="example1" class="table  table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
{{--                        <th class="wd-5px"><input type="checkbox" aria-label="Checkbox for following text input"></th>--}}
                        <th class="wd-5px">SN.</th>
                        <th class="wd-10p">Add Date</th>
                        <th class="wd-15p">Lead Name</th>
                        <th class="wd-15p">Address</th>
                        <th class="wd-10p">Follow Date</th>
                        <th class="wd-20p">Added By</th>
                        <th class="wd-20p">TO do</th>
                        <th class="wd-10p">Status</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lead_management as $key =>$lead_management)
                        <tr>
{{--                            <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
                            <td>{{++$key}}</td>
                            <td>{{$lead_management->created_at}}</td>
                            <td>{{$lead_management->name}}</td>
                            <td>{{$lead_management->address}}</td>
                            <td>{{$lead_management->followUpDate}}</td>
                            <td> @if(isset($lead_management->user1))
                                    {{-- {{dd($lead_assign->users->userName)}}--}}
                                    {{$lead_management->user1->name}}
                                    {{--{{$user->name}}--}}
                                @endif</td>
                            <td>@if(isset($lead_management->todo))
                                    {{$lead_management->todo}}
                                    @else
                                    {{$lead_management->comment}}
                                @endif
                            </td>
                            <td>
                                @if($lead_management->status1=='running')
                                <span class="badge badge-pill badge-success wd-70">{{$lead_management->status1}}</span>
                            @endif
                                    @if($lead_management->status1=='new')
                                        <span class="badge badge-pill badge-info wd-70">{{$lead_management->status1}}</span>
                                    @endif
                                    @if($lead_management->status1=='lost')
                                        <span class="badge badge-pill badge-danger wd-70">{{$lead_management->status1}}</span>
                                    @endif
                            </td>
                            <td class="d-md-flex">
                                @if(Auth::guard('admin')->check())
                                    <div class="mg-r-20" title="View"><a href="{{route('lead-management.show',$lead_management->id)}}"><i class="icon ion-clipboard text-success"></i></a></div>
                                @else
                                @can('lead-view')
                                    <div class="mg-r-20" title="View"><a href="{{route('lead-management.show',$lead_management->id)}}"><i class="icon ion-clipboard text-success"></i></a></div>
                                @endcan
                                @endif

                                 @if(Auth::guard('admin')->check())
                                    <div class="mg-r-20" title="Delete"><a href=" " class="delete_data" data-id="{{$lead_management->id}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                                @else
                                @can('lead-delete')
                                    <div class="mg-r-20" title="Delete"><a href=" " class="delete_data" data-id="{{$lead_management->id}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                                @endcan
                                @endif


{{--                                <div class="mg-r-20" title="Delete"><a href=""><i class="icon ion-trash-b text-danger"></i></a></div>--}}





                                {{--                            fot delete popup--}}

                                <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                <div class="conform_del">
                                                    <input type="hidden" class="lead_id321">
                                                    <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                                </div><!-- conform_del -->

                                                <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                                    <a class="final_delete btn btn-success mg-r-10" href="  ">Yes</a>
                                                    <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    @endforeach

                   {{-- for edit start--}}

{{--                    <div class="col-sm-0 tx-right col-lg-7">--}}
{{--                        --}}{{--                    <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10"  data-toggle="modal" data-target="#exampleModalCenter1">Add Lead</button>--}}
{{--                        <div class="modal fade modal_cust lead_list" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">--}}
{{--                            <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">--}}
{{--                                <div class="modal-content">--}}
{{--                                    <div class="modal-header">--}}
{{--                                        <h5 class="modal-title" id="exampleModalCenterTitle">NEW LEAD</h5>--}}
{{--                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                            <span aria-hidden="true">×</span>--}}
{{--                                        </button>--}}
{{--                                    </div>--}}
{{--                                    <div class="modal-body body_width">--}}
{{--                                        <form action="{{route('lead-management.update',$lead_management->id)}}" method="post" enctype="multipart/form-data">--}}
{{--                                            @csrf--}}
{{--                                            @method('put')--}}
{{--                                            <div class="row">--}}
{{--                                                <div class="col-md-3">--}}
{{--                                                    <div class="upload_type">--}}
{{--                                                        <img id="blah" src="http://placehold.it/180" alt="your image">--}}

{{--                                                    </div><!-- upload_type -->--}}
{{--                                                </div><!-- col -->--}}
{{--                                                <div class="col-md-9">--}}
{{--                                                    <div class="mg-t-5">--}}
{{--                                                        Company Name: <input type="text" name="name" class="form-control "  value="{{ old('name', isset($lead_management) ? $lead_management->name : '') }}" placeholder="Enter Company Name" required>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="mg-t-5">--}}


{{--                                                        Company Address:    <input type="text" name="address" class="form-control " value="{{ old('address', isset($lead_management) ? $lead_management->address : '') }}" placeholder="Enter Customer Address" >--}}
{{--                                                    </div>--}}


{{--                                                </div><!-- col -->--}}
{{--                                                <div class="col-md-12">--}}
{{--                                                    <div class="mg-t-5 if_function_input">--}}
{{--                                                        <input type="file" name="image" onchange="readURL(this);">--}}
{{--                                                    </div>--}}
{{--                                                </div><!-- col -->--}}
{{--                                            </div><!-- row -->--}}
{{--                                            <div class="row">--}}
{{--                                                <div class="col-md-6 mg-t-5">--}}

{{--                                                    Status: <select name="status" class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">--}}
{{--                                                        <option value="select" >Select Status</option>--}}
{{--                                                        <option value="running" @if($lead_management->status1 == 'running') selected @endif>Running</option>--}}
{{--                                                        <option value="new" @if($lead_management->status1 == 'new') selected @endif>New</option>--}}
{{--                                                        <option value="lost" @if($lead_management->status1 == 'lost') selected @endif>Lost</option>--}}
{{--                                                    </select>--}}

{{--                                                </div><!-- col -->--}}
{{--                                                <div class="col-md-6 mg-t-5">--}}

{{--                                                    Priority:  <select name="priority" class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">--}}
{{--                                                        <option label="Select Priority">select priority</option>--}}
{{--                                                        <option value="h" @if($lead_management->priority == 'h') selected @endif>Hign</option>--}}
{{--                                                        <option value="m" @if($lead_management->priority == 'm') selected @endif>Mid</option>--}}
{{--                                                        <option value="l"  @if($lead_management->priority == 'l') selected @endif>Low</option>--}}
{{--                                                    </select>--}}

{{--                                                </div><!-- col -->--}}
{{--                                            </div><!-- row -->--}}
{{--                                            <div class="row">--}}
{{--                                                <div class="col-md-6 mg-t-5">--}}

{{--                                                    Contact Number: <input type="text" name="phone_no" class="form-control " value="{{ old('phoneNumber', isset($lead_management) ? $lead_management->phoneNumber : '') }}" placeholder="Contact No" >--}}

{{--                                                </div><!-- col -->--}}
{{--                                                <div class="col-md-6 mg-t-5">--}}

{{--                                                    Email:  <input type="text" name="email" class="form-control " value="{{ old('mailId', isset($lead_management) ? $lead_management->mailId : '') }}" placeholder="Enter Customer Email">--}}

{{--                                                </div><!-- col -->--}}

{{--                                            </div><!-- row -->--}}
{{--                                            <div class="row">--}}
{{--                                                <div class="col-md-6 mg-t-5">--}}

{{--                                                    Source: <select  class="form-control bd bd-gray-900" name="source" aria-label="Example text with button addon" aria-describedby="button-addon1">--}}
{{--                                                        <option value="select">select source</option>--}}
{{--                                                        <option value="facebook" @if($lead_management->source == 'facebook') selected @endif>Facebook</option>--}}
{{--                                                        <option value="other" @if($lead_management->source == 'other') selected @endif>Other</option>--}}
{{--                                                        --}}{{--                                        <option value="audi">Pawan</option>--}}
{{--                                                    </select>--}}

{{--                                                </div><!-- col -->--}}
{{--                                                <div class="col-md-6 mg-t-5">--}}

{{--                                                    Added By: <select name="added_by" class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">--}}
{{--                                                        <option value="select">Select User</option>--}}
{{--                                                        @foreach($user as $user)--}}
{{--                                                            <option value="{{$user->id}}" {{ (isset($lead_management) && $lead_management->user_id==$user->id) ? 'selected': "" }}> {{$user->userName}}</option>--}}
{{--                                                        @endforeach--}}
{{--                                                        --}}{{----}}{{--                                        <option value="opel">Manoj</option>--}}
{{--                                                        --}}{{----}}{{--                                        <option value="audi">Pawan</option>--}}
{{--                                                    </select>--}}

{{--                                                </div><!-- col -->--}}

{{--                                            </div><!-- row -->--}}
{{--                                            <div class="row">--}}
{{--                                                <div class="col-md-12 mg-t-5">--}}

{{--                                                    Company Website: <input type="text" name="website" class="form-control " value="{{ old('website', isset($lead_management) ? $lead_management->website : '') }}"placeholder="https://softechpark.com/" >--}}

{{--                                                </div><!-- col -->--}}
{{--                                                <div class="col-md-12 mg-t-5">--}}
{{--                                                    Description: <textarea rows="3" name="description"  placeholder="Write something about supplier" class="text_area_custom"> {{ old('comment', isset($lead_management) ? $lead_management->comment : '') }}</textarea>--}}

{{--                                                </div><!-- col -->--}}
{{--                                            </div><!-- row -->--}}

{{--                                            <div class="modal-footer modal-footer_footer modal-footer-right">--}}
{{--                                                <button  class="btn btn-success mg-r-10">Update</button>--}}
{{--                                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>--}}
{{--                                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
{{--                                                    <button type="button" class="btn btn-primary">Save changes</button> -->--}}
{{--                                            </div>--}}
{{--                                        </form>--}}
{{--                                    </div>--}}

{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div><!-- col -->--}}



                    {{--end of edit --}}

                    {{--                    <tr>--}}
                    {{--                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
                    {{--                        <td>2</td>--}}
                    {{--                        <td>2076/12/15</td>--}}
                    {{--                        <td>Company Name</td>--}}
                    {{--                        <td>Tinkune, Kathmandu</td>--}}
                    {{--                        <td>2076/12/15</td>--}}
                    {{--                        <td>Bhagwan Das Agrahari</td>--}}
                    {{--                        <td>--}}
                    {{--                            <span class="badge badge-pill badge-info wd-70">New</span>--}}
                    {{--                        </td>--}}
                    {{--                        <td class="d-md-flex">--}}
                    {{--                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>--}}
                    {{--                            <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>--}}
                    {{--                            <div class="mg-r-20" title="Delete"><a href=""><i class="icon ion-trash-b text-danger"></i></a></div>--}}
                    {{--                        </td>--}}
                    {{--                    </tr>--}}
                    {{--                    <tr>--}}
                    {{--                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
                    {{--                        <td>3</td>--}}
                    {{--                        <td>2076/12/15</td>--}}
                    {{--                        <td>Company Name</td>--}}
                    {{--                        <td>Tinkune, Kathmandu</td>--}}
                    {{--                        <td>2076/12/15</td>--}}
                    {{--                        <td>Bhagwan Das Agrahari</td>--}}
                    {{--                        <td>--}}
                    {{--                            <span class="badge badge-pill badge-danger wd-70">Lost</span>--}}
                    {{--                        </td>--}}
                    {{--                        <td class="d-md-flex">--}}
                    {{--                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>--}}
                    {{--                            <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>--}}
                    {{--                            <div class="mg-r-20" title="Delete"><a href=""><i class="icon ion-trash-b text-danger"></i></a></div>--}}
                    {{--                        </td>--}}
                    {{--                    </tr>--}}
                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
    </div><!-- content -->




@endsection

@push('scripts')


    <script>

        //for the delete data
        $(".delete_data").click(function(e){
            e.preventDefault()

            var lead_id = $(this).data('id');
            // alert(customer_id);


            $.ajax({
                type: 'POST',
                url: '{{url('/lead-edit')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    lead_id : lead_id,


                },
                success: function (data, status) {

                    if(data.error){

                        return;
                    }

                    // $('#show_modal').modal('hide');
                    // toastr.success(data.success);

                    // console.log(data.customer.fullName);
                    // window.location.reload();
                    // alert('test');

                    $('.lead_id321').append($('.lead_id321').val(data.lead.id));


                },
                error: function (xhr, status, error) {
                    // console.log('error');
                    // toastr.error(error.errors);
                }
            });

        });


        //for final delete
        $(".final_delete").click(function(e){
            e.preventDefault()
            // alert('vd');
            var lead_id= $(".lead_id321").val();
            // var lead_id = $(this).data('id');


            // alert(lead_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/lead-delete')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    lead_id : lead_id,


                },
                success: function (data, status) {

                    if(data.errors){
                        toastr.error(data.errors);
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);



                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // toastr.error(data.errors);
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });

    </script>

@endpush
