@extends('admin.layouts.app')
@section('content')


    {{--<!DOCTYPE html>--}}
{{--<html lang="en">--}}
{{--<head>--}}

{{--    <!-- Required meta tags -->--}}
{{--    <meta charset="utf-8">--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">--}}

{{--    <!-- Meta -->--}}
{{--    <meta name="description" content="Responsive Bootstrap 4 Dashboard and Admin Template">--}}
{{--    <meta name="author" content="ThemePixels">--}}

{{--    <!-- Favicon -->--}}
{{--    <link rel="shortcut icon" type="image/x-icon" href="../assets/img/favicon.png">--}}

{{--    <title>Cassie Responsive Bootstrap 4 Dashboard and Admin Template</title>--}}

{{--    <!-- vendor css -->--}}
{{--    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">--}}
{{--    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">--}}
{{--    <link href="../lib/prismjs/themes/prism-tomorrow.css" rel="stylesheet">--}}
{{--    <link href="../lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">--}}
{{--    <link href="../lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">--}}
{{--    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">--}}

{{--    <!-- template css -->--}}
{{--    <link rel="stylesheet" href="../assets/css/cassie.css">--}}

{{--</head>--}}
{{--<body data-spy="scroll" data-target="#navSection" data-offset="100">--}}

{{--<div class="sidebar">--}}
{{--    <div class="sidebar-header">--}}
{{--        <div>--}}
{{--            <a href="../index.html" class="sidebar-logo"><span>cassie</span></a>--}}
{{--            <small class="sidebar-logo-headline">Responsive Dashboard Template</small>--}}
{{--        </div>--}}
{{--    </div><!-- sidebar-header -->--}}
{{--    <div id="dpSidebarBody" class="sidebar-body">--}}
{{--        <ul class="nav nav-sidebar">--}}
{{--            <li class="nav-label"><label class="content-label">Template Pages</label></li>--}}
{{--            <li class="nav-item show">--}}
{{--                <a href="../pages/dashboard-two.html" class="nav-link"><i data-feather="box"></i> Dashboard</a>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="layout"></i> Sales</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../pages/app-newSales.html" class="nav-sub-link">New Sales</a>--}}
{{--                    <a href="../pages/app-allSales.html" class="nav-sub-link">All Sales</a>--}}
{{--                    <a href="#" class="nav-sub-link">Sales Return</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="lock"></i> Purchase</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="page-signin.html" class="nav-sub-link">New Purchase</a>--}}
{{--                    <a href="page-signup.html" class="nav-sub-link">All Purchase</a>--}}
{{--                    <a href="page-forgot.html" class="nav-sub-link">Purchase Return</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="user"></i> Customer</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../newPages/newCustomer.html" class="nav-sub-link">New Customer</a>--}}
{{--                    <a href="page-timeline.html" class="nav-sub-link">All Customer</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="file-text"></i> Supplier</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="page-invoice.html" class="nav-sub-link">New Supplier</a>--}}
{{--                    <a href="page-pricing.html" class="nav-sub-link">All Supplier</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="x-circle"></i> Product and Services</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="page-404.html" class="nav-sub-link">Product</a>--}}
{{--                    <a href="page-404.html" class="nav-sub-link">Damage Product</a>--}}
{{--                    <a href="page-500.html" class="nav-sub-link">Catagories</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link"><i data-feather="layers"></i> Stock</a>--}}
{{--            </li>--}}
{{--            <li class="nav-label"><label class="content-label">Admin</label></li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="life-buoy"></i> Account</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/form-elements.html" class="nav-sub-link">Expense</a>--}}
{{--                    <a href="../components/form-input-group.html" class="nav-sub-link">Income</a>--}}
{{--                    <a href="../components/form-input-tags.html" class="nav-sub-link">Pay-in Receipt</a>--}}
{{--                    <a href="../components/form-input-masks.html" class="nav-sub-link">Pay-out Receipt</a>--}}
{{--                    <a href="../components/form-validation.html" class="nav-sub-link">Account Head</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="book"></i> Branch</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/con-grid.html" class="nav-sub-link">Branches</a>--}}
{{--                    <a href="../components/con-icons.html" class="nav-sub-link">Stock Transfer</a>--}}
{{--                    <a href="../components/con-images.html" class="nav-sub-link">Employee Transfer</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="layers"></i> Human Resource</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/com-accordion.html" class="nav-sub-link">Employee</a>--}}
{{--                    <a href="../components/com-alerts.html" class="nav-sub-link">Salary Payroll</a>--}}
{{--                    <a href="../components/com-avatar.html" class="nav-sub-link">Attendance</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="monitor"></i> Report</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/util-animation.html" class="nav-sub-link">Sales Report</a>--}}
{{--                    <a href="../components/util-background.html" class="nav-sub-link">Purchase Report</a>--}}
{{--                    <a href="../components/util-border.html" class="nav-sub-link">Expense Report</a>--}}
{{--                    <a href="../components/util-display.html" class="nav-sub-link">Income Report</a>--}}
{{--                    <a href="../components/util-divider.html" class="nav-sub-link">Payin Report</a>--}}
{{--                    <a href="../components/util-flex.html" class="nav-sub-link">Payout Report</a>--}}
{{--                    <a href="../components/util-height.html" class="nav-sub-link">Payment in/out</a>--}}
{{--                    <a href="../components/util-margin.html" class="nav-sub-link">Stock</a>--}}
{{--                    <a href="../components/util-padding.html" class="nav-sub-link">Customer Ledger</a>--}}
{{--                    <a href="../components/util-position.html" class="nav-sub-link">Supplier Ledger</a>--}}
{{--                    <a href="../components/util-typography.html" class="nav-sub-link">Account Ledger</a>--}}
{{--                    <a href="../components/util-width.html" class="nav-sub-link">Customer Due Report</a>--}}
{{--                    <a href="../components/util-extras.html" class="nav-sub-link">Supplier Due Report</a>--}}
{{--                    <a href="../components/util-extras.html" class="nav-sub-link">Profit/loss Report</a>--}}
{{--                    <a href="../components/util-extras.html" class="nav-sub-link">All Bills</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="pie-chart"></i> Configuration</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/chart-flot.html" class="nav-sub-link">General Setting</a>--}}
{{--                    <a href="../components/chart-chartjs.html" class="nav-sub-link">Payment Method</a>--}}
{{--                    <a href="../components/chart-peity.html" class="nav-sub-link">User</a>--}}
{{--                    <a href="../components/com-badge.html" class="nav-sub-link">Role and Premission</a>--}}
{{--                    <a href="../components/chart-peity.html" class="nav-sub-link">Bank</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--        </ul>--}}

{{--        <hr class="mg-t-30 mg-b-25">--}}

{{--        <ul class="nav nav-sidebar">--}}
{{--            <li class="nav-item"><a href="../pages/themes.html" class="nav-link"><i data-feather="aperture"></i> Themes</a></li>--}}
{{--            <li class="nav-item"><a href="../docs.html" class="nav-link"><i data-feather="help-circle"></i> Documentation</a></li>--}}
{{--        </ul>--}}


{{--    </div><!-- sidebar-body -->--}}
{{--</div><!-- sidebar -->--}}

{{--<div class="content">--}}
{{--    <div class="header">--}}
{{--        <div class="header-left">--}}
{{--            <a href="" class="burger-menu"><i data-feather="menu"></i></a>--}}

{{--            <div class="header-search">--}}
{{--                <i data-feather="search"></i>--}}
{{--                <input type="search" class="form-control" placeholder="What are you looking for?">--}}
{{--            </div><!-- header-search -->--}}
{{--        </div><!-- header-left -->--}}

{{--        <div class="header-right">--}}
{{--            <a href="" class="header-help-link"><i data-feather="help-circle"></i></a>--}}
{{--            <div class="dropdown dropdown-notification">--}}
{{--                <a href="" class="dropdown-link new" data-toggle="dropdown"><i data-feather="bell"></i></a>--}}
{{--                <div class="dropdown-menu dropdown-menu-right">--}}
{{--                    <div class="dropdown-menu-header">--}}
{{--                        <h6>Notifications</h6>--}}
{{--                        <a href=""><i data-feather="more-vertical"></i></a>--}}
{{--                    </div><!-- dropdown-menu-header -->--}}
{{--                    <div class="dropdown-menu-body">--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle text-primary bg-primary-light">s</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Socrates Itumay</strong> marked the task as completed.</p>--}}
{{--                                <span>5 hours ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-pink bg-pink-light">r</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Reynante Labares</strong> marked the task as incomplete.</p>--}}
{{--                                <span>8 hours ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-success bg-success-light">d</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Dyanne Aceron</strong> responded to your comment on this <strong>post</strong>.</p>--}}
{{--                                <span>a day ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-indigo bg-indigo-light">k</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Kirby Avendula</strong> marked the task as incomplete.</p>--}}
{{--                                <span>2 days ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div><!-- dropdown-menu-body -->--}}
{{--                    <div class="dropdown-menu-footer">--}}
{{--                        <a href="">View All Notifications</a>--}}
{{--                    </div>--}}
{{--                </div><!-- dropdown-menu -->--}}
{{--            </div>--}}
{{--            <div class="dropdown dropdown-loggeduser">--}}
{{--                <a href="" class="dropdown-link" data-toggle="dropdown">--}}
{{--                    <div class="avatar avatar-sm">--}}
{{--                        <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">--}}
{{--                    </div><!-- avatar -->--}}
{{--                </a>--}}
{{--                <div class="dropdown-menu dropdown-menu-right">--}}
{{--                    <div class="dropdown-menu-header">--}}
{{--                        <div class="media align-items-center">--}}
{{--                            <div class="avatar">--}}
{{--                                <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">--}}
{{--                            </div><!-- avatar -->--}}
{{--                            <div class="media-body mg-l-10">--}}
{{--                                <h6>Louise Kate Lumaad</h6>--}}
{{--                                <span>Administrator</span>--}}
{{--                            </div>--}}
{{--                        </div><!-- media -->--}}
{{--                    </div>--}}
{{--                    <div class="dropdown-menu-body">--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="user"></i> View Profile</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="edit-2"></i> Edit Profile</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="briefcase"></i> Account Settings</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="shield"></i> Privacy Settings</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="log-out"></i> Sign Out</a>--}}
{{--                    </div>--}}
{{--                </div><!-- dropdown-menu -->--}}
{{--            </div>--}}
{{--        </div><!-- header-right -->--}}
{{--    </div><!-- header -->--}}
    <div class="content-header justify-content-between">
        <div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Lead</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Leads</li>
                </ol>
            </nav>
            <h4 class="content-title content-title-xs">All Leads</h4>
        </div>
    </div><!-- content-header -->
    <div class="content-body content-body-calendar" style="margin-top: -50px;">
        <hr>
        <form action="{{route('lead-management.search')}}" method="post" enctype="multipart/form-data">
            @csrf
        <div class="row">
{{--            <form action="{{route('lead-management.search')}}" method="post" enctype="multipart/form-data">--}}
            <div class="col-3 form-group">
                <input type="text" name="date" class="bod-picker form-control form-control-sm" placeholder="Follow Up Date" >
            </div><!-- form-group -->
            <div class="col-3 form-group">
                <select name="priority" class="form-control form-control-sm select2-no-search">
                    <option label="Select Priority"></option>
                    <option value="h">High</option>
                    <option value="m">Mid</option>
                    <option value="l">Low</option>
                </select>
            </div><!-- form-group -->
            <div class="col-3 form-group">
                <select name="lead" class="form-control form-control-sm select2-no-search">
                    <option  value="u" label="Usable"></option>
                    <option value="un">Unusable</option>
                    <option value="a">All Lead</option>
                </select>
            </div><!-- form-group -->
            <button class="btn btn-brand-02">search</button>
{{--            </form>--}}
        </div>
        </form>
        <hr class="mg-0">

        <div class="row pd-sm-r-15">
            @foreach($lead_management as $key =>$lead_management)
            <div class="col-md-6 col-xl-4 mg-t-15 mg-sm-t-20 pd-sm-r-0">
                <div class="card card-hover card-project-two">
                    <div class="card-header bg-transparent">
                        <div>
                            <h6 class="mg-b-5"><a href="#" class="tx-black">{{$lead_management->name}}</a></h6>
                            <span>Uplode: {{$lead_management->created_at}} <br> Follow: {{$lead_management->followUpDate}}</span>
                            <span>Assigned to:
                              @if(isset($lead_management->users))
                            {{-- {{dd($lead_assign->users->userName)}}--}}
                            {{$lead_management->users->userName}}
                            {{--{{$user->name}}--}}
                            @endif
                            </span>
                        </div>
                        <nav class="nav nav-card-icon">
                            <a href=""><i data-feather="check-square" class="svg-14"></i> 21</a>
                            <a href=""><i data-feather="message-square" class="svg-14"></i> 85</a>
                        </nav>
                    </div><!-- card-header -->
                    <div class="card-body pd-t-10">
                        <p class="tx-13 tx-gray-700"><strong class="tx-bold">To Do:</strong> {{$lead_management->todo}}</p>
                        <div class="card-progress">
                            <label class="content-label mg-b-0">Priority</label>
                            <div class="progress">
                                <div class="progress-bar bg-danger wd-25p" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <label class="content-label mg-b-0">25%</label>
                        </div>

                    </div>
                </div><!-- card -->

            </div><!-- col -->
            @endforeach
{{--            <div class="col-md-6 col-xl-4 mg-t-15 mg-sm-t-20 pd-sm-r-0">--}}
{{--                <div class="card card-hover card-project-two">--}}
{{--                    <div class="card-header bg-transparent">--}}
{{--                        <div>--}}
{{--                            <h6 class="mg-b-5"><a href="#" class="tx-black">Softechpark Pvt. Ltd.</a></h6>--}}
{{--                            <span>Uplode: Yesterday 10:15am | Follow: Today</span>--}}
{{--                            <span>Assigned to: Kiran Kandel</span>--}}
{{--                        </div>--}}
{{--                        <nav class="nav nav-card-icon">--}}
{{--                            <a href=""><i data-feather="check-square" class="svg-14"></i> 21</a>--}}
{{--                            <a href=""><i data-feather="message-square" class="svg-14"></i> 85</a>--}}
{{--                        </nav>--}}
{{--                    </div><!-- card-header -->--}}
{{--                    <div class="card-body pd-t-10">--}}
{{--                        <p class="tx-13 tx-gray-700"><strong class="tx-bold">To Do:</strong> Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deser.</p>--}}
{{--                        <div class="card-progress">--}}
{{--                            <label class="content-label mg-b-0">Priority</label>--}}
{{--                            <div class="progress">--}}
{{--                                <div class="progress-bar bg-warning wd-60p" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                            </div>--}}
{{--                            <label class="content-label mg-b-0">25%</label>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div><!-- card -->--}}
{{--            </div><!-- col -->--}}
{{--            <div class="col-md-6 col-xl-4 mg-t-15 mg-sm-t-20 pd-sm-r-0">--}}
{{--                <div class="card card-hover card-project-two">--}}
{{--                    <div class="card-header bg-transparent">--}}
{{--                        <div>--}}
{{--                            <h6 class="mg-b-5"><a href="#" class="tx-black">Softechpark Pvt. Ltd.</a></h6>--}}
{{--                            <span>Uplode: Yesterday 10:15am | Follow: Today</span>--}}
{{--                            <span>Assigned to: Kiran Kandel</span>--}}
{{--                        </div>--}}
{{--                        <nav class="nav nav-card-icon">--}}
{{--                            <a href=""><i data-feather="check-square" class="svg-14"></i> 21</a>--}}
{{--                            <a href=""><i data-feather="message-square" class="svg-14"></i> 85</a>--}}
{{--                        </nav>--}}
{{--                    </div><!-- card-header -->--}}
{{--                    <div class="card-body pd-t-10">--}}
{{--                        <p class="tx-13 tx-gray-700"><strong class="tx-bold">To Do:</strong> Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deser.</p>--}}
{{--                        <div class="card-progress">--}}
{{--                            <label class="content-label mg-b-0">Priority</label>--}}
{{--                            <div class="progress">--}}
{{--                                <div class="progress-bar bg-green wd-70p" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                            </div>--}}
{{--                            <label class="content-label mg-b-0">70%</label>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div><!-- card -->--}}
{{--            </div><!-- col -->--}}
        </div><!--row-->
{{--            @endforeach--}}
    </div>
{{--</div><!-- content -->--}}



{{--<script src="../lib/jquery/jquery.min.js"></script>--}}
{{--<script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>--}}
{{--<script src="../lib/feather-icons/feather.min.js"></script>--}}
{{--<script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>--}}
{{--<script src="../lib/prismjs/prism.js"></script>--}}
{{--<script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>--}}
{{--<script src="../lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>--}}
{{--<script src="../lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>--}}
{{--<script src="../lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>--}}
{{--<script src="../lib/select2/js/select2.min.js"></script>--}}
{{--<script src="../lib/js-cookie/js.cookie.js"></script>--}}

{{--<script src="../assets/js/cassie.js"></script>--}}
{{--<script>--}}
{{--    $(function(){--}}
{{--        'use strict'--}}

{{--        $('#example1').DataTable({--}}
{{--            language: {--}}
{{--                searchPlaceholder: 'Search...',--}}
{{--                sSearch: '',--}}
{{--                lengthMenu: '_MENU_ items/page',--}}
{{--            }--}}
{{--        });--}}

{{--        $('#example2').DataTable({--}}
{{--            responsive: true,--}}
{{--            language: {--}}
{{--                searchPlaceholder: 'Search...',--}}
{{--                sSearch: '',--}}
{{--                lengthMenu: '_MENU_ items/page',--}}
{{--            }--}}
{{--        });--}}

{{--        $('#example8').DataTable({--}}
{{--            responsive: true,--}}
{{--            language: {--}}
{{--                searchPlaceholder: 'Search...',--}}
{{--                sSearch: '',--}}
{{--                lengthMenu: '_MENU_ items/page',--}}
{{--            }--}}
{{--        });--}}



{{--        // Select2--}}
{{--        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });--}}

{{--    });--}}
{{--</script>--}}
{{--</body>--}}
{{--</html>--}}
@endsection
