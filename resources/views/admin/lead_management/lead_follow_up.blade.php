@extends('admin.layouts.app')
@section('content')


    <link rel="stylesheet" href="{{asset('assets/css/changes.css')}}">

    <div class="content-header justify-content-between">
        <div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Lead</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Follow Up</li>
                </ol>
            </nav>
            <h4 class="content-title content-title-xs">Follow Up Leads</h4>
        </div>
    </div><!-- content-header -->
    <div class="content-body content-body-calendar" style="margin-top: -50px;">
        <hr>
        <form action="{{route('lead-followup.search')}}" method="post" enctype="multipart/form-data">
            @csrf
        <div class="row">
            <div class="col-3 form-group">
                <input type="text" name="date" class="bod-picker form-control form-control-sm" placeholder="Follow Up Date" >
            </div><!-- form-group -->
            <div class="col-3 form-group">
                <select class="form-control form-control-sm select2-no-search" name="priority">
                    <option label="Select Priority"></option>
                    <option value="h">Hign</option>
                    <option value="m">Mid</option>
                    <option value="l">Low</option>
                </select>
            </div><!-- form-group -->
            <button style="float: right" class="btn btn-brand-02">search</button>
        </div>

        </form>
        <hr class="mg-0">
        <div class="row pd-sm-r-15">
            @foreach($lead_management as $key =>$lead_management)
            <div class="col-md-6 col-xl-4 mg-t-15 mg-sm-t-20 pd-sm-r-0">
                <div class="card card-hover card-project-two">
                    <div class="card-header bg-transparent">
                        <div>
                            <h6 class="mg-b-5"><a href="#" class="tx-black" id="lead-detail">{{$lead_management->name}}</a></h6>
                            <span>Uplode: {{$lead_management->created_at}}|<br>Follow: {{$lead_management->followUpDate}}</span>
                            <span>Assign: <a href="#" id="popup-container">

{{--                                    @foreach($lead_assign as $lead_assign)--}}
                                    @if(isset($lead_management->users))
                                        {{--                                        {{dd($lead_assign->users->userName)}}--}}
                                        {{$lead_management->users->userName}}
                                        {{--                                    {{$user->name}}--}}
                                    @endif
                                </a></span >
                        </div>
                        <nav class="nav nav-card-icon">
                            <a href=""><i data-feather="check-square" class="svg-14"></i> 21</a>
                            <a href=""><i data-feather="message-square" class="svg-14"></i> 85</a>
                        </nav>
                    </div><!-- card-header -->
                    <div class="card-body pd-t-10">
                        <p class="tx-13 tx-gray-700"><strong class="tx-bold">To Do:</strong>{{$lead_management->todo}}</p>
                        <div class="card-progress">
                            <label class="content-label mg-b-0">Priority</label>
                            <div class="progress">
                                <div class="progress-bar bg-danger wd-25p" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <label class="content-label mg-b-0">25%</label>
                        </div>

                    </div>
                </div><!-- card -->
            </div><!-- col -->
            @endforeach
{{--            <div class="col-md-6 col-xl-4 mg-t-15 mg-sm-t-20 pd-sm-r-0">--}}
{{--                <div class="card card-hover card-project-two">--}}
{{--                    <div class="card-header bg-transparent">--}}
{{--                        <div>--}}
{{--                            <h6 class="mg-b-5" id="employee-search"><a href="#" class="tx-black" id="lead-detail">Softechpark Pvt. Ltd.</a></h6>--}}
{{--                            <span>Uplode: Yesterday 10:15am | Follow: Today</span>--}}
{{--                            <span>Assign: Kiran Kandel</span>--}}
{{--                        </div>--}}
{{--                        <nav class="nav nav-card-icon">--}}
{{--                            <a href=""><i data-feather="check-square" class="svg-14"></i> 21</a>--}}
{{--                            <a href=""><i data-feather="message-square" class="svg-14"></i> 85</a>--}}
{{--                        </nav>--}}
{{--                    </div><!-- card-header -->--}}
{{--                    <div class="card-body pd-t-10">--}}
{{--                        <p class="tx-13 tx-gray-700"><strong class="tx-bold">To Do:</strong> Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deser.</p>--}}
{{--                        <div class="card-progress">--}}
{{--                            <label class="content-label mg-b-0">Priority</label>--}}
{{--                            <div class="progress">--}}
{{--                                <div class="progress-bar bg-warning wd-60p" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                            </div>--}}
{{--                            <label class="content-label mg-b-0">25%</label>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div><!-- card -->--}}
{{--            </div><!-- col -->--}}
{{--            <div class="col-md-6 col-xl-4 mg-t-15 mg-sm-t-20 pd-sm-r-0">--}}
{{--                <div class="card card-hover card-project-two">--}}
{{--                    <div class="card-header bg-transparent">--}}
{{--                        <div>--}}
{{--                            <h6 class="mg-b-5"><a href="#" class="tx-black">Softechpark Pvt. Ltd.</a></h6>--}}
{{--                            <span>Uplode: Yesterday 10:15am | Follow: Today</span>--}}
{{--                            <span>Assign: <a href="">Kiran Kandel</a></span>--}}
{{--                        </div>--}}
{{--                        <nav class="nav nav-card-icon">--}}
{{--                            <a href=""><i data-feather="check-square" class="svg-14"></i> 21</a>--}}
{{--                            <a href=""><i data-feather="message-square" class="svg-14"></i> 85</a>--}}
{{--                        </nav>--}}
{{--                    </div><!-- card-header -->--}}
{{--                    <div class="card-body pd-t-10">--}}
{{--                        <p class="tx-13 tx-gray-700"><strong class="tx-bold">To Do:</strong> Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deser.</p>--}}
{{--                        <div class="card-progress">--}}
{{--                            <label class="content-label mg-b-0">Priority</label>--}}
{{--                            <div class="progress">--}}
{{--                                <div class="progress-bar bg-green wd-70p" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                            </div>--}}
{{--                            <label class="content-label mg-b-0">70%</label>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div><!-- card -->--}}
{{--            </div><!-- col -->--}}
        </div><!--row-->
    </div>
</div><!-- content -->

<div class="lead-detail wd-90p" style="z-index: 999;">
    <div class="content-body-calendar wd-100p bd bg-white pd-b-0">
        <h4># Softechpark Pvt. Ltd.</h4>
        <div class="close closeregister" id="lead-detail-close">+</div>
        <div class="row">
            <div class="col-lg-3 pd-sm-r-0">
                <div class="card">
                    <div class="profile-sidebar-body pd-10">
{{--                        @foreach($lead_management as $key =>$lead_management)--}}
                        <label class="content-label">Contact Information</label>
                        <ul class="list-unstyled profile-info-list mg-b-0">
                            <li><i data-feather="briefcase"></i> <span class="tx-color-03">{{$lead_management->address}}</span></li>
                            <li><i class="fas fa-user"></i> <span class="tx-color-03">&nbsp; {{$lead_management->name}}</span></li>
                            <li><i data-feather="phone"></i> <a href="">{{$lead_management->phoneNumber}}</a></li>
                            <li><i data-feather="mail"></i> <a href="">{{$lead_management->mailId}}</a></li>
                            <li><i data-feather="globe"></i><a href="">{{$lead_management->website}}</a></li>
                        </ul>
{{--                        @endforeach--}}
                        <hr>
                        <form action="{{route('lead-management.lead_followup_store')}}" method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group mg-b-10">
                                <label class="content-label tx-12 tx-bold mg-b-5">Follow-Up Report</label>
                                <textarea name="follow_up_report" class="form-control form-control-sm" rows="3" placeholder="Write something about Customer"required></textarea>
                            </div><!-- form-group -->
                            <div class="form-group mg-b-10">
                                <label class="content-label tx-12 tx-bold mg-b-5">Next Follow-Up Date</label>
                                <input type="text" name="next_followup_date" class="bod-picker form-control form-control-sm" placeholder="select date">
                            </div><!-- form-group -->
                            <div class="form-group mg-b-10">
                                <label class="content-label tx-12 tx-bold mg-b-5">To Do</label>
                                <textarea  name="todo" class="form-control form-control-sm" rows="2" placeholder="Write something about Customer" required></textarea>
                            </div><!-- form-group -->
                            <div class="form-group">
                                <button class="btn btn-sm btn-danger float-right">Not Req.</button>
                                <button class="btn btn-sm btn-success float-right mg-r-10">Converted</button>
                                <button class="btn btn-sm btn-info float-right mg-r-10">Update</button>
                            </div>

                        </form>
                    </div>

                    <!-- <span class="pd-5 tx-color-04"><u>Contact Detail</u></span>
                    <ul>
                        <li>Phone: <span>9867003419</span></li>
                        <li>Email: <span>agrahari@softechpark.com</span></li>
                        <li>Website: <span>www.softechpark.com</span></li>
                    </ul> -->
                </div>
            </div>
            <div class="col-lg-9 mg-t-10 mg-sm-t-0 ht-550">
                <div class="mail-body-content">
                    <div class="mail-navbar">
                        <div class="d-flex align-items-center">
                            <div class="tx-14 tx-color-04 mg-lg-l-15">
                                <span>Sort by:</span>
                                <a href="#" class="link-01">Newest Report</a>
                            </div>
                        </div>
                        <div class="tx-primary">
                            <span>Assign:</span>
                            <a href="#" class="link-01">
                                @if(isset($lead_management->users))
                                    {{-- {{dd($lead_assign->users->userName)}}--}}
                                    {{$lead_management->users->userName}}
                                    {{--{{$user->name}}--}}
                                @endif
                            </a>
                        </div>
                    </div><!-- mail-navbar -->
                    <ul class="mail-list">
{{--                        <li class="mail-item unread">--}}
{{--                            <div class="avatar"><img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt=""></div>--}}
{{--                            <div class="mail-item-body">--}}
{{--                                <div>--}}
{{--                                    <span>Molly Manson</span>--}}
{{--                                    <span>2076/08/28 | 11:15am</span>--}}
{{--                                </div>--}}
{{--                                <p class="tx-14"><strong class="tx-primary">Follow-Report:</strong> Simple And Best Responsive Web Design Using CSS</p>--}}
{{--                                <span class="tx-14 tx-success mg-l-10 mg-sm-l-30"><u>To Do:</u></span>--}}
{{--                                <div class="d-sm-flex mg-sm-r-60">--}}
{{--                                    <div class="card pd-5 mg-l-10 mg-sm-l-30 tx-semibold">Phone Call </div>--}}
{{--                                    <span class="pd-5 mg-l-5 mg-sm-l-0 tx-12">2076/08/28</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </li>--}}
                        <li class="mail-item unread">
                            <div class="avatar"><img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt=""></div>
                            <div class="mail-item-body">
{{--                                @foreach($lead_management as $key =>$lead_management)--}}
                                <div>
                                    <span>{{$lead_management->name}}</span>
                                    <span>{{$lead_management->created_at}}</span>
                                </div>
{{--                                @endforeach--}}
                                @foreach($comment as $comment)
                                <p class="tx-14"><strong class="tx-primary">Follow-Report:</strong>{{$comment->followup_report}}</p>
                                <span class="tx-14 tx-success mg-l-10 mg-sm-l-30"><u>To Do:</u></span>
                                <div class="d-sm-flex mg-sm-r-60">
                                    <div class="card pd-5 mg-l-10 mg-sm-l-30 tx-semibold">{{$comment->nextTodo}} </div>
                                    <span class="pd-5 mg-l-5 mg-sm-l-0 tx-12">{{$comment->nextFollowUpDate}} </span>
                                </div>
                                @endforeach
                            </div>
                        </li>
{{--                        <li class="mail-item unread">--}}
{{--                            <div class="avatar"><img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt=""></div>--}}
{{--                            <div class="mail-item-body">--}}
{{--                                <div>--}}
{{--                                    <span>Molly Manson</span>--}}
{{--                                    <span>2076/08/28 | 11:15am</span>--}}
{{--                                </div>--}}
{{--                                <p class="tx-14"><strong class="tx-primary">Follow-Report:</strong> Simple And Best Responsive Web Design Using CSS</p>--}}
{{--                                <span class="tx-14 tx-success mg-l-10 mg-sm-l-30"><u>To Do:</u></span>--}}
{{--                                <div class="d-sm-flex mg-sm-r-60">--}}
{{--                                    <div class="card pd-5 mg-l-10 mg-sm-l-30 tx-semibold">Phone Call frdghdugut sjfgdsufyuyge ysfugdsfuersh syufgdsuigd fsfhusfhref iusdfiufh sdjcfdiu hdfsfusirgf nbsf </div>--}}
{{--                                    <span class="pd-5 mg-l-5 mg-sm-l-0 tx-12">2076/08/28</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </li>--}}
{{--                        <li class="mail-item unread">--}}
{{--                            <div class="avatar"><img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt=""></div>--}}
{{--                            <div class="mail-item-body">--}}
{{--                                <div>--}}
{{--                                    <span>Molly Manson</span>--}}
{{--                                    <span>2076/08/28 | 11:15am</span>--}}
{{--                                </div>--}}
{{--                                <p class="tx-14"><strong class="tx-primary">Follow-Report:</strong> Simple And Best Responsive Web Design Using CSS</p>--}}
{{--                                <span class="tx-14 tx-success mg-l-10 mg-sm-l-30"><u>To Do:</u></span>--}}
{{--                                <div class="d-sm-flex mg-sm-r-60">--}}
{{--                                    <div class="card pd-5 mg-l-10 mg-sm-l-30 tx-semibold">Phone Call frdghdugut sjfgdsufyuyge ysfugdsfuersh syufgdsuigd fsfhusfhref iusdfiufh sdjcfdiu hdfsfusirgf nbsf </div>--}}
{{--                                    <span class="pd-5 mg-l-5 mg-sm-l-0 tx-12">2076/08/28</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </li>--}}
{{--                        <li class="mail-item unread">--}}
{{--                            <div class="avatar"><img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt=""></div>--}}
{{--                            <div class="mail-item-body">--}}
{{--                                <div>--}}
{{--                                    <span>Molly Manson</span>--}}
{{--                                    <span>2076/08/28 | 11:15am</span>--}}
{{--                                </div>--}}
{{--                                <p class="tx-14"><strong class="tx-primary">Follow-Report:</strong> Simple And Best Responsive Web Design Using CSS</p>--}}
{{--                                <span class="tx-14 tx-success mg-l-10 mg-sm-l-30"><u>To Do:</u></span>--}}
{{--                                <div class="d-sm-flex mg-sm-r-60">--}}
{{--                                    <div class="card pd-5 mg-l-10 mg-sm-l-30 tx-semibold">Phone Call frdghdugut sjfgdsufyuyge ysfugdsfuersh syufgdsuigd fsfhusfhref iusdfiufh sdjcfdiu hdfsfusirgf nbsf </div>--}}
{{--                                    <span class="pd-5 mg-l-5 mg-sm-l-0 tx-12">2076/08/28</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </li>--}}

                    </ul>
                </div><!-- mail-body-content -->

            </div>
        </div>
    </div><!-- content-body -->
</div>
</div>
<div class="popup-container wp-100p" style="z-index: 999;">
    <div class="wd-sm-30p mg-t-auto mg-b-auto">
        <div class="card">
            <div class="card-header tx-medium pd-10">Featured</div>
            <div class="close closeregister" id="popup-close">+</div>
            <div class="card-body">
                <form action="">
                    <h6 class="tx-uppercase tx-12 tx-gray-900 mg-b-10">Assigned To: Kiran Kandel</h6>
                    <label class="content-label tx-12 tx-bold mg-b-5">Change assignment</label>
                    <select class="form-control select2 mg-b-10">
                        <option label="Choose one"></option>
                        <option value="Firefox">Firefox</option>
                        <option value="Chrome">Chrome</option>
                        <option value="Safari">Safari</option>
                        <option value="Opera">Opera</option>
                        <option value="Internet Explorer">Internet Explorer</option>
                    </select>
                    <div class="form-group">
                        <button class="btn btn-sm btn-danger float-right">Cancle</button>
                        <button class="btn btn-sm btn-info float-right mg-r-10">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<script src="{{asset('assets/js/customs.js')}}"></script>

@endsection
