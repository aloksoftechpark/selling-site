@extends('admin.layouts.app')
@section('content')

    {{--    <!DOCTYPE html>--}}
    {{--<html lang="en">--}}
    {{--<head>--}}

    {{--    <!-- Required meta tags -->--}}
    {{--    <meta charset="utf-8">--}}
    {{--    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">--}}

    {{--    <!-- Meta -->--}}
    {{--    <meta name="description" content="Responsive Bootstrap 4 Dashboard and Admin Template">--}}
    {{--    <meta name="author" content="ThemePixels">--}}

    {{--    <!-- Favicon -->--}}
    {{--    <link rel="shortcut icon" type="image/x-icon" href="../assets/img/favicon.png">--}}

    {{--    <title>Cassie Responsive Bootstrap 4 Dashboard and Admin Template</title>--}}

    {{--    <!-- vendor css -->--}}
    {{--    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">--}}
    {{--    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">--}}
    {{--    <link href="../lib/prismjs/themes/prism-tomorrow.css" rel="stylesheet">--}}
    {{--    <link href="../lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">--}}
    {{--    <link href="../lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">--}}
    {{--    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">--}}

    {{--    <!-- template css -->--}}
    {{--    <link rel="stylesheet" href="../assets/css/cassie.css">--}}
    {{--    <link rel="stylesheet" href="../assets/css/changes.css">--}}

    {{--</head>--}}
    {{--<body data-spy="scroll" data-target="#navSection" data-offset="100">--}}

    {{--<div class="sidebar">--}}
    {{--    <div class="sidebar-header">--}}
    {{--        <div>--}}
    {{--            <a href="../index.html" class="sidebar-logo"><span>cassie</span></a>--}}
    {{--            <small class="sidebar-logo-headline">Responsive Dashboard Template</small>--}}
    {{--        </div>--}}
    {{--    </div><!-- sidebar-header -->--}}
    {{--    <div id="dpSidebarBody" class="sidebar-body">--}}
    {{--        <ul class="nav nav-sidebar">--}}
    {{--            <li class="nav-label"><label class="content-label">Template Pages</label></li>--}}
    {{--            <li class="nav-item show">--}}
    {{--                <a href="../pages/dashboard-two.html" class="nav-link"><i data-feather="box"></i> Dashboard</a>--}}
    {{--            </li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a href="" class="nav-link with-sub"><i data-feather="layout"></i> Sales</a>--}}
    {{--                <nav class="nav nav-sub">--}}
    {{--                    <a href="app-newSales.html" class="nav-sub-link">New Sales</a>--}}
    {{--                    <a href="app-allSales.html" class="nav-sub-link">All Sales</a>--}}
    {{--                    <a href="#" class="nav-sub-link">Sales Return</a>--}}
    {{--                </nav>--}}
    {{--            </li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a href="" class="nav-link with-sub"><i data-feather="lock"></i> Purchase</a>--}}
    {{--                <nav class="nav nav-sub">--}}
    {{--                    <a href="page-signin.html" class="nav-sub-link">New Purchase</a>--}}
    {{--                    <a href="page-signup.html" class="nav-sub-link">All Purchase</a>--}}
    {{--                    <a href="page-forgot.html" class="nav-sub-link">Purchase Return</a>--}}
    {{--                </nav>--}}
    {{--            </li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a href="" class="nav-link with-sub"><i data-feather="user"></i> Customer</a>--}}
    {{--                <nav class="nav nav-sub">--}}
    {{--                    <a href="page-profile.html" class="nav-sub-link">New Customer</a>--}}
    {{--                    <a href="page-timeline.html" class="nav-sub-link">All Customer</a>--}}
    {{--                </nav>--}}
    {{--            </li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a href="" class="nav-link with-sub"><i data-feather="file-text"></i> Supplier</a>--}}
    {{--                <nav class="nav nav-sub">--}}
    {{--                    <a href="page-invoice.html" class="nav-sub-link">New Supplier</a>--}}
    {{--                    <a href="page-pricing.html" class="nav-sub-link">All Supplier</a>--}}
    {{--                </nav>--}}
    {{--            </li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a href="" class="nav-link with-sub"><i data-feather="x-circle"></i> Product and Services</a>--}}
    {{--                <nav class="nav nav-sub">--}}
    {{--                    <a href="page-404.html" class="nav-sub-link">Product</a>--}}
    {{--                    <a href="page-404.html" class="nav-sub-link">Damage Product</a>--}}
    {{--                    <a href="page-500.html" class="nav-sub-link">Catagories</a>--}}
    {{--                </nav>--}}
    {{--            </li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a href="" class="nav-link"><i data-feather="layers"></i> Stock</a>--}}
    {{--            </li>--}}
    {{--            <li class="nav-label"><label class="content-label">Admin</label></li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a href="" class="nav-link with-sub"><i data-feather="life-buoy"></i> Account</a>--}}
    {{--                <nav class="nav nav-sub">--}}
    {{--                    <a href="../components/form-elements.html" class="nav-sub-link">Expense</a>--}}
    {{--                    <a href="../components/form-input-group.html" class="nav-sub-link">Income</a>--}}
    {{--                    <a href="../components/form-input-tags.html" class="nav-sub-link">Pay-in Receipt</a>--}}
    {{--                    <a href="../components/form-input-masks.html" class="nav-sub-link">Pay-out Receipt</a>--}}
    {{--                    <a href="../components/form-validation.html" class="nav-sub-link">Account Head</a>--}}
    {{--                </nav>--}}
    {{--            </li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a href="" class="nav-link with-sub"><i data-feather="book"></i> Branch</a>--}}
    {{--                <nav class="nav nav-sub">--}}
    {{--                    <a href="../components/con-grid.html" class="nav-sub-link">Branches</a>--}}
    {{--                    <a href="../components/con-icons.html" class="nav-sub-link">Stock Transfer</a>--}}
    {{--                    <a href="../components/con-images.html" class="nav-sub-link">Employee Transfer</a>--}}
    {{--                </nav>--}}
    {{--            </li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a href="" class="nav-link with-sub"><i data-feather="layers"></i> Human Resource</a>--}}
    {{--                <nav class="nav nav-sub">--}}
    {{--                    <a href="../components/com-accordion.html" class="nav-sub-link">Employee</a>--}}
    {{--                    <a href="../components/com-alerts.html" class="nav-sub-link">Salary Payroll</a>--}}
    {{--                    <a href="../components/com-avatar.html" class="nav-sub-link">Attendance</a>--}}
    {{--                </nav>--}}
    {{--            </li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a href="" class="nav-link with-sub"><i data-feather="monitor"></i> Report</a>--}}
    {{--                <nav class="nav nav-sub">--}}
    {{--                    <a href="../components/util-animation.html" class="nav-sub-link">Sales Report</a>--}}
    {{--                    <a href="../components/util-background.html" class="nav-sub-link">Purchase Report</a>--}}
    {{--                    <a href="../components/util-border.html" class="nav-sub-link">Expense Report</a>--}}
    {{--                    <a href="../components/util-display.html" class="nav-sub-link">Income Report</a>--}}
    {{--                    <a href="../components/util-divider.html" class="nav-sub-link">Payin Report</a>--}}
    {{--                    <a href="../components/util-flex.html" class="nav-sub-link">Payout Report</a>--}}
    {{--                    <a href="../components/util-height.html" class="nav-sub-link">Payment in/out</a>--}}
    {{--                    <a href="../components/util-margin.html" class="nav-sub-link">Stock</a>--}}
    {{--                    <a href="../components/util-padding.html" class="nav-sub-link">Customer Ledger</a>--}}
    {{--                    <a href="../components/util-position.html" class="nav-sub-link">Supplier Ledger</a>--}}
    {{--                    <a href="../components/util-typography.html" class="nav-sub-link">Account Ledger</a>--}}
    {{--                    <a href="../components/util-width.html" class="nav-sub-link">Customer Due Report</a>--}}
    {{--                    <a href="../components/util-extras.html" class="nav-sub-link">Supplier Due Report</a>--}}
    {{--                    <a href="../components/util-extras.html" class="nav-sub-link">Profit/loss Report</a>--}}
    {{--                    <a href="../components/util-extras.html" class="nav-sub-link">All Bills</a>--}}
    {{--                </nav>--}}
    {{--            </li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a href="" class="nav-link with-sub"><i data-feather="pie-chart"></i> Configuration</a>--}}
    {{--                <nav class="nav nav-sub">--}}
    {{--                    <a href="../components/chart-flot.html" class="nav-sub-link">General Setting</a>--}}
    {{--                    <a href="../components/chart-chartjs.html" class="nav-sub-link">Payment Method</a>--}}
    {{--                    <a href="../components/chart-peity.html" class="nav-sub-link">User</a>--}}
    {{--                    <a href="../components/com-badge.html" class="nav-sub-link">Role and Premission</a>--}}
    {{--                    <a href="../components/chart-peity.html" class="nav-sub-link">Bank</a>--}}
    {{--                </nav>--}}
    {{--            </li>--}}
    {{--        </ul>--}}

    {{--        <hr class="mg-t-30 mg-b-25">--}}

    {{--        <ul class="nav nav-sidebar">--}}
    {{--            <li class="nav-item"><a href="../pages/themes.html" class="nav-link"><i data-feather="aperture"></i> Themes</a></li>--}}
    {{--            <li class="nav-item"><a href="../docs.html" class="nav-link"><i data-feather="help-circle"></i> Documentation</a></li>--}}
    {{--        </ul>--}}


    {{--    </div><!-- sidebar-body -->--}}
    {{--</div><!-- sidebar -->--}}

    {{--<div class="content">--}}
    {{--    <div class="header">--}}
    {{--        <div class="header-left">--}}
    {{--            <a href="" class="burger-menu"><i data-feather="menu"></i></a>--}}

    {{--            <div class="header-search">--}}
    {{--                <i data-feather="search"></i>--}}
    {{--                <input type="search" class="form-control" placeholder="What are you looking for?">--}}
    {{--            </div><!-- header-search -->--}}
    {{--        </div><!-- header-left -->--}}

    {{--        <div class="header-right">--}}
    {{--            <a href="" class="header-help-link"><i data-feather="help-circle"></i></a>--}}
    {{--            <div class="dropdown dropdown-notification">--}}
    {{--                <a href="" class="dropdown-link new" data-toggle="dropdown"><i data-feather="bell"></i></a>--}}
    {{--                <div class="dropdown-menu dropdown-menu-right">--}}
    {{--                    <div class="dropdown-menu-header">--}}
    {{--                        <h6>Notifications</h6>--}}
    {{--                        <a href=""><i data-feather="more-vertical"></i></a>--}}
    {{--                    </div><!-- dropdown-menu-header -->--}}
    {{--                    <div class="dropdown-menu-body">--}}
    {{--                        <a href="" class="dropdown-item">--}}
    {{--                            <div class="avatar"><span class="avatar-initial rounded-circle text-primary bg-primary-light">s</span></div>--}}
    {{--                            <div class="dropdown-item-body">--}}
    {{--                                <p><strong>Socrates Itumay</strong> marked the task as completed.</p>--}}
    {{--                                <span>5 hours ago</span>--}}
    {{--                            </div>--}}
    {{--                        </a>--}}
    {{--                        <a href="" class="dropdown-item">--}}
    {{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-pink bg-pink-light">r</span></div>--}}
    {{--                            <div class="dropdown-item-body">--}}
    {{--                                <p><strong>Reynante Labares</strong> marked the task as incomplete.</p>--}}
    {{--                                <span>8 hours ago</span>--}}
    {{--                            </div>--}}
    {{--                        </a>--}}
    {{--                        <a href="" class="dropdown-item">--}}
    {{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-success bg-success-light">d</span></div>--}}
    {{--                            <div class="dropdown-item-body">--}}
    {{--                                <p><strong>Dyanne Aceron</strong> responded to your comment on this <strong>post</strong>.</p>--}}
    {{--                                <span>a day ago</span>--}}
    {{--                            </div>--}}
    {{--                        </a>--}}
    {{--                        <a href="" class="dropdown-item">--}}
    {{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-indigo bg-indigo-light">k</span></div>--}}
    {{--                            <div class="dropdown-item-body">--}}
    {{--                                <p><strong>Kirby Avendula</strong> marked the task as incomplete.</p>--}}
    {{--                                <span>2 days ago</span>--}}
    {{--                            </div>--}}
    {{--                        </a>--}}
    {{--                    </div><!-- dropdown-menu-body -->--}}
    {{--                    <div class="dropdown-menu-footer">--}}
    {{--                        <a href="">View All Notifications</a>--}}
    {{--                    </div>--}}
    {{--                </div><!-- dropdown-menu -->--}}
    {{--            </div>--}}
    {{--            <div class="dropdown dropdown-loggeduser">--}}
    {{--                <a href="" class="dropdown-link" data-toggle="dropdown">--}}
    {{--                    <div class="avatar avatar-sm">--}}
    {{--                        <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">--}}
    {{--                    </div><!-- avatar -->--}}
    {{--                </a>--}}
    {{--                <div class="dropdown-menu dropdown-menu-right">--}}
    {{--                    <div class="dropdown-menu-header">--}}
    {{--                        <div class="media align-items-center">--}}
    {{--                            <div class="avatar">--}}
    {{--                                <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">--}}
    {{--                            </div><!-- avatar -->--}}
    {{--                            <div class="media-body mg-l-10">--}}
    {{--                                <h6>Louise Kate Lumaad</h6>--}}
    {{--                                <span>Administrator</span>--}}
    {{--                            </div>--}}
    {{--                        </div><!-- media -->--}}
    {{--                    </div>--}}
    {{--                    <div class="dropdown-menu-body">--}}
    {{--                        <a href="" class="dropdown-item"><i data-feather="user"></i> View Profile</a>--}}
    {{--                        <a href="" class="dropdown-item"><i data-feather="edit-2"></i> Edit Profile</a>--}}
    {{--                        <a href="" class="dropdown-item"><i data-feather="briefcase"></i> Account Settings</a>--}}
    {{--                        <a href="" class="dropdown-item"><i data-feather="shield"></i> Privacy Settings</a>--}}
    {{--                        <a href="" class="dropdown-item"><i data-feather="log-out"></i> Sign Out</a>--}}
    {{--                    </div>--}}
    {{--                </div><!-- dropdown-menu -->--}}
    {{--            </div>--}}
    {{--        </div><!-- header-right -->--}}
    {{--    </div><!-- header -->--}}
    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Lead</a></li>
                            <li class="breadcrumb-item active" aria-current="page">All Leads</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Leads</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10"  data-toggle="modal" data-target="#exampleModalCenter1">Add Lead</button>
            <div class="modal fade modal_cust lead_list" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">NEW LEAD</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body body_width">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="upload_type">
                                        <img id="blah" src="http://placehold.it/180" alt="your image">

                                    </div><!-- upload_type -->
                                </div><!-- col -->
                                <div class="col-md-9">
                                    <div class="mg-t-5">
                                        Company Name: <input type="text" name="FirstName" value="">
                                    </div>
                                    <div class="mg-t-5">


                                        Company Address: <input type="text" name="FirstName" value="">
                                    </div>


                                </div><!-- col -->
                                <div class="col-md-12">
                                    <div class="mg-t-5 if_function_input">
                                        <input type="file" onchange="readURL(this);">
                                    </div>
                                </div><!-- col -->
                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-6 mg-t-5">

                                    Status: <select class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <option value="volvo">Guest</option>
                                        <option value="saab">Ranjan</option>
                                        <option value="opel">Manoj</option>
                                        <option value="audi">Pawan</option>
                                    </select>

                                </div><!-- col -->
                                <div class="col-md-6 mg-t-5">

                                    Priority:  <select class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <option value="volvo">Guest</option>
                                        <option value="saab">Ranjan</option>
                                        <option value="opel">Manoj</option>
                                        <option value="audi">Pawan</option>
                                    </select>

                                </div><!-- col -->
                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-6 mg-t-5">

                                    Contact Number: <input type="text" name="FirstName" value="">

                                </div><!-- col -->
                                <div class="col-md-6 mg-t-5">

                                    Email: <input type="text" name="FirstName" value="">

                                </div><!-- col -->

                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-6 mg-t-5">

                                    Source: <select class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <option value="volvo">Guest</option>
                                        <option value="saab">Ranjan</option>
                                        <option value="opel">Manoj</option>
                                        <option value="audi">Pawan</option>
                                    </select>

                                </div><!-- col -->
                                <div class="col-md-6 mg-t-5">

                                    Added By: <select class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <option value="volvo">Guest</option>
                                        <option value="saab">Ranjan</option>
                                        <option value="opel">Manoj</option>
                                        <option value="audi">Pawan</option>
                                    </select>

                                </div><!-- col -->

                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-12 mg-t-5">

                                    Company Website: <input type="text" name="FirstName" value="">

                                </div><!-- col -->
                                <div class="col-md-12 mg-t-5">
                                    Description: <textarea rows="3" placeholder="Write something about supplier" class="text_area_custom"></textarea>

                                </div><!-- col -->
                            </div><!-- row -->

                            <div class="modal-footer modal-footer_footer modal-footer-right">
                                <button type="button" class="btn btn-success mg-r-10">Update</button>
                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-2 pd-r-0">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="All Status"></option>
                            <option value="Firefox">New</option>
                            <option value="Chrome">Running</option>
                            <option value="Opera">Lost</option>
                            <option value="Opera">Client</option>
                        </select>
                    </div>
                    <div class="col-sm-2 pd-r-0">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="Assigned To"></option>
                            <option value="Firefox">Employee 1</option>
                            <option value="Chrome">Employee 2</option>
                            <option value="Opera">Employee 3</option>
                            <option value="Opera">Employee 4</option>
                        </select>
                    </div>
                    <div class="col-sm-2 pd-r-0">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="Added By"></option>
                            <option value="Firefox">Employee 1</option>
                            <option value="Chrome">Employee 2</option>
                            <option value="Opera">Employee 3</option>
                            <option value="Opera">Employee 4</option>
                        </select>
                    </div>
                    <div class="col-sm-2 pd-r-0">
                        <input type="text" class="form-control form-control-sm" placeholder="By Follow Up Date">
                    </div>
                    <div class="col-sm-2 pd-r-0">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                    <div class="col-sm mail-navbar bd-b-0 justify-content-end mg-r-10" style="height: auto;">
                        <div class="d-none d-lg-flex">
                            <a href=""><i data-feather="printer"></i></a>
                            <a href=""><i data-feather="folder"></i></a>
                            <a href=""><i class="fas fa-download"></i></a>
                        </div>
                    </div>
                </div>
            </div> <!--form-group-->
            <div class="card">
                <table id="example1" class="table">
                    <thead>
                    <tr>
                        <th class="wd-5px"><input type="checkbox" aria-label="Checkbox for following text input"></th>
                        <th class="wd-5px">SN.</th>
                        <th class="wd-10p">Add Date</th>
                        <th class="wd-15p">Lead Name</th>
                        <th class="wd-15p">Address</th>
                        <th class="wd-10p">Follow Date</th>
                        <th class="wd-20p">Added By</th>
                        <th class="wd-10p">Status</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                        <td>1</td>
                        <td>2076/12/15</td>
                        <td>Company Name</td>
                        <td>Tinkune, Kathmandu</td>
                        <td>2076/12/15</td>
                        <td>Bhagwan Das Agrahari</td>
                        <td>
                            <span class="badge badge-pill badge-success wd-70">Running</span>
                        </td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href="../newPages/new-lead-detail.html"><i class="icon ion-clipboard text-success"></i></a></div>
                            <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>
                            <div class="mg-r-20" title="Delete"><a href=""><i class="icon ion-trash-b text-danger"></i></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                        <td>2</td>
                        <td>2076/12/15</td>
                        <td>Company Name</td>
                        <td>Tinkune, Kathmandu</td>
                        <td>2076/12/15</td>
                        <td>Bhagwan Das Agrahari</td>
                        <td>
                            <span class="badge badge-pill badge-info wd-70">New</span>
                        </td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>
                            <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>
                            <div class="mg-r-20" title="Delete"><a href=""><i class="icon ion-trash-b text-danger"></i></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                        <td>3</td>
                        <td>2076/12/15</td>
                        <td>Company Name</td>
                        <td>Tinkune, Kathmandu</td>
                        <td>2076/12/15</td>
                        <td>Bhagwan Das Agrahari</td>
                        <td>
                            <span class="badge badge-pill badge-danger wd-70">Lost</span>
                        </td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>
                            <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>
                            <div class="mg-r-20" title="Delete"><a href=""><i class="icon ion-trash-b text-danger"></i></a></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
    {{--    <div class="content-footer">--}}
    {{--        &copy; 2019. All Rights Reserved. Created by <a href="http://themepixels.me" target="_blank">ThemePixels</a>--}}
    {{--    </div><!-- content-footer -->--}}
    </div><!-- content -->



    {{--<script src="../lib/jquery/jquery.min.js"></script>--}}
    {{--<script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>--}}
    {{--<script src="../lib/feather-icons/feather.min.js"></script>--}}
    {{--<script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>--}}
    {{--<script src="../lib/prismjs/prism.js"></script>--}}
    {{--<script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>--}}
    {{--<script src="../lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>--}}
    {{--<script src="../lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>--}}
    {{--<script src="../lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>--}}
    {{--<script src="../lib/select2/js/select2.min.js"></script>--}}
    {{--<script src="../lib/js-cookie/js.cookie.js"></script>--}}

    {{--<script src="../assets/js/cassie.js"></script>--}}

    {{--</body>--}}
    {{--</html>--}}

@endsection
