@extends('admin.layouts.app')
@section('content')



    <div class="content-body content-body-profile">
        <div class="profile-sidebar">
            <div class="content-header pd-l-5 pd-t-0">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Lead</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Lead Detail</li>
                        </ol>
                    </nav>
                </div>
            </div><!-- content-header -->
            <div class="profile-sidebar-header">
                <div class="avatar">
                    @if(isset($lead_management->images))
                        @if(!$lead_management->images->isEmpty())
                            @foreach($lead_management->images as $key=>$image)
                                @if($key == 0)
                            <img src="{{asset(''.$image->image)}}" height="70px" width="70px" class="rounded-circle" alt=""></div>
                                 @endif
                             @endforeach
                        @else
                            <img src="" height="70px" width="70px" class="rounded-circle" alt=""></div>
                        @endif
                    @endif
                <h5>{{$lead_management->name}}</h5>
                <!-- <div class="d-flex">
                    <span class="text-white badge badge-pill badge-success wd-70">Running</span>
                    <span class="mg-l-20 float-right " title="Edit"><a href=""><h5><i class="far fa-edit text-warning"></i></h5></a></span>
                </div> -->
                <div>
                    <select class="text-white bd rounded-5 pd-l-5 pd-r-5 bg-success select2-no-search">
                        <option label="Running" value="{{$lead_management->status1}}" class="bg-white text-success">{{$lead_management->status1}}</option>
{{--                        <option value="Firefox" class="bg-white text-info">New</option>--}}
{{--                        <option value="Opera" class="bg-white text-danger">Lost</option>--}}
{{--                        <option value="Opera" class="bg-white text-warning">Client</option>--}}
                    </select>
                </div>


                <div class="d-flex align-self-stretch mg-t-30">
                    <a href="mailto:{{$lead_management->mailId}} " class="btn btn-brand-01 btn-sm btn-uppercase flex-fill">Email</a>
{{--                    <a href="" class="btn btn-white btn-sm btn-uppercase flex-fill mg-l-5">Edit</a>--}}
                     @if(Auth::guard('admin')->check())
                        <button type="button" class="btn btn-white btn-sm btn-uppercase flex-fill mg-l-5"  data-toggle="modal" data-target="#exampleModalCenter1">EDit</button>
                    @else
                    @can('lead-edit')
                        <button type="button" class="btn btn-white btn-sm btn-uppercase flex-fill mg-l-5"  data-toggle="modal" data-target="#exampleModalCenter1">EDit</button>
                    @endcan
                    @endif

                </div>

                <div class="col-sm-0 tx-right col-lg-7">
{{--                    <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10"  data-toggle="modal" data-target="#exampleModalCenter1">Add Lead</button>--}}
                    <div class="modal fade modal_cust lead_list" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">NEW LEAD</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body body_width">
                                    <form action="{{route('lead-management.update',$lead_management->id)}}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        @method('put')
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="upload_type">
{{--                                                    <img id="blah" src="http://placehold.it/180" alt="your image">--}}
                                                    <div class="avatar">
                                                        @if(isset($lead_management->images))
                                                            @if(!$lead_management->images->isEmpty())
                                                                @foreach($lead_management->images as $key=>$image)
                                                                    @if($key == 0)
                                                                        <img src="{{asset(''.$image->image)}}" height="70px" width="70px" class="rounded-circle" alt=""></div>
                                                    @endif
                                                    @endforeach
                                                    @else
                                                        <img src="" height="70px" width="70px" class="rounded-circle" alt=""></div>
                                                @endif
                                                @endif


                                                </div><!-- upload_type -->
                                            </div><!-- col -->
                                            <div class="col-md-9">
                                                <div class="mg-t-5">
                                                    Company Name: <input type="text" name="name" class="form-control "  value="{{ old('name', isset($lead_management) ? $lead_management->name : '') }}" placeholder="Enter Company Name" required>
                                                </div>
                                                <div class="mg-t-5">


                                                    Company Address:    <input type="text" name="address" class="form-control " value="{{ old('address', isset($lead_management) ? $lead_management->address : '') }}" placeholder="Enter Customer Address" >
                                                </div>


                                            </div><!-- col -->
                                            <div class="col-md-12">
                                                <div class="mg-t-5 if_function_input">
                                                    <input type="file" name="image" onchange="readURL(this);">
                                                </div>
                                            </div><!-- col -->
                                        </div><!-- row -->
                                        <div class="row">
                                            <div class="col-md-6 mg-t-5">

                                                Status: <select name="status" class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                    <option value="select" >Select Status</option>
                                                    <option value="running" @if($lead_management->status1 == 'running') selected @endif>Running</option>
                                                    <option value="new" @if($lead_management->status1 == 'new') selected @endif>New</option>
                                                    <option value="lost" @if($lead_management->status1 == 'lost') selected @endif>Lost</option>
                                                </select>

                                            </div><!-- col -->
                                            <div class="col-md-6 mg-t-5">

                                                Priority:  <select name="priority" class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                    <option label="Select Priority">select priority</option>
                                                    <option value="h" @if($lead_management->priority == 'h') selected @endif>Hign</option>
                                                    <option value="m" @if($lead_management->priority == 'm') selected @endif>Mid</option>
                                                    <option value="l"  @if($lead_management->priority == 'l') selected @endif>Low</option>
                                                </select>

                                            </div><!-- col -->
                                        </div><!-- row -->
                                        <div class="row">
                                            <div class="col-md-6 mg-t-5">

                                                Contact Number: <input type="text" name="phone_no" class="form-control " value="{{ old('phoneNumber', isset($lead_management) ? $lead_management->phoneNumber : '') }}" placeholder="Contact No" >

                                            </div><!-- col -->
                                            <div class="col-md-6 mg-t-5">

                                                Email:  <input type="text" name="email" class="form-control " value="{{ old('mailId', isset($lead_management) ? $lead_management->mailId : '') }}" placeholder="Enter Customer Email">

                                            </div><!-- col -->

                                        </div><!-- row -->
                                        <div class="row">
                                            <div class="col-md-6 mg-t-5">

                                                Source: <select  class="form-control bd bd-gray-900" name="source" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                    <option value="select">select source</option>
                                                    <option value="facebook" @if($lead_management->source == 'facebook') selected @endif>Facebook</option>
                                                    <option value="other" @if($lead_management->source == 'other') selected @endif>Other</option>
                                                    {{--                                        <option value="audi">Pawan</option>--}}
                                                </select>

                                            </div><!-- col -->
                                            <div class="col-md-6 mg-t-5">

                                                Added By: <select name="added_by" class="form-control bd bd-gray-900" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                    <option value="select">Select User</option>
                                                    @foreach($user as $user)
                                                        <option value="{{$user->id}}" {{ (isset($lead_management) && $lead_management->user_id==$user->id) ? 'selected': "" }}> {{$user->name}}</option>
                                                    @endforeach
                                                    {{--                                        <option value="opel">Manoj</option>--}}
                                                    {{--                                        <option value="audi">Pawan</option>--}}
                                                </select>

                                            </div><!-- col -->

                                        </div><!-- row -->
                                        <div class="row">
                                            <div class="col-md-6 mg-t-5">

                                                Company Website: <input type="text" name="website" class="form-control " value="{{ old('website', isset($lead_management) ? $lead_management->website : '') }}"placeholder="https://softechpark.com/" >

                                            </div><!-- col -->
                                            <div class="col-md-6 mg-t-5">

                                                Follow up Date : <input type="text" name="followUpDate"  value="{{ old('followUpDate', isset($lead_management) ? $lead_management->followUpDate : '') }}" class="bod-picker form-control">

                                            </div><!-- col -->
                                            <div class="col-md-12 mg-t-5">
                                                Description: <textarea rows="3" name="description"  placeholder="Write something about supplier" class="text_area_custom"> {{ old('comment', isset($lead_management) ? $lead_management->comment : '') }}</textarea>

                                            </div><!-- col -->
                                        </div><!-- row -->

                                        <div class="modal-footer modal-footer_footer modal-footer-right">
                                            <button  class="btn btn-success mg-r-10">Update</button>
                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                            <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save changes</button> -->
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div><!-- col -->





















            </div><!-- profile-sidebar-header -->
            <div class="card-progress">
                <label class="content-label mg-b-0">Priority</label>
                <div class="progress">
                    <div class="progress-bar bg-green wd-70p" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <label class="content-label mg-b-0">70%</label>
            </div>
            <div class="profile-sidebar-body">

                <label class="content-label">Company Contact</label>
                <ul class="list-unstyled profile-info-list mg-b-0">
                    <li><i data-feather="briefcase"></i> <span class="tx-color-03">{{$lead_management->address}}</span></li>
                    <li><i data-feather="phone"></i> <a href="">{{$lead_management->phoneNumber}}</a></li>
                    <li><i data-feather="mail"></i> <a href="">{{$lead_management->mailId}}</a></li>
                    <li><i data-feather="mail"></i> <a href="#">@if(isset($lead_management)){{$lead_management->website}}@endif</a></li>
                </ul>



                <hr class="mg-y-25">

                <label class="content-label">Other Detail</label>
                <ul class="list-unstyled profile-info-list">
                    <li><span class="tx-color-03">Lead Created: {{$lead_management->created_at}}</span></li>
                    <li><span class="tx-color-03">Source: {{$lead_management->source}}</span></li>
{{--                    <li><span class="tx-color-03">Added By:@if(isset($lead_management->users)){{$lead_management->users->name}}@else{{$lead_management->admin->name}} @endif</span></li>--}}
                    <li><span class="tx-color-03">Assigned To:@if(isset($lead_management->user1)){{$lead_management->user1->name}}@endif </span></li>
                </ul>

                <hr class="mg-y-25">

            </div><!-- profile-sidebar-body -->
        </div><!-- profile-sidebar -->
        <div class="profile-body">
            <div class="profile-body-header">
                <div class="nav-wrapper">
                    <ul class="nav nav-line" id="profileTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
                        </li>
                        <li class="nav-item">
                            @if(Auth::guard('admin')->check())
                                <a class="nav-link" id="projects-tab" data-toggle="tab" href="#bills" role="tab" aria-controls="projects" aria-selected="false">Follow Up</a>
                            @else
                            @can('followup-list')
                                <a class="nav-link" id="projects-tab" data-toggle="tab" href="#bills" role="tab" aria-controls="projects" aria-selected="false">Follow Up</a>
                            @endcan
                            @endif

                        </li>
                        <li class="nav-item">
                            @if(Auth::guard('admin')->check())
                                <a class="nav-link" id="people-tab" data-toggle="tab" href="#receipts" role="tab" aria-controls="people" aria-selected="false">Estimate</a>
                            @else
                            @can('estimate-list')
                                <a class="nav-link" id="people-tab" data-toggle="tab" href="#receipts" role="tab" aria-controls="people" aria-selected="false">Estimate</a>
                            @endcan
                            @endif

                        </li>
                    </ul>
                </div><!-- nav-wrapper -->
            </div><!-- profile-body-header -->
            <div class="tab-content pd-15 pd-sm-20">
                <div id="overview" class="tab-pane active show">

                    <div class="stat-profile">
                        <div class="stat-profile-body">
                            <div class="row row-xs">
                                <div class="col">
                                    <div class="card card-body pd-10 pd-md-15 bd-0 shadow-none bg-primary-light">
                                        <h1 class="tx-light tx-sans tx-spacing--4 tx-primary mg-b-5">1,00,55,000</h1>
                                        <p class="tx-13 tx-lg-14 tx-color-02 mg-b-0">Estimate Amount</p>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="card card-body pd-10 pd-md-15 bd-0 shadow-none bg-teal-light">
                                        <h1 class="mg-b-5 tx-sans tx-spacing--2 tx-light tx-teal">5</h1>
                                        <p class="tx-13 tx-lg-14 tx-color-03 mg-b-0">Follow Up Count</p>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="card card-body pd-10 pd-md-15 bd-0 shadow-none bg-pink-light">
                                        <h1 class="mg-b-5 tx-sans tx-spacing--2 tx-light tx-pink">105</h1>
                                        <p class="tx-13 tx-lg-14 tx-color-03 mg-b-0">Lead Age Days</p>
                                    </div>
                                </div>
                            </div><!-- row -->
                        </div><!-- stat-profile-body -->
                    </div><!-- stat-profile -->

                    <hr class="mg-y-15 op-0">

                    <label class="content-label content-label-lg mg-b-15 tx-color-01">Description</label>
                    <p class="tx-color-03">{{$lead_management->comment}}</p>

                    <hr class="mg-y-25">


                    @foreach($contact_person as $key=>$contact_person)
                        <input type="hidden" id="contact_id44" class="contact_id44">
                    <label class="content-label content-label-lg mg-b-15 tx-color-01">Contact Person-{{++$key}}
{{--                        <span class="mg-r-0 float-right " title="Delete"><a href=""><h5><i class="icon ion-trash-b text-danger"></i></h5></a></span>--}}

                         @if(Auth::guard('admin')->check())
                            <span class="mg-r-0 float-right " title="Delete"><a href=" " data-toggle="modal" data-target="#exampleModalCenter10"><h5><i class="icon ion-trash-b text-danger"></i></h5></a></span>
                        @else
                        @can('contact-person-delete')
                            <span class="mg-r-0 float-right " title="Delete"><a href=" " data-toggle="modal" data-target="#exampleModalCenter10"><h5><i class="icon ion-trash-b text-danger"></i></h5></a></span>
                        @endcan
                        @endif



                        {{--                            fot delete popup--}}

                        <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <div class="conform_del">
                                            <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                        </div><!-- conform_del -->

                                        <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                            <a class="btn btn-success mg-r-10" href="">Yes</a>
                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(Auth::guard('admin')->check())
                            <span class="mg-r-15 float-right " title="Edit"><a href="" id="contact" data-id="{{$contact_person->id}}" class="editData" data-toggle="modal" data-target="#exampleModalCenter123"><h5><i class="far fa-edit text-warning"></i></h5></a></span>
                        @else
                        @can('contact-person-edit')
                            <span class="mg-r-15 float-right " title="Edit"><a href="" id="contact" data-id="{{$contact_person->id}}" class="editData" data-toggle="modal" data-target="#exampleModalCenter123"><h5><i class="far fa-edit text-warning"></i></h5></a></span>
                        @endcan
                        @endif


                    </label>

                    <ul class="list-unstyled profile-info-list">
                        <li><span class="tx-color-03">Name: {{$contact_person->name}}</span></li>
                        <li><span class="tx-color-03">Job Title: {{$contact_person->jobTitle}}</span></li>
                        <li><span class="tx-color-03">Phone: {{$contact_person->phoneNumber}}</span></li>
                        <li><span class="tx-color-03">Email:{{$contact_person->email}}</span></li>
                    </ul>
                        <input type="hidden" class="contact_id" id="contact_id" value="{{$contact_person->id}}">
                    @endforeach


                    <hr class="mg-y-25">
                    <div class="row">
                        <div class="col-sm-12 tx-right">
{{--                            <button type="button" class="btn btn-sm btn-primary mg-t-0 mg-r-20 mg-b-0">Add Contact Person</button>--}}
                            @if(Auth::guard('admin')->check())
                                <button type="button" class="btn btn-sm btn-primary mg-t-0 mg-r-20 mg-b-0" data-toggle="modal" data-target="#exampleModalCenter">Add Contact Person</button>
                            @else
                            @can('contact-person-create')
                                <button type="button" class="btn btn-sm btn-primary mg-t-0 mg-r-20 mg-b-0" data-toggle="modal" data-target="#exampleModalCenter">Add Contact Person</button>
                            @endcan
                            @endif

                            <div class="modal fade show" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-modal="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">ADD CONTACT PERSON</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-left">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    Name: <input type="text" id="name" name="name" value=""><br>
                                                </div><!-- col -->
                                                <div class="col-md-6">
                                                    Job Title: <input type="text" id="job_title" name="job_title" value=""><br>
                                                </div><!-- col -->
                                            </div><!-- row -->
                                            <div class="row mg-t-5">
                                                <div class="col-md-6">
                                                    Phone Number: <input type="number" id="phone_number" name="phone_number" value=""><br>
                                                </div><!-- col -->
                                                <div class="col-md-6">
                                                    Email: <input type="Email" id="email" name="email" value=""><br>
                                                </div><!-- col -->
                                            </div><!-- row -->
                                            <div class="modal-footer modal-footer_footer">
                                                <button  id="saveData" class="btn btn-success mg-r-10 saveData">Update</button>
                                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                  <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div><!-- col -->



                    </div>
                    <hr class="mg-y-25">
                    <div class="row">
                        <div class="col-sm-12 tx-right">
                            {{--                            <button type="button" class="btn btn-sm btn-primary mg-t-0 mg-r-20 mg-b-0">Add Contact Person</button>--}}
{{--                            <button type="button" class="btn btn-sm btn-primary mg-t-0 mg-r-20 mg-b-0" data-toggle="modal" data-target="#exampleModalCenter">Add Contact Person</button>--}}
                            <div class="modal fade show" id="exampleModalCenter123" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-modal="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">ADD CONTACT PERSON</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-left">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    Name: <input type="text" id="name1" name="name" value=""><br>
                                                </div><!-- col -->
                                                <div class="col-md-6">
                                                    Job Title: <input type="text" id="jobTitle1" name="job_title" value=""><br>
                                                </div><!-- col -->
                                            </div><!-- row -->
                                            <div class="row mg-t-5">
                                                <div class="col-md-6">
                                                    Phone Number: <input type="number" id="phone_number1" name="phone_number" value=""><br>
                                                </div><!-- col -->
                                                <div class="col-md-6">
                                                    Email: <input type="Email" id="email1" name="email" value=""><br>
                                                </div><!-- col -->
                                            </div><!-- row -->
                                            <div class="modal-footer modal-footer_footer">
                                                <button  id="updateData"   class="btn btn-success mg-r-10 updateData">Update</button>
                                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                  <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div><!-- col -->



                    </div>


                </div>
                <div class="tab-pane fade" id="bills" role="tabpanel" aria-labelledby="contact-tab5">
                    <!-- <label class="content-label content-label-lg mg-b-0 tx-color-01"><h5>All Bills</h5></label> -->

                    <div>
                        <div class="form-group">
                            <div class="row pd-l-0">
                                <div class="col-sm-3 pd-r-0">
                                    <input type="text" class="form-control form-control-sm bod-picker"  placeholder="Form">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control form-control-sm  bod-picker" placeholder="TO">
                                </div>
                                <div class="col-sm mail-navbar bd-b-0 justify-content-end pd-r-0" style="height: auto;">
                                    <div class="d-none d-lg-flex">
                                        @if(Auth::guard('admin')->check())
                                            <a href=""><i data-feather="printer"></i></a>
                                        @else
                                        @can('followup-print')
                                            <a href=""><i data-feather="printer"></i></a>
                                        @endcan
                                        @endif

                                        <a href=""><i data-feather="folder"></i></a>
                                        @if(Auth::guard('admin')->check())
                                            <a href=""><i class="fas fa-download"></i></a>
                                        @else
                                        @can('followup-download')
                                            <a href=""><i class="fas fa-download"></i></a>
                                        @endcan
                                        @endif

                                    </div>
                                </div>
                                <div class="col-sm-2 tx-right pd-r-20">
{{--                                    <button type="button" class="btn btn-sm btn-primary mg-t-0 mg-r-0 mg-b-10">Add Follow</button>--}}
                                    @if(Auth::guard('admin')->check())
                                        <button type="button" class="btn btn-sm btn-primary mg-t-0 mg-r-0 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter2">Add Follow</button>
                                    @else
                                    @can('followup-create')
                                        <button type="button" class="btn btn-sm btn-primary mg-t-0 mg-r-0 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter2">Add Follow</button>
                                    @endcan
                                    @endif



                                    <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalCenterTitle">ADD FOLLOW</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <input type="hidden" id="lead_id" value="{{$lead_management->id}}">
                                                <div class="modal-body text-left">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            Follow up date: <input type="text" id="follow_up_date" class="bod-picker" name="FirstName" value=""><br>
                                                        </div><!-- col -->
                                                        <div class="col-md-6">
                                                            Next Follow up date: <input type="text" id="next_follow_up_date" class="bod-picker" name="FirstName" value=""><br>
                                                        </div><!-- col -->
                                                    </div><!-- row -->
                                                    <div class="row mg-t-5">
                                                        <div class="col-md-6">
                                                            Todo:
                                                            <div class="input-group">
                                                                <select id="todo" class="todo select_option_main">

                                                                    @foreach($lead_setups as $key=> $lead_setup)
                                                                        @if($lead_setup->todo)
                                                                            <option value="{{$lead_setup->todo}}">{{$lead_setup->todo}}</option>
                                                                        @endif
                                                                    @endforeach
{{--                                                                    <option value="phone call">Phone call</option>--}}
{{--                                                                    <option value="meeting">Meeting</option>--}}
{{--                                                                    <option value="closed">closed</option>--}}
                                                                </select>
                                                            </div><!-- input-group -->
                                                            <br>

                                                            <div class="modal-footer modal-footer_footer no-pad">
                                                                <button id="saveData1" class="btn btn-success m-r-125">Update</button>
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                  <button type="button" class="btn btn-primary">Save changes</button> -->
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 textarea_custom">
                                                            Follow Up Report
                                                            <textarea rows="4" id="follow_up_report" cols="50" placeholder=""></textarea>
                                                        </div><!-- col -->
                                                    </div><!-- row -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div><!-- col -->
                            </div>
                        </div> <!--form-group-->
                        <div class="card">
                            <table id="example1" class="table">
                                <thead>
                                <tr>
{{--                                    <th class="wd-5px"><input type="checkbox" aria-label="Checkbox for following text input"></th>--}}
                                    <th class="wd-5px">SN.</th>
                                    <th class="wd-10p">Date</th>
                                    <th class="wd-50p">Follow Up Report</th>
                                    <th class="wd-10p">Next Follow Date</th>
                                    <th class="wd-10p">To Do</th>
                                    <th class="wd-10p">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($add_follow as $key=> $add_follows)
                                <tr>
{{--                                    <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
                                    <td>{{++$key}}</td>
                                    <input type="hidden" class="follow_id44">
                                    <td>{{$add_follows->followUpDate}}</td>
                                    <td>
{{--                                        <p>--}}
{{--                                            Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.--}}
{{--                                        </p>--}}
                                        {{$add_follows->followUpReport}}
                                        <span><strong>Followed By:</strong> <a href=""> </a></span>
                                    </td>
                                    <td> {{$add_follows->nextFollowUpDate}}</td>
                                    <td>
                                        <span class="badge badge-pill badge-success wd-70">{{$add_follows->toDo}}</span>
                                    </td>
                                    <td class="d-md-flex">
                                         @if(Auth::guard('admin')->check())
                                            <div class="mg-r-20" title="Edit"><a href="" class="editData132 follow1" id="follow" data-follow-id="{{$add_follows->id}}" data-toggle="modal" data-target="#exampleModalCenter231" ><i class="far fa-edit text-warning"></i></a></div>
                                        @else
                                        @can('followup-edit')
                                            <div class="mg-r-20" title="Edit"><a href="" class="editData132 follow1" id="follow" data-follow-id="{{$add_follows->id}}" data-toggle="modal" data-target="#exampleModalCenter231" ><i class="far fa-edit text-warning"></i></a></div>
                                        @endcan
                                        @endif

{{--                                        <div class="mg-r-20" title="Delete"><a href=""><i class="icon ion-trash-b text-danger"></i></a></div>--}}

                                         @if(Auth::guard('admin')->check())
                                           <div class="mg-r-20" title="Delete"><a href=" " data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                                        @else
                                        @can('followup-delete')
                                           <div class="mg-r-20" title="Delete"><a href=" " data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                                        @endcan
                                        @endif



                                        {{--                            fot delete popup--}}

                                        <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">

                                                        <div class="conform_del">
                                                            <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                                        </div><!-- conform_del -->

                                                        <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                                            <a class="btn btn-success mg-r-10" href=" ">Yes</a>
                                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </td>
                                </tr>
                                    @endforeach

                                </tbody>
                            </table>
                            <div class="col-sm-2 tx-right pd-r-20">
                                {{--                                    <button type="button" class="btn btn-sm btn-primary mg-t-0 mg-r-0 mg-b-10">Add Follow</button>--}}
{{--                                <button type="button" class="btn btn-sm btn-primary mg-t-0 mg-r-0 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter231">Add Follow</button>--}}


                                <div class="modal fade" id="exampleModalCenter231" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle">ADD FOLLOW</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <input type="hidden" id="lead_id" value="{{$lead_management->id}}">
                                            <div class="modal-body text-left">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        Follow up date: <input type="text" id="follow_up_date1" class="follow_up_date1 bod-picker p" name="FirstName" value=""><br>
                                                    </div><!-- col -->
                                                    <div class="col-md-6">
                                                        Next Follow up date: <input type="text" id="next_follow_up_date1" class="next_follow_up_date1 bod-picker123 b" name="FirstName" value=""><br>
                                                    </div><!-- col -->
                                                </div><!-- row -->
                                                <div class="row mg-t-5">
                                                    <div class="col-md-6">
                                                        Todo:
                                                        <div class="input-group">
                                                            <select id="todo1" class="todo1 select_option_main">

                                                                <option value="phone call">Phone call</option>
                                                                <option value="meeting">Meeting</option>
                                                                <option value="closed">closed</option>
                                                            </select>
                                                        </div><!-- input-group -->
                                                        <br>

                                                        <div class="modal-footer modal-footer_footer no-pad">
                                                            <button id="updateData355"  class="btn btn-success m-r-125">Update</button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                                            <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                              <button type="button" class="btn btn-primary">Save changes</button> -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 textarea_custom">
                                                        Follow Up Report
                                                        <textarea rows="4" id="follow_up_report1" class="follow_up_report1" cols="50" placeholder=""></textarea>
                                                    </div><!-- col -->
                                                </div><!-- row -->
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div><!-- col -->




                        </div>
                    </div><!-- component-section -->
                </div><!---individual content-->
                <div class="tab-pane fade" id="receipts" role="tabpanel" aria-labelledby="contact-tab5">
                    <div>
                        <div class="form-group">
                            <div class="row pd-l-0">
                                <div class="col-sm-3 pd-r-0">
                                    <input type="search" class="form-control form-control-sm" placeholder="Search">
                                </div>
                                <div class="col-sm mail-navbar bd-b-0 justify-content-end pd-r-0" style="height: auto;">
                                    <div class="d-none d-lg-flex">
                                        @if(Auth::guard('admin')->check())
                                            <a href=""><i data-feather="printer"></i></a>
                                        @else
                                        @can('estimate-print')
                                            <a href=""><i data-feather="printer"></i></a>
                                        @endcan
                                        @endif

                                        <a href=""><i data-feather="folder"></i></a>
                                        @if(Auth::guard('admin')->check())
                                            <a href=""><i class="fas fa-download"></i></a>
                                        @else
                                        @can('estimate-download')
                                            <a href=""><i class="fas fa-download"></i></a>
                                        @endcan
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-3 tx-right pd-r-20">
{{--                                    <button type="button" class="btn btn-sm btn-primary mg-t-0 mg-r-0 mg-b-10">Add Estimate</button>--}}

                                    @if(Auth::guard('admin')->check())
                                            <button type="button" class="btn btn-sm btn-primary mg-t-0 mg-r-0 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter6">Add Estimate</button>
                                        @else
                                        @can('estimate-create')
                                            <button type="button" class="btn btn-sm btn-primary mg-t-0 mg-r-0 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter6">Add Estimate</button>
                                        @endcan
                                        @endif

                                    <form  method="post" action="{{route('add-estimate.store')}}" id="estimateForm">
                                        @csrf
                                    <div class="modal fade modal_cust" id="exampleModalCenter6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle1" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalCenterTitle6">ADD ESTIMATE</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <input type="hidden" id="lead_id" name="lead_id" value="{{$lead_management->id}}">
{{--                                                <input type="hidden" class="" name="lead_id" value="2">--}}
                                                <div class="modal-body appended_height">

                                                    <div class="row">
{{--                                                        <input type="file" name="image">--}}
                                                        <div class="offset-md-6 col-md-3">
                                                            Estimate Date <input type="text" class="bod-picker estimate_date" name="estimate_date" value="">
                                                        </div><!-- col -->
                                                        <div class="col-md-3">
                                                            Valid Date <input type="text" class="bod-picker valid_date" name="valid_date" value="">
                                                        </div><!-- col -->

                                                    </div><!-- row -->
                                                    <div class="field_wrapper">
                                                        <div class="appended_wrapper appended_wrapper_main">
                                                            <div class="row">

                                                                <div class="col-md-3">
                                                                    Item
                                                                    <!-- <input type="text" class="form-control" placeholder="Firstname"> -->
                                                                    <div class="d-md-flex mg-t-5">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text form-control-sm wd-100p brief_icon" id="basic-addon1"><i class="icon ion-briefcase"></i></span>
                                                                        </div>
                                                                        <input type="text" name="item[]" class="item form-control-sm" aria-label="item" aria-describedby="basic-addon1">
                                                                        <p id="item_error" style="color: red"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="mg-t-5">
                                                                        Qty <input type="text" class="quantity" name="quantity[]" value="">
                                                                    </div>
                                                                </div><!-- col -->
                                                                <div class="col-md-3">
                                                                    <div class="mg-t-5">
                                                                        Rate <input type="text" class="rate" name="rate[]" value="">
                                                                    </div>
                                                                </div><!-- col -->
                                                                <div class="col-md-3">
                                                                    <div class="mg-t-5">
                                                                        Total <input type="text" class="total" name="total[]" value="">
                                                                    </div>
                                                                </div><!-- col -->
                                                            </div><!-- row -->
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <div class="mg-t-5">
                                                                        Discount
                                                                        <div class="d-md-flex pd-0">
                                                                            <input type="text" name="discount_percent[]" class="discount_percent form-control form-control-sm wd-40" required="">
                                                                            <div class="input-group-append">
                                                                                <span class="input-group-text form-control-sm pd-5 border_r_0" id="basic-addon2">%</span>
                                                                            </div>
                                                                            <input type="text" name="discount_amount[]" class="discount_amount form-control form-control-sm wd-80" required="">
                                                                        </div>

                                                                    </div>
                                                                </div><!-- col -->
                                                                <div class="col-md-9">
                                                                    <div class="mg-t-5">
                                                                        Description<input type="text" class="description" name="description[]">

                                                                    </div>
                                                                </div><!-- col -->
                                                            </div><!-- row -->
                                                        </div>
                                                        <div class="appended">
                                                            <a href="javascript:void(0);" class="add_button" title="Add field"><img src="../assets/img/add-icon.png"/></a>
                                                        </div>
                                                    </div>
                                                    <div class="row m-t-26px">
                                                        <hr class="step2_hr">
                                                        <div class="col-md-6">

                                                            <div class="mg-t-5">
                                                                Final Note
                                                                <textarea rows="5" placeholder="" name="final_note" class="final_note wd-100p"></textarea>
                                                            </div>
                                                        </div><!-- col -->
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="mg-t-5">
                                                                        Sub-Total
                                                                        <input type="text" name="sub_total" class="sub_total form-control form-control-sm" readonly>
                                                                    </div>
                                                                </div><!-- col -->
                                                                <div class="col-md-6">
                                                                    <div class="mg-t-5">
                                                                        Additional Charges
                                                                        <input type="text" class="additional_charge" name="additional_charge">
                                                                    </div>
                                                                </div><!-- col -->
                                                            </div><!-- row -->
                                                            <div class="mg-t-5">
                                                                Grand Total
                                                                <input type="text" name="grand_total" class="grand_total form-control form-control-sm" readonly>
                                                            </div>

                                                        </div><!-- col -->


                                                    </div><!-- row -->
                                                    <div class="modal-footer modal-footer_footer">
                                                        <button  class="estimateSubbmit btn btn-success mg-r-10">Update</button>
                                                        <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                        <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                          <button type="button" class="btn btn-primary">Save changes</button> -->
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>



                                </div><!-- col -->
                            </div>
                        </div> <!--form-group-->
                        <div class="card">
                            <table id="example1" class="table">
                                <thead>
                                <tr>
{{--                                    <th class="wd-5px"><input type="checkbox" aria-label="Checkbox for following text input"></th>--}}
                                    <th class="wd-10px">SN.</th>
                                    <th class="wd-15p">Estimate No.</th>
                                    <th class="wd-20p">Date</th>
                                    <th class="wd-15p">Amount</th>
                                    <th class="wd-20p">Validity</th>
                                    <th class="wd-15p">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    @foreach($estimate as $key=> $estimates)
{{--                                    <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
                                    <td>{{++$key}}</td>
                                    <td>EST00{{$estimates->estimate_number}}</td>
                                    <td>{{$estimates->estimate_date}}</td>
                                    <td>{{$estimates->grand_total}}</td>
                                    <td>{{$estimates->valid_date}}</td>
                                    <td class="d-md-flex">
                                        @if(Auth::guard('admin')->check())
                                            <div class="mg-r-20" title="View"><a href="{{route('add-estimate.show',$estimates->id)}}"><i class="icon ion-clipboard text-success"></i></a></div>
                                        @else
                                        @can('estimate-view')
                                            <div class="mg-r-20" title="View"><a href="{{route('add-estimate.show',$estimates->id)}}"><i class="icon ion-clipboard text-success"></i></a></div>
                                        @endcan
                                        @endif

                                        @if(Auth::guard('admin')->check())
                                            <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>
                                        @else
                                        @can('estimate-edit')
                                            <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>
                                        @endcan
                                        @endif

                                        @if(Auth::guard('admin')->check())
                                            <div class="mg-r-20" title="Delete"><a href="{{route('add-estimate.destroy',$estimates->id)}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                                        @else
                                        @can('estimate-delete')
                                            <div class="mg-r-20" title="Delete"><a href="{{route('add-estimate.destroy',$estimates->id)}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                                        @endcan
                                        @endif



{{--                                        <div class="mg-r-20" title="Delete"><a href="{{route('add-estimate.destroy',$estimates->id)}}" onclick="return confirm('Are you sure you want to delete this item?');"><i class="icon ion-trash-b text-danger"></i></a></div>--}}




                                        {{--                            fot delete popup--}}

                                        <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">

                                                        <div class="conform_del">
                                                            <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                                        </div><!-- conform_del -->

                                                        <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                                            <a class="btn btn-success mg-r-10" href="{{route('add-estimate.destroy',$estimates->id)}}">Yes</a>
                                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div><!-- component-section -->
                </div><!--individual-content-->
            </div><!-- tab-content -->
        </div><!-- profile-body -->
    </div><!-- content-body -->
</div><!-- content -->

@endsection
@push('scripts')

    <script>



        //for estimate save

        $(".estimateSubbmit").click(function(e){
            e.preventDefault();
            var $form = $("#estimateForm");
            // var lead_id = $('#lead_id').val();
            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize(),
                success: function (data, status) {
                    if(data.error){
                        return;
                    }
                    $('#myModal11').modal('hide');
                    toastr.success(data.success);
                    window.location.reload();
                },
                error: function (xhr, status, error) {
                    var err = JSON.parse(xhr.responseText);
                    // alert(err.errors.valid_date);
                    // $("#firstName_error").css('display','block');

                    // $('#item_error').html(err.errors.valid_date);
                    // // $('#rphoneNumber_error').html(err.errors.phoneNumber).css('display', 'block');
                    // $("#middleName_error").css('display','block');
                    // $('#middleName_error').html(err.errors.middleName);
                    // $("#lastName_error").css('display','block');
                    // $('#lastName_error').html(err.errors.lastName);
                    // $("#loan_email_error").css('display','block');

                }
            });
        });

        //for rate quantity and total

        $(".rate").on('input',function () {

            // var rate1 = $('.rate1').val();
            // alert(rate1);
            var rate = $('.rate').val();
            var quantity= $('.quantity').val();
            // alert(quantity);
            var total =(parseFloat(rate) * parseFloat(quantity));

            if(rate){

                $('.total').append($('.total')).val(total);
                $('.sub_total').append($('.sub_total')).val(total);
                $('.grand_total').append($('.grand_total')).val(total);

            }
            else{
                $('.total').append($('.total')).val(0);
                $('.sub_total').append($('.sub_total')).val(0);
                $('.grand_total').append($('.grand_total')).val(0);
            }
        });


        //for dynamic field of rate quantity total
        $(".rate1").on('input',function () {

            var rate = $('.rate1').val();
            var quantity= $('.quantity1').val();

            alert(quantity);
            var total =(parseFloat(rate) * parseFloat(quantity));

            if(rate){

                $('.total1').append($('.total1')).val(total);

            }
            else{
                $('.total1').append($('.total1')).val(0);
            }
        });

        //for discount percent of 1st field

        $(".discount_percent").on('input',function () {

            var discount_percent= $('.discount_percent').val();
            var total= $('.total').val();



            var discount_amount =((parseFloat(total)) * parseFloat(discount_percent))/100;
            var sub_total=((parseFloat(total)) - parseFloat(discount_amount));
            if(discount_amount){

                $('.discount_amount').append($('.discount_amount')).val(discount_amount);
                $('.sub_total').append($('.sub_total')).val(sub_total);
                $('.grand_total').append($('.grand_total')).val(sub_total);


            }
            else{
                $('.discount_amount').append($('.discount_amount')).val(0);
                $('.sub_total').append($('.sub_total')).val(total);
                $('.grand_total').append($('.grand_total')).val(total);
            }



        });

        //for final total and sub total

        $(".additional_charge").on('input',function () {

            var sub_total= $('.sub_total').val();
            var additional_charge= $('.additional_charge').val();
            var total = $('.total').val();
            // var discount_amount = $('.discount_amount').val();


            // alert(total);
            var grand_total =((parseFloat(sub_total)) + parseFloat(additional_charge));

            if(additional_charge){

                var grand_total =((parseFloat(sub_total)) + parseFloat(additional_charge));
                $('.grand_total').append($('.grand_total')).val(grand_total);

            }
            else{
                var grand_total =((parseFloat(sub_total)) + parseFloat(0));

                $('.grand_total').append($('.grand_total')).val(grand_total);
            }



        });

        //for contact person store
        $("#saveData").click(function () {

            var lead_id = $('#lead_id').val();
            var name = $('#name').val();
            var job_title = $('#job_title').val();
            var email = $('#email').val();
            var phone_number = $('#phone_number').val();

            // alert(branchId);
            //
            $.ajax({

                type: "POST",
                url: '{{url('/contact-person-store')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    lead_id : lead_id,
                    name : name,
                    job_title : job_title,
                    email : email,
                    phone_number : phone_number

                },

                success: function (data, status) {
                    if(data.error){
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log('success');

                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    $('#category_error').append(err.errors.title);
                }

            });

        });
//for add followup
        $("#saveData1").click(function () {
// alert('fjdkgl');
            var lead_id = $('#lead_id').val();
            var follow_up_date = $('#follow_up_date').val();
            var next_follow_up_date = $('#next_follow_up_date').val();
            var follow_up_report = $('#follow_up_report').val();
            var todo = $('.todo').val();


            // alert(branchId);
            //
            $.ajax({

                type: "POST",
                url: '{{url('/add-follow-store')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    lead_id : lead_id,
                    next_follow_up_date : next_follow_up_date,
                    todo : todo,
                    follow_up_date : follow_up_date,
                    follow_up_report : follow_up_report

                },

                success: function (data, status) {
                    if(data.error){
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log('success');

                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    $('#category_error').append(err.errors.title);
                }

            });

        });
//for contact person update

        $(".updateData").click(function(e){
            e.preventDefault()
            var contact_id = $('.contact_id44').val();
            var lead_id = $('#lead_id').val();
            // var contact_id = $('#contact_id').val();
            var name = $('#name1').val();
            var job_title = $('#jobTitle1').val();
            var phone_no = $('#phone_number1').val();
            var email = $('#email1').val();


            // alert(contact_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/contact-person-update')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    contact_id : contact_id,
                    lead_id : lead_id,
                    name : name,
                    job_title: job_title,
                    phone_no: phone_no,
                    email:email,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);
                    // var advance_due= $('#advance_due').val();


                    // $('#name1').append($('#name1').val(data.contact_person.name));
                    // $('#jobTitle1').append($('#jobTitle1').val(data.contact_person.jobTitle));
                    // $('#phone_number1').append($('#phone_number1').val(data.contact_person.phoneNumber));
                    // $('#email1').append($('#email1').val(data.contact_person.email));


                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });


        $(".editData").click(function(e){
            e.preventDefault()
            var contact_id = $(this).data('id');



            // alert(contact_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/contact-person-edit')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    contact_id : contact_id,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // toastr.success(data.success);
                    // window.location.reload();
                    console.log(data);
                    // var advance_due= $('#advance_due').val();


                    $('#name1').append($('#name1').val(data.contact_person.name));
                    $('.contact_id44').append($('.contact_id44').val(data.contact_person.id));
                    $('#jobTitle1').append($('#jobTitle1').val(data.contact_person.jobTitle));
                    $('#phone_number1').append($('#phone_number1').val(data.contact_person.phoneNumber));
                    $('#email1').append($('#email1').val(data.contact_person.email));


                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });


        $(".editData132").click(function(e){
            e.preventDefault()
            var follow_id = $(this).data('follow-id');
            // var follow_id = $("#follow_id");




            // alert(follow_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/add-follow-edit')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    follow_id : follow_id,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // toastr.success(data.success);
                    // window.location.reload();
                    console.log(data);
                    // var advance_due= $('#advance_due').val();


                    $('.follow_up_date1 ').append($('.follow_up_date1 ').val(data.add_follow.followUpDate));
                    $('.follow_id44 ').append($('.follow_id44 ').val(data.add_follow.id));
                    $('.next_follow_up_date1 ').append($('.next_follow_up_date1 ').val(data.add_follow.nextFollowUpDate));
                    $('.todo1').append($('.todo1').val(data.add_follow.toDo));
                    $('.follow_up_report1').append($('.follow_up_report1').val(data.add_follow.followUpReport));


                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });
//for follow up update

        $("#updateData355").click(function(e){
            e.preventDefault()
            // var follow_id = $(this).data('id');
            var lead_id = $('#lead_id').val();
            // var follow_id = $('.follow1').data('follow-id');
            var follow_id = $('.follow_id44').val();

            var follow_up_date = $('#follow_up_date1').val();
            var next_follow_up_date = $('#next_follow_up_date1').val();
            var todo = $('#todo1').val();
            var follow_up_report = $('#follow_up_report1').val();

            // alert(follow_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/add-follow-update')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    follow_id : follow_id,
                    lead_id : lead_id,
                    follow_up_date : follow_up_date,
                    next_follow_up_date: next_follow_up_date,
                    todo: todo,
                    follow_up_report:follow_up_report,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);
                    // var advance_due= $('#advance_due').val();


                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });



    </script>
{{--    <script type="text/javascript">--}}
{{--        $(".bod-picker123").nepaliDatePicker({--}}
{{--            // dateFormat: "%D, %M %d, %y",--}}

{{--            dateFormat: "%y-%m-%d",--}}
{{--            closeOnDateSelect: true--}}

{{--            // $('#input-id').val(getNepaliDate());--}}
{{--        });--}}
{{--        $("#clear-bth").on("click", function(event) {--}}
{{--            $(".bod-picker").val('');--}}
{{--        });--}}

{{--    </script>--}}
@endpush
