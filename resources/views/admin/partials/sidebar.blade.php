<div class="sidebar">
    <div class="sidebar-header">
        <div>
            <a href="{{route('home')}}" class="sidebar-logo"><span>e-billing</span></a>
            <small class="sidebar-logo-headline">welcome!!!</small>
        </div>
    </div><!-- sidebar-header -->
    <div id="dpSidebarBody" class="sidebar-body">
        <ul class="nav nav-sidebar">
            <li class="nav-label"><label class="content-label">Our feature Pages</label></li>
            <li class="nav-item show">
                <a href="{{route('home')}}" class="nav-link"><i data-feather="box"></i> Dashboard</a>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="layout"></i> Demo Request</a>
                <nav class="nav nav-sub">
                    <a href="{{route('demo-pending.index')}}" class="nav-sub-link">Pending Request</a>
                    <a href="{{url('demo-approved')}}" class="nav-sub-link">Approved Request</a>
                    <a href="{{url('demo-rejected')}}" class="nav-sub-link">Rejected Request</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="layout"></i> Order</a>
                <nav class="nav nav-sub">
                    <a href="{{route('order-pending.index')}}" class="nav-sub-link">Pending Order</a>
                    <a href="{{url('order-approved')}}" class="nav-sub-link">Approved Order</a>
                    <a href="{{url('order-rejected')}}" class="nav-sub-link">Rejected Order</a>
                    <a href="{{url('order-suspended')}}" class="nav-sub-link">Suspended Order</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="layout"></i> Bill</a>
                <nav class="nav nav-sub">
                    <a href="{{route('bill-pending.index')}}" class="nav-sub-link">Pending Bills</a>
                    <a href="{{route('bill-paid.index')}}" class="nav-sub-link">Paid Bills</a>
                    <a href="{{route('bill-canceled.index')}}" class="nav-sub-link">Cancled Bills</a>
                </nav>
            </li>
            @if(Auth::guard('admin')->check())
            <li class="nav-item">
                <a href="{{route('client.index')}}" class="nav-link"><i data-feather="user"></i> Client</a>
            </li>

            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="layout"></i> Student</a>
                <nav class="nav nav-sub">
                    <a href="{{route('student-pending.index')}}" class="nav-sub-link">Pending Students</a>
                    <a href="{{route('student-approved.index')}}" class="nav-sub-link">Approved Students</a>
                    <a href="{{route('student-rejected.index')}}" class="nav-sub-link">Rejected Students</a>
                    <a href="{{route('student-suspended.index')}}" class="nav-sub-link">Suspanded Students</a>
                </nav>
            </li>

            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="layout"></i> Partners</a>
                <nav class="nav nav-sub">
                    <a href="{{route('partner-pending.index')}}" class="nav-sub-link">Pending Partners</a>
                    <a href="{{route('partner-approved.index')}}" class="nav-sub-link">Approved Partners</a>
                    <a href="{{route('partner-rejected.index')}}" class="nav-sub-link">Rejected Partners</a>
                    <a href="{{route('partner-suspended.index')}}" class="nav-sub-link">Suspanded Partners</a>
                </nav>
            </li>
                <li class="nav-item">
                    <a href="" class="nav-link with-sub"><i data-feather="layout"></i> Affilated</a>
                    <nav class="nav nav-sub">
                        <a href="{{route('affilated-pending.index')}}" class="nav-sub-link">Pending Affiliated</a>
                        <a href="{{route('affilated-approved.index')}}" class="nav-sub-link">Approved Affiliated</a>
                        <a href="{{route('affilated-rejected.index')}}" class="nav-sub-link">Rejected Affiliated</a>
                        <a href="{{route('affilated-suspended.index')}}" class="nav-sub-link">Suspanded Affiliated</a>
                    </nav>
                </li>
            @endif
            <li class="nav-item">
                <a href="{{route('user.index')}}" class="nav-link"><i data-feather="user"></i> User</a>
            </li>
            <li class="nav-item">
                <a href="{{route('lead-management.index')}}" class="nav-link"><i data-feather="layers"></i> Lead</a>
            </li>
            @if(Auth::guard('admin')->check())
            <li class="nav-item">
                <a href="{{route('feedback.index')}}" class="nav-link"><i data-feather="user"></i> Feedbacks</a>
            </li>
            <li class="nav-label"><label class="content-label">Admin</label></li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="life-buoy"></i> Account</a>
                <nav class="nav nav-sub">
                    <a href="#" class="nav-sub-link">Payments</a>
                    <a href="{{route('expense.index')}}" class="nav-sub-link">Expense</a>
                    <a href="{{route('income.index')}}" class="nav-sub-link">Other Income</a>
                    <a href="{{route('bank.index')}}" class="nav-sub-link">Bank</a>
                    <a href="{{route('account-head.index')}}" class="nav-sub-link">Account Head</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link"><i data-feather="book"></i> Branches</a>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="layers"></i> Human Resource</a>
                <nav class="nav nav-sub">
                    <a href="{{route('employee.index')}}" class="nav-sub-link">Employee</a>
                    <a href="{{route('salary-payroll.index')}}" class="nav-sub-link">Salary Payroll</a>
                    <a href="{{route('salary-due.index')}}" class="nav-sub-link">Salary Due</a>
                    <a href="#" class="nav-sub-link">Attendance</a>
                    <a href="{{route('salary-master.index')}}" class="nav-sub-link">Salary Master</a>
                    <a href="{{route('salary-type.index')}}" class="nav-sub-link">Salary Type Setup</a>
                </nav>
            </li>
            <!-- <li class="nav-item">
              <a href="" class="nav-link with-sub"><i data-feather="layers"></i> Lead Management</a>
              <nav class="nav nav-sub">
                <a href="../newPages/new-lead.html" class="nav-sub-link">New Lead</a>
                <a href="../newPages/lead-follow-up.html" class="nav-sub-link">Follow Up</a>
                <a href="../newPages/lead-assignment.html" class="nav-sub-link">Assignment</a>
                <a href="../newPages/lead-list.html" class="nav-sub-link">All Lead</a>
              </nav>
            </li> -->
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="monitor"></i> Report</a>
                <nav class="nav nav-sub">
                    <a href="#" class="nav-sub-link">Journal</a>
                    <a href="#" class="nav-sub-link">Ledger</a>
                    <a href="#" class="nav-sub-link">Sales Report</a>
                    <a href="#" class="nav-sub-link">Expense Report</a>
                    <a href="#" class="nav-sub-link">Income Report</a>
                    <a href="#" class="nav-sub-link">Payin Report</a>
                    <a href="#" class="nav-sub-link">Payout Report</a>
                    <a href="#" class="nav-sub-link">Customer Due</a>
                    <a href="#" class="nav-sub-link">Employee Salary Due</a>
                    <a href="#" class="nav-sub-link">All Bills</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="pie-chart"></i> Configuration</a>
                <nav class="nav nav-sub">
                    <a href="#" class="nav-sub-link">Setting</a>
                    <a href="#" class="nav-sub-link">Payment Method</a>
                    <a href="#" class="nav-sub-link">User</a>
                    <a href="#" class="nav-sub-link">Role and Premission</a>

                </nav>
            </li>
            <li class="nav-label"><label class="content-label">Product Setup</label></li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="pie-chart"></i> Product Setup</a>
                <nav class="nav nav-sub">
                    <a href="{{route('package.index')}}" class="nav-sub-link">Packages</a>
{{--                    <a href="{{route('sub-package.index')}}" class="nav-sub-link">Sub Packages</a>--}}
                    <a href="{{route('plan-pricing.index')}}" class="nav-sub-link">Plans And Pricing</a>
                    <a href="{{route('offer-discount.index')}}" class="nav-sub-link">Offers and Discounts</a>
                    <a href="{{route('payment-type.index')}}" class="nav-sub-link">Payment Type</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="pie-chart"></i> Front-end Setup</a>
                <nav class="nav nav-sub">
                    <a href="{{route('about.index')}}" class="nav-sub-link">Aboutus</a>
                    <a href="{{route('contact.index')}}" class="nav-sub-link">Contact</a>
                    <a href="{{route('service.index')}}" class="nav-sub-link">Services</a>
                    <a href="{{route('slider.index')}}" class="nav-sub-link">Banner</a>
                    <a href="{{route('social.index')}}" class="nav-sub-link">Social link</a>
                    <a href="{{route('contact-view')}}" class="nav-sub-link">Contact view</a>

                </nav>
            </li>
            @endif
            <li class="nav-item">

            </li>


            @if (Auth::guard('web')->check())
                <li class="nav-label"><label class="content-label">Client</label></li>
{{--                <li class="nav-item">--}}
                    <a href="{{route('student-pending.create')}}" class="nav-link"><i data-feather="layout"></i> Student</a>
{{--                    <nav class="nav nav-sub">--}}
{{--                        <a href="{{route('student-pending.index')}}" class="nav-sub-link">Pending Students</a>--}}
{{--                        <a href="{{route('student-approved.index')}}" class="nav-sub-link">Approved Students</a>--}}
{{--                        <a href="{{route('student-rejected.index')}}" class="nav-sub-link">Rejected Students</a>--}}
{{--                        <a href="{{route('student-suspended.index')}}" class="nav-sub-link">Suspanded Students</a>--}}
{{--                    </nav>--}}
{{--                </li>--}}

{{--                <li class="nav-item">--}}
                    <a href="{{route('partner-pending.create')}}" class="nav-link"><i data-feather="layout"></i> Partners</a>
{{--                    <nav class="nav nav-sub">--}}
{{--                        <a href="{{route('partner-pending.index')}}" class="nav-sub-link">Pending Partners</a>--}}
{{--                        <a href="{{route('partner-approved.index')}}" class="nav-sub-link">Approved Partners</a>--}}
{{--                        <a href="{{route('partner-rejected.index')}}" class="nav-sub-link">Rejected Partners</a>--}}
{{--                        <a href="{{route('partner-suspended.index')}}" class="nav-sub-link">Suspanded Partners</a>--}}
{{--                    </nav>--}}
{{--                </li>--}}
{{--                <li class="nav-item">--}}
                    <a href="{{route('affilated-pending.create')}}" class="nav-link"><i data-feather="layout"></i> Affilated</a>
{{--                    <nav class="nav nav-sub">--}}
{{--                        <a href="{{route('affilated-pending.index')}}" class="nav-sub-link">Pending Affiliated</a>--}}
{{--                        <a href="{{route('affilated-approved.index')}}" class="nav-sub-link">Approved Affiliated</a>--}}
{{--                        <a href="{{route('affilated-rejected.index')}}" class="nav-sub-link">Rejected Affiliated</a>--}}
{{--                        <a href="{{route('affilated-suspended.index')}}" class="nav-sub-link">Suspanded Affiliated</a>--}}
{{--                    </nav>--}}
{{--                </li>--}}

            @endif

            @if (Auth::guard('web')->check())
            <li class="nav-label"><label class="content-label">Admin</label></li>
            <li class="nav-item">
                <a href="" class="nav-link"><i data-feather="layers"></i> Income</a>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link"><i data-feather="layers"></i> Received Payments</a>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link"><i data-feather="layers"></i> Setting</a>
            </li>
            @endif


        </ul>

        <hr class="mg-t-30 mg-b-25">



        <ul class="nav nav-sidebar">
            @if (Auth::guard('web')->check())
            <li class="nav-item">
                <a href="" class="nav-link"><i data-feather="layout"></i> Support</a>
            </li>
            @endif
            @if (Auth::guard('admin')->check())
            <li class="nav-item"><a href="{{route('themes.index')}}" class="nav-link"><i data-feather="aperture"></i> Themes</a></li>

                @endif
                <li class="nav-item"><a href="#" class="nav-link"><i data-feather="help-circle"></i> Documentation</a></li>
        </ul>


    </div><!-- sidebar-body -->
</div><!-- sidebar -->
