<div class="content-header justify-content-between">
    <div>
        <!-- <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Pages</a></li>
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Analytics &amp; Monitoring</li>
          </ol>
        </nav> -->
        <h4 class="content-title content-title-xs">Dashboard</h4>
    </div>
</div><!-- content-header -->
<div class="content-body">
    <div class="row row-sm">
        <div class="col-xl-8">
            <div class="card card-hover card-analytics-one">
                <div class="card-body">
                    <div class="row row-sm">
                        <div class="col-sm-8 col-md-8">
                            <label class="tx-medium tx-14 tx-color-01 mg-b-2">Summery &amp; Counter</label>
                            <p class="tx-12 tx-color-03 mg-b-20">Detailed breakdown is available on your report page. <a href="" class="link-05">Learn more</a></p>
                            <div class="chart-wrapper">
                                <div id="flotChart" class="flot-chart"></div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4 mg-t-15 mg-sm-t-0">
                            <div class="d-flex mg-b-20">
                                <select class="custom-select custom-select-sm tx-12">
                                    <option selected="">Last 30 days</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                                <button class="btn btn-secondary btn-xs btn-icon pd-y-0 mg-l-5 flex-shrink-0"><i data-feather="download"></i></button>
                            </div>
                            <label class="tx-color-03 tx-12 mg-b-5">Total InComing</label>
                            <h4 class="card-value"><small>NPR</small> 9,174,700</h4>
                            <span class="tx-12 tx-color-04"><span class="tx-success"><i class="icon ion-android-arrow-up"></i> 1.5%</span> higher from last year</span>

                            <hr class="mg-y-10 op-0">

                            <label class="tx-color-03 tx-12 mg-b-5">Total OutGoing</label>
                            <h4 class="card-value"><small>NPR</small> 2,628,740</h4>
                            <span class="tx-12 tx-color-04"><span class="tx-danger"><i class="icon ion-android-arrow-up"></i> 0.9%</span> higher from last year</span>

                        </div><!-- col -->
                    </div><!-- row -->
                </div>
                <div class="card-footer bg-transparent">
                    <div class="row no-gutters">
                        <div class="col-6 col-sm">
                            <div class="d-flex align-items-baseline mg-b-5">
                                <h4 class="tx-numeric tx-normal">134</h4>
                                <span class="card-value-sub tx-success"><i class="icon ion-android-arrow-up"></i> 3.8%</span>
                            </div>
                            <label class="content-label content-label-xs">Total Sales Bill</label>
                        </div><!-- col -->
                        <div class="col-6 col-sm">
                            <div class="d-flex align-items-baseline mg-b-5">
                                <h4 class="tx-numeric tx-normal">874</h4>
                                <span class="card-value-sub tx-danger"><i class="icon ion-android-arrow-down"></i> 0.3%</span>
                            </div>
                            <label class="content-label content-label-xs">Total Purchase Entry</label>
                        </div><!-- col -->
                        <div class="col-6 col-sm mg-t-15 mg-sm-t-0">
                            <div class="d-flex align-items-baseline mg-b-5">
                                <h4 class="tx-numeric tx-normal">30</h4>
                                <span class="card-value-sub tx-danger"><i class="icon ion-android-arrow-down"></i> 0.2%</span>
                            </div>
                            <label class="content-label content-label-xs">Total Customer</label>
                        </div><!-- col -->
                        <div class="col-6 col-sm mg-t-15 mg-sm-t-0">
                            <div class="d-flex align-items-baseline mg-b-5">
                                <h4 class="tx-numeric tx-normal">20</h4>
                                <span class="card-value-sub tx-success"><i class="icon ion-android-arrow-up"></i> 0.3%</span>
                            </div>
                            <label class="content-label content-label-xs">Total Supplier</label>
                        </div><!-- col -->
                        <div class="col-6 col-sm mg-t-15 mg-sm-t-0">
                            <div class="d-flex align-items-baseline mg-b-5">
                                <h4 class="tx-numeric tx-normal">505</h4>
                                <span class="card-value-sub tx-danger"><i class="icon ion-android-arrow-down"></i> 1.1%</span>
                            </div>
                            <label class="content-label content-label-xs">Total Items</label>
                        </div><!-- col -->
                        <div class="col-6 col-sm mg-t-15 mg-sm-t-0">
                            <div class="d-flex align-items-baseline mg-b-5">
                                <h4 class="tx-numeric tx-normal">205</h4>
                                <span class="card-value-sub tx-danger"><i class="icon ion-android-arrow-down"></i> 1.1%</span>
                            </div>
                            <label class="content-label content-label-xs">Total Leads</label>
                        </div><!-- col -->
                    </div><!-- row -->
                </div><!-- card-footer -->
            </div><!-- card -->
        </div><!-- col -->
        <div class="col-md-5 col-lg-6 col-xl-4 mg-t-15 mg-sm-t-20 mg-xl-t-0">
            <!-- <div class="ht-100p bd-sm-l pd-x-15 pd-sm-x-20 pd-t-15 pd-sm-t-20 pd-b-15">
              <div id="datepicker1"></div>
            </div> -->
            <div class="card card-hover card-transactions">
                <div class="card-header bg-transparent">
                    <h6 class="card-title mg-b-0">Calander</h6>
                </div>
            </div>
        </div><!-- col -->
        <div class="col-sm-6 col-xl mg-sm-t-20">
            <div class="card card-hover card-todo">
                <div class="card-header bg-transparent pd-y-15 pd-l-15 pd-r-10">
                    <h6 class="card-title mg-b-0">Latest Follow Up</h6>
                    <nav class="nav">
                        <a href="" class="link-gray-500"><i data-feather="help-circle" class="svg-16"></i></a>
                        <a href="" class="link-gray-500"><i data-feather="more-vertical" class="svg-16"></i></a>
                    </nav>
                </div><!-- card-header -->
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="d-flex align-items-center justify-content-between">
                            <h6 class="tx-13 mg-b-3"><a href="" class="link-01">Javascript countdown timer link</a></h6>
                            <span class="badge bg-orange tx-white">Meeting</span>
                        </div>
                        <p class="todo-date">Today, July 29, 2019</p>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex align-items-center justify-content-between">
                            <h6 class="tx-13 mg-b-3"><a href="" class="link-01">Javascript countdown timer link</a></h6>
                            <span class="badge bg-orange tx-white">Meeting</span>
                        </div>
                        <p class="todo-date">Today, July 29, 2019</p>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex align-items-center justify-content-between">
                            <h6 class="tx-13 mg-b-3"><a href="" class="link-01">Javascript countdown timer link</a></h6>
                            <span class="badge bg-orange tx-white">Phone Call</span>
                        </div>
                        <p class="todo-date">Today, July 29, 2019</p>
                    </li>
                    <li class="list-group-item">
                        <div class="d-flex align-items-center justify-content-between">
                            <h6 class="tx-13 mg-b-3"><a href="" class="link-01">Javascript countdown timer link</a></h6>
                            <span class="badge bg-orange tx-white">Phone Call</span>
                        </div>
                        <p class="todo-date">Today, July 29, 2019</p>
                    </li>
                </ul>
                <div class="card-footer bg-transparent bd-t-0">
                    <a href="" class="btn btn-block"><i class="icon ion-plus tx-12"></i> Add Lead</a>
                </div><!-- card-footer -->
            </div><!-- card -->
        </div><!-- col -->


        <div class="col-md-12 col-xl-5 mg-t-15 mg-sm-t-20 order-md-3 order-lg-0">
            <div class="card card-hover card-transactions">
                <div class="card-header bg-transparent">
                    <h6 class="card-title mg-b-0">Recent Transactions</h6>
                    <nav class="nav nav-card-icon">
                        <a href=""><i data-feather="download"></i></a>
                        <a href=""><i data-feather="printer"></i></a>
                        <a href=""><i data-feather="more-horizontal"></i></a>
                    </nav>
                </div><!-- card-header -->
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="avatar"><span class="avatar-initial rounded-circle bg-primary-light"><i data-feather="shopping-bag" class="svg-18"></i></span></div>
                        <div class="mg-l-10 mg-sm-l-15">
                            <h6>Purchase Receipt - D107046</h6>
                            <small>Today, August 01, 2019</small>
                        </div>
                        <div class="mg-l-auto tx-right">
                            <h6>-$69.99</h6>
                            <small class="d-none d-sm-inline">Paid via PayPal</small>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="avatar"><span class="avatar-initial rounded-circle bg-teal-light"><i data-feather="package" class="svg-18"></i></span></div>
                        <div class="mg-l-10 mg-sm-l-15">
                            <h6>Purchase Shipment - D108015</h6>
                            <small>Today, August 01, 2019</small>
                        </div>
                        <div class="mg-l-auto tx-right">
                            <h6>+$310.50</h6>
                            <small class="d-none d-sm-inline">Bank transfer</small>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="avatar"><span class="avatar-initial rounded-circle bg-pink-light"><i data-feather="share" class="svg-18"></i></span></div>
                        <div class="mg-l-10 mg-sm-l-15">
                            <h6>Purchase Receipt - D107046</h6>
                            <small>Yesterday, July 31, 2019</small>
                        </div>
                        <div class="mg-l-auto tx-right">
                            <h6>-$69.99</h6>
                            <small class="d-none d-sm-inline">Paid via card</small>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="avatar"><span class="avatar-initial rounded-circle bg-pink-light"><i data-feather="share" class="svg-18"></i></span></div>
                        <div class="mg-l-10 mg-sm-l-15">
                            <h6>Purchase Receipt - D107046</h6>
                            <small>Monday, July 27, 2019</small>
                        </div>
                        <div class="mg-l-auto tx-right">
                            <h6>-$69.99</h6>
                            <small class="d-none d-sm-inline">Paid via card</small>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="avatar"><span class="avatar-initial rounded-circle bg-teal-light"><i data-feather="package" class="svg-18"></i></span></div>
                        <div class="mg-l-10 mg-sm-l-15">
                            <h6>Purchase Shipment - D108015</h6>
                            <small>Saturday, July 20, 2019</small>
                        </div>
                        <div class="mg-l-auto tx-right">
                            <h6>+$310.50</h6>
                            <small class="d-none d-sm-inline">Bank transfer</small>
                        </div>
                    </li>
                </ul><!-- list-group -->
                <div class="card-footer bg-transparent">
                    <a href="">View All Transactions<i class="icon ion-chevron-right"></i><i class="icon ion-chevron-right"></i></a>
                </div><!-- card-footer -->
            </div><!-- card -->
        </div><!-- col -->
        <div class="col-sm-6 col-xl-3 mg-t-15 mg-sm-t-20 order-sm-1 order-xl-0">
            <div class="card card-hover card-project-pink">
                <h5 class="card-title mg-10">Sticky Notes (Private)</h5>
                <textarea class="wd-100p ht-100p bd bd-none bg-transparent pd-10"></textarea>
            </div><!-- card -->
        </div><!-- col -->

        <div class="col-md-12 col-xl-8 mg-t-15 mg-sm-t-20">
            <div class="row row-sm mg-b-20">
                <div class="col-sm-6">
                    <div class="card card-hover card-profile-visits">
                        <div>
                            <p class="content-label mg-b-5 tx-pink">Total Customer Due</p>
                            <div class="d-flex align-items-end">
                                <h4 class="tx-color-01 tx-numeric tx-normal mg-b-0">361,782</h4>
                                <span class="d-flex align-items-center tx-success mg-l-2 tx-13"><i data-feather="arrow-up" class="svg-14 stroke-25"></i>1.2%</span>
                            </div>
                        </div>
                        <div class="flot-wrapper">
                            <div id="flotChart10" class="flot-chart"></div>
                        </div>
                    </div><!-- card -->
                </div><!-- col -->
                <div class="col-sm-6 mg-t-15 mg-sm-t-0">
                    <div class="card card-hover card-profile-visits">
                        <div>
                            <p class="content-label mg-b-5 tx-orange">Total Supplier Due</p>
                            <div class="d-flex align-items-end">
                                <h4 class="tx-color-01 tx-numeric tx-normal mg-b-0">102,004</h4>
                                <span class="d-flex align-items-center tx-danger mg-l-2 tx-13"><i data-feather="arrow-down" class="svg-14 stroke-25"></i>0.4%</span>
                            </div>
                        </div>
                        <div class="flot-wrapper">
                            <div id="flotChart11" class="flot-chart"></div>
                        </div>
                    </div><!-- card -->
                </div><!-- col -->
            </div><!--row-->
            <div class="card card-hover card-sale-location">
                <div class="card-header bg-transparent">
                    <div>
                        <h6 class="card-title mg-b-0">Sales Revenue by Branches</h6>
                    </div>
                    <nav class="nav">
                        <a href="" class="link-gray-500"><i data-feather="help-circle" class="svg-16"></i></a>
                        <a href="" class="link-gray-500"><i data-feather="more-vertical" class="svg-16"></i></a>
                    </nav>
                </div><!-- card-header -->
                <div class="card-body">
                    <div class="list-group-wrapper order-2 order-md-0 mg-t-20 mg-sm-t-30 mg-md-t-0">
                        <label class="content-label mg-b-8">Your Top Branches</label>
                        <ul class="list-group list-group-flush mg-b-15">
                            <li class="list-group-item">
                                <span class="bg-primary"></span>
                                <span>Kathmandu</span>
                                <span class="tx-medium">$530</span>
                            </li>
                            <li class="list-group-item">
                                <span class="bg-teal"></span>
                                <span>Pokhara</span>
                                <span class="tx-medium">$500</span>
                            </li>
                            <li class="list-group-item">
                                <span class="bg-warning"></span>
                                <span>Butwal</span>
                                <span class="tx-medium">$475</span>
                            </li>
                            <li class="list-group-item">
                                <span class="bg-pink"></span>
                                <span>Birjung</span>
                                <span class="tx-medium">$430</span>
                            </li>
                            <li class="list-group-item">
                                <span class="bg-purple"></span>
                                <span>Nepalgung</span>
                                <span class="tx-medium">$405</span>
                            </li>
                            <li class="list-group-item">
                                <span class="bg-success"></span>
                                <span>Palpa</span>
                                <span class="tx-medium">$381</span>
                            </li>
                        </ul>
                        <a href="" class="d-flex align-items-center tx-12">Show full report <i class="icon ion-android-arrow-forward mg-l-5"></i></a>
                    </div>
                    <div class="vmap-wrapper">
                        <div id="vmap" class="vmap order-1 order-md-0"></div>
                    </div>
                </div><!-- card-body -->
            </div><!-- card -->
        </div><!-- col -->
        <div class="col-sm-6 col-xl-4 mg-t-15 mg-sm-t-20">
            <div class="card card-hover card-pie-one">
                <!--
                  <div class="d-flex">

                    <h6 class="card-title mg-b-0">Payment Report</h6>
                    <select class="custom-select custom-select-sm tx-12">
                      <option selected="">Last 30 days</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </div> -->

                <!-- </div>card-header -->
                <div class="card pd-t-15 pd-r-15 pd-b-0 pd-l-15">
                    <div class="row row-sm">
                        <div class="col-sm-6 col-md-6">
                            <label class="tx-medium tx-14 tx-color-01 mg-b-2">Payments Report</label>
                            <p class="tx-12 tx-color-03">Detailed breakdown is</p>
                        </div>
                        <div class="col-sm-6 col-md-6 mg-t-15 mg-sm-t-0">
                            <div class="d-flex">
                                <select class="custom-select custom-select-sm tx-12">
                                    <option selected="">Today</option>
                                    <option value="1">Yesterday</option>
                                    <option value="2">This Month</option>
                                    <option value="3">This Year</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart-wrapper mg-b-25"><canvas id="chartDonut"></canvas></div>
                    <div class="row row-sm">
                        <div class="col-6">
                            <p class="tx-medium tx-12 mg-b-5">Total Sales</p>
                            <div class="d-flex align-items-end">
                                <h4 class="tx-color-01 tx-numeric tx-normal mg-b-0">361,782</h4>
                                <span class="d-flex align-items-center tx-success mg-l-2 tx-13"><i data-feather="arrow-up" class="svg-14 stroke-25"></i>1.2%</span>
                            </div>
                        </div><!-- col -->
                        <div class="col-6">
                            <p class="tx-medium tx-12 mg-b-5">Total Purchase</p>
                            <div class="d-flex align-items-end">
                                <h4 class="tx-color-01 tx-numeric tx-normal mg-b-0">361,782</h4>
                                <span class="d-flex align-items-center tx-success mg-l-2 tx-13"><i data-feather="arrow-up" class="svg-14 stroke-25"></i>1.2%</span>
                            </div>
                        </div><!-- col -->
                        <div class="col-6 mg-t-20">
                            <p class="tx-medium tx-12 mg-b-5">Total Other Income</p>
                            <div class="d-flex align-items-end">
                                <h4 class="tx-color-01 tx-numeric tx-normal mg-b-0">361,782</h4>
                                <span class="d-flex align-items-center tx-success mg-l-2 tx-13"><i data-feather="arrow-up" class="svg-14 stroke-25"></i>1.2%</span>
                            </div>
                        </div><!-- col -->
                        <div class="col-6 mg-t-20">
                            <p class="tx-medium tx-12 mg-b-5">Total Expense</p>
                            <div class="d-flex align-items-end">
                                <h4 class="tx-color-01 tx-numeric tx-normal mg-b-0">361,782</h4>
                                <span class="d-flex align-items-center tx-success mg-l-2 tx-13"><i data-feather="arrow-up" class="svg-14 stroke-25"></i>1.2%</span>
                            </div>
                        </div><!-- col -->
                    </div><!-- row -->
                </div>
            </div><!-- card -->
        </div><!-- col -->
        <div class="col-sm-6 col-xl-4 mg-t-15 mg-sm-t-20">
            <div class="card card-hover card-connection-one">
                <div class="card-header">
                    <h6 class="card-title mg-b-0">Recent Emails <span>926</span></h6>
                    <a href="" class="tx-12 tx-medium">Show all <i class="icon ion-android-arrow-forward"></i></a>
                </div><!-- card-header -->
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="avatar"><img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt=""></div>
                            <div class="list-body">
                                <p class="person-name"><a href="">Jacquelyn Harley</a></p>
                                <p class="person-location">Market St., San Francisco, CA</p>
                            </div><!-- list-body -->
                            <a href="" class="person-more"><i data-feather="more-vertical" class="svg-16"></i></a>
                        </li>
                        <li class="list-group-item">
                            <div class="avatar"><img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt=""></div>
                            <div class="list-body">
                                <p class="person-name"><a href="">Larisa Commons</a></p>
                                <p class="person-location">Cost Ave., Alexandria, MD</p>
                            </div><!-- list-body -->
                            <a href="" class="person-more"><i data-feather="more-vertical" class="svg-16"></i></a>
                        </li>
                        <li class="list-group-item">
                            <div class="avatar"><span class="avatar-initial rounded-circle bg-primary">j</span></div>
                            <div class="list-body">
                                <p class="person-name"><a href="">Jeffrey Ledbetter</a></p>
                                <p class="person-location">Virgil St., Graceville, FL</p>
                            </div><!-- list-body -->
                            <a href="" class="person-more"><i data-feather="more-vertical" class="svg-16"></i></a>
                        </li>
                        <li class="list-group-item">
                            <div class="avatar"><span class="avatar-initial rounded-circle bg-teal">d</span></div>
                            <div class="list-body">
                                <p class="person-name"><a href="">Debbie Winslett</a></p>
                                <p class="person-location">Randolph St., Natick, MA</p>
                            </div><!-- list-body -->
                            <a href="" class="person-more"><i data-feather="more-vertical" class="svg-16"></i></a>
                        </li>
                        <li class="list-group-item">
                            <div class="avatar"><img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt=""></div>
                            <div class="list-body">
                                <p class="person-name"><a href="">Lawrence Green</a></p>
                                <p class="person-location">Clair St., Gordon, TX</p>
                            </div><!-- list-body -->
                            <a href="" class="person-more"><i data-feather="more-vertical" class="svg-16"></i></a>
                        </li>
                    </ul>
                    <div class="d-flex card-footer bg-transparent">
                        <a href="">Show More 5 <i class="icon ion-chevron-right"></i><i class="icon ion-chevron-right"></i></a>
                    </div><!-- card-footer -->
                </div>
            </div><!-- card -->
        </div><!--col-->
        <div class="col-md-12 col-xl-8 mg-t-15 mg-sm-t-20">
            <div class="card card-hover card-deal">
                <div class="card-header bg-transparent bd-b-0">
                    <h6 class="card-title mg-b-0">Employee Status</h6>
                    <nav class="nav nav-card-icon">
                        <a href=""><i data-feather="save"></i></a>
                        <a href=""><i data-feather="printer"></i></a>
                        <a href=""><i data-feather="more-horizontal"></i></a>
                    </nav>
                </div><!-- card-header -->
                <div class="card-body pd-0">
                    <div class="table-responsive">
                        <table class="table mg-b-0">
                            <thead>
                            <tr>
                                <th class="wd-35p">Name</th>
                                <th class="wd-15p">Mobile</th>
                                <th class="wd-25p">Attandence</th>
                                <th class="wd-25p">Salary Due (NPR)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-xs"><span class="avatar-initial rounded-circle bg-secondary">s</span></div>
                                        <span class="tx-medium mg-l-10">Socrates Itumay</span>
                                    </div>
                                </td>
                                <td>9840680875</td>
                                <td>
                                    10:00AM-4:00PM
                                </td>
                                <td>302, 422.50</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-xs"><img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt=""></div>
                                        <span class="tx-medium mg-l-10">Dianne Aceron</span>
                                    </div>
                                </td>
                                <td>9840680875</td>
                                <td>
                                    <span class="badge badge-pill badge-danger">Absent</span>
                                </td>
                                <td>264, 090.00</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-xs"><img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt=""></div>
                                        <span class="tx-medium mg-l-10">Katherine Movera</span>
                                    </div>
                                </td><td>9840680875</td>
                                <td>
                                    10:00AM-4:00PM
                                </td>
                                <td>238, 720.80</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-xs"><span class="avatar-initial rounded-circle bg-primary">r</span></div>
                                        <span class="tx-medium mg-l-10">Reynante Labares</span>
                                    </div>
                                </td><td>9840680875</td>
                                <td>
                                    10:00AM-4:00PM
                                </td>
                                <td>227, 063.20</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-xs"><span class="avatar-initial rounded-circle bg-dark">d</span></div>
                                        <span class="tx-medium mg-l-10">Dexter Dela Cruz</span>
                                    </div>
                                </td><td>9840680875</td>
                                <td>
                                    10:00AM-4:00PM
                                </td>
                                <td>202, 918.00</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-xs"><span class="avatar-initial rounded-circle bg-purple">j</span></div>
                                        <span class="tx-medium mg-l-10">Johnwyne Mendez</span>
                                    </div>
                                </td><td>9840680875</td>
                                <td>
                                    10:00AM-4:00PM
                                </td>
                                <td>202, 918.00</td>
                            </tr>
                            </tbody>
                        </table>
                    </div><!-- table-responsive -->
                </div><!-- card-body -->
                <div class="card-footer bg-transparent">
                    <a href="">Show More 5 <i class="icon ion-chevron-right"></i><i class="icon ion-chevron-right"></i></a>
                </div><!-- card-footer -->
            </div><!-- card -->
        </div><!-- col -->

    </div><!-- row -->
</div><!-- content-body -->
</div><!-- content -->
