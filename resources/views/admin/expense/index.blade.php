@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Account</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Expense</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Expense</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            @if(Auth::guard('admin')->check())
               <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">Add Expanse</button>
            @else
            @can('expense-create')
               <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">Add Expanse</button>
            @endcan
            @endif


            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">NEW EXPENSES</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body text-left">
                            <form action="{{route('expense.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                            <div class="row">
                                <div class="col-md-6">

                                    Date <input type="text" class="bod-picker"  name="date" placeholder="select date" value="">

                                </div><!-- col -->
                                <div class="col-md-6">

                                    Account Head
                                    <div class="input-group">
                                        <select name="account_head_id" class="form-control form-control-sm select2-no-search no-pad">
                                            <option label="select Account Head">select Account Head</option>
{{--                                            {{dd($account_head)}}--}}
                                            @foreach($account_head as $key=>$account_heads)


                                            <option value="{{$account_heads->id}}">{{$account_heads->title}}</option>
                                          @endforeach
                                        </select>
                                    </div><!-- input-group -->

                                </div>


                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="mg-t-5">
                                        Payment Method
                                        <div class="input-group">
                                            <select name="payment_method_id" class="form-control form-control-sm select2-no-search no-pad">
                                                <option  value="" >Select Payment Method</option>
                                                @forelse($payments as $payment)
                                                    <option value="{{$payment->id}}">{{$payment->title}}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                        </div><!-- input-group -->
                                    </div>


                                </div><!-- col -->
                                <div class="col-md-6">
                                    <div class="mg-t-5">
                                        Amount
                                        <div class="input-group mg-b-0">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-outline-light btn_small" type="button" id="button-addon1">NPR</button>
                                            </div>
                                            <input type="text" name="amount" class="form-control margin_t_0" placeholder="0.0" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        </div>
                                    </div>

                                </div><!-- col -->

                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-6">


                                    <div class="mg-t-5">
                                        Bank Name: <input type="text" name="bank_name" value="">
                                    </div>

                                    <div class="mg-t-5">
                                        Transaction ID: <input type="text" name="transaction_id" value="">
                                    </div>

                                </div><!-- col -->
                                <div class="col-md-6 textarea_custom">
                                    <div class="mg-t-5">
                                        Purpose
                                        <textarea rows="4" id="description" name="description" placeholder=""></textarea>
                                    </div>

                                </div>


                            </div>
                            <div class="modal-footer modal-footer_footer">
                                <button  class="btn btn-success mg-r-10">Update</button>
                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="d-lg-flex pd-l-0">
                    <div class="pd-lg-r-20">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="All Branch"></option>
                            <option value="Firefox">Kathmandu</option>
                            <option value="Chrome">Pokhhara</option>
                            <option value="Opera">Butwal</option>
                        </select>
                    </div>
                    <div class="pd-lg-r-20">
                        <button class="btn btn-sm btn-light pd-5"><i class="fas fa-arrow-left"></i></button>
                        <label for="" class="btn btn-sm btn-light pd-5 mg-0">Today</label>
                        <button class="btn btn-sm btn-light pd-5"><i class="fas fa-arrow-right"></i></button>
                        <button class="btn btn-sm btn-light pd-5 mg-l-10"><i class="fas fa-arrow-left"></i></button>
                        <label for="" class="btn btn-sm btn-light pd-5 mg-0">This Month</label>
                        <button class="btn btn-sm btn-light pd-5"><i class="fas fa-arrow-right"></i></button>
                    </div>
                    <div class="pd-r-0">
                        <input type="text" class="form-control form-control-sm" placeholder="From">
                    </div>
                    <div class="pd-l-0 pd-r-20">
                        <input type="text" class="form-control form-control-sm" placeholder="TO">
                    </div>
                    <div class="pd-l-0 wd-20p">
                        <input type="search" class="form-control form-control-sm" placeholder="Search Account head">
                    </div>
                </div>
            </div> <!--form-group-->
{{--                    <div class="col-sm mail-navbar bd-b-0 justify-content-end mg-r-10" style="height: auto;">--}}
{{--                        <div class="d-none d-lg-flex">--}}
{{--                            @if(Auth::guard('admin')->check())--}}
{{--                               <a href=""><i data-feather="printer"></i></a>--}}
{{--                            @else--}}
{{--                            @can('expense-print')--}}
{{--                               <a href=""><i data-feather="printer"></i></a>--}}
{{--                            @endcan--}}
{{--                            @endif--}}

{{--                            <a href=""><i data-feather="folder"></i></a>--}}

{{--                            @if(Auth::guard('admin')->check())--}}
{{--                               <a href=""><i class="fas fa-download"></i></a>--}}
{{--                            @else--}}
{{--                            @can('expense-download')--}}
{{--                               <a href=""><i class="fas fa-download"></i></a>--}}
{{--                            @endcan--}}
{{--                            @endif--}}

{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div> <!--form-group-->
            <div class="card">
                <table id="example1" class="table  table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
{{--                        <th class="wd-5p"><input type="checkbox" aria-label="Checkbox for following text input"></th>--}}
                        <th class="wd-5p">SN.</th>
                        <th class="wd-15p">Date</th>
                        <th class="wd-25p">Account Head</th>
                        <th class="wd-15p">Amount</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($expense as $key=> $expenses)
                    <tr>
{{--                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
                        <td>{{++$key}}</td>
                        <input type="hidden" class="expense_id123">
                        <td>{{$expenses->date}}</td>
                        <td>@if(isset($expenses->account_head)){{$expenses->account_head->title}}@endif</td>
                        <td>{{$expenses->amount}}</td>
                        <td class="d-md-flex">
                            @if(Auth::guard('admin')->check())
                               <div class="mg-r-20" title="View"><a href="" class="view" data-id="{{$expenses->id}}"  data-toggle="modal" data-target="#exampleModalCenter2"><i class="icon ion-clipboard text-success"></i></a></div>
                            @else
                            @can('expense-view')
                               <div class="mg-r-20" title="View"><a href="" class="view" data-id="{{$expenses->id}}"  data-toggle="modal" data-target="#exampleModalCenter2"><i class="icon ion-clipboard text-success"></i></a></div>
                            @endcan
                            @endif

                            @if(Auth::guard('admin')->check())
                               <div class="mg-r-20" title="Edit"><a href="" data-toggle="modal" data-id="{{$expenses->id}}" class="edit" data-target="#exampleModalCenter1"><i class="far fa-edit text-warning"></i></a></div>
                            @else
                            @can('expense-edit')
                               <div class="mg-r-20" title="Edit"><a href="" data-toggle="modal" data-id="{{$expenses->id}}" class="edit" data-target="#exampleModalCenter1"><i class="far fa-edit text-warning"></i></a></div>
                            @endcan
                            @endif

{{--                            @if(Auth::guard('admin')->check())--}}
{{--                               <div class="mg-r-20" title="Edit"><a href="" data-toggle="modal" data-id="{{$expenses->id}}" class="edit" data-target="#exampleModalCenter1"><i class="far fa-edit text-warning"></i></a></div>--}}
{{--                            @else--}}
{{--                            @can('expense-edit')--}}
{{--                               <div class="mg-r-20" title="Edit"><a href="" data-toggle="modal" data-id="{{$expenses->id}}" class="edit" data-target="#exampleModalCenter1"><i class="far fa-edit text-warning"></i></a></div>--}}
{{--                            @endcan--}}
{{--                            @endif--}}

                            @if(Auth::guard('admin')->check())
                               <div class="mg-r-20" title="Delete"><a href=" " class="delete_data"  data-id="{{$expenses->id}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                            @else
                            @can('expense-delete')
                               <div class="mg-r-20" title="Delete"><a href=" " class="delete_data" data-id="{{$expenses->id}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                            @endcan
                            @endif





{{--                            <div class="mg-r-20" title="Cancle"><a href=""><i class="icon ion-trash-b text-danger"></i></a></div>--}}




                            {{--                            fot delete popup--}}

                            <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <div class="conform_del">
                                                <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                            </div><!-- conform_del -->

                                            <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                                <a class="final_delete btn btn-success mg-r-10" href=" ">Yes</a>
                                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle2" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle2">EXPENSES DETAIL</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <div class="detail_lead">
                                                <ul>
                                                    <li>Date : <span class="date321"></span></li>
                                                    <li>Account Head : <span class="account_head321"></span></li>
                                                    <li>Payment Method : <span class="payment_method321"></span></li>
                                                    <li>Amount : <span class="amount321"></span></li>
                                                    <li>Transaction ID : <span class="transaction_id321"></span></li>
                                                    <li>Purpose : <span class="purpose321"></span></li>
                                                </ul>
                                            </div><!-- detail_lead -->
                                            <!-- <div class="modal-footer modal-footer_footer">
                                              <button type="button" class="btn btn-success mg-r-10">Update</button>
                                              <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                                      </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>






                        </td>
                    </tr>
                   @endforeach
                    </tbody>
                </table>

                <div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalCenterTitle">NEW EXPENSES</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body text-left">
                                <form action="{{route('expense.store')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <div class="row">
                                        <div class="col-md-6">

                                            Date <input type="text" class="date bod-picker"  name="date" placeholder="select date" value="">

                                        </div><!-- col -->
                                        <div class="col-md-6">

                                            Account Head
                                            <div class="input-group">
                                                <select name="account_head_id" class="account_head form-control form-control-sm select2-no-search no-pad">
                                                    <option label="select Account Head">select Account Head</option>
                                                    {{--                                            {{dd($account_head)}}--}}
                                                    @foreach($account_head as $key=>$account_heads)


                                                        <option value="{{$account_heads->id}}">{{$account_heads->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div><!-- input-group -->

                                        </div>


                                    </div><!-- row -->
                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class="mg-t-5">
                                                Payment Method
                                                <div class="input-group">
                                                    <select name="payment_method_id" class="payment_method form-control form-control-sm select2-no-search no-pad">
                                                        <option  value="" >Select Payment Method</option>
                                                        @forelse($payments as $payment)
                                                            <option value="{{$payment->id}}">{{$payment->title}}</option>
                                                        @empty
                                                        @endforelse
                                                    </select>
                                                </div><!-- input-group -->
                                            </div>


                                        </div><!-- col -->
                                        <div class="col-md-6">
                                            <div class="mg-t-5">
                                                Amount
                                                <div class="input-group mg-b-0">
                                                    <div class="input-group-prepend">
                                                        <button class="btn btn-outline-light btn_small" type="button" id="button-addon1">NPR</button>
                                                    </div>
                                                    <input type="text" name="amount" class="amount form-control margin_t_0" placeholder="0.0" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                </div>
                                            </div>

                                        </div><!-- col -->

                                    </div><!-- row -->
                                    <div class="row">
                                        <div class="col-md-6">


                                            <div class="mg-t-5">
                                                Bank Name: <input type="text" name="bank_name" class="bank_name" value="">
                                            </div>

                                            <div class="mg-t-5">
                                                Transaction ID: <input type="text" class="transaction_id" name="transaction_id" value="">
                                            </div>

                                        </div><!-- col -->
                                        <div class="col-md-6 textarea_custom">
                                            <div class="mg-t-5">
                                                Purpose
                                                <textarea rows="4" id="description" class="purpose" name="description" placeholder=""></textarea>
                                            </div>

                                        </div>


                                    </div>
                                    <div class="modal-footer modal-footer_footer">
                                        <button  class="update btn btn-success mg-r-10">Update</button>
                                        <button  class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                        <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                          <button type="button" class="btn btn-primary">Save changes</button> -->
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
{{--    <div class="content-footer">--}}
{{--        &copy; 2019. All Rights Reserved. Created by <a href="http://themepixels.me" target="_blank">ThemePixels</a>--}}
{{--    </div><!-- content-footer -->--}}
</div><!-- content -->



@endsection


@push('scripts')
    <script>


            $(".edit").click(function (e) {
                e.preventDefault()

                var expense_id= $(this).data('id');
                // alert(expense_id);
                $.ajax({

                    url: "{{url('/expense-edit')}}",
                    type: "POST",
                    data: {
                        _token : "{{csrf_token()}}",
                        expense_id : expense_id
                    },
                    success: function (data) {
                        console.log(data);

                        $('.date').append($('.date').val(data.expense.date));
                        $('.expense_id123').append($('.expense_id123').val(expense_id));
                        $('.account_head').append($('.account_head').val(data.expense.expense_head_id));
                        $('.payment_method').append($('.payment_method').val(data.expense.payment_method_id));
                        $('.amount').append($('.amount').val(data.expense.amount));

                        $('.bank_name').append($('.bank_name').val(data.expense.bank_name));
                        $('.purpose').append($('.purpose').val(data.expense.purpose));
                        $('.transaction_id').append($('.transaction_id').val(data.expense.transaction_id));

                    },

                    error: function (error) {

                        console.log('error');

                    }


                });

            });


            $(".view").click(function (e) {
                e.preventDefault()

                var expense_id= $(this).data('id');
                // alert(expense_id);
                $.ajax({

                    url: "{{url('/expense-edit')}}",
                    type: "POST",
                    data: {
                        _token : "{{csrf_token()}}",
                        expense_id : expense_id
                    },
                    success: function (data) {
                        console.log(data);


                        $('.date321').empty().html(data.expense.date);
                        $('.account_head321').empty().html(data.expense.account_head.title);
                        $('.payment_method321').empty().html(data.expense.payment_method.title);
                        $('.amount321').empty().html(data.expense.amount);

                        $('.bank_name321').empty().html(data.expense.bank_name);
                        $('.purpose321').empty().html(data.expense.purpose);
                        $('.transaction_id321').empty().html(data.expense.transaction_id);

                    },

                    error: function (error) {

                        console.log('error');

                    }


                });

            });





            $(".update").click(function (e) {
                e.preventDefault()

                // var expense_id= $(this).data('expense_id');
                var expense_id=$('.expense_id123').val();
                var date = $('.date').val();
                var account_head = $('.account_head').val();
                var payment_method = $('.payment_method').val();
                var amount = $('.amount').val();
                var bank_name = $('.bank_name').val();
                var purpose = $('.purpose').val();
                var transaction_id = $('.transaction_id').val();

                // alert(expense_id);
                $.ajax({

                    url: "{{url('/expense-update')}}",
                    type: "POST",
                    data: {
                        _token : "{{csrf_token()}}",
                        expense_id :expense_id,
                        date:date,
                        account_head_id:account_head,
                        payment_method_id:payment_method,
                        amount:amount,
                        bank_name:bank_name,
                        description:purpose,
                        transaction_id:transaction_id

                    },
                    success: function (data) {
                        console.log(data);


                        toastr.success(data.success);
                        // console.log('success');
                        window.location.reload();

                    },

                    error: function (error) {

                        console.log('error');

                    }


                });

            });

            //for the delete data
            $(".delete_data").click(function(e){
                e.preventDefault()

                var expense_id = $(this).data('id');
                // alert(customer_id);


                $.ajax({
                    type: 'POST',
                    url: '{{url('/expense-edit')}}',
                    data: {
                        _token : "{{csrf_token()}}",
                        expense_id : expense_id,


                    },
                    success: function (data, status) {

                        if(data.error){

                            return;
                        }

                        // $('#show_modal').modal('hide');
                        // toastr.success(data.success);

                        console.log(data.expense.id);
                        // window.location.reload();
                        // alert('test');

                        $('.expense_id123').append($('.expense_id123').val(data.expense.id));


                    },
                    error: function (xhr, status, error) {
                        // console.log('error');
                        // toastr.error(error.errors);
                    }
                });

            });


            //for final delete
            $(".final_delete").click(function(e){
                e.preventDefault()

                var expense_id= $(".expense_id123").val();


                // alert(category_id1234);

                $.ajax({
                    type: 'POST',
                    url: '{{url('/expense-delete')}}',
                    data: {
                        _token : "{{csrf_token()}}",
                        expense_id : expense_id,


                    },
                    success: function (data, status) {

                        if(data.errors){
                            toastr.error(data.errors);
                            return;
                        }

                        toastr.success(data.success);
                        window.location.reload();
                        console.log(data);



                    },
                    error: function (xhr, status, error) {
                        console.log('error');
                        // toastr.error(data.errors);
                        // var err = JSON.parse(xhr.responseText);
                        // $('#category_error').append(err.errors.title);
                    }
                });

            });

    </script>
@endpush
