<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard and Admin Template">
    <meta name="author" content="ThemePixels">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="../assets/img/favicon.png">

    <title>Cassie Responsive Bootstrap 4 Dashboard and Admin Template</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="../lib/prismjs/themes/prism-tomorrow.css" rel="stylesheet">
    <link href="../lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="../lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- template css -->
    <link rel="stylesheet" href="../assets/css/cassie.css">

    <link rel="stylesheet" href="../assets/css/changes.css">

</head>

<body data-spy="scroll" data-target="#navSection" data-offset="100">

<div class="sidebar">
    <div class="sidebar-header">
        <div>
            <a href="../index.html" class="sidebar-logo"><span>cassie</span></a>
            <small class="sidebar-logo-headline">Responsive Dashboard Template</small>
        </div>
    </div><!-- sidebar-header -->
    <div id="dpSidebarBody" class="sidebar-body">
        <ul class="nav nav-sidebar">
            <li class="nav-label"><label class="content-label">Template Pages</label></li>
            <li class="nav-item show">
                <a href="../pages/dashboard-two.html" class="nav-link"><i data-feather="box"></i> Dashboard</a>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="layout"></i> Sales</a>
                <nav class="nav nav-sub">
                    <a href="../pages/app-newSales.html" class="nav-sub-link">New Sales</a>
                    <a href="../pages/app-allSales.html" class="nav-sub-link">All Sales</a>
                    <a href="#" class="nav-sub-link">Sales Return</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="lock"></i> Purchase</a>
                <nav class="nav nav-sub">
                    <a href="page-signin.html" class="nav-sub-link">New Purchase</a>
                    <a href="page-signup.html" class="nav-sub-link">All Purchase</a>
                    <a href="page-forgot.html" class="nav-sub-link">Purchase Return</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="user"></i> Customer</a>
                <nav class="nav nav-sub">
                    <a href="../newPages/newCustomer.html" class="nav-sub-link">New Customer</a>
                    <a href="page-timeline.html" class="nav-sub-link">All Customer</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="file-text"></i> Supplier</a>
                <nav class="nav nav-sub">
                    <a href="page-invoice.html" class="nav-sub-link">New Supplier</a>
                    <a href="page-pricing.html" class="nav-sub-link">All Supplier</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="x-circle"></i> Product and Services</a>
                <nav class="nav nav-sub">
                    <a href="page-404.html" class="nav-sub-link">Product</a>
                    <a href="page-404.html" class="nav-sub-link">Damage Product</a>
                    <a href="page-500.html" class="nav-sub-link">Catagories</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link"><i data-feather="layers"></i> Stock</a>
            </li>
            <li class="nav-label"><label class="content-label">Admin</label></li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="life-buoy"></i> Account</a>
                <nav class="nav nav-sub">
                    <a href="../components/form-elements.html" class="nav-sub-link">Expense</a>
                    <a href="../components/form-input-group.html" class="nav-sub-link">Income</a>
                    <a href="../components/form-input-tags.html" class="nav-sub-link">Pay-in Receipt</a>
                    <a href="../components/form-input-masks.html" class="nav-sub-link">Pay-out Receipt</a>
                    <a href="../components/form-validation.html" class="nav-sub-link">Account Head</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="book"></i> Branch</a>
                <nav class="nav nav-sub">
                    <a href="../components/con-grid.html" class="nav-sub-link">Branches</a>
                    <a href="../components/con-icons.html" class="nav-sub-link">Stock Transfer</a>
                    <a href="../components/con-images.html" class="nav-sub-link">Employee Transfer</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="layers"></i> Human Resource</a>
                <nav class="nav nav-sub">
                    <a href="../components/com-accordion.html" class="nav-sub-link">Employee</a>
                    <a href="../components/com-alerts.html" class="nav-sub-link">Salary Payroll</a>
                    <a href="../components/com-avatar.html" class="nav-sub-link">Attendance</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="monitor"></i> Report</a>
                <nav class="nav nav-sub">
                    <a href="../components/util-animation.html" class="nav-sub-link">Sales Report</a>
                    <a href="../components/util-background.html" class="nav-sub-link">Purchase Report</a>
                    <a href="../components/util-border.html" class="nav-sub-link">Expense Report</a>
                    <a href="../components/util-display.html" class="nav-sub-link">Income Report</a>
                    <a href="../components/util-divider.html" class="nav-sub-link">Payin Report</a>
                    <a href="../components/util-flex.html" class="nav-sub-link">Payout Report</a>
                    <a href="../components/util-height.html" class="nav-sub-link">Payment in/out</a>
                    <a href="../components/util-margin.html" class="nav-sub-link">Stock</a>
                    <a href="../components/util-padding.html" class="nav-sub-link">Customer Ledger</a>
                    <a href="../components/util-position.html" class="nav-sub-link">Supplier Ledger</a>
                    <a href="../components/util-typography.html" class="nav-sub-link">Account Ledger</a>
                    <a href="../components/util-width.html" class="nav-sub-link">Customer Due Report</a>
                    <a href="../components/util-extras.html" class="nav-sub-link">Supplier Due Report</a>
                    <a href="../components/util-extras.html" class="nav-sub-link">Profit/loss Report</a>
                    <a href="../components/util-extras.html" class="nav-sub-link">All Bills</a>
                </nav>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i data-feather="pie-chart"></i> Configuration</a>
                <nav class="nav nav-sub">
                    <a href="../components/chart-flot.html" class="nav-sub-link">General Setting</a>
                    <a href="../components/chart-chartjs.html" class="nav-sub-link">Payment Method</a>
                    <a href="../components/chart-peity.html" class="nav-sub-link">User</a>
                    <a href="../components/com-badge.html" class="nav-sub-link">Role and Premission</a>
                    <a href="../components/chart-peity.html" class="nav-sub-link">Bank</a>
                </nav>
            </li>
        </ul>

        <hr class="mg-t-30 mg-b-25">

        <ul class="nav nav-sidebar">
            <li class="nav-item"><a href="../pages/themes.html" class="nav-link"><i data-feather="aperture"></i> Themes</a>
            </li>
            <li class="nav-item"><a href="../docs.html" class="nav-link"><i data-feather="help-circle"></i>
                    Documentation</a></li>
        </ul>


    </div><!-- sidebar-body -->
</div><!-- sidebar -->

<div class="content">
    <div class="header">
        <div class="header-left">
            <a href="" class="burger-menu"><i data-feather="menu"></i></a>

            <div class="header-search">
                <i data-feather="search"></i>
                <input type="search" class="form-control" placeholder="What are you looking for?">
            </div><!-- header-search -->
        </div><!-- header-left -->

        <div class="header-right">
            <a href="" class="header-help-link"><i data-feather="help-circle"></i></a>
            <div class="dropdown dropdown-notification">
                <a href="" class="dropdown-link new" data-toggle="dropdown"><i data-feather="bell"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-menu-header">
                        <h6>Notifications</h6>
                        <a href=""><i data-feather="more-vertical"></i></a>
                    </div><!-- dropdown-menu-header -->
                    <div class="dropdown-menu-body">
                        <a href="" class="dropdown-item">
                            <div class="avatar"><span class="avatar-initial rounded-circle text-primary bg-primary-light">s</span>
                            </div>
                            <div class="dropdown-item-body">
                                <p><strong>Socrates Itumay</strong> marked the task as completed.</p>
                                <span>5 hours ago</span>
                            </div>
                        </a>
                        <a href="" class="dropdown-item">
                            <div class="avatar"><span class="avatar-initial rounded-circle tx-pink bg-pink-light">r</span></div>
                            <div class="dropdown-item-body">
                                <p><strong>Reynante Labares</strong> marked the task as incomplete.</p>
                                <span>8 hours ago</span>
                            </div>
                        </a>
                        <a href="" class="dropdown-item">
                            <div class="avatar"><span class="avatar-initial rounded-circle tx-success bg-success-light">d</span>
                            </div>
                            <div class="dropdown-item-body">
                                <p><strong>Dyanne Aceron</strong> responded to your comment on this <strong>post</strong>.</p>
                                <span>a day ago</span>
                            </div>
                        </a>
                        <a href="" class="dropdown-item">
                            <div class="avatar"><span class="avatar-initial rounded-circle tx-indigo bg-indigo-light">k</span></div>
                            <div class="dropdown-item-body">
                                <p><strong>Kirby Avendula</strong> marked the task as incomplete.</p>
                                <span>2 days ago</span>
                            </div>
                        </a>
                    </div><!-- dropdown-menu-body -->
                    <div class="dropdown-menu-footer">
                        <a href="">View All Notifications</a>
                    </div>
                </div><!-- dropdown-menu -->
            </div>
            <div class="dropdown dropdown-loggeduser">
                <a href="" class="dropdown-link" data-toggle="dropdown">
                    <div class="avatar avatar-sm">
                        <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">
                    </div><!-- avatar -->
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-menu-header">
                        <div class="media align-items-center">
                            <div class="avatar">
                                <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">
                            </div><!-- avatar -->
                            <div class="media-body mg-l-10">
                                <h6>Louise Kate Lumaad</h6>
                                <span>Administrator</span>
                            </div>
                        </div><!-- media -->
                    </div>
                    <div class="dropdown-menu-body">
                        <a href="" class="dropdown-item"><i data-feather="user"></i> View Profile</a>
                        <a href="" class="dropdown-item"><i data-feather="edit-2"></i> Edit Profile</a>
                        <a href="" class="dropdown-item"><i data-feather="briefcase"></i> Account Settings</a>
                        <a href="" class="dropdown-item"><i data-feather="shield"></i> Privacy Settings</a>
                        <a href="" class="dropdown-item"><i data-feather="log-out"></i> Sign Out</a>
                    </div>
                </div><!-- dropdown-menu -->
            </div>
        </div><!-- header-right -->
    </div><!-- header -->
    <div class="d-md-flex">
        <div class="col-sm-12">
            <div class="content-header pd-l-0">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Human Resource</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Supplier</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Reseller/Partner Regiatration Form</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
    </div>
    <div class="registration_main container">
        <div class="radius_angle">
            <div class="registration_head">
                <h2>Company Detail</h2>
            </div>
            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Company Name </label>
                            <input type="text" class="form-control form-control-sm" placeholder="your Company Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Company Email </label>
                            <input type="text" class="form-control form-control-sm" placeholder="your Company Email" required="">
                        </div>

                        <div class="form-group">
                            <label class="form-label">Country </label>
                            <select class="form-control form-control-sm">
                                <option label="Select Nationality"></option>
                                <option value="Firefox">Nepal</option>
                                <option value="Chrome">India</option>
                                <option value="Opera">America</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">State </label>
                            <select class="form-control form-control-sm">
                                <option label="Select Nationality"></option>
                                <option value="Firefox">Province 1</option>
                                <option value="Chrome">Province 2</option>
                                <option value="Opera">Province 3</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">City </label>
                            <select class="form-control form-control-sm">
                                <option label="Select Nationality"></option>
                                <option value="Firefox">Kathmandu</option>
                                <option value="Chrome">Pokhara</option>
                                <option value="Opera">Lumbini</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Office Address </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Kathmandu" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">PAN/VAT </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Number.." required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Form Of Business </label>
                            <select class="form-control form-control-sm">
                                <option label="Private"></option>
                                <option value="Firefox">Public</option>
                                <option value="Chrome">Private</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="upload_reseller_main">
                            <div class="upload_type reseller_photo">
                                <img id="reseller" src="http://placehold.it/180" alt="your image" />
                            </div>
                            <div class="mg-t-5 if_function_input">
                                <input type="file" onchange="readURL(this);" />
                            </div>
                            <!-- upload_type -->
                        </div>

                        <div class="form-group">
                            <label class="form-label">Partnership Behalf </label>
                            <select class="form-control form-control-sm">
                                <option label="self"></option>
                                <option value="Firefox">abcd</option>
                                <option value="Chrome">India</option>
                                <option value="Opera">America</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Company Website</label>
                            <input type="text" class="form-control form-control-sm" placeholder="www.abcd.com" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Telephone Number</label>
                            <input type="text" class="form-control form-control-sm" placeholder="12232434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Company Established Year</label>
                            <input type="text" class="form-control form-control-sm" placeholder="1993" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Location On map</label>
                            <div class="map_form">
                                <div class="mapouter">
                                    <div class="gmap_canvas"><iframe width="639" height="112" id="gmap_canvas"
                                                                     src="https://maps.google.com/maps?q=tinkunne%20kathmandu&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                                                     frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a
                                            href="https://www.emojilib.com"></a></div>
                                    <style>
                                        .mapouter {
                                            position: relative;
                                            text-align: right;
                                            height: 112px;
                                            width: 100%;
                                        }

                                        .gmap_canvas {
                                            overflow: hidden;
                                            background: none !important;
                                            height: 112px;
                                            width: 100%;
                                        }
                                    </style>
                                </div>
                            </div><!-- map_form -->




                        </div>




                    </div>
                </div><!-- row -->
            </div><!-- form_blk -->

            <div class="registration_head">
                <h2>Personal Detail Of Reseller/Partner</h2>
            </div>
            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Full Name </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>

                        <div class="form-group">
                            <label class="form-label">Nationality </label>
                            <select class="form-control form-control-sm">
                                <option label="Select Nationality"></option>
                                <option value="Firefox">Nepalese</option>
                                <option value="Chrome">Indian</option>
                                <option value="Opera">American</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Temporary Address </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Kathmandu" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Permanent Address </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Kathmandu" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Email </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Province 3" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Date Of Birth </label>
                            <input type="text" class="form-control form-control-sm" placeholder="2020/20/02" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Sex</label>
                            <div class="d-flex gender_btn form-control-sm">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio5" name="customRadio" class="custom-control-input" checked="">
                                    <label class="custom-control-label" for="customRadio5">Male</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio6" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio6">Female</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio7" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio7">Other</label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Citizenship Number </label>
                            <input type="text" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Passport Number </label>
                            <input type="text" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Maritial Status </label>
                            <div class="d-flex justify-space-between form-control-sm">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked="">
                                    <label class="custom-control-label" for="customRadio1">Unmarried</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio2">Married</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio3">Divorced</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio4">Widowed</label>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Father's Name </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Fateher's Contact Number </label>
                            <input type="text" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Mother's Name </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Mother's Mobile Number </label>
                            <input type="text" class="form-control form-control-sm" placeholder="9841*******" required="">
                        </div>
                    </div>
                </div><!-- row -->
            </div>
            <div class="registration_head">
                <h2>Nominee Details</h2>
            </div>

            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Full Name</label>
                            <input type="text" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Relationship to me</label>
                            <input type="text" class="form-control form-control-sm" placeholder="Nepalese" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Date Of Birth</label>
                            <input type="text" class="form-control form-control-sm" placeholder="2020/02/09" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Citizenship Number</label>
                            <input type="text" class="form-control form-control-sm" placeholder="132323234224" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Telephone Number</label>
                            <input type="text" class="form-control form-control-sm" placeholder="01-42****** required="">
                        </div>
                        <div class=" form-group">
                            <label class="form-label">Mobile Number</label>
                            <input type="text" class="form-control form-control-sm" placeholder="9841*******" required="">
                        </div>
                        <div class=" form-group">
                            <label class="form-label">Email</label>
                            <input type="text" class="form-control form-control-sm" placeholder="www.abcd@gmail.com" required="">
                        </div>
                    </div>
                </div>
            </div>

            <div class=" registration_head">
                <h2>Bank Details</h2>
            </div>

            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Bank Name </label>
                            <select class="form-control form-control-sm">
                                <option label="Select Nationality"></option>
                                <option value="Firefox">Nabil Bank</option>
                                <option value="Chrome">Everest Bank</option>
                                <option value="Opera">American Bank</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Account Holder Name</label>
                            <input type="text" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Account Number</label>
                            <input type="text" class="form-control form-control-sm" placeholder="1231241414" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Account Issued Branch Name</label>
                            <input type="text" class="form-control form-control-sm" placeholder="Koteshwor" required="">
                        </div>
                    </div>
                </div>
            </div>


            <div class="registration_head">
                <h2>Company Partner Detail (If Any)</h2>
            </div>
            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Full Name </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>

                        <div class="form-group">
                            <label class="form-label">Nationality </label>
                            <select class="form-control form-control-sm">
                                <option label="Select Nationality"></option>
                                <option value="Firefox">Nepalese</option>
                                <option value="Chrome">Indian</option>
                                <option value="Opera">American</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Temporary Address </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Kathmandu" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Permanent Address </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Kathmandu" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Email </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Province 3" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Date Of Birth </label>
                            <input type="text" class="form-control form-control-sm" placeholder="2020/20/02" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Sex</label>
                            <div class="d-flex gender_btn form-control-sm">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio8" name="customRadio" class="custom-control-input" checked="">
                                    <label class="custom-control-label" for="customRadio8">Male</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio9" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio9">Female</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio10" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio10">Other</label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Citizenship Number </label>
                            <input type="text" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Passport Number </label>
                            <input type="text" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Maritial Status </label>
                            <div class="d-flex justify-space-between form-control-sm">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio11" name="customRadio" class="custom-control-input" checked="">
                                    <label class="custom-control-label" for="customRadio11">Unmarried</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio12" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio12">Married</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio13" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio13">Divorced</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio14" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio14">Widowed</label>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Father's Name </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Fateher's Contact Number </label>
                            <input type="text" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Mother's Name </label>
                            <input type="text" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Mother's Mobile Number </label>
                            <input type="text" class="form-control form-control-sm" placeholder="9841*******" required="">
                        </div>
                    </div>
                </div><!-- row -->
            </div>

            <div class="registration_head">
                <h2>Documents</h2>
            </div>
            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Citizenship Copy</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01"
                                           aria-describedby="inputGroupFileAddon01" />
                                    <label class="custom-file-label" for="inputGroupFile01">Front</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">PAN/VAT Certificates</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile02"
                                           aria-describedby="inputGroupFileAddon02" />
                                    <label class="custom-file-label" for="inputGroupFile02">Front</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Personal Photo</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile03"
                                           aria-describedby="inputGroupFileAddon03" />
                                    <label class="custom-file-label" for="inputGroupFile03">Owner's</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Others</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile04"
                                           aria-describedby="inputGroupFileAddon04" />
                                    <label class="custom-file-label" for="inputGroupFile04">Other document</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Citizenship Copy</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile05"
                                           aria-describedby="inputGroupFileAddon05" />
                                    <label class="custom-file-label" for="inputGroupFile05">Back</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="note_document">
          Notes : You Can Upload Only PDF or JPG file in here.
        </span>
            </div>

            <div class="submit_registration form_blk">
                <button type="button" class="btn btn-success">Submit</button>
            </div><!-- submit_registration -->
        </div><!-- radius_angle -->
    </div><!-- registration_main -->





    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/feather-icons/feather.min.js"></script>
    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../lib/prismjs/prism.js"></script>
    <script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="../lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="../lib/select2/js/select2.min.js"></script>
    <script src="../lib/js-cookie/js.cookie.js"></script>
    <script src="../assets/js/custom.js"></script>
    <script src="../assets/js/cassie.js"></script>
    <script>
        $(function () {
            'use strict'

            $('#example1').DataTable({
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            $('#example2').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            $('#example8').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });



            // Select2
            $('.dataTables_length select').select2({
                minimumResultsForSearch: Infinity
            });

        });
    </script>
</body>

</html>
