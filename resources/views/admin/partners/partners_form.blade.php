@extends('admin.layouts.app')
@section('content')

    <div class="d-md-flex">
        <div class="col-sm-12">
            <div class="content-header pd-l-0">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Partners</a></li>
                            <li class="breadcrumb-item active" aria-current="page">partners</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Reseller/Partner Regiatration Form</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
    </div>
    <div class="registration_main container">
        <form action="{{route('partner-pending.store')}}" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">

            @csrf
        <div class="radius_angle">
            <div class="registration_head">
                <h2>Company Detail</h2>
            </div>
            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Company Name </label>
                            <input type="text" name="company_name" class="form-control form-control-sm" placeholder="your Company Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Company Email </label>
                            <input type="text" name="company_email" class="form-control form-control-sm" placeholder="your Company Email" required="">
                        </div>

                        <div class="form-group">
                            <label class="form-label">Country </label>
                            <select  name="country" class="form-control form-control-sm">
                                <option label="Select Nationality"></option>
                                <option value="Nepal">Nepal</option>
                                <option value="India">India</option>
                                <option value="America">America</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">State </label>
                            <select name="state" class="form-control form-control-sm">
                                <option label="Select Nationality"></option>
                                <option value="Province 1">Province 1</option>
                                <option value="Province 2">Province 2</option>
                                <option value="Province 3">Province 3</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">City </label>
                            <select name="city" class="form-control form-control-sm">
                                <option label="Select Nationality"></option>
                                <option value="Kathmandu">Kathmandu</option>
                                <option value="Pokhara">Pokhara</option>
                                <option value="Lumbini">Lumbini</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Office Address </label>
                            <input type="text" name="office_address" class="form-control form-control-sm" placeholder="Kathmandu" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">PAN/VAT </label>
                            <input type="text" name="pan" class="form-control form-control-sm" placeholder="Number.." required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Form Of Business </label>
                            <select name="form_of_business" class="form-control form-control-sm">
                                <option label="Private"></option>
                                <option value="Public">Public</option>
                                <option value="Private">Private</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="upload_reseller_main">
                            <div class="upload_type reseller_photo">
                                <img id="reseller" src="http://placehold.it/180" alt="your image" />
                            </div>
                            <div class="mg-t-5 if_function_input">
                                <input type="file" name="image" onchange="readURL(this);" />
                            </div>
                            <!-- upload_type -->
                        </div>

                        <div class="form-group">
                            <label class="form-label">Partnership Behalf </label>
                            <select name="partnership" class="form-control form-control-sm">
                                <option label="self"></option>
                                <option value="Firefox">abcd</option>
                                <option value="Chrome">India</option>
                                <option value="Opera">America</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Company Website</label>
                            <input type="text" name="company_website" class="form-control form-control-sm" placeholder="www.abcd.com" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Telephone Number</label>
                            <input type="text" name="telephone_number" class="form-control form-control-sm" placeholder="12232434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Company Established Year</label>
                            <input type="text" name="company_established_year" class="form-control form-control-sm" placeholder="1993" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Location On map</label>
                            <div class="map_form">
                                <div class="mapouter">
                                    <div class="gmap_canvas"><iframe width="639" height="112" id="gmap_canvas"
                                                                     src="https://maps.google.com/maps?q=tinkunne%20kathmandu&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                                                     frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a
                                            href="https://www.emojilib.com"></a></div>
                                    <style>
                                        .mapouter {
                                            position: relative;
                                            text-align: right;
                                            height: 112px;
                                            width: 100%;
                                        }

                                        .gmap_canvas {
                                            overflow: hidden;
                                            background: none !important;
                                            height: 112px;
                                            width: 100%;
                                        }
                                    </style>
                                </div>
                            </div><!-- map_form -->




                        </div>




                    </div>
                </div><!-- row -->
            </div><!-- form_blk -->

            <div class="registration_head">
                <h2>Personal Detail Of Reseller/Partner</h2>
            </div>
            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Full Name </label>
                            <input type="text" name="full_name" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>

                        <div class="form-group">
                            <label class="form-label">Nationality </label>
                            <select name="nationality" class="form-control form-control-sm">
                                <option label="Select Nationality"></option>
                                <option value="Nepalese">Nepalese</option>
                                <option value="Indian">Indian</option>
                                <option value="American">American</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Temporary Address </label>
                            <input type="text" name="temporary_address" class="form-control form-control-sm" placeholder="Kathmandu" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Permanent Address </label>
                            <input type="text" name="permanent_address" class="form-control form-control-sm" placeholder="Kathmandu" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Email </label>
                            <input type="text" name="email" class="form-control form-control-sm" placeholder="Province 3" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Date Of Birth </label>
                            <input type="text" name="date_of_birth" class="bod-picker form-control form-control-sm" placeholder="2020/20/02" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Sex</label>
                            <div class="d-flex gender_btn form-control-sm">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio5" value="Male" name="sex" class="custom-control-input" checked="">
                                    <label class="custom-control-label" for="customRadio5">Male</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio6" value="Female" name="sex" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio6">Female</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio7" value="Other" name="sex" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio7">Other</label>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label">Mobile Number </label>
                            <input type="text" name="mobile_number" class="form-control form-control-sm" placeholder="9841*******" required="">
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Citizenship Number </label>
                            <input type="text" name="citizenship_number" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Passport Number </label>
                            <input type="text" name="passport_number" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Maritial Status </label>
                            <div class="d-flex justify-space-between form-control-sm">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" value="Unmarried" name="marital_status" class="custom-control-input" checked="">
                                    <label class="custom-control-label" for="customRadio1">Unmarried</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" value="Married" name="marital_status" class="custom-control-input">
                                    <label class="custom-control-label"  for="customRadio2">Married</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio3" value="Divorced" name="marital_status" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio3">Divorced</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio4" value="Widowed" name="marital_status" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio4">Widowed</label>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Father's Name </label>
                            <input type="text" name="father_name" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Fateher's Contact Number </label>
                            <input type="text" name="father_contact_number" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Mother's Name </label>
                            <input type="text" name="mother_name" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Mother's Mobile Number </label>
                            <input type="text" name="mother_contact_number" class="form-control form-control-sm" placeholder="9841*******" required="">
                        </div>
                        @if((Auth::guard('admin')->check()))
                        <div class="form-group">
                            <label class="form-label">User </label>
                            <select name="user_id" class="form-control form-control-sm">
                                <option label="Select User"></option>
                                @foreach($users as $key=> $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                    </div>
                </div><!-- row -->
            </div>
            <div class="registration_head">
                <h2>Nominee Details</h2>
            </div>

            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Full Name</label>
                            <input type="text" name="nominee_full_name" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Relationship to me</label>
                            <input type="text" name="nominee_relationship" class="form-control form-control-sm" placeholder="Nepalese" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Date Of Birth</label>
                            <input type="text" name="nominee_date_of_birth" class="bod-picker form-control form-control-sm" placeholder="2020/02/09" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Citizenship Number</label>
                            <input type="text" name="nominee_citizenship_number" class="form-control form-control-sm" placeholder="132323234224" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Telephone Number</label>
                            <input type="text" name="nominee_telephone_number" class="form-control form-control-sm" placeholder="01-42****** required="">
                        </div>
                        <div class=" form-group">
                            <label class="form-label">Mobile Number</label>
                            <input type="text" name="nominee_mobile_number" class="form-control form-control-sm" placeholder="9841*******" required="">
                        </div>
                        <div class=" form-group">
                            <label class="form-label">Email</label>
                            <input type="text" name="nominee_email" class="form-control form-control-sm" placeholder="www.abcd@gmail.com" required="">
                        </div>
                    </div>
                </div>
            </div>

            <div class=" registration_head">
                <h2>Bank Details</h2>
            </div>

            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Bank Name </label>
                            <select name="bank_name" class="form-control form-control-sm">
                                <option label="Select bank"></option>
                                <option value="Nabil Bank">Nabil Bank</option>
                                <option value="Everest Bank">Everest Bank</option>
                                <option value="NMB Bank">NMB Bank</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Account Holder Name</label>
                            <input name="account_holder_name" type="text" class="form-control form-control-sm" placeholder="Ram Krishna" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Account Number</label>
                            <input type="text" name="account_number" class="form-control form-control-sm" placeholder="242424344" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Account Issued Branch Name</label>
                            <input type="text" name="account_issued_branch_name" class="form-control form-control-sm" placeholder="Koteshwor" required="">
                        </div>
                    </div>
                </div>
            </div>


            <div class="registration_head">
                <h2>Company Partner Detail (If Any)</h2>
            </div>
            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Full Name </label>
                            <input type="text" name="partner_full_name" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>

                        <div class="form-group">
                            <label class="form-label">Nationality </label>
                            <select name="partner_nationality" class="form-control form-control-sm">
                                <option label="Select Nationality"></option>
                                <option value="Nepalese">Nepalese</option>
                                <option value="Indian">Indian</option>
                                <option value="American">American</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Temporary Address </label>
                            <input type="text" name="partner_temporary_address" class="form-control form-control-sm" placeholder="Kathmandu" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Permanent Address </label>
                            <input type="text" name="partner_permanent_address" class="form-control form-control-sm" placeholder="Kathmandu" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Email </label>
                            <input type="text" name="partner_email" class="form-control form-control-sm" placeholder="Province 3" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Date Of Birth </label>
                            <input type="text" name="partner_date_of_birth" class="bod-picker form-control form-control-sm" placeholder="2020/20/02" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Sex</label>
                            <div class="d-flex gender_btn form-control-sm">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio8" value="Male" name="partner_sex" class="custom-control-input" checked="">
                                    <label class="custom-control-label" for="customRadio8">Male</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio9" value="Female" name="partner_sex" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio9">Female</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio10" value="Other" name="partner_sex" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio10">Other</label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Citizenship Number </label>
                            <input type="text" name="partner_citizenship_number" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Passport Number </label>
                            <input type="text" name="partner_passport_number" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Maritial Status </label>
                            <div class="d-flex justify-space-between form-control-sm">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio11" value="Unmarried" name="partner_marital_status" class="custom-control-input" checked="">
                                    <label class="custom-control-label" for="customRadio11">Unmarried</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio12" value="Married" name="partner_marital_status" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio12">Married</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio13" value="Divorced" name="partner_marital_status" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio13">Divorced</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio14" value="Widowed" name="partner_marital_status" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio14">Widowed</label>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Father's Name </label>
                            <input type="text" name="partner_father_name" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Fateher's Contact Number </label>
                            <input type="text" name="partner_father_contact_number" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Mother's Name </label>
                            <input type="text" name="partner_mother_name" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Mother's Mobile Number </label>
                            <input type="text" name="partner_mother_contact_number" class="form-control form-control-sm" placeholder="9841*******" required="">
                        </div>
                    </div>
                </div><!-- row -->
            </div>

            <div class="registration_head">
                <h2>Documents</h2>
            </div>
            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Citizenship Copy</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="citizenship_front" class="custom-file-input" id="inputGroupFile01"
                                           aria-describedby="inputGroupFileAddon01" />
                                    <label class="custom-file-label" for="inputGroupFile01">Front</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">PAN/VAT Certificates</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="citizenship_back" class="custom-file-input" id="inputGroupFile02"
                                           aria-describedby="inputGroupFileAddon02" />
                                    <label class="custom-file-label" for="inputGroupFile02">Front</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Personal Photo</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file"  name="pan_certificate" class="custom-file-input" id="inputGroupFile03"
                                           aria-describedby="inputGroupFileAddon03" />
                                    <label class="custom-file-label" for="inputGroupFile03">Owner's</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Others</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="personal_photo" class="custom-file-input" id="inputGroupFile04"
                                           aria-describedby="inputGroupFileAddon04" />
                                    <label class="custom-file-label" for="inputGroupFile04">Other document</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Citizenship Copy</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="other_document" class="custom-file-input" id="inputGroupFile05"
                                           aria-describedby="inputGroupFileAddon05" />
                                    <label class="custom-file-label" for="inputGroupFile05">Back</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="note_document">
            Notes : You Can Upload Only PDF or JPG file in here.
        </span>
            </div>

            <div class="submit_registration form_blk">
                <button  class="btn btn-success">Submit</button>
            </div><!-- submit_registration -->
        </div><!-- radius_angle -->
        </form>
    </div><!-- registration_main -->


@endsection
