@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Partner</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Rejected Partners</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Rejected Partners</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-3">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                    <div class="col-sm mail-navbar bd-b-0 justify-content-end mg-r-10" style="height: auto;">
                        <div class="d-none d-lg-flex">
                            <a href=""><i data-feather="printer"></i></a>
                            <a href=""><i data-feather="folder"></i></a>
                            <a href=""><i class="fas fa-download"></i></a>
                        </div>
                    </div>
                </div>
            </div> <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5p">SN.</th>
                        <th class="wd-15p">Reg. ID</th>
                        <th class="wd-25p">Partner Name</th>
                        <th class="wd-15p">Address</th>
                        <th class="wd-15p">Email</th>
                        <th class="wd-15p">Phone No</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>REG001</td>
                        <td>Partner Name</td>
                        <td>Tinkune, Kathmandu</td>
                        <td>abc@gmail.com</td>
                        <td>9840680875</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href="../extraPages/Partner-detail.html"><i class="icon ion-clipboard text-success"></i></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>REG002</td>
                        <td>Partner Name</td>
                        <td>Tinkune, Kathmandu</td>
                        <td>abc@gmail.com</td>
                        <td>9840680875</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href="../extraPages/Partner-detail.html"><i class="icon ion-clipboard text-success"></i></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>REG003</td>
                        <td>Partner Name</td>
                        <td>Tinkune, Kathmandu</td>
                        <td>abc@gmail.com</td>
                        <td>9840680875</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href="../extraPages/Partner-detail.html"><i class="icon ion-clipboard text-success"></i></a></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->



@endsection
