@extends('admin.layouts.app')
@section('content')


    <div class="row">
        <div class="col-sm-5">
            <div class="content-header">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Account</a></li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Payment Method</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
        	@if(Auth::guard('admin')->check())
                <a href="{{route('payment-method.create')}}" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">New Payment Method</a>
            @else
            @can('payment-method-create')
                <a href="{{route('payment-method.create')}}" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">New Payment Method</a>
            @endcan
            @endif


                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalCenterTitle">NEW PAYMENT METHOD</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="{{route('payment-method.store')}}" method="post" enctype="multipart/form-data">

                                    @csrf
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div>
                                                Payment Method: <input type="text" name="name" id="name" class="form-control" placeholder="Enter your payment method name " required><br>
                                            </div>
                                        </div><!-- col -->
                                    </div><!-- row -->
                                    <div class="modal-footer modal-footer_footer">
                                        {{--                                <button type="button" class="btn btn-success mg-r-10">Update</button>--}}
                                        <button class="btn btn-success mg-r-10">Update</button>
                                        <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                        <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                          <button type="button" class="btn btn-primary">Save changes</button> -->
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
{{--            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10">New Payment Method</button>--}}
        </div><!-- col -->
    </div><!-- row -->


    <div class="content-body" style="margin-top: -30px;">
        <div class="component-section">

            <table id="example1" class="table">
                <thead>
                <tr>
                    <th class="wd-5p"><input type="checkbox" aria-label="Checkbox for following text input"></th>
                    <th class="wd-15p">Method Id</th>
                    <th class="wd-25p">Payment Method</th>
                    <th class="wd-15p">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($payment_method as $key =>$payment_method )
                <tr>
                    <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                    <td>{{++$key}}</td>
                    <td>{{$payment_method->title}}</td>
                    <td class="d-md-flex">
{{--                    	@if(Auth::guard('admin')->check())--}}
{{--			                <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>--}}
{{--			            @else--}}
{{--			            @can('payment-method-view')--}}
{{--			                <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>--}}
{{--			            @endcan--}}
{{--			            @endif--}}

			            @if(Auth::guard('admin')->check())
			                <div class="mg-r-20" title="Edit"><a href="{{route('payment-method.edit',$payment_method->id)}}" data-id="{{$payment_method->id}}" class="editData" data-toggle="modal" data-target="#exampleModalCenter1"><i class="far fa-edit text-warning"></i></a></div>
			            @else
			            @can('payment-method-edit')
			                <div class="mg-r-20" title="Edit"><a href="{{route('payment-method.edit',$payment_method->id)}}" data-id="{{$payment_method->id}}" class="editData" data-toggle="modal" data-target="#exampleModalCenter1"><i class="far fa-edit text-warning"></i></a></div>
			            @endcan
			            @endif

                            <div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">UPDATE PAYMENT METHOD</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
{{--                                            <form action="{{route('payment-method.store')}}" method="post" enctype="multipart/form-data">--}}

{{--                                                @csrf--}}
                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div>
                                                            <input type="hidden" class="payment_method_id123">
                                                            Payment Method: <input type="text" name="name" id="name1" class="form-control" placeholder="Enter your payment method name " required><br>
                                                        </div>
                                                    </div><!-- col -->
                                                </div><!-- row -->
                                                <div class="modal-footer modal-footer_footer">
                                                    {{--                                <button type="button" class="btn btn-success mg-r-10">Update</button>--}}
                                                    <button class="update_payment_method btn btn-success mg-r-10">Update</button>
                                                    <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                    <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                      <button type="button" class="btn btn-primary">Save changes</button> -->
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
			            @if(Auth::guard('admin')->check())
			                <div class="mg-r-20" title="Delete"><a href="{{route('payment-method.destroy',$payment_method->id)}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
			            @else
			            @can('payment-method-delete')
			                <div class="mg-r-20" title="Delete"><a href="{{route('payment-method.destroy',$payment_method->id)}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
			            @endcan
			            @endif


                            {{--                            fot delete popup--}}

                            <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <div class="conform_del">
                                                <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                            </div><!-- conform_del -->

                                            <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                                <a class="btn btn-success mg-r-10" href="{{route('payment-method.destroy',$payment_method->id)}}">Yes</a>
                                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->




@endsection
@push('scripts')

    <script>

        $(".editData").click(function(e){
            e.preventDefault()
            var payment_method_id = $(this).data('id');




            // alert(contact_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/payment-method-edit')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    payment_method_id : payment_method_id,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // toastr.success(data.success);
                    // window.location.reload();
                    console.log(data);
                    // var advance_due= $('#advance_due').val();


                    // $('.bod-picker').append($('.bod-picker').val(data.category.date));
                    $('#name1').append($('#name1').val(data.payment_method.title));
                    $('.payment_method_id123').append($('.payment_method_id123').val(data.payment_method.id));



                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });


        $(".update_payment_method").click(function(e){
            e.preventDefault()
            // var category_id = $(this).data('id');
            var payment_method_id123= $(".payment_method_id123").val();
            var name1= $("#name1").val();



            $.ajax({
                type: 'POST',
                url: '{{url('/payment-method-update')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    name:name1,
                    payment_method_id:payment_method_id123

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);



                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });



    </script>
@endpush
