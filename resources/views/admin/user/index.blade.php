@extends('admin.layouts.app')
@section('content')


    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Users</a></li>
                            <li class="breadcrumb-item active" aria-current="page">All Users</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Users</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"
                    data-target="#exampleModalCenter2">Add User</button>
            <div class="modal fade modal_cust" id="exampleModalCenter2" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle1">ADD USER</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body salary_payroll">

                            <div class="row">
                                <div class="col-md-6">
                                    Full Name
                                    <input type="text" name="name" class="name form-control-sm">
                                    <p id="name_error" style="color: red"></p>
                                    <div class="mg-t-5">
                                        Contact Number
                                        <input type="text" name="phone" class="phone form-control-sm">
                                        <p id="phone_error" style="color: red"></p>
                                    </div>
                                    <div class="mg-t-5">
                                        Refral  Code
                                        <input type="text" name="refral_code" class="refral_code form-control-sm">
                                        <p id="phone_error" style="color: red"></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    Email
                                    <input type="text" name="email" class="email form-control-sm">
                                    <p id="email_error" style="color: red"></p>
                                    <div class="mg-t-5">
                                        Password
                                        <input type="password" name="password" class="password form-control-sm">
                                        <p id="password_error" style="color: red"></p>
                                    </div>
                                    <div class="mg-t-5">
                                       Confirm Password
                                        <input type="password" name="confirm_password" class="confirm_password form-control-sm">

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-30">
                                <button  class="save_user btn btn-success mg-r-10">Update</button>
                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>





                        </div>
                    </div>
                </div>
            </div>
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-3">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                </div>
            </div>
            <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5p">SN.</th>
                        <th class="wd-15p">Reg. ID</th>
                        <th class="wd-25p">User Name</th>
                        <th class="wd-20p">Email</th>
                        <th class="wd-20p">Phone No</th>
                        <th class="wd-15p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $key=> $user)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->phone}}</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href="../extraPages/client-detail.html"><i
                                        class="icon ion-clipboard text-success"></i></a></div>
                            <div class="mg-r-20" title="Edit"><a href="" class="editData" data-toggle="modal" data-id="{{$user->id}}" data-target="#exampleModalCenter23" ><i class="far fa-edit text-warning"></i></a></div>

{{--                            for edit--}}
                            <div class="col-sm-0 tx-right col-lg-7">
{{--                                <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"--}}
{{--                                        data-target="#exampleModalCenter23">Add User</button>--}}
                                <div class="modal fade modal_cust" id="exampleModalCenter23" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle1">ADD USER</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body salary_payroll">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        Full Name
                                                        <input type="text" name="name" class="name123 form-control-sm">
                                                        <input type="hidden"  class="user_id123 form-control-sm">
                                                        <p id="name_error123" style="color: red"></p>
                                                        <div class="mg-t-5">
                                                            Contact Number
                                                            <input type="text" name="phone" class="phone123 form-control-sm">
                                                            <p id="phone_error123" style="color: red"></p>
                                                        </div>
                                                        <div class="mg-t-5">
                                                            Refral  Code
                                                            <input type="text" name="refral_code" class="refral_code123 form-control-sm">
                                                            <p id="phone_error123" style="color: red"></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        Email
                                                        <input type="text" name="email" class="email123 form-control-sm">
                                                        <p id="email_error123" style="color: red"></p>
                                                        <div class="mg-t-5">
                                                            Password
                                                            <input type="password" name="password" class="password123 form-control-sm">
                                                            <p id="password_error123" style="color: red"></p>
                                                        </div>
                                                        <div class="mg-t-5">
                                                            Confirm Password
                                                            <input type="password" name="confirm_password" class="confirm_password123 form-control-sm">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-30">
                                                    <button  class="update_user btn btn-success mg-r-10">Update</button>
                                                    <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                    <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn-primary">Save changes</button> -->
                                                </div>





                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- col -->

                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->

</div><!-- content -->




@endsection


@push('scripts')

    <script>

        $(".editData").click(function(e){
            e.preventDefault()
            var user_id = $(this).data('id');




            // alert(contact_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/edit-user')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    user_id : user_id,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // toastr.success(data.success);
                    // window.location.reload();
                    console.log(data);

                    $('.name123').append($('.name123').val(data.user.name));
                    $('.user_id123').append($('.user_id123').val(data.user.id));
                    $('.email123').append($('.email123').val(data.user.email));
                    $('.phone123').append($('.phone123').val(data.user.phone));
                    $('.refral_code123').append($('.refral_code123').val(data.user.refral_code));


                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });

        $(".update_user").click(function(e){
            e.preventDefault()
            // var category_id = $(this).data('id');
            var name= $(".name123").val();
            var email= $(".email123").val();
            var user_id= $(".user_id123").val();
            var phone= $(".phone123").val();
            var password= $(".password123").val();
            var confirm_password= $(".confirm_password123").val();
            var refral_code= $(".refral_code123").val();

            // alert(category_id1234);

            $.ajax({
                type: 'POST',
                url: '{{url('/update-user')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    user_id : user_id,
                    name : name,
                    email:email,
                    phone:phone,
                    password:password,
                    refral_code:refral_code,
                    confirm_password:confirm_password

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);



                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                    $('#name_error123').html(err.errors.name);
                    $('#email_error123').html(err.errors.email);
                    $('#phone_error123').html(err.errors.phone);
                    $('#password_error123').html(err.errors.password);
                    // if (err.errors.password) {
                    //     toastr.error(err.errors.password);
                    // }
                }
            });

        });


        $(".save_user").click(function(e){
            e.preventDefault()
            // var category_id = $(this).data('id');
            var name= $(".name").val();
            var email= $(".email").val();
            var phone= $(".phone").val();
            var password= $(".password").val();
            var confirm_password= $(".confirm_password").val();
            var refral_code= $(".refral_code").val();

            // alert(category_id1234);

            $.ajax({
                type: 'POST',
                url: '{{url('/add-user')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    name : name,
                    email:email,
                    phone:phone,
                    password:password,
                    refral_code:refral_code,
                    confirm_password:confirm_password

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);



                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                    $('#name_error').html(err.errors.name);
                    $('#email_error').html(err.errors.email);
                    $('#phone_error').html(err.errors.phone);
                    $('#password_error').html(err.errors.password);
                    // if (err.errors.password) {
                    //     toastr.error(err.errors.password);
                    // }
                }
            });

        });



    </script>
@endpush
