<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="ThemePixels">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/logosmall.png')}}">

    <title>e-billing and business management</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="../lib/prismjs/themes/prism-vs.css" rel="stylesheet">

    <!-- Cassie CSS -->
    <link rel="stylesheet" href="../assets/css/cassie.css">
    <link rel="stylesheet" href="../assets/css/changes.css">
</head>

<body>

<div class="login_main_1">
    <div class="container">
        <div class="row">

            <div class="col-md-4">

                <form action="{{route('login.store')}}" method="post" class=" " >
                    @csrf

                    <div class="login_head">
                        <h2>LOGIN</h2>
                        <h3>EMAIL ADDRESS </h3>
                        <input type="text"  name="email" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"placeholder="yourname@email.com">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                        <h3 class="mt-4 mb-0">PASSWORD </h3>
                        <input type="password"  id="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="...............">

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif

                        <a href="#" class="forget_password">Forget your password ?</a>
                        <div class="login_main1">
                            {{--                        <a href="#">LOGIN</a>--}}
                            <button >LOGIN</button>
                        </div><!-- login_main1 -->
                    </div><!-- login_head -->
                </form>
            </div>

            <div class="offset-md-1 col-md-4">
                <div class="login_person_img">
                    <img src="../assets/img/man.png">
                </div>
            </div>
            <div class="col-md-3">
                <div class="login_main1_register">
                    <h2>If you have not registered till now click on the button below for registration !</h2>
                    <div class="login_main1">
                        <a href="#" class="mt-0">Register</a>
                    </div><!-- login_main1 -->
                </div>
            </div>
        </div>
    </div><!-- container -->
</div><!-- login_main -->


<script src="../lib/jquery/jquery.min.js"></script>
<script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../lib/feather-icons/feather.min.js"></script>
<script src="../lib/prismjs/prism.js"></script>
<script src="../assets/js/cassie.js"></script>

</body>

</html>
