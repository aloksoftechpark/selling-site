@extends('admin.layouts.app')
@section('content')
    <style>
        .demoForm input,
        .demoForm select {
            width: 60% !important;
            border: none !important;
            padding: 5px !important;
            -moz-appearance: none;
            -webkit-appearance: none;
        }

        .demoForm select::-ms-expand {
            display: none !important;
        }

    </style>
    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Product Validities</a></li>
                            <li class="breadcrumb-item active" aria-current="page">All Validities</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Product Validities</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->

        <div class="col-sm-0 tx-right col-lg-7">
            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"
                    data-target="#addProductValidity">Add Validity</button>
            <div class="modal fade modal_cust" id="addProductValidity" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">ADD PRODUCT VALIDITY</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form action="{{ route('validities.store') }}" method="post" enctype="multipart/form-data" id="addPlanForm">
                            @csrf
                            <div class="modal-body salary_payroll">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mg-t-5">
                                            Title:
                                            <input type="text" name="title"
                                                   class="form-control form-control-sm wd-120" placeholder="title" required>
                                        </div>
                                        <div class="mg-t-5">
                                            Months:
                                            <select name="months"
                                                    class="form-control form-control-sm modal_select_option_height bd bd-gray-900"
                                                    aria-label="Example text with button addon"
                                                    aria-describedby="button-addon1" required>
                                                <option value="">Select Months</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                                <option value="4">Four</option>
                                                <option value="5">Five</option>
                                                <option value="6">Six</option>
                                                <option value="7">Seven</option>
                                                <option value="8">Eight</option>
                                                <option value="9">Nine</option>
                                                <option value="10">Ten</option>
                                                <option value="11">Eleven</option>
                                                <option value="12">Twelve</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mg-t-5">
                                            Discount:
                                            <input type="number" step="any" name="discount"
                                                   class="form-control form-control-sm wd-120" placeholder="Discount" required>
                                        </div>
                                        <div class="mg-t-5">
                                            <div
                                                class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-14">
                                                <button class="btn btn-success mg-r-10" type="submit">Submit</button>
                                                <button type="button" class="btn btn-danger"
                                                        data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- col -->

    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="row py-5">
                {{-- for report table --}}
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-10 text-capitalize">
                            sales <i class="fas fa-chevron-right"></i> <span style="color:#2570D3;">all sales</span>
                            <h3>Welcome to Sales</h3>
                        </div>
                        <div class="col-md-2">
                            <div class="table-print text-right py-1 mb-1 pr-3">
                                <button onclick="exportFile('productValiditiesTable','json')"><i class="fas fa-print"></i></button>
                                <button onclick="exportFile('productValiditiesTable','pdf')"><i class="fas fa-file-pdf"></i></button>
                                <button onclick="exportFile('productValiditiesTable','csv')"><i class="fas fa-file-excel"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="bg-theam-secondary table-search-head p-1">
                        <div class="row">
                            <div class="col-md-2 mr-0 pr-0">
                                <select name="size" id="size">
                                    <option value="1000">1000</option>
                                </select>
                                <select name="size" id="size">
                                    <option value="all branch">All Branch</option>
                                </select>
                            </div>
                            <div class="col-md-4 mx-0 px-0 mt-1">
                                <ul>
                                    <li><a href="javascript:;">
                                            <i class="fas fa-arrow-left"></i>
                                        </a></li>
                                    <li><a href="javascript:;">
                                            Today
                                        </a></li>
                                    <li><a href="javascript:;">
                                            <i class="fas fa-arrow-right"></i>
                                        </a></li>
                                </ul>
                                <ul class="ml-1">
                                    <li><a href="javascript:;">
                                            <i class="fas fa-arrow-left"></i>
                                        </a></li>
                                    <li><a href="javascript:;">
                                            Month
                                        </a></li>
                                    <li><a href="javascript:;">
                                            <i class="fas fa-arrow-right"></i>
                                        </a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 ml-0 pl-0">
                                <input type="text" placeholder="From">
                                <input type="text" placeholder="To">
                                <input type="text" class="float-right" placeholder="Search...">
                            </div>
                        </div>
                    </div>
                    <table class="table table2 table-hover" id="productValiditiesTable">
                        <thead>
                        <tr>
                            <th scope="col">S.N</th>
                            <th scope="col">Title</th>
                            <th scope="col">Month</th>
                            <th scope="col">Discount</th>
                            <th scope="col" class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($validities as $key=>$validity)
                        <tr>
                            <th scope="row">{{++$key}}</th>
                            <td>{{$validity->title}}</td>
                            <td>{{$validity->months}}</td>
                            <td>{{$validity->discount}} %</td>
                            <td class="text-center">
                                <a href="javascript:;">
                                    <span class="badge badge-pill badge-primary">
                                        <i class="fa fa-eye"></i>
                                    </span>
                                </a>
                                <a href="javascript:;">
                                    <span class="badge badge-pill badge-danger">X</span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$validities->links()}}
                </div>
            </div>
        </div>

    </div><!-- content-body -->
    </div><!-- content -->
@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.5/jspdf.plugin.autotable.min.js"></script>
    <script src="https://www.jqueryscript.net/demo/export-table-json-csv-txt-pdf/src/tableHTMLExport.js"></script>
    <script>
        function exportFile(table, to) {
            $(`#${table}`).tableHTMLExport({
                type: to,
                filename: `${table}.${to}`
            });
        }

    </script>
@endpush
