@extends('admin.layouts.app')
@section('content')

    <!-- header -->
    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Bill</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Pending Bills
                            </li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Pending Bills</h4>
                </div>
            </div>
            <!-- content-header -->
        </div>
        <!-- col -->
    </div>
    <!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="d-lg-flex pd-l-0">
                    <div class="pd-lg-r-20 wd-40p">
                        <input type="search" class="form-control form-control-sm"
                               placeholder="Search with Bill No, Order No, Company Name" />
                    </div>
                </div>
            </div>
            <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5">SN</th>
                        <th class="wd-15p">Date</th>
                        <th class="wd-10p">Bill NO.</th>
                        <th class="wd-15p">Order ID</th>
                        <th class="wd-25p">Company Name</th>
                        <th class="wd-15p">Bill Amount</th>
                        <th class="wd-15p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bill_pending as $key=> $bill_pendings)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$bill_pendings->created_at->toDateString()}}</td>
                        <td>00000{{$bill_pendings->id}}</td>
                        <td>Req00{{$bill_pendings->order_pending->id}}</td>
                        <td>{{$bill_pendings->order_pending->company_name}}</td>
                        <td>{{$bill_pendings->total}}</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View">
                                <a href="{{route('bill-pending.show',$bill_pendings->id)}}"><i class="icon ion-clipboard text-success"></i></a>
                            </div>
                            <div class="mg-r-20" title="Paid">
                                <a href="" data-toggle="modal" class="edit" data-id="{{$bill_pendings->id}}" data-target="#exampleModalCenter2">
                                    <i class="icon ion-forward text-primary"></i></a>
                                <div class="modal fade modal_cust" id="exampleModalCenter2" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalCenterTitle1" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle1">DUE PAYMENT</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body salary_payroll">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        Date <input type="text" name="date" value="" class="bod-picker date form-control-sm">
                                                        <div class="mg-t-5">
                                                            Due Amount:
                                                            <input type="text" name="due" class="due_amount form-control form-control-sm wd-120"
                                                                   placeholder="" readonly>
                                                        </div>
{{--                                                        <div class="mg-t-5">--}}
{{--                                                            Pomo Code:--}}
{{--                                                            <input type="text" name="pomo" class="promo_code form-control form-control-sm wd-120"--}}
{{--                                                                   placeholder="">--}}
{{--                                                        </div>--}}
                                                        <div class="mg-t-5">
                                                            Payment Method:
                                                            <select class="payment_method_id form-control form-control-sm modal_select_option_height bd bd-gray-900"
                                                                    aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                                <option value=" ">select payment method</option>
                                                                @foreach($payment_method as $key=> $payment_methods)
                                                                <option value="{{$payment_methods->id}}">{{$payment_methods->title}}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
{{--                                                        Discount--}}
{{--                                                        <div class="input-group">--}}
{{--                                                            <input type="text" class="discount form-control form-control-sm bd-r-0" placeholder="0.0"--}}
{{--                                                                   aria-label="Username" aria-describedby="basic-addon1">--}}
{{--                                                            <div class="input-group-prepend wd-25p">--}}
{{--                                                            <span class="input-group-text form-control wd-100p justify-content-center"--}}
{{--                                                            id="basic-addon1">%</span>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="mg-t-5">--}}
{{--                                                            Receivable Amount:--}}
{{--                                                            <input type="text" name="Amount" class="receivable_amount form-control form-control-sm wd-120"--}}
{{--                                                                   placeholder="">--}}
{{--                                                        </div>--}}
                                                        <div class="mg-t-5">
                                                            Remaining Amount:
                                                            <input type="text" name="Amount" class="remaining_amount form-control form-control-sm wd-120"
                                                                   placeholder="">
                                                        </div>
                                                        <div class="mg-t-5">
                                                            Paid Amount:
                                                            <input type="text" name="Amount" class="paid_amount form-control form-control-sm wd-120"
                                                                   placeholder="">
                                                            <p id="paid_amount_error123" style="color: red"></p>
                                                        </div>
                                                        <div class="mg-t-5">
                                                            Payment ID:
                                                            <input type="text" name="ID" class="paymentId form-control form-control-sm wd-120" placeholder="">
                                                        </div>
                                                    </div>

                                                </div>
                                                <div
                                                    class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-30">
                                                    <button  class="update btn btn-success mg-r-10">Paid</button>
                                                    <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
{{--                                                    <div class="mg-r-20" title="Cancle"><a href="" class="delete_data" data-id="{{$bill_pendings->id}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>--}}
                                                    <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button> -->
                                                </div>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mg-r-20" title="Cancle">
{{--                                <a href=""><i class="icon ion-close text-danger"></i></a>--}}
                                <div class="mg-r-20" title="Cancle"><a href="" class="delete_data" data-id="{{$bill_pendings->id}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                            </div>
                        </td>



                        {{--                            fot delete popup--}}



                        <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalCenterTitle1" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal_ac_head text-left modal_cancle_bill" role="document">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle1">BILL CANCELATION</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">


                                        <div class="cancle_bill">
                                            <p class="mg-b-5">Specify the reason why you want to canel the bill:</p>
                                            <div class="cancel_reason">
                                                <textarea id="mytextarea" rows="5" cols="2" placeholder="Description.."></textarea>
                                            </div>
                                            <p id="cancel_error" style="color: red"></p>
{{--                                            <input type="hidden" class="billing_id123">--}}
                                            <input type="hidden" class="bill_pending_id123">
                                            <span>Note : You need to specify the reason clearly for cancelation.</span>
                                        </div>

                                        <div class="modal-footer modal-footer_footer modal-footer-right text-center">
                                            <button  class="final_delete btn btn-success mg-r-10">Submit</button>
                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                            <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn-primary">Save changes</button> -->
                                        </div>

                                    </div>




{{--                                    <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">--}}
{{--                            <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">--}}
{{--                                <div class="modal-content">--}}
{{--                                    <div class="modal-header">--}}
{{--                                        <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>--}}
{{--                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                            <span aria-hidden="true">×</span>--}}
{{--                                        </button>--}}
{{--                                    </div>--}}
{{--                                    <div class="modal-body">--}}

{{--                                        <div class="conform_del">--}}
{{--                                            <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>--}}
{{--                                        </div><!-- conform_del -->--}}

{{--                                        <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">--}}
{{--                                        <input type="hidden" class="bill_pending_id123">--}}
{{--                                            <a class="final_delete btn btn-success mg-r-10" href=" ">Yes</a>--}}
{{--                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>--}}

{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}


                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- component-section -->
    </div>
    <!-- content-body -->
</div>
<!-- content -->


@endsection

@push('scripts')

    <script>

        $(".edit").click(function(e){
            e.preventDefault()
            var bill_pending_id = $(this).data('id');
            // alert(customer_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/edit-bill-pending')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    bill_pending_id : bill_pending_id,

                },
                success: function (data, status) {

                    if(data.error){

                        return;
                    }

                    // $('#show_modal').modal('hide');
                    // toastr.success(data.success);

                    // console.log(data.package.id);
                    // window.location.reload();
                    // alert('test');

                    $('.bill_pending_id123').append($('.bill_pending_id123').val(data.bill_pending.id));
                    $('.due_amount').append($('.due_amount').val(data.bill_pending.total));

                },
                error: function (xhr, status, error) {
                    // console.log('error');
                    // toastr.error(error.errors);
                }
            });

        });



        {{--$(".promo_code").on('input',function () {--}}

        {{--    var promo_code= $(".promo_code").val();--}}
        {{--    var price= $('.due_amount').val();--}}
        {{--    // var discount= $('.discount1012').val();--}}

        {{--    $.ajax({--}}

        {{--        url: "{{url('/edit-offer-discount2')}}",--}}
        {{--        type: "POST",--}}
        {{--        data: {--}}
        {{--            _token : "{{csrf_token()}}",--}}

        {{--            promo_code:promo_code,--}}
        {{--            price : price,--}}
        {{--            // discount_amount : discount,--}}
        {{--        },--}}
        {{--        success: function (data) {--}}
        {{--            console.log(data);--}}

        {{--            // if(data.success)--}}
        {{--            // {--}}
        {{--                $('.discount').append($('.discount').val(data.dis));--}}
        {{--                $('.receivable_amount').append($('.receivable_amount').val(data.total));--}}

        {{--             // }--}}


        {{--        },--}}

        {{--        error: function (xhr, status, error) {--}}
        {{--            console.log('error');--}}
        {{--            var err = JSON.parse(xhr.responseText);--}}
        {{--            $('#promo_code_error').append(err.errors.message);--}}
        {{--            // if (err.errors.password) {--}}
        {{--            //     toastr.error(err.errors.password);--}}
        {{--            // }--}}
        {{--        }--}}

        {{--    });--}}


        {{--});--}}


            $(".paid_amount").on('input',function () {
            var due_amount= $('.due_amount').val();
            // var receivable_amount=
            var paid_amount= $('.paid_amount').val();

            var remaining_amount123= (parseFloat(due_amount) - parseFloat(paid_amount));
            var remaining_amount1234= (parseFloat(due_amount));

            // alert(remaining_amount123);
            if (paid_amount)
            {
                $('.remaining_amount').append($('.remaining_amount').val(remaining_amount123));
            }
            else {

                $('.remaining_amount').append($('.remaining_amount').val(remaining_amount1234));
            }


        });

        $(".update").click(function(e){
            e.preventDefault()


            var bill_pending_id= $(".bill_pending_id123").val();
            var promo_code= $(".promo_code").val();
            var due_amount= $('.due_amount').val();
            var date= $('.date').val();
            var discount= $('.discount').val();
            var receivable_amount= $('.receivable_amount').val();
            var paid_amount= $('.paid_amount').val();
            var payment_method_id= $('.payment_method_id').val();
            var paymentId= $('.paymentId').val();

            // alert(category_id1234);

            $.ajax({
                type: 'POST',
                url: '{{url('/add-bill-paid')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    bill_pending_id : bill_pending_id,
                    promo_code : promo_code,
                    due_amount : due_amount,
                    date : date,
                    discount : discount,
                    receivable_amount : receivable_amount,
                    paid_amount : paid_amount,
                    payment_method_id : payment_method_id,
                    paymentId : paymentId,
                    status : 2,


                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    if(data.success){

                        toastr.success(data.success);
                        window.location.reload();
                        console.log(data);
                    }


                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                    $('#paid_amount_error123').html(err.errors.paid_amount);

                    // if (err.errors.password) {
                    //     toastr.error(err.errors.password);
                    // }
                }
            });

        });



        //for the delete data
        $(".delete_data").click(function(e){
            e.preventDefault()

            var bill_pending_id = $(this).data('id');
            // alert(customer_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/edit-bill-pending')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    bill_pending_id : bill_pending_id,

                },
                success: function (data, status) {

                    if(data.error){

                        return;
                    }

                    // $('#show_modal').modal('hide');
                    // toastr.success(data.success);

                    // console.log(data.package.id);
                    // window.location.reload();
                    // alert('test');

                    $('.bill_pending_id123').append($('.bill_pending_id123').val(data.bill_pending.id));

                },
                error: function (xhr, status, error) {
                    // console.log('error');
                    // toastr.error(error.errors);
                }
            });

        });


        //for final delete
        $(".final_delete").click(function(e){
            e.preventDefault()

            var bill_pending_id= $(".bill_pending_id123").val();
            var cancel_reason =$('textarea#mytextarea').val();

            // alert(category_id1234);

            $.ajax({
                type: 'POST',
                url: '{{url('/delete-bill-pending')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    bill_pending_id : bill_pending_id,
                    cancel_reason:cancel_reason,
                    status : 0,


                },
                success: function (data, status) {

                    if(data.errors){
                        toastr.error(data.errors);
                        return;
                    }

                    if(data.success){


                        toastr.success(data.success);
                        window.location.reload();
                        console.log(data);
                    }

                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // toastr.error(data.errors);
                    var err = JSON.parse(xhr.responseText);
                    $('#cancel_error').append(err.errors.cancel_reason);
                }
            });

        });

    </script>

@endpush
