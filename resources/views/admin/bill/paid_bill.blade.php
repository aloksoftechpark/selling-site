@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Bill</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Paid Bills</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Paid Bills</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="d-lg-flex pd-l-0">
                    <div class="pd-lg-r-20 wd-40p">
                        <input type="search" class="form-control form-control-sm" placeholder="Search with Bill No, Order No, Company Name">
                    </div>
                </div>
            </div> <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5">SN</th>
                        <th class="wd-15p">Bill Date</th>
                        <th class="wd-15p">Payment Date</th>
                        <th class="wd-10p">Bill NO.</th>
                        <th class="wd-10p">Order ID</th>
                        <th class="wd-20p">Company Name</th>
                        <th class="wd-15p">Bill Amount</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bill_paid as $Key => $bill_paids)
                    <tr>
                        <td>{{++$Key}}</td>
                        <td>{{$bill_paids->bill_pending->created_at->toDateString()}}</td>
                        <td>{{$bill_paids->date}}</td>
                        <td>00000{{$bill_paids->bill_pending->id}}</td>
                        <td>Req00{{$bill_paids->bill_pending->id}}</td>
                        <td>{{$bill_paids->bill_pending->order_pending->company_name}}</td>
                        <td>{{$bill_paids->bill_pending->total}}</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href="{{route('bill-pending.show',$bill_paids->bill_pending->id)}}"><i class="icon ion-clipboard text-success"></i></a></div>
                            <div class="mg-r-20" title="Cancle"><a href=""><i class="icon ion-trash-b text-danger"></i></a></div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->



@endsection
