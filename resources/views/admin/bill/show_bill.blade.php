@extends('admin.layouts.app')
@section('content')

    <div class="content-header">
        <div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Pages</a></li>
                    <li class="breadcrumb-item"><a href="#">Extras</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Invoice Page</li>
                </ol>
            </nav>
            <h4 class="content-title content-title-sm">Invoice Page</h4>
        </div>
    </div><!-- content-header -->
    <div class="content-body">
        <div class="card card-invoice">
            <div class="card-header">
                <div>
                    <h5 class="mg-b-3">Invoice #DF032AZ00022</h5>
                    <span class="tx-sm text-muted">Due on September 15, 2019</span>
                </div>
                <div class="btn-group-invoice">
                    <a href="" class="btn btn-white btn-sm btn-uppercase"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                               height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                               stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail">
                            <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                            <polyline points="22,6 12,13 2,6"></polyline>
                        </svg> Email</a>
                    <a href="" class="btn btn-white btn-sm btn-uppercase"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                               height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                               stroke-linecap="round" stroke-linejoin="round" class="feather feather-download">
                            <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                            <polyline points="7 10 12 15 17 10"></polyline>
                            <line x1="12" y1="15" x2="12" y2="3"></line>
                        </svg> Downlode</a>
                    <a href="" class="btn btn-white btn-sm btn-uppercase"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                               height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                               stroke-linecap="round" stroke-linejoin="round" class="feather feather-printer">
                            <polyline points="6 9 6 2 18 2 18 9"></polyline>
                            <path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path>
                            <rect x="6" y="14" width="12" height="8"></rect>
                        </svg> Print</a>
                </div>
            </div>
            <div class="card-header flex_center">
                <div class="canceled_bill">
                    @if($bill_pending->status == '1')
                    <p>Due Bill</p>
{{--                    @endif--}}

                        @elseif($bill_pending->status == '0')
                            <p>Canceled Bill</p>
{{--                        @endif--}}
                    @else

{{--                        @if($bill_pending->status == '1')--}}
                            <p>Paid Bill</p>
                        @endif
                </div>
                <div class="text-center">
                    <h6 class="tx-26 mg-b-5">ThemePixels, Inc.</h6>
                    <p class="mg-b-0">201 Something St., Something Town, YT 242, Country 6546</p>
                    <p class="mg-b-0">Tel No: 324 445-4544 / youremail@companyname.com</p>
                    <p class="mg-b-0">PAN: 123456789</p>
                </div><!-- col -->
                <!--     <div class="btn-group-invoice print_bill">
                  <a href="" class="btn btn-white btn-sm btn-uppercase"><i data-feather="printer"></i> Print</a>
                </div> -->
            </div><!-- card-header -->
            <div class="card-body bill_body">
                <div class="row bill_head_detail">

                    <div class="col-sm-6 col-lg-8 mg-t-40 mg-sm-t-0">
                        <label class="content-label">Billed To</label>
                        <!--    <h6 class="tx-15 mg-b-10">Juan Dela Cruz</h6> -->
                        <p class="mg-b-0">NAME : {{$bill_pending->order_pending->company_name}}</p>
                        <p class="mg-b-0">ADDRESS : {{$bill_pending->order_pending->company_address}}</p>
                        <p class="mg-b-0">PAN : {{$bill_pending->order_pending->pan}}</p>
                    </div><!-- col -->
                    <div class="col-sm-6 col-lg-4 mg-t-40">
                        <p class="mg-b-0 text-right">INVOICE NUMBER : DF032AZ00022</p>
                        <p class="mg-b-0 text-right">ISSUE DATE : {{$bill_pending->created_at->toDateString()}}</p>

                    </div><!-- col -->
                </div><!-- row -->

                <div class="table-responsive mg-t-25">
                    <table class="table table-invoice bd-b">
                        <thead>
                        <tr>
                            <th class="tx-center">SN.</th>
                            <th class="wd-40p d-none d-sm-table-cell">Description</th>
                            <th class="tx-center">Unit</th>
                            <th class="tx-center">Unit Price</th>
{{--                            <th class="tx-center">Discount</th>--}}
                            <th class="tx-center">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="tx-center">1</td>
                            <td class="d-none d-sm-table-cell tx-color-03">{{$bill_pending->package->name}}  </td>
                            <td class="tx-center">@if(isset($bill_pending->payment_type)){{$bill_pending->payment_type->no_of_month}}@endif</td>
                            <td class="tx-center">{{$bill_pending->package->plan_pricing[0]->price}}</td>
{{--                            <td class="tx-center">10 %</td>--}}
                            <td class="tx-center">{{$bill_pending->amount}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="row justify-content-between mg-t-25">
                    <div class="col-sm-6 col-lg-6 order-2 order-sm-0 mg-t-40 mg-sm-t-0">
                        <div class="table-responsive">
                            @if($bill_pending->status == '2')
                            <table class="table table-sm table-bordered mg-b-0 table_lr_border">
                                <thead>
                                <tr>
                                    <th scope="col" class="border-right_1">Payment Type</th>
                                    <th scope="col" class="border-right_1">Payment ID</th>
                                    <th scope="col">Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$bill_paid[0]->payment_method->title}}</td>
                                    <td>{{$bill_paid[0]->paymentId}}</td>
                                    <td>{{$bill_paid[0]->paid_amount}}</td>
                                </tr>
                                </tbody>
                            </table>
                            @endif
                        </div><!-- table-responsive -->
                    </div><!-- col -->
                    <div class="col-sm-6 col-lg-4 order-1 order-sm-0">
                        <ul class="list-unstyled lh-7 pd-r-10">
                            <li class="d-flex justify-content-between">
                                <span>Sub-Total</span>
{{--                                <span>6,500.00</span>--}}
                                <span>{{$bill_pending->amount}}</span>
                            </li>

                            @if($bill_pending->discount_amount)
                            <li class="d-flex justify-content-between">
                                <span>Discount</span>
                                <span>{{$bill_pending->discount_amount}}</span>
                            </li>
                            @endif
                            <li class="d-flex justify-content-between">
                                <span>VAT (13%)</span>
                                <span>{{$bill_pending->vat_amount}}</span>
                            </li>

                            <li class="d-flex justify-content-between">
                                <strong>Total</strong>
                                <strong>{{$bill_pending->total}}</strong>
                            </li>
                        </ul>
                    </div><!-- col -->
                </div>
                <hr>
                @if($bill_pending->cancel_reason)
                <div class="footer_note">
                    <h6>NOTES :</h6>
                    <p> {{$bill_pending->cancel_reason}}</p>
                </div>
                @else
                <div class="footer_note">
                    <h6>NOTES :</h6>
                    <p> Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia
                        non numquam eius modi tempora incidunt. qui dolorem ipsum quia dolor sit amet, consectetur, adipisci
                        velit, sed quia
                        non numquam eius modi tempora incidunt.</p>
                </div>
                @endif
                <div class="footer_signature">
                    <p>SIGNATURE</p>
                    <p>SIGNATURE</p>
                </div><!-- footer_signature -->

            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- content-body -->
</div><!-- content -->



@endsection
