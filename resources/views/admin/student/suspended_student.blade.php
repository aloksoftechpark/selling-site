@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Student</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Suspanded Students</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Suspanded Students</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-3">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                </div>
            </div> <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5p">SN.</th>
                        <th class="wd-10p">Reg. ID</th>
                        <th class="wd-15p">Student Name</th>
                        <th class="wd-15p">Address</th>
                        <th class="wd-15p">Email</th>
                        <th class="wd-10p">Phone No</th>
                        <th class="wd-10p">Client Count</th>
                        <th class="wd-10p">Due Amount</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>REG001</td>
                        <td>Student Name</td>
                        <td>Tinkune, Kathmandu</td>
                        <td>abc@gmail.com</td>
                        <td>9840680875</td>
                        <td>0</td>
                        <td>999999</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="Pay"><a href=""><i class="icon ion-forward text-primary"></i></a></div>
                            <div class="mg-r-20" title="View"><a href="../extraPages/student-detail.html"><i class="icon ion-clipboard text-success"></i></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>REG002</td>
                        <td>Student Name</td>
                        <td>Tinkune, Kathmandu</td>
                        <td>abc@gmail.com</td>
                        <td>9840680875</td>
                        <td>5</td>
                        <td>255635</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="Pay"><a href=""><i class="icon ion-forward text-primary"></i></a></div>
                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>REG003</td>
                        <td>Student Name</td>
                        <td>Tinkune, Kathmandu</td>
                        <td>abc@gmail.com</td>
                        <td>9840680875</td>
                        <td>3</td>
                        <td>9840</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="Pay"><a href=""><i class="icon ion-forward text-primary"></i></a></div>
                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->


@endsection
