@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Student</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Approved Students</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Approved Students</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-3">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                </div>
            </div>
            <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5p">SN.</th>
                        <th class="wd-10p">Reg. ID</th>
                        <th class="wd-15p">Student Name</th>
                        <th class="wd-15p">Address</th>
                        <th class="wd-15p">Email</th>
                        <th class="wd-10p">Phone No</th>
                        <th class="wd-10p">Client Count</th>
                        <th class="wd-10p">Due Amount</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>REG001</td>
                        <td>Student Name</td>
                        <td>Tinkune, Kathmandu</td>
                        <td>abc@gmail.com</td>
                        <td>9840680875</td>
                        <td>0</td>
                        <td>999999</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="Pay"><a href="" data-toggle="modal" data-target="#exampleModalCenter2"><i
                                        class="icon ion-forward text-primary"></i></a></div>
                            <div class="modal fade modal_cust" id="exampleModalCenter2" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle1">STUDENT / AGENT PAYMENT </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body salary_payroll">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    Date
                                                    <input type="text" name="date" value="" class="form-control-sm">
                                                    <div class="mg-t-5">
                                                        Payable Due Amount
                                                        <input type="text" name="Amount" value="" class="form-control-sm">
                                                    </div>
                                                    <div class="mg-t-5">
                                                        Payment Method
                                                        <select class="form-control form-control-sm modal_select_option_height bd bd-gray-900"
                                                                aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                            <option value="volvo">Guest</option>
                                                            <option value="saab">Ranjan</option>
                                                            <option value="opel">Manoj</option>
                                                            <option value="audi">Pawan</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="col-md-6">
                                                    Paid Amount
                                                    <input type="text" name="Amount" value="" class="form-control-sm">
                                                    <div class="mg-t-5">
                                                        Remaining Due Amount
                                                        <input type="text" name="Amount" value="" class="form-control-sm">
                                                    </div>
                                                    <div class="mg-t-5">
                                                        Payment ID
                                                        <input type="text" name="number" value="" class="form-control-sm">
                                                    </div>
                                                </div>
                                            </div>

                                            <div
                                                class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-30">
                                                <button type="button" class="btn btn-success mg-r-10">Submit</button>
                                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mg-r-20" title="View"><a href="../extraPages/student-detail.html"><i
                                        class="icon ion-clipboard text-success"></i></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>REG002</td>
                        <td>Student Name</td>
                        <td>Tinkune, Kathmandu</td>
                        <td>abc@gmail.com</td>
                        <td>9840680875</td>
                        <td>5</td>
                        <td>255635</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="Pay"><a href=""><i class="icon ion-forward text-primary"></i></a></div>
                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>REG003</td>
                        <td>Student Name</td>
                        <td>Tinkune, Kathmandu</td>
                        <td>abc@gmail.com</td>
                        <td>9840680875</td>
                        <td>3</td>
                        <td>9840</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="Pay"><a href=""><i class="icon ion-forward text-primary"></i></a></div>
                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->





@endsection
