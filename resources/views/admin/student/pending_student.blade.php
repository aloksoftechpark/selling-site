@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Student</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Pending Students</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Pending Student Requests</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
{{--            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10">Add Student</button>--}}
            <a href="{{route('student-pending.create')}}" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10">Add Student</a>
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-3">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                </div>
            </div> <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5p">SN.</th>
                        <th class="wd-15p">Reg. ID</th>
                        <th class="wd-20p">Student Name</th>
                        <th class="wd-20p">Address</th>
                        <th class="wd-15p">Email</th>
                        <th class="wd-15p">Phone No</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($student_pending as $key=> $student_pendings)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>REG00{{$student_pendings->user->id}}</td>
                        <td>{{$student_pendings->full_name}}</td>
                        <td>{{$student_pendings->temporary_address}}</td>
                        <td>{{$student_pendings->email}}</td>
                        <td>{{$student_pendings->mobile_number}}</td>
                        <td>
                            <button class="bg-success bd rounded-5 text-white">Verify</button>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->




@endsection
