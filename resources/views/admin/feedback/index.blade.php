@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Feedback</a></li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Feedback</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"
                    data-target="#exampleModalCenter2">Add Feedback</button>
            <div class="modal fade modal_cust" id="exampleModalCenter2" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">ADD FEEDBACK</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body text-left salary_payroll">
                            <div class="row">
                                <div class="col-md-6">

                                    Date
                                    <input type="text" name="date" value="" class="bod-picker date form-control-sm">
                                    <div class="mg-t-5">
                                        User
                                        <div class="input-group">
                                            <div class="input-group-prepend wd-15p">
                                               <span class="input-group-text form-control wd-100p justify-content-center"
                                                  id="basic-addon1"></span>
                                            </div>
                                            <select id="user_id" name="user_id" class="user_id form-control form-control-sm modal_select_option_height bd bd-gray-900 user_id"
                                                    aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                <option value="" id="user_id">Select user</option>

                                                @forelse($users as $user)
                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                @empty
                                                @endforelse
                                            </select>

                                        </div>
                                    </div>
                                    <p id="user_id_error" style="color: red"></p>
                                    <div class="mg-t-5">
                                        Source:
                                        <select class="source form-control form-control-sm modal_select_option_height bd bd-gray-900"
                                                aria-label="Example text with button addon" aria-describedby="button-addon1">
                                            <option value="volvo">Guest</option>
                                            <option value="saab">Ranjan</option>
                                            <option value="opel">Manoj</option>
                                            <option value="audi">Pawan</option>
                                        </select>
                                    </div>
                                    <div class="mg-t-5">
                                        Full Name:
                                        <input type="text" name="name" class="full_name form-control form-control-sm wd-120" placeholder="">
                                        <p id="full_name_error" style="color: red"></p>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    Email
                                    <input type="text" name="email" value="" class="email form-control-sm">
                                    <p id="email_error" style="color: red"></p>
                                    <div class="mg-t-5">
                                        Message:
                                        <textarea cols="2" id="message" rows="4" class="wd-100p pd-6"></textarea>
                                        <p id="message_error" style="color: red"></p>
                                    </div>
                                    <div class="mg-t-5">
                                        <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-10">
                                            <button  class="save_feedback btn btn-success mg-r-10">Submit</button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                            <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                           <button type="button" class="btn btn-primary">Save changes</button> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-3">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                </div>
            </div>
            <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5p">SN.</th>
                        <th class="wd-10p">Date</th>
                        <th class="wd-20p">Source</th>
                        <th class="wd-55p">Message</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($feedback as $key=> $feedback)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$feedback->date}}</td>
                        <td>{{$feedback->source}}</td>
                        <td>
                            {{$feedback->message}}
                        </td>
                        <td>
                            <div class="d-md-flex">
                                <div class="mg-r-20" title="View"><a href="../extraPages/client-detail.html"><i
                                            class="icon ion-clipboard text-success"></i></a></div>
                                <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>
                                <div class="mg-r-20" title="Cancle"><a href=""><i class="icon ion-trash-b text-danger"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->


@endsection
@push('scripts')

    <script>

        $(".save_feedback").click(function(e){
            e.preventDefault()
            // var category_id = $(this).data('id');
            var date= $(".date").val();
            var email= $(".email").val();
            var user_id= $(".user_id").val();
            var source= $(".source").val();
            var full_name= $(".full_name").val();
            var message= $("textarea#message").val();

            // alert(message);

            $.ajax({
                type: 'POST',
                url: '{{url('/add-feedback')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    date : date,
                    email:email,
                    user_id:user_id,
                    source:source,
                    message:message,
                    full_name:full_name

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);



                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                    $('#email_error').html(err.errors.email);
                    $('#message_error').html(err.errors.message);
                    $('#full_name_error').html(err.errors.full_name);
                    $('#user_id_error').html(err.errors.user);
                    // if (err.errors.password) {
                    //     toastr.error(err.errors.password);
                    // }
                }
            });

        });

    </script>
@endpush
