<!-- quotation Field -->
<div class="form-group col-sm-12">
    {!! Form::label('quotation', 'Quotation:') !!}
    {!! Form::text('quotation', null, ['class' => 'form-control']) !!}
</div>
<!-- description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description', 'description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control editor']) !!}
</div>
<div class="form-group col-sm-3 mt-2 ml-2 custom-control custom-switch">
    <input type="checkbox" name="status" class="custom-control-input" id="status" checked="">
    <label class="custom-control-label" for="status">Status</label>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-3">
    <button type="submit" class="btn btn-success waves-effect waves-themed">Submit</button>
    <a href="{{ route('introduction.index') }}" class="btn btn-default">Cancel</a>
</div>
