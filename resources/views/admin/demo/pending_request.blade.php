@extends('admin.layouts.app')
@section('content')
    <style>
        .demoForm input,
        .demoForm select {
            width: 60% !important;
            border: none !important;
            padding: 5px !important;
            -moz-appearance: none;
            -webkit-appearance: none;
        }

        .demoForm select::-ms-expand {
            display: none !important;
        }

    </style>
    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Demo Request</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Pending Requests</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Pending Demo Requests</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->

        <div class="col-sm-0 tx-right col-lg-7">
            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"
                data-target="#exampleModalCenter2">Add Request</button>
            <div class="modal fade modal_cust" id="exampleModalCenter2" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">ADD DEMO REQUEST</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form action="{{ route('demo-pending.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body salary_payroll">
                                <div class="row">
                                    <div class="col-md-6">
                                        User
                                        <div class="input-group">
                                            <div class="input-group-prepend wd-15p">
                                                <span class="input-group-text form-control wd-100p justify-content-center"
                                                    id="basic-addon1"></span>
                                            </div>
                                            <select id="user_id" name="user_id"
                                                class="form-control form-control-sm modal_select_option_height bd bd-gray-900 user_id"
                                                aria-label="Example text with button addon"
                                                aria-describedby="button-addon1">
                                                <option value="" id="user_id">Select user</option>

                                                @forelse($users as $user)
                                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                @empty
                                                @endforelse
                                            </select>

                                        </div>
                                        <div class="mg-t-5">
                                            Company Name:
                                            <input type="text" name="company_name"
                                                class="form-control form-control-sm wd-120" placeholder="">
                                        </div>
                                        <div class="mg-t-5">
                                            Company Address:
                                            <input type="text" name="company_address"
                                                class="form-control form-control-sm wd-120" placeholder="">
                                        </div>
                                        <div class="mg-t-5">
                                            Contact Number:
                                            <input type="text" name="contact_number"
                                                class="form-control form-control-sm wd-120" placeholder="">
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        PAN
                                        <input type="number" step="any" name="pan" value="" class="form-control-sm">
                                        <div class="mg-t-5">
                                            Package
                                            <select name="package"
                                                class="form-control form-control-sm modal_select_option_height bd bd-gray-900"
                                                aria-label="Example text with button addon"
                                                aria-describedby="button-addon1">
                                                <option value="service oriented">Service Oriented</option>
                                                <option value="product oriented">Product Oriented</option>
                                                <option value="third party">Third party</option>
                                            </select>
                                        </div>

                                        <div class="mg-t-5">
                                            <div
                                                class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-14">
                                                <button class="btn btn-success mg-r-10">Submit</button>
                                                <button type="button" class="btn btn-danger"
                                                    data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div><!-- col -->

    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="d-lg-flex pd-l-0">
                    <div class="pd-lg-r-20">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="Packages"></option>
                            <option value="">Product Oriented</option>
                            <option value="">Service Oriented</option>
                        </select>
                    </div>
                    <div class="pd-lg-r-20">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                </div>
            </div>
            <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                        <tr>
                            <th class="wd-5p">SN.</th>
                            <th class="wd-15p">Date</th>
                            <th class="wd-15p">Reg. ID</th>
                            <th class="wd-25p">Request Name</th>
                            <th class="wd-15p">Phone</th>
                            <th class="wd-15p">Package</th>
                            <th class="wd-10p text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($demo_pending as $key => $pending)
                            <tr id="dataRow{{ $pending->id }}">
                                <td>{{ ++$key }}</td>
                                <td>{{ $pending->created_at->toDateString() }}</td>
                                <td>REG{{ $pending->user->id }}</td>
                                <td>{{ $pending->company_name }}</td>
                                <td>{{ $pending->contact_number }}</td>
                                <td>{{ $pending->package }}</td>
                                <td class="text-center">
                                    <button class="verified bg-primary bd rounded-5 text-white" data-id="{{ $pending->id }}"
                                        data-toggle="modal" data-target="#model{{ $pending->id }}"><i
                                            class="fas fa-info-circle"></i></button>
                                    <div class="modal fade" id="model{{ $pending->id }}" tabindex="-1" role="dialog"
                                        aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-dialog-centered modal_ac_head new_item"
                                            role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalCenterTitle">DEMO REQUEST
                                                        VERIFICATION</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body text-left">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <form method="POST"
                                                                action="{{ route('demo-pending.update', $pending->id) }}"
                                                                class="demoForm detail_lead verification_lead"
                                                                id="demoForm{{ $pending->id }}">
                                                                @csrf
                                                                @method('PATCH')
                                                                <ul>
                                                                    <li>
                                                                        User :
                                                                        <select id="user_id" name="user_id"
                                                                            aria-describedby="button-addon1">
                                                                            <option value="">Select user</option>
                                                                            @foreach ($users as $user)
                                                                                <option value="{{ $user->id }}" @if ($user->id == $pending->user['id'])
                                                                                    selected
                                                                            @endif>{{ $user->name }}
                                                                            </option>
                        @endforeach
                        </select>
                        </li>
                        <li>
                            Company Name :
                            <input type="text" name="company_name" value="{{ $pending->company_name }}">
                        </li>
                        <li>
                            Company Address :
                            <input type="text" name="company_address" value="{{ $pending->company_address }}">
                        </li>
                        <li>
                            Contact Number :
                            <input type="text" name="contact_number" value="{{ $pending->contact_number }}">
                        </li>
                        <li>
                            PAN :
                            <input type="number" name="pan" value="{{ $pending->pan }}">
                        </li>
                        <li>
                            Package :
                            <select name="package">
                                <option value="service oriented" @if ($pending->package == 'service oriented') selected @endif
                                    >Service Oriented</option>
                                <option value="product oriented" @if ($pending->package == 'product oriented') selected @endif
                                    >Product Oriented</option>
                                <option value="third party" @if ($pending->package == 'third party') selected @endif>Third
                                    party</option>
                            </select>
                        </li>
                        </ul>
                        </form><!-- detail_lead -->
            </div>
            <div class="col-md-4">
                <div class="modal-footer modal-footer_footer modal-footer-right text-center footer_verification">
                    <button type="button" class="btn btn-success wd-80p mg-r-10 d-block"
                        onclick="submitForm('demoForm{{ $pending->id }}')">Update</button>
                    <form method="post" action="{{ url('demo-status', $pending->id) }}" id="activeForm{{ $pending->id }}">
                        @csrf
                        <input type="hidden" value="1" name="status">
                    </form>
                    <button type="button" class="btn btn-primary wd-80p mg-r-10 d-block"
                        onclick="submitForm('activeForm{{ $pending->id }}')">Approve</button>
                    <form method="post" action="{{ url('demo-status', $pending->id) }}" id="rejectForm{{ $pending->id }}">
                        @csrf
                        <input type="hidden" value="2" name="status">
                    </form>
                    <button type="button" class="btn btn-danger wd-80p mg-r-10 d-block"
                        onclick="submitForm('rejectForm{{ $pending->id }}')">Reject</button>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </td>
    </tr>
    @endforeach
    </tbody>
    </table>
    </div>
    </div><!-- component-section -->

    </div><!-- content-body -->
    </div><!-- content -->
    <script>
        function submitForm(formId) {
            if (confirm('Are you sure want to submit?')) {
                let form = $(`#${formId}`);
                $.ajax({
                    type: form.attr("method"),
                    url: form.attr("action"),
                    data: form.serialize(),
                    success: function(data, status) {
                        console.log(data);
                        if (data.indexOf('success') >= 0) {
                            $(".modal").modal('hide');
                            toastr.success(data);
                            location.reload(true);
                        } else {
                            toastr.error(data);
                        }
                    },
                    error: function(xhr, status, error) {
                        console.log(error);
                        toastr.error('Something went wrong. please try again later.');
                    }
                });
            }
            return false;
        }

    </script>
@endsection
