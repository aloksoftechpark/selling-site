@extends('admin.layouts.app')
@section('content')

    <div class="d-md-flex">
        <div class="col-sm-12">
            <div class="content-header pd-l-0">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Affilated</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Affilated</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Affilated Registration Form</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
    </div>

    <div class="registration_main container">
        <form action="{{route('affilated-pending.store')}}" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">

            @csrf
        <div class="radius_angle">
            <div class="registration_head">
                <h2>Personal Detail Of Reseller/Partner</h2>
            </div>
            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Full Name </label>
                            <input type="text" name="full_name" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>

                        <div class="form-group">
                            <label class="form-label">Nationality </label>
                            <select name="nationality" class="form-control form-control-sm">
                                <option label="Select Nationality"></option>
                                <option value="Nepalese">Nepalese</option>
                                <option value="Indian">Indian</option>
                                <option value="American">American</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Temporary Address </label>
                            <input type="text"  name="temporary_address" class="form-control form-control-sm" placeholder="Kathmandu" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Permanent Address </label>
                            <input type="text" name="permanent_address" class="form-control form-control-sm" placeholder="Kathmandu" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Email </label>
                            <input type="text" name="email" class="form-control form-control-sm" placeholder="Province 3" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Date Of Birth </label>
                            <input type="text" name="date_of_birth" class="bod-picker form-control form-control-sm" placeholder="2020/20/02" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Sex</label>
                            <div class="d-flex gender_btn form-control-sm">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio5" name="sex" value="Male" class="custom-control-input" checked="">
                                    <label class="custom-control-label" for="customRadio5">Male</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio6" name="sex"  value="Female" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio6">Female</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio7" name="sex" value="Other" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio7">Other</label>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Mobile Number </label>
                            <input type="text" name="mobile_number" class="form-control form-control-sm" placeholder="9841*******" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Citizenship Number </label>
                            <input type="text" name="citizenship_number" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Passport Number </label>
                            <input type="text" name="passport_number" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Maritial Status </label>
                            <div class="d-flex justify-space-between form-control-sm">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" value="Unmarried" name="marital_status" class="custom-control-input" checked="">
                                    <label class="custom-control-label" for="customRadio1">Unmarried</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" value="Married" name="marital_status" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio2">Married</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio3" value="Divorced" name="marital_status" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio3">Divorced</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio4" value="Widowed" name="marital_status" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio4">Widowed</label>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Father's Name </label>
                            <input type="text" name="father_name" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Fateher's Contact Number </label>
                            <input type="text" name="father_contact_number" class="form-control form-control-sm" placeholder="12232343434" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Mother's Name </label>
                            <input type="text" name="mother_name" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Mother's Mobile Number </label>
                            <input type="text" name="mother_contact_number" class="form-control form-control-sm" placeholder="9841*******" required="">
                        </div>
                        @if((Auth::guard('admin')->check()))
                        <div class="form-group">
                            <label class="form-label">User </label>
                            <select name="user_id" class="form-control form-control-sm">
                                <option label="Select User"></option>
                                @foreach($users as $key=> $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                    </div>
                </div><!-- row -->
            </div><!-- form_blk -->

            <div class="registration_head">
                <h2>School/college Detail</h2>
            </div>

            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Institute Name</label>
                            <input type="text" name="institute_name" class="form-control form-control-sm" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Address</label>
                            <input type="text" name="school_address" class="form-control form-control-sm" placeholder="Kathmandu" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Website</label>
                            <input type="text" name="website" class="form-control form-control-sm" placeholder="www.abcd.com" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Acedemic Grade</label>
                            <input type="text" name="acedemic_grade" class="form-control form-control-sm" placeholder="Masters" required="">
                        </div>
                    </div>
                </div>
            </div>


            <div class="registration_head">
                <h2>Bank Details</h2>
            </div>

            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Bank Name </label>
                            <select name="bank_name" class="form-control form-control-sm">
                                <option label="Select bank"></option>
                                <option value="Nabil Bank">Nabil Bank</option>
                                <option value="Everest Bank">Everest Bank</option>
                                <option value="NMB Bank">NMB Bank</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Account Holder Name</label>
                            <input name="account_holder_name" type="text" class="form-control form-control-sm" placeholder="Ram Krishna" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Account Number</label>
                            <input type="text" name="account_number" class="form-control form-control-sm" placeholder="242424344" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Account Issued Branch Name</label>
                            <input type="text" name="account_issued_branch_name" class="form-control form-control-sm" placeholder="Koteshwor" required="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="registration_head">
                <h2>Documents</h2>
            </div>
            <div class="form_blk">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Citizenship Copy</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="citizenship_front" class="custom-file-input" id="inputGroupFile01"
                                           aria-describedby="inputGroupFileAddon01" />
                                    <label class="custom-file-label" for="inputGroupFile01">Front</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">PAN/VAT Certificates</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="citizenship_back" class="custom-file-input" id="inputGroupFile02"
                                           aria-describedby="inputGroupFileAddon02" />
                                    <label class="custom-file-label" for="inputGroupFile02">Front</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Personal Photo</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file"  name="pan_certificate" class="custom-file-input" id="inputGroupFile03"
                                           aria-describedby="inputGroupFileAddon03" />
                                    <label class="custom-file-label" for="inputGroupFile03">Owner's</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Others</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="personal_photo" class="custom-file-input" id="inputGroupFile04"
                                           aria-describedby="inputGroupFileAddon04" />
                                    <label class="custom-file-label" for="inputGroupFile04">Other document</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label">Citizenship Copy</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="other_document" class="custom-file-input" id="inputGroupFile05"
                                           aria-describedby="inputGroupFileAddon05" />
                                    <label class="custom-file-label" for="inputGroupFile05">Back</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="note_document">
            Notes : You Can Upload Only PDF or JPG file in here.
          </span>
            </div>

            <div class="submit_registration form_blk">
                <button class="btn btn-success">Submit</button>
            </div><!-- submit_registration -->

        </div><!-- radius_angle -->
        </form>
    </div><!-- registration_main -->

@endsection
