@extends('admin.layouts.app')
@section('content')

<div class="row">
    <div class="col-sm-5">
        <div class="content-header">
            <div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Account</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Account Head</li>
                    </ol>
                </nav>
                <h4 class="content-title content-title-sm">Account Head</h4>
            </div>
        </div><!-- content-header -->
    </div><!-- col -->
    <div class="col-sm-0 tx-right col-lg-7">
        <!--     <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10">New Head</button> -->

        <!-- Button trigger modal -->

        @if(Auth::guard('admin')->check())
           <button type="button" class="btn btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">
            New Head
        </button>
        @else
        @can('account-head-create')
           <button type="button" class="btn btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">
            New Head
        </button>
        @endcan
        @endif


        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">ACCOUNT HEAD</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('account-head.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                        <div class="row">
{{--                            <div class="col-md-4">--}}

{{--                                    Account ID:<input type="text" class="{{ $errors->has('account_head->account_headId') ? 'has-error' : '' }}" value="{{isset($account_head1) ? $account_head1->account_headId+1 : 1 }}"  placeholder="customer Id" readonly>--}}
{{--                                    @if ($errors->has('account_headId'))--}}
{{--                                        <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $errors->first('account_headId') }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @endif--}}
{{--                                    <input type="text" name="FirstName" value=""><br>--}}

{{--                            </div><!-- col -->--}}
                            <div class="col-md-4">

                                    Account Head:<input type="text" name="title"  required><br>

                            </div><!-- col -->
                            <div class="col-md-4">
                                Type:
                                <select class="form-control bd bd-gray-900" name="type" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                    <option value="income">Income</option>
                                    <option value="expense">Expense</option>

                                </select>
                            </div>
                            <div class="col-md-4">

                                    Opening Due:<br> <div class="input-group mg-b-10">
                                        <div class="wd-25p">
                                            <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                        </div>
                                        <input type="text" name="opening_due" class="form-control form-control-sm group_right" placeholder="0.0" >
{{--                                        <input type="text" name="FirstName" value="" class="form-control form-control-sm group_right" placeholder="0.0">--}}


                                    </div><br>

                            </div><!-- col -->
                        </div><!-- row -->
                        <div class="modal-footer modal-footer_footer">
                            <button  class="btn btn-success mg-r-10">Update</button>
                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                            <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              <button type="button" class="btn btn-primary">Save changes</button> -->
                        </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div><!-- col -->
</div><!-- row -->

{{--    <div class="row">--}}
{{--        <div class="col-sm-5">--}}
{{--            <div class="content-header">--}}
{{--                <div>--}}
{{--                    <nav aria-label="breadcrumb">--}}
{{--                        <ol class="breadcrumb">--}}
{{--                            <li class="breadcrumb-item"><a href="#">Account</a></li>--}}
{{--                            <li class="breadcrumb-item active" aria-current="page">Account Head</li>--}}
{{--                        </ol>--}}
{{--                    </nav>--}}
{{--                    <h4 class="content-title content-title-sm">Account Head</h4>--}}
{{--                </div>--}}
{{--            </div><!-- content-header -->--}}
{{--        </div><!-- col -->--}}
{{--        <div class="col-sm-0 tx-right col-lg-7">--}}
{{--            <a href="{{route('account-head.create')}}"class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10">New Account Head</a>--}}
{{--            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10">New Head</button>--}}
{{--        </div><!-- col -->--}}
{{--    </div><!-- row -->--}}


    <div class="content-body" style="margin-top: -30px;">
        <div class="component-section">


            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-2 pd-r-0">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="All Status"></option>
                            <option value="Firefox">Expense</option>
                            <option value="Chrome">Income</option>
                            <option value="Opera">Assets</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="search" class="form-control form-control-sm" placeholder="Search Account Head">
                    </div>
                    <div class="col-sm mail-navbar bd-b-0 justify-content-end mg-r-10" style="height: auto;">
                        <div class="d-none d-lg-flex">
                            @if(Auth::guard('admin')->check())
                               <a href=""><i data-feather="printer"></i></a>
                            @else
                            @can('account-head-print')
                               <a href=""><i data-feather="printer"></i></a>
                            @endcan
                            @endif

                            <a href=""><i data-feather="folder"></i></a>

                            @if(Auth::guard('admin')->check())
                                <a href=""><i class="fas fa-download"></i></a>
                            @else
                            @can('account-head-download')
                                <a href=""><i class="fas fa-download"></i></a>
                            @endcan
                            @endif

                        </div>
                    </div>
                </div>
            </div> <!--form-group-->
            <div class="card">


            <table id="example1" class="table  table table-sm table-bordered mg-b-0">
                <thead>
                <tr>
{{--                    <th class="wd-5p"><input type="checkbox" aria-label="Checkbox for following text input"></th>--}}
                    <th class="wd-15p">S.N</th>
                    <th class="wd-25p">Account Head</th>
                    <th class="wd-10p">Status</th>
{{--                    <th class="wd-10p">Cr</th>--}}
{{--                    <th class="wd-15p">Balance</th>--}}
                    <th class="wd-15p">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($account_head as $key =>$account_head )
                <tr>
{{--                    <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
                    <td>{{++$key}}</td>
                    <input type="hidden" class="account_head_id1234">
{{--                    <td>{{$account_head->account_headId}}</td>--}}
                    <td>{{$account_head->title}}</td>
                    <td>
                        @if($account_head->type=='expense')
                        <span class="badge badge-pill badge-warning wd-70">{{$account_head->type}}</span>
                       @endif
                        @if($account_head->type=='income')
                            <span class="badge badge-pill badge-success wd-70">Income</span>
                        @endif
                    </td>
{{--                    <td>--}}
{{--                        <span class="badge badge-pill badge-success wd-70">Income</span>--}}
{{--                    </td>--}}

                    <td class="d-md-flex">
                        @if(Auth::guard('admin')->check())
                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>
                        @else
                        @can('account-head-view')
                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>
                        @endcan
                        @endif

                        @if(Auth::guard('admin')->check())
                            <div class="mg-r-20" title="Edit"><a href="{{route('account-head.edit',$account_head->id)}}"  class="edit" data-id="{{$account_head->id}}" data-toggle="modal" data-target="#exampleModalCenter1" ><i class="far fa-edit text-warning"></i></a></div>
                        @else
{{--                        @can('account-head-edit')--}}
{{--                            <div class="mg-r-20" title="Edit"><a href="{{route('account-head.edit',$account_head->id)}}"  class="edit" data-id="{{$account_head->id}}" data-toggle="modal" data-target="#exampleModalCenter1" ><i class="far fa-edit text-warning"></i></a></div>--}}
{{--                        @endcan--}}
                        @endif

                        @if(Auth::guard('admin')->check())
                            <div class="mg-r-20" title="Delete"><a href=" " data-toggle="modal" class="delete_data" data-id="{{$account_head->id}}" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                        @else
{{--                        @can('account-head-delete')--}}
{{--                            <div class="mg-r-20" title="Delete"><a href=" " data-toggle="modal" class="delete_data"  data-id="{{$account_head->id}}" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>--}}
{{--                        @endcan--}}
                        @endif







                        {{--                            fot delete popup--}}

                        <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <div class="conform_del">
                                            <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                        </div><!-- conform_del -->

                                        <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                            <a class="final_delete btn btn-success mg-r-10" href=" ">Yes</a>
                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-0 tx-right col-lg-7">
                            <!--     <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10">New Head</button> -->

                            <!-- Button trigger modal -->
{{--                            <button type="button" class="btn btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">--}}
{{--                                New Head--}}
{{--                            </button>--}}

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">ACCOUNT HEAD</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
{{--                                            <form action="{{route('account-head.update',$account_head->id)}}" method="post" enctype="multipart/form-data">--}}
{{--                                                @csrf--}}
{{--                                                @method('put')--}}
                                                <div class="row">
                                                    {{--                            <div class="col-md-4">--}}

                                                    {{--                                    Account ID:<input type="text" class="{{ $errors->has('account_head->account_headId') ? 'has-error' : '' }}" value="{{isset($account_head1) ? $account_head1->account_headId+1 : 1 }}"  placeholder="customer Id" readonly>--}}
                                                    {{--                                    @if ($errors->has('account_headId'))--}}
                                                    {{--                                        <span class="invalid-feedback" role="alert">--}}
                                                    {{--                                        <strong>{{ $errors->first('account_headId') }}</strong>--}}
                                                    {{--                                    </span>--}}
                                                    {{--                                    @endif--}}
                                                    {{--                                    <input type="text" name="FirstName" value=""><br>--}}

                                                    {{--                            </div><!-- col -->--}}
                                                    <div class="col-md-4">

                                                        Account Head:<input type="text" id="account_head123" class="account_head123" name="title"><br>

                                                    </div><!-- col -->
                                                    <div class="col-md-4">
                                                        Type:
                                                        <select class="type form-control bd bd-gray-900" name="type" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                            <option value="income">Income</option>
                                                            <option value="expense">Expense</option>

                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">

                                                        Opening Due:<br> <div class="input-group mg-b-10">
                                                            <div class="wd-25p">
                                                                <span class="input-group-text form-control-sm wd-100p" id="basic-addon1">NPR</span>
                                                            </div>
                                                            <input type="text" name="opening_due" class="opening_due form-control form-control-sm group_right" placeholder="0.0" >
                                                            {{--                                        <input type="text" name="FirstName" value="" class="form-control form-control-sm group_right" placeholder="0.0">--}}


                                                        </div><br>

                                                    </div><!-- col -->
                                                </div><!-- row -->
                                                <div class="modal-footer modal-footer_footer">
                                                    <button  class="update btn btn-success mg-r-10">Update</button>
                                                    <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                    <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                      <button type="button" class="btn btn-primary">Save changes</button> -->
                                                </div>
{{--                                            </form>--}}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div><!-- col -->


{{--                        <div class="mg-r-20" title="Delete"><a href="{{route('account-head.destroy',$account_head->id)}}"><i class="icon ion-trash-b text-danger"></i></a></div>--}}
{{--                        <div class="mg-r-20" title="Delete"><a href="{{route('account-head.destroy',$account_head->id)}}" onclick="return confirm('Are you sure you want to delete this item?');"><i class="icon ion-trash-b text-danger"></i></a></div>--}}
                    </td>
                </tr>
               @endforeach
                </tbody>
            </table>
        </div><!-- component-section -->

    </div><!-- content-body -->

</div>

@endsection

        @push('scripts')
            <script>

                $(".edit").click(function (e) {
                    e.preventDefault()

                    var account_head_id= $(this).data('id');
                    // alert(account_head_id);
                    $.ajax({

                        url: "{{url('/account-head-edit')}}",
                        type: "POST",
                        data: {
                            _token : "{{csrf_token()}}",
                            account_head_id : account_head_id
                        },
                        success: function (data) {
                            console.log(data);

                            $('#account_head123').append($('#account_head123').val(data.account_head.title));
                            $('.account_head_id1234').append($('.account_head_id1234').val(account_head_id));
                            $('.type').append($('.type').val(data.account_head.type));
                            $('.opening_due').append($('.opening_due').val(data.account_head.opening_due));



                        },

                        error: function (error) {

                            console.log('error');

                        }


                    });

                });


                $(".update").click(function (e) {
                    e.preventDefault()

                    // var expense_id= $(this).data('expense_id');
                    var account_head_id= $('.account_head_id1234').val();
                    var account_head = $('#account_head123').val();
                    var type = $('.type').val();
                    var opening_due = $('.opening_due').val();


                    // alert(account_head_id);
                    $.ajax({

                        url: "{{url('/account-head-update')}}",
                        type: "POST",
                        data: {
                            _token : "{{csrf_token()}}",
                            account_head_id :account_head_id,
                            title:account_head,
                            type:type,
                            opening_due:opening_due,

                        },
                        success: function (data) {
                            console.log(data);


                            toastr.success(data.success);
                            // console.log('success');
                            window.location.reload();

                        },

                        error: function (error) {

                            console.log('error');

                        }


                    });

                });

                //for the delete data
                $(".delete_data").click(function(e){
                    e.preventDefault()

                    var account_head_id = $(this).data('id');
                    // alert(account_head_id);


                    $.ajax({
                        type: 'POST',
                        url: '{{url('/account-head-edit')}}',
                        data: {
                            _token : "{{csrf_token()}}",
                            account_head_id : account_head_id,


                        },
                        success: function (data, status) {

                            if(data.error){

                                return;
                            }

                            // $('#show_modal').modal('hide');
                            // toastr.success(data.success);

                            console.log(data.account_head.id);
                            // window.location.reload();
                            // alert('test');

                            $('.account_head_id1234').append($('.account_head_id1234').val(data.account_head.id));


                        },
                        error: function (xhr, status, error) {
                            // console.log('error');
                            // toastr.error(error.errors);
                        }
                    });

                });


                //for final delete
                $(".final_delete").click(function(e){
                    e.preventDefault()

                    var account_head_id= $(".account_head_id1234").val();


                    // alert(account_head_id);

                    $.ajax({
                        type: 'POST',
                        url: '{{url('/account-head-delete')}}',
                        data: {
                            _token : "{{csrf_token()}}",
                            account_head_id : account_head_id,


                        },
                        success: function (data, status) {

                            if(data.errors){
                                toastr.error(data.errors);
                                return;
                            }

                            toastr.success(data.success);
                            window.location.reload();
                            console.log(data);



                        },
                        error: function (xhr, status, error) {
                            console.log('error');
                            // toastr.error(data.errors);
                            // var err = JSON.parse(xhr.responseText);
                            // $('#category_error').append(err.errors.title);
                        }
                    });

                });
            </script>
@endpush
