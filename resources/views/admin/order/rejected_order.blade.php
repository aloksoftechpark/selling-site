@extends('admin.layouts.app')
@section('content')
<style>
        .orderForm input, .orderForm select {
            width: 60% !important;
            border: none !important;
            padding: 5px !important;
            -moz-appearance: none;
            -webkit-appearance: none;
        }
        .orderForm select::-ms-expand {
            display: none !important;
        }
    </style>

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Order</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Rejected Orders</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Rejected Orders</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="d-lg-flex pd-l-0">
                    <div class="pd-lg-r-20">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="Packages"></option>
                            <option value="2075-2076">Product Oriented</option>
                            <option value="2074-2075">Service Oriented</option>
                        </select>
                    </div>
                    <div class="pd-lg-r-20">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="Plan"></option>
                            <option value="">Silver</option>
                            <option value="">Gold</option>
                            <option value="">Platinum</option>
                        </select>
                    </div>
                    <div class="pd-lg-r-20">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                </div>
            </div> <!--form-group-->
            <div class="table-responsive">
               <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5p">SN.</th>
                        <th class="wd-10p">Reg. ID</th>
                        <th class="wd-20p">Company Name</th>
                        <th class="wd-15p">Package</th>
                        <th class="wd-15p">Plan</th>
                        <th class="wd-15p">Payment</th>
                        <th class="wd-10p">Due Amount</th>
                        <th class="wd-10p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                     @foreach($order_rejected as $key=> $rejected)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>Req{{$rejected->id}}</td>
                        <td>{{$rejected->company_name}}</td>
                        <td>{{$rejected->package}}</td>
                        <td>
                            @if($rejected->plan == 'gold')
                            <span class="badge badge-pill badge-warning wd-80">{{$rejected->plan}}</span>
                            @elseif($rejected->plan == 'platinum')
                                <span class="badge badge-pill badge-info wd-80">Platinum</span>
                            @else
                                <span class="badge badge-pill badge-light wd-80">Silver</span>
                            @endif
                        </td>
                        <td>{{$rejected->payment_type}}</td>
                        <td>
                            @if($rejected->payment_status == 'yes')
                            <span class="badge badge-pill badge-success wd-80">Paid</span>
                            @else
                                <span class="badge badge-pill badge-danger wd-80">UnPaid</span>
                            @endif
                        </td>
                        <td>
                            <button class="verified bg-primary bd rounded-5 text-white" data-id="{{$rejected->id}}" data-toggle="modal"
                                    data-target="#orderModal{{$rejected->id}}"><i class="fas fa-info-circle"></i></button>
                            <div class="modal fade" id="orderModal{{$rejected->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="orderModalTitle" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head new_item" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="orderModalTitle">ADD ORDER VERIFICATION</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <form method="POST" action="{{route('order-pending.update',$rejected->id)}}" class="orderForm detail_lead verification_lead" id="orderForm{{$rejected->id}}">
                                                       @csrf
                                                        @method('PATCH')
                                                        <ul>
                                                            <li>
                                                                User :
                                                                <select id="user_id" name="user_id" aria-describedby="button-addon1">
                                                                    <option value="">Select user</option>
                                                                    @foreach($users as $user)
                                                                        <option value="{{$user->id}}" @if($user->id==$rejected->user['id']) selected @endif>{{$user->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </li>
                                                            <li>
                                                                Company Name :
                                                                <input type="text" name="company_name" value="{{$rejected->company_name}}">
                                                            </li>
                                                            <li>
                                                                Company Address :
                                                                <input type="text" name="company_address" value="{{$rejected->company_address}}">
                                                            </li>
                                                            <li>
                                                                Contact Number :
                                                                <input type="text" name="contact_number" value="{{$rejected->contact_number}}">
                                                            </li>
                                                            <li>
                                                                PAN :
                                                                <input type="number" name="pan" value="{{$rejected->pan}}">
                                                            </li>
                                                            <li>
                                                                Package :
                                                                <select name="package">
                                                                    <option value="">Select Package</option>
                                                                    <option value="service oriented" @if($rejected->package=='service oriented') selected @endif>Service Oriented</option>
                                                                    <option value="product oriented" @if($rejected->package=='product oriented') selected @endif>Product Oriented</option>
                                                                    <option value="third party" @if($rejected->package=='third party') selected @endif>Third party</option>
                                                                </select>
                                                            </li>
                                                            <li>
                                                                Plan :
                                                                <select name="plan">
                                                                    <option value="">Select Plan</option>
                                                                    <option value="gold" @if($rejected->plan=='gold') selected @endif>Gold</option>
                                                                    <option value="platinum"  @if($rejected->plan=='platinum') selected @endif>Platinum</option>
                                                                    <option value="silver" @if($rejected->plan=='silver') selected @endif>Silver</option>
                                                                </select>
                                                            </li>
                                                            <li>
                                                                Payment Type :
                                                                <select name="payment_type">
                                                                    <option value="">Select Type</option>
                                                                    <option value="quartely" @if($rejected->payment_type=='quartely') selected @endif>Quartely</option>
                                                                    <option value="anually" @if($rejected->payment_type=='anually') selected @endif>Anually</option>
                                                                    <option value="half_anually" @if($rejected->payment_type=='half_anually') selected @endif>Half Anually</option>
                                                                </select>   
                                                            </li>
                                                        </ul>
                                                    </form><!-- detail_lead -->
                                                    <form method="post" action="{{url('order-status',$rejected->id)}}" id="activeForm{{$rejected->id}}">
                                                            @csrf
                                                            <input type="hidden" value="1" name="status">
                                                    </form>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="detail_lead verification_lead d-flex justify-content-end">
                                                        <ul>
                                                            <li>Total : <small>1500 NPR</small></li>
                                                            <li>Discount (10%) : <small>150 NPR</small>
                                                            </li>
                                                            <li>
                                                                <div class="hidden d-inline v-hidden">Discount (10%)</div> <small>(Promo Code
                                                                    Dashain15)</small>
                                                            </li>
                                                            <li>Discount (25%) : <small>150 NPR</small>
                                                            </li>
                                                            <li>
                                                                <div class="hidden d-inline v-hidden">Discount (25%)</div> <small>(For Annual
                                                                    Payment)</small>
                                                            </li>
                                                            <li> VAT (13%) : <small>150 NPR</small></li>
                                                            <li> Payable Amount : <small>1500 NPR</small></li>
                                                            <li> Payment Status : <small><span class="payment_status"></span></small></li>
                                                            <li> Payment ID : <small><span class="payment_id"></span></small></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer modal-footer_footer modal-footer-right text-center">
                                                <button type="button" class="btn btn-success mg-r-10" onclick="submitForm('orderForm{{$rejected->id}}')">Update</button>
                                                <button type="button" class="btn btn-primary mg-r-10" onclick="submitForm('activeForm{{$rejected->id}}')">Approve</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->

<script>
        function  submitForm(formId) {
            if(confirm('Are you sure want to submit?')){
                let form= $(`#${formId}`);
                $.ajax({
                    type: form.attr("method"),
                    url: form.attr("action"),
                    data: form.serialize(),
                    success: function (data, status) {
                        console.log(data);
                        if(data.indexOf('success')>=0){
                            $(".modal").modal('hide');
                            toastr.success(data);
                            location.reload(true);
                        }else{
                            toastr.error(data);
                        }
                    },
                    error: function (xhr, status, error) {
                        console.log(error);
                        toastr.error('Something went wrong. please try again later.');
                    }
                });
            }return false;
        }
    </script>
@endsection
