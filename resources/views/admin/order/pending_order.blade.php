@extends('admin.layouts.app')
@section('content')
    <style>
        .orderForm input,
        .orderForm select {
            width: 60% !important;
            border: none !important;
            padding: 5px !important;
            -moz-appearance: none;
            -webkit-appearance: none;
        }

        .orderForm select::-ms-expand {
            display: none !important;
        }

    </style>

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Order</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Pending Order</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Pending Order Requests</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"
                data-target="#addNewOrder">Add Order</button>
            <div class="modal fade modal_cust" id="addNewOrder" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle1">ADD ORDER</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body salary_payroll">
                            <ul class="nav nav-tabs d-none">
                                <li><a class="active" href="#tab100" data-toggle="tab">Shipping</a></li>
                                <li><a href="#tab101" data-toggle="tab" class="">Quantities</a></li>
                            </ul>
                            <form action="{{ route('order-pending.store') }}" method="post" enctype="multipart/form-data"
                                id="addNewOrderForm">
                                @csrf
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab100">
                                        <div class="row">
                                            <div class="col-md-6">
                                                User
                                                <div class="input-group">
                                                    <div class="input-group-prepend wd-15p">
                                                        <span
                                                            class="input-group-text form-control wd-100p justify-content-center"
                                                            id="basic-addon1"></span>
                                                    </div>
                                                    <select id="user_id" name="user_id"
                                                        class="form-control form-control-sm modal_select_option_height bd bd-gray-900 user_id"
                                                        aria-label="Example text with button addon"
                                                        aria-describedby="button-addon1">
                                                        <option value="" id="user_id">Select user</option>

                                                        @forelse($users as $user)
                                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                        @empty
                                                        @endforelse
                                                    </select>

                                                </div>
                                                <div class="mg-t-5">
                                                    Company Name:
                                                    <input type="text" name="company_name"
                                                        class="form-control form-control-sm wd-120" placeholder="">
                                                </div>
                                                <div class="mg-t-5">
                                                    Company Address:
                                                    <input type="text" name="company_address"
                                                        class="form-control form-control-sm wd-120" placeholder="">
                                                </div>
                                                <div class="mg-t-5">
                                                    Contact Number:
                                                    <input type="text" name="contact_number"
                                                        class="form-control form-control-sm wd-120" placeholder="">
                                                </div>
                                                <div class="mg-t-5">
                                                    PAN:
                                                    <input type="number" name="pan"
                                                        class="form-control form-control-sm wd-120" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                IRD Verified ?
                                                <div class="d-flex mg-t-5 mg-b-14">
                                                    <div class="custom-control custom-radio mg-r-20">
                                                        <input type="radio" id="customRadio1" name="ird_yes"
                                                            class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">YES</label>
                                                    </div>
                                                    <div class="custom-control custom-radio mg-r-10">
                                                        <input type="radio" id="customRadio2" name="ird_no"
                                                            class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">NO</label>
                                                    </div>
                                                </div>
                                                <div class="mg-t-5">
                                                    Package:
                                                    <select name="package"
                                                        class="form-control form-control-sm modal_select_option_height bd bd-gray-900"
                                                        aria-label="Example text with button addon"
                                                        aria-describedby="button-addon1">
                                                        <option value="service oriented">Service Oriented</option>
                                                        <option value="product oriented">Product Oriented</option>
                                                        <option value="third party">Third party</option>

                                                    </select>
                                                </div>
                                                <div class="mg-t-5">
                                                    Plan:
                                                    <select name="plan"
                                                        class="form-control form-control-sm modal_select_option_height bd bd-gray-900"
                                                        aria-label="Example text with button addon"
                                                        aria-describedby="button-addon1">
                                                        <option value="gold">Gold</option>
                                                        <option value="platinum">Platinum</option>
                                                        <option value="silver">Silver</option>

                                                    </select>
                                                </div>
                                                <div class="mg-t-5">
                                                    Payment Type:
                                                    <select name="payment_type"
                                                        class="form-control form-control-sm modal_select_option_height bd bd-gray-900"
                                                        aria-label="Example text with button addon"
                                                        aria-describedby="button-addon1">
                                                        <option value="quartely">Quartely</option>
                                                        <option value="anually">Anually</option>
                                                        <option value="half_anually">Half Anually</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="btn_nxt_prev mg-t-25">
                                            <a href="#" class="btn btn-primary btnNext wd-20p"> Next </a>
                                        </div><!-- btn_nxt_prev -->
                                    </div>
                                    <div class="tab-pane" id="tab101">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table
                                                        class="table table-bordered-i table-bordered table-border-main mg-b-0">
                                                        <thead>
                                                            <tr class="text-center">
                                                                <th scope="col">SN</th>
                                                                <th scope="col" class="text-left">Particular</th>
                                                                <th scope="col">Amount</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="text-center">
                                                                <th scope="row">1</th>
                                                                <td class="text-left">Product Oriented with GOLD plan for
                                                                    Education</td>
                                                                <td>20,000 /-</td>
                                                            </tr>
                                                            <tr class="text-center">
                                                                <td></td>
                                                                <td class="bd-l-0-i">Discount</td>
                                                                <td>20,000 /-</td>
                                                            </tr>
                                                            <tr class="text-center">
                                                                <td></td>
                                                                <td class="bd-l-0-i">VAT (13%)</td>
                                                                <td>10,000 /-</td>
                                                            </tr>
                                                            <tr class="text-center">
                                                                <td></td>
                                                                <td class="bd-l-0-i">Total</td>
                                                                <td>30,000 /-</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 mg-t-10">
                                                Payment Status
                                                <div class="d-flex mg-t-5 mg-b-14">
                                                    <div class="custom-control custom-radio mg-r-20">
                                                        <input type="radio" id="customRadio1" name="payment_yes"
                                                            class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">YES</label>
                                                    </div>
                                                    <div class="custom-control custom-radio mg-r-10">
                                                        <input type="radio" id="customRadio2" name="payment_no"
                                                            class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">NO</label>
                                                    </div>
                                                </div>
                                                <div class="mg-t-5">
                                                    Payment Method
                                                    <select name="payment_method"
                                                        class="form-control form-control-sm modal_select_option_height bd bd-gray-900"
                                                        aria-label="Example text with button addon"
                                                        aria-describedby="button-addon1">
                                                        <option value="volvo">Guest</option>
                                                        <option value="saab">Ranjan</option>
                                                        <option value="opel">Manoj</option>
                                                        <option value="audi">Pawan</option>
                                                    </select>
                                                </div>
                                                <div class="mg-t-5">
                                                    Payment ID:
                                                    <input type="text" name="payment_id"
                                                        class="form-control form-control-sm wd-120" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-md-6 mg-t-10">
                                                Referral Code
                                                <input type="text" name="referral_code"
                                                    class="form-control form-control-sm wd-120" placeholder="">
                                                <div class="mg-t-5">
                                                    Promo Code
                                                    <input type="text" name="promo_code"
                                                        class="form-control form-control-sm wd-120" placeholder="">
                                                </div>
                                                <div class="mg-t-5">
                                                    <div
                                                        class="modal-footer modal-footer_footer justify-content-center footer_inline_pending pd-b-0 pd-t-14 d-flex">
                                                        <button type="button"
                                                            class="btn btn-primary btnPrevious mg-r-10">Previous</button>
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="d-lg-flex pd-l-0">
                    <div class="pd-lg-r-20">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="Packages"></option>
                            <option value="2075-2076">Product Oriented</option>
                            <option value="2074-2075">Service Oriented</option>
                        </select>
                    </div>
                    <div class="pd-lg-r-20">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="Plan"></option>
                            <option value="">Silver</option>
                            <option value="">Gold</option>
                            <option value="">Platinum</option>
                        </select>
                    </div>
                    <div class="pd-lg-r-20">
                        <select class="form-control form-control-sm select2-no-search">
                            <option label="Status"></option>
                            <option value="">Paid</option>
                            <option value="">Unpaid</option>
                        </select>
                    </div>
                    <div class="pd-lg-r-20">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                </div>
            </div>
            <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                        <tr>
                            <th class="wd-5p">SN.</th>
                            <th class="wd-10p">Reg. ID</th>
                            <th class="wd-20p">Company Name</th>
                            <th class="wd-10p">Phone</th>
                            <th class="wd-15p">Packages</th>
                            <th class="wd-10p">Plan</th>
                            <th class="wd-10p">Payment</th>
                            <th class="wd-10p">Status</th>
                            <th class="wd-10p">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($order_pending as $key => $pending)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>Req{{ $pending->id }}</td>
                                <td>{{ $pending->company_name }}</td>
                                <td>{{ $pending->contact_number }}</td>
                                <td>{{ $pending->package }}</td>
                                <td>
                                    @if ($pending->plan == 'gold')
                                        <span class="badge badge-pill badge-warning wd-80">{{ $pending->plan }}</span>
                                    @elseif($pending->plan == 'platinum')
                                        <span class="badge badge-pill badge-info wd-80">Platinum</span>
                                    @else
                                        <span class="badge badge-pill badge-light wd-80">Silver</span>
                                    @endif
                                </td>
                                <td>{{ $pending->payment_type }}</td>
                                <td>
                                    @if ($pending->payment_status == 'yes')
                                        <span class="badge badge-pill badge-success wd-80">Paid</span>
                                    @else
                                        <span class="badge badge-pill badge-danger wd-80">UnPaid</span>
                                    @endif
                                </td>
                                <td>
                                    <button class="verified bg-primary bd rounded-5 text-white" data-id="{{ $pending->id }}"
                                        data-toggle="modal" data-target="#orderModal{{ $pending->id }}"><i
                                            class="fas fa-info-circle"></i></button>
                                    <div class="modal fade" id="orderModal{{ $pending->id }}" tabindex="-1" role="dialog"
                                        aria-labelledby="orderModalTitle" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-dialog-centered modal_ac_head new_item"
                                            role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="orderModalTitle">ADD ORDER VERIFICATION</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <form method="POST"
                                                                action="{{ route('order-pending.update', $pending->id) }}"
                                                                class="orderForm detail_lead verification_lead"
                                                                id="orderForm{{ $pending->id }}">
                                                                @csrf
                                                                @method('PATCH')
                                                                <ul>
                                                                    <li>
                                                                        User :
                                                                        <select id="user_id" name="user_id"
                                                                            aria-describedby="button-addon1">
                                                                            <option value="">Select user</option>
                                                                            @foreach ($users as $user)
                                                                                <option value="{{ $user->id }}" @if ($user->id == $pending->user['id'])
                                                                                    selected
                                                                            @endif>{{ $user->name }}
                                                                            </option>
                        @endforeach
                        </select>
                        </li>
                        <li>
                            Company Name :
                            <input type="text" name="company_name" value="{{ $pending->company_name }}">
                        </li>
                        <li>
                            Company Address :
                            <input type="text" name="company_address" value="{{ $pending->company_address }}">
                        </li>
                        <li>
                            Contact Number :
                            <input type="text" name="contact_number" value="{{ $pending->contact_number }}">
                        </li>
                        <li>
                            PAN :
                            <input type="number" name="pan" value="{{ $pending->pan }}">
                        </li>
                        <li>
                            Package :
                            <select name="package">
                                <option value="">Select Package</option>
                                <option value="service oriented" @if ($pending->package == 'service oriented') selected @endif
                                    >Service Oriented</option>
                                <option value="product oriented" @if ($pending->package == 'product oriented') selected @endif
                                    >Product Oriented</option>
                                <option value="third party" @if ($pending->package == 'third party') selected @endif>Third
                                    party</option>
                            </select>
                        </li>
                        <li>
                            Plan :
                            <select name="plan">
                                <option value="">Select Plan</option>
                                <option value="gold" @if ($pending->plan == 'gold') selected
                                    @endif>Gold</option>
                                <option value="platinum" @if ($pending->plan == 'platinum')
                                    selected @endif>Platinum</option>
                                <option value="silver" @if ($pending->plan == 'silver')
                                    selected @endif>Silver</option>
                            </select>
                        </li>
                        <li>
                            Payment Type :
                            <select name="payment_type">
                                <option value="">Select Type</option>
                                <option value="quartely" @if ($pending->payment_type == 'quartely') selected @endif
                                    >Quartely</option>
                                <option value="anually" @if ($pending->payment_type == 'anually') selected @endif
                                    >Anually</option>
                                <option value="half_anually" @if ($pending->payment_type == 'half_anually') selected @endif
                                    >Half Anually</option>
                            </select>
                        </li>
                        </ul>
                        </form><!-- detail_lead -->
                        <form method="post" action="{{ url('order-status', $pending->id) }}"
                            id="activeForm{{ $pending->id }}">
                            @csrf
                            <input type="hidden" value="1" name="status">
                        </form>
                        <form method="post" action="{{ url('order-status', $pending->id) }}"
                            id="rejectForm{{ $pending->id }}">
                            @csrf
                            <input type="hidden" value="2" name="status">
                        </form>
            </div>
            <div class="col-md-5">
                <div class="detail_lead verification_lead d-flex justify-content-end">
                    <ul>
                        <li>Total : <small>1500 NPR</small></li>
                        <li>Discount (10%) : <small>150 NPR</small>
                        </li>
                        <li>
                            <div class="hidden d-inline v-hidden">Discount (10%)</div> <small>(Promo Code
                                Dashain15)</small>
                        </li>
                        <li>Discount (25%) : <small>150 NPR</small>
                        </li>
                        <li>
                            <div class="hidden d-inline v-hidden">Discount (25%)</div> <small>(For Annual
                                Payment)</small>
                        </li>
                        <li> VAT (13%) : <small>150 NPR</small></li>
                        <li> Payable Amount : <small>1500 NPR</small></li>
                        <li> Payment Status : <small><span class="payment_status"></span></small></li>
                        <li> Payment ID : <small><span class="payment_id"></span></small></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="modal-footer modal-footer_footer modal-footer-right text-center">
            <button type="button" class="btn btn-success mg-r-10"
                onclick="submitForm('orderForm{{ $pending->id }}')">Update</button>
            <button type="button" class="btn btn-primary mg-r-10"
                onclick="submitForm('activeForm{{ $pending->id }}')">Approve</button>
            <button type="button" class="btn btn-danger mg-r-10"
                onclick="submitForm('rejectForm{{ $pending->id }}')">Reject</button>
        </div>
    </div>
    </div>
    </div>
    </div>
    </td>
    </tr>
    @endforeach

    </tbody>
    </table>
    </div>
    </div><!-- component-section -->
    </div><!-- content-body -->
    <script>
        function submitForm(formId) {
            if (confirm('Are you sure want to submit?')) {
                let form = $(`#${formId}`);
                $.ajax({
                    type: form.attr("method"),
                    url: form.attr("action"),
                    data: form.serialize(),
                    success: function(data, status) {
                        console.log(data);
                        if (data.indexOf('success') >= 0) {
                            $(".modal").modal('hide');
                            toastr.success(data);
                            location.reload(true);
                        } else {
                            toastr.error(data);
                        }
                    },
                    error: function(xhr, status, error) {
                        console.log(error);
                        toastr.error('Something went wrong. please try again later.');
                    }
                });
            }
            return false;
        }

    </script>
@endsection
