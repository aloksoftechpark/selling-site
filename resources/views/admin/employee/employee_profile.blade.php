@extends('admin.layouts.app')
@section('content')


    {{--<!DOCTYPE html>--}}
{{--<html lang="en">--}}
{{--<head>--}}

{{--    <!-- Required meta tags -->--}}
{{--    <meta charset="utf-8">--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">--}}

{{--    <!-- Meta -->--}}
{{--    <meta name="description" content="Responsive Bootstrap 4 Dashboard and Admin Template">--}}
{{--    <meta name="author" content="ThemePixels">--}}

{{--    <!-- Favicon -->--}}
{{--    <link rel="shortcut icon" type="image/x-icon" href="../assets/img/favicon.png">--}}

{{--    <title>Cassie Responsive Bootstrap 4 Dashboard and Admin Template</title>--}}

{{--    <!-- vendor css -->--}}
{{--    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">--}}
{{--    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">--}}

{{--    <!-- template css -->--}}
{{--    <link rel="stylesheet" href="../assets/css/cassie.css">--}}

{{--</head>--}}
{{--<body>--}}

{{--<div class="sidebar">--}}
{{--    <div class="sidebar-header">--}}
{{--        <div>--}}
{{--            <a href="../index.html" class="sidebar-logo"><span>cassie</span></a>--}}
{{--            <small class="sidebar-logo-headline">Responsive Dashboard Template</small>--}}
{{--        </div>--}}
{{--    </div><!-- sidebar-header -->--}}
{{--    <div id="dpSidebarBody" class="sidebar-body">--}}
{{--        <ul class="nav nav-sidebar">--}}
{{--            <li class="nav-label"><label class="content-label">Template Pages</label></li>--}}
{{--            <li class="nav-item show">--}}
{{--                <a href="../pages/dashboard-two.html" class="nav-link"><i data-feather="box"></i> Dashboard</a>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="layout"></i> Sales</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="app-newSales.html" class="nav-sub-link">New Sales</a>--}}
{{--                    <a href="app-allSales.html" class="nav-sub-link">All Sales</a>--}}
{{--                    <a href="../newPages/sales-Return.html" class="nav-sub-link">Sales Return</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="lock"></i> Purchase</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../newPages/new-purchase.html" class="nav-sub-link">New Purchase</a>--}}
{{--                    <a href="app-allSales.html" class="nav-sub-link">All Purchase</a>--}}
{{--                    <a href="../newPages/purchase-Return.html" class="nav-sub-link">Purchase Return</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="user"></i> Customer</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../newPages/newCustomer.html" class="nav-sub-link">New Customer</a>--}}
{{--                    <a href="page-timeline.html" class="nav-sub-link">All Customer</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="file-text"></i> Supplier</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../newPages/new-Supplier.html" class="nav-sub-link">New Supplier</a>--}}
{{--                    <a href="page-pricing.html" class="nav-sub-link">All Supplier</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="x-circle"></i> Product and Services</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../newPages/new-Product.html" class="nav-sub-link">New Product</a>--}}
{{--                    <a href="page-404.html" class="nav-sub-link">Product</a>--}}
{{--                    <a href="page-404.html" class="nav-sub-link">Damage Product</a>--}}
{{--                    <a href="app-catagory.html" class="nav-sub-link">Catagories</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="../newPages/Stock.html" class="nav-link"><i data-feather="layers"></i> Stock</a>--}}
{{--            </li>--}}
{{--            <li class="nav-label"><label class="content-label">Admin</label></li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="life-buoy"></i> Account</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../newPages/new-Expense.html" class="nav-sub-link">Expense</a>--}}
{{--                    <a href="../components/form-input-group.html" class="nav-sub-link">Income</a>--}}
{{--                    <a href="../newPages/receipt.html" class="nav-sub-link">Pay-in Receipt</a>--}}
{{--                    <a href="../newPages/recipt-out.html" class="nav-sub-link">Pay-out Receipt</a>--}}
{{--                    <a href="../" class="nav-sub-link">Account Head</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="book"></i> Branch</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../newPages/branches-list.html" class="nav-sub-link">Branches</a>--}}
{{--                    <a href="../components/con-icons.html" class="nav-sub-link">Stock Transfer</a>--}}
{{--                    <a href="../components/con-images.html" class="nav-sub-link">Employee Transfer</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="layers"></i> Human Resource</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../newPages/employee-list.html" class="nav-sub-link">Employee</a>--}}
{{--                    <a href="../newPages/Salary-payroll-list.html" class="nav-sub-link">Salary Payroll</a>--}}
{{--                    <a href="../newPages/attandence.html" class="nav-sub-link">Attendance</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="monitor"></i> Report</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="app-report.html" class="nav-sub-link">Sales Report</a>--}}
{{--                    <a href="../components/util-background.html" class="nav-sub-link">Purchase Report</a>--}}
{{--                    <a href="../components/util-border.html" class="nav-sub-link">Expense Report</a>--}}
{{--                    <a href="../components/util-display.html" class="nav-sub-link">Income Report</a>--}}
{{--                    <a href="../components/util-divider.html" class="nav-sub-link">Payin Report</a>--}}
{{--                    <a href="../components/util-flex.html" class="nav-sub-link">Payout Report</a>--}}
{{--                    <a href="../components/util-height.html" class="nav-sub-link">Payment in/out</a>--}}
{{--                    <a href="../newPages/salary-due.html" class="nav-sub-link">Due Salary Report</a>--}}
{{--                    <a href="../newPages/report-stock.html" class="nav-sub-link">Stock</a>--}}
{{--                    <a href="../components/util-padding.html" class="nav-sub-link">Customer Ledger</a>--}}
{{--                    <a href="../components/util-position.html" class="nav-sub-link">Supplier Ledger</a>--}}
{{--                    <a href="../components/util-typography.html" class="nav-sub-link">Account Ledger</a>--}}
{{--                    <a href="../components/util-width.html" class="nav-sub-link">Customer Due Report</a>--}}
{{--                    <a href="../components/util-extras.html" class="nav-sub-link">Supplier Due Report</a>--}}
{{--                    <a href="../components/util-extras.html" class="nav-sub-link">Profit/loss Report</a>--}}
{{--                    <a href="../components/util-extras.html" class="nav-sub-link">All Bills</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="pie-chart"></i> Configuration</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../components/chart-flot.html" class="nav-sub-link">General Setting</a>--}}
{{--                    <a href="../components/chart-chartjs.html" class="nav-sub-link">Payment Method</a>--}}
{{--                    <a href="../components/chart-peity.html" class="nav-sub-link">User</a>--}}
{{--                    <a href="../components/com-badge.html" class="nav-sub-link">Role and Premission</a>--}}
{{--                    <a href="../components/chart-peity.html" class="nav-sub-link">Bank</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="" class="nav-link with-sub"><i data-feather="pie-chart"></i> Other Pages</a>--}}
{{--                <nav class="nav nav-sub">--}}
{{--                    <a href="../newPages/bill.html" class="nav-sub-link">Bill</a>--}}
{{--                    <a href="../newPages/receipt-print.html" class="nav-sub-link">Receipt Printable</a>--}}
{{--                    <a href="../newPages/add-stock.html" class="nav-sub-link">Add Stock</a>--}}
{{--                    <a href="../newPages/add-damage.html" class="nav-sub-link">Add Damage Item</a>--}}
{{--                    <a href="../newPages/add-branch.html" class="nav-sub-link">Add Branch</a>--}}
{{--                    <a href="../newPages/new-Employee.html" class="nav-sub-link">New Employee</a>--}}
{{--                    <a href="../newPages/Attandence-list.html" class="nav-sub-link">Employee Attandence</a>--}}
{{--                    <a href="../newPages/update-attandence.html" class="nav-sub-link">Update attandence</a>--}}
{{--                    <a href="../newPages/salary-payment.html" class="nav-sub-link">New Payment (Salary)</a>--}}
{{--                    <a href="../newPages/employee-profile.html" class="nav-sub-link">Employee Profile</a>--}}
{{--                    <a href="../newPages/employee-detail.html" class="nav-sub-link">Employee Details</a>--}}
{{--                </nav>--}}
{{--            </li>--}}
{{--        </ul>--}}

{{--        <hr class="mg-t-30 mg-b-25">--}}

{{--        <ul class="nav nav-sidebar">--}}
{{--            <li class="nav-item"><a href="themes.html" class="nav-link"><i data-feather="aperture"></i> Themes</a></li>--}}
{{--            <li class="nav-item"><a href="../docs.html" class="nav-link"><i data-feather="help-circle"></i> Documentation</a></li>--}}
{{--        </ul>--}}


{{--    </div><!-- sidebar-body -->--}}
{{--</div><!-- sidebar -->--}}

{{--<div class="content">--}}
{{--    <div class="header">--}}
{{--        <div class="header-left">--}}
{{--            <a href="" class="burger-menu"><i data-feather="menu"></i></a>--}}

{{--            <div class="header-search">--}}
{{--                <i data-feather="search"></i>--}}
{{--                <input type="search" class="form-control" placeholder="What are you looking for?">--}}
{{--            </div><!-- header-search -->--}}
{{--        </div><!-- header-left -->--}}

{{--        <div class="header-right">--}}
{{--            <a href="" class="header-help-link"><i data-feather="help-circle"></i></a>--}}
{{--            <div class="dropdown dropdown-notification">--}}
{{--                <a href="" class="dropdown-link new" data-toggle="dropdown"><i data-feather="bell"></i></a>--}}
{{--                <div class="dropdown-menu dropdown-menu-right">--}}
{{--                    <div class="dropdown-menu-header">--}}
{{--                        <h6>Notifications</h6>--}}
{{--                        <a href=""><i data-feather="more-vertical"></i></a>--}}
{{--                    </div><!-- dropdown-menu-header -->--}}
{{--                    <div class="dropdown-menu-body">--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle text-primary bg-primary-light">s</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Socrates Itumay</strong> marked the task as completed.</p>--}}
{{--                                <span>5 hours ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-pink bg-pink-light">r</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Reynante Labares</strong> marked the task as incomplete.</p>--}}
{{--                                <span>8 hours ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-success bg-success-light">d</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Dyanne Aceron</strong> responded to your comment on this <strong>post</strong>.</p>--}}
{{--                                <span>a day ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                        <a href="" class="dropdown-item">--}}
{{--                            <div class="avatar"><span class="avatar-initial rounded-circle tx-indigo bg-indigo-light">k</span></div>--}}
{{--                            <div class="dropdown-item-body">--}}
{{--                                <p><strong>Kirby Avendula</strong> marked the task as incomplete.</p>--}}
{{--                                <span>2 days ago</span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div><!-- dropdown-menu-body -->--}}
{{--                    <div class="dropdown-menu-footer">--}}
{{--                        <a href="">View All Notifications</a>--}}
{{--                    </div>--}}
{{--                </div><!-- dropdown-menu -->--}}
{{--            </div>--}}
{{--            <div class="dropdown dropdown-loggeduser">--}}
{{--                <a href="" class="dropdown-link" data-toggle="dropdown">--}}
{{--                    <div class="avatar avatar-sm">--}}
{{--                        <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">--}}
{{--                    </div><!-- avatar -->--}}
{{--                </a>--}}
{{--                <div class="dropdown-menu dropdown-menu-right">--}}
{{--                    <div class="dropdown-menu-header">--}}
{{--                        <div class="media align-items-center">--}}
{{--                            <div class="avatar">--}}
{{--                                <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">--}}
{{--                            </div><!-- avatar -->--}}
{{--                            <div class="media-body mg-l-10">--}}
{{--                                <h6>Louise Kate Lumaad</h6>--}}
{{--                                <span>Administrator</span>--}}
{{--                            </div>--}}
{{--                        </div><!-- media -->--}}
{{--                    </div>--}}
{{--                    <div class="dropdown-menu-body">--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="user"></i> View Profile</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="edit-2"></i> Edit Profile</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="briefcase"></i> Account Settings</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="shield"></i> Privacy Settings</a>--}}
{{--                        <a href="" class="dropdown-item"><i data-feather="log-out"></i> Sign Out</a>--}}
{{--                    </div>--}}
{{--                </div><!-- dropdown-menu -->--}}
{{--            </div>--}}
{{--        </div><!-- header-right -->--}}
{{--    </div><!-- header -->--}}
    <div class="content-header">
        <div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Pages</a></li>
                    <li class="breadcrumb-item"><a href="#">User Pages</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Profile Settings</li>
                </ol>
            </nav>
            <h4 class="content-title content-title-sm">Profile Settings</h4>
        </div>
    </div><!-- content-header -->
    <div class="content-body">
        <div class="row row-xs">
            <div class="col-md-4">
                <ul class="list-group list-group-settings">
                    <li class="list-group-item list-group-item-action">
                        <a href="#paneProfile" data-toggle="tab" class="media active">
                            <i data-feather="user"></i>
                            <div class="media-body">
                                <h6>Profile Information</h6>
                                <span>About your personal information</span>
                            </div>
                        </a>
                    </li>
                    <li class="list-group-item list-group-item-action">
                        <a href="#paneAccount" data-toggle="tab" class="media">
                            <i data-feather="settings"></i>
                            <div class="media-body">
                                <h6>Account Settings</h6>
                                <span>Manage your account setting options</span>
                            </div>
                        </a>
                    </li>
                    <li class="list-group-item list-group-item-action">
                        <a href="#paneSecurity" data-toggle="tab" class="media">
                            <i data-feather="shield"></i>
                            <div class="media-body">
                                <h6>Security</h6>
                                <span>Manage your security information</span>
                            </div>
                        </a>
                    </li>
                    <li class="list-group-item list-group-item-action">
                        <a href="#paneNotification" data-toggle="tab" class="media">
                            <i data-feather="bell"></i>
                            <div class="media-body">
                                <h6>Notification</h6>
                                <span>Choose how you receive notifications</span>
                            </div>
                        </a>
                    </li>
                    <li class="list-group-item list-group-item-action">
                        <a href="#paneBilling" data-toggle="tab" class="media">
                            <i data-feather="credit-card"></i>
                            <div class="media-body">
                                <h6>Billing</h6>
                                <span>Your billing and payment information</span>
                            </div>
                        </a>
                    </li>
                </ul>
            </div><!-- col -->
            <div class="col-md-8">
                <div class="card card-body pd-sm-40 pd-md-30 pd-xl-y-35 pd-xl-x-40">
                    <div class="tab-content">
                        <div id="paneProfile" class="tab-pane active show">
                            <!-- <div class="d-flex">
                              <h6 class="tx-uppercase tx-semibold tx-color-01 mg-b-0">Your Profile Information</h6>
                              <div class="btn-group-invoice justify-right">
                                <a href="" class="btn btn-white btn-sm btn-uppercase"><i data-feather="printer"></i> Print</a>
                              </div>
                            </div> -->
                            <div class="row">
                                <div class="col-sm-5">
                                    <h6 class="tx-uppercase tx-semibold tx-color-01 mg-b-0">Your Profile Information</h6>
                                </div><!-- col -->
                                <div class="col-sm-0 tx-right col-lg-7">
                                    <button type="button" class="btn btn-sm btn-secondary mt-n3 mg-b-0">Edit Profile</button>
                                </div><!-- col -->
                            </div><!-- row -->

                            <hr>

                            <label class="content-label content-label-lg mg-b-15 tx-color-01">Biography</label>
                            <p class="tx-color-03">Redhead, Innovator, Saviour of Mankind, Hopeless Romantic, Attractive 20-something Yogurt Enthusiast. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. </p>

                            <hr class="mg-y-25">
                            <div class="form-settings">
                                <label class="content-label content-label-lg mg-b-15 tx-color-01">Contact Detail</label>
                                <ul class="list-unstyled profile-info-list mg-b-0">
                                    <li><i data-feather="briefcase"></i> <span class="tx-color-03">Bay Area, San Francisco, CA</span></li>
                                    <li><i data-feather="home"></i> <span class="tx-color-03">Westfield, Oakland, CA</span></li>
                                    <li><i data-feather="smartphone"></i> <a href="">(+1) 012 345 6789</a></li>
                                    <li><i data-feather="phone"></i> <a href="">(+1) 987 654 3201</a></li>
                                    <li><i data-feather="mail"></i> <a href="">me@fenchiumao.me</a></li>
                                </ul>

                                <hr class="mg-y-25">

                                <label class="content-label content-label-lg mg-b-15 tx-color-01">Other Detail</label>
                                <ul class="list-unstyled profile-info-list">
                                    <li><span class="tx-color-03">Citizenship No: 20022/02/25</span></li>
                                    <li><span class="tx-color-03">Joining Date: 2076/08/28</span></li>
                                    <li><span class="tx-color-03">Date of Birth: 2076/08/28</span></li>
                                    <li><span class="tx-color-03">Salary: NRs. 50,000</span></li>
                                    <li><span class="tx-color-03">Blood Group: B+</span></li>
                                </ul>

                                <hr class="mg-y-15 op-0">

                                <label class="content-label content-label-lg mg-b-15 tx-color-01">Parents Detail</label>
                                <ul class="list-unstyled media-list-profile">
                                    <li class="media">
                                        <div class="wd-40 ht-40 bg-gray-400"></div>
                                        <div class="media-body">
                                            <h6 class="mg-b-5 tx-semibold">Father Name</h6>
                                            <p class="tx-color-03 tx-13">9840680875</p>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="wd-40 ht-40 bg-gray-400"></div>
                                        <div class="media-body">
                                            <h6 class="mg-b-5 tx-semibold">Mother Name</h6>
                                            <p class="tx-color-03 tx-13">9840680875</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div><!-- tab-pane -->
                        <div id="paneAccount" class="tab-pane">
                            <h6 class="tx-uppercase tx-semibold tx-color-01 mg-b-0">Account Settings</h6>

                            <hr>
                            <div class="form-settings">
                                <div class="form-group">
                                    <label class="form-label">Username</label>
                                    <input type="text" class="form-control" placeholder="Enter your username" value="dwayne.johnson">
                                    <div class="tx-11 tx-sans tx-color-04 mg-t-5">After changing your username, your old username becomes available for anyone else to claim.</div>
                                </div><!-- form-group -->

                                <hr>

                                <div class="form-group">
                                    <label class="form-label text-danger">Delete Account</label>
                                    <p class="tx-sm tx-color-04">Once you delete your account, there is no going back. Please be certain.</p>
                                    <button class="btn btn-sm btn-danger">Delete Account</button>
                                </div><!-- form-group -->
                            </div><!-- form-settings -->
                        </div><!-- tab-pane -->
                        <div id="paneSecurity" class="tab-pane">
                            <h6 class="tx-uppercase tx-semibold tx-color-01 mg-b-0">Security Settings</h6>
                            <hr>
                            <div class="form-settings">
                                <div class="form-group">
                                    <label class="form-label">Change Old Password</label>
                                    <input type="text" class="form-control" placeholder="Enter your old password">
                                    <input type="text" class="form-control mg-t-5" placeholder="New password">
                                    <input type="text" class="form-control mg-t-5" placeholder="Confirm new password">
                                </div><!-- form-group -->

                                <hr>

                                <div class="form-group">
                                    <label class="form-label">Two Factor Authentication</label>
                                    <button class="btn btn-brand-02 tx-sm">Enable two-factor authentication</button>
                                    <div class="tx-11 tx-sans tx-color-04 mg-t-7">Two-factor authentication adds an additional layer of security to your account by requiring more than just a password to log in.</div>
                                </div><!-- form-group -->

                                <hr>

                                <div class="form-group">
                                    <label class="form-label">Sessions</label>
                                    <p class="tx-sm tx-color-03">This is a list of devices that have logged into your account. Revoke any sessions that you do not recognize.</p>

                                    <ul class="list-group list-group-session">
                                        <li class="list-group-item">
                                            <div>
                                                <h6>San Francisco City 190.24.335.55</h6>
                                                <span>Your current session seen in United States</span>
                                            </div>
                                            <a href="" class="btn btn-xs btn-white">More Info</a>
                                        </li>
                                    </ul>
                                </div><!-- form-group -->
                            </div><!-- form-settings -->
                        </div><!-- tab-pane -->
                        <div id="paneNotification" class="tab-pane">
                            <h6 class="tx-uppercase tx-semibold tx-color-01 mg-b-0">Notification Settings</h6>
                            <hr>
                            <div class="form-settings mx-wd-100p">
                                <div class="form-group">
                                    <label class="form-label mg-b-2">Security Alerts</label>
                                    <p class="tx-sm tx-color-04">Receive security alert notifications via email</p>

                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Email each time a vulnerability is found</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2">Email a digest summary of vulnerabilities</label>
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label class="form-label">SMS Notifications</label>
                                    <ul class="list-group list-group-notification">
                                        <li class="list-group-item">
                                            <p class="mg-b-0">Comments</p>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch1">
                                                <label class="custom-control-label" for="customSwitch1">&nbsp;</label>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <p class="mg-b-0">Updates From People</p>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch2">
                                                <label class="custom-control-label" for="customSwitch2">&nbsp;</label>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <p class="mg-b-0">Reminders</p>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch3">
                                                <label class="custom-control-label" for="customSwitch3">&nbsp;</label>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <p class="mg-b-0">Events</p>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch4">
                                                <label class="custom-control-label" for="customSwitch4">&nbsp;</label>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <p class="mg-b-0">Pages You Follow</p>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch5">
                                                <label class="custom-control-label" for="customSwitch5">&nbsp;</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div><!-- form-group -->
                            </div><!-- form-setting -->
                        </div><!-- tab-pane -->
                        <div id="paneBilling" class="tab-pane">
                            <h6 class="tx-uppercase tx-semibold tx-color-01 mg-b-0">Salary Detail</h6>
                            <hr>
                            <table id="example1" class="table table-sm">
                                <thead>
                                <tr>
                                    <th class="wd-5p"><input type="checkbox" aria-label="Checkbox for following text input"></th>
                                    <th class="wd-10p">Date</th>
                                    <th class="wd-10p">Payment Id</th>
                                    <th class="wd-10p">Paid Amount</th>
                                    <th class="wd-10p">Due Amount</th>
                                    <th class="wd-15p">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                                    <td>2076/08/28</td>
                                    <td>000001</td>
                                    <td>9999998</td>
                                    <td>9999998</td>
                                    <td><a href="#">View</a></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                                    <td>2076/08/28</td>
                                    <td>000001</td>
                                    <td>9999998</td>
                                    <td>9999998</td>
                                    <td><a href="#">View</a></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                                    <td>2076/08/28</td>
                                    <td>000001</td>
                                    <td>9999998</td>
                                    <td>9999998</td>
                                    <td><a href="#">View</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div><!-- form-settings -->
                    </div><!-- tab-pane -->
                </div><!-- tab-content -->
            </div><!-- card -->
        </div><!-- col -->
    </div><!-- row -->
</div><!-- content-body -->
</div><!-- content -->

{{--<script src="../lib/jquery/jquery.min.js"></script>--}}
{{--<script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>--}}
{{--<script src="../lib/feather-icons/feather.min.js"></script>--}}
{{--<script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>--}}
{{--<script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>--}}
{{--<script src="../lib/js-cookie/js.cookie.js"></script>--}}

{{--<script src="../assets/js/cassie.js"></script>--}}
{{--<script>--}}
{{--    $(function(){--}}

{{--        'use strict'--}}

{{--        $('#example1').DataTable({--}}
{{--            language: {--}}
{{--                searchPlaceholder: 'Search...',--}}
{{--                sSearch: '',--}}
{{--                lengthMenu: '_MENU_ items/page',--}}
{{--            }--}}
{{--        });--}}

{{--    })--}}
{{--</script>--}}
{{--</body>--}}
{{--</html>--}}
@endsection
