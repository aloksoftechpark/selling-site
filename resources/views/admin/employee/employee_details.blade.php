@extends('admin.layouts.app')
@section('content')


    <div class="content-body content-body-profile">
        <div class="profile-sidebar">
            <h3>Employee Detail</h3>
            <hr>
            <div class="profile-sidebar-header">
                <div class="avatar">
{{--                    <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">--}}
                    @if(isset($employee->images))
                        @if(!$employee->images->isEmpty())
                            @foreach($employee->images as $key=>$image)
                                @if($key == 0)
                                    <img src="{{asset(''.$image->image)}}" height="70px" width="70px" class="rounded-circle" alt="">

                @endif
                @endforeach
                @else
                    <img src="" height="70px" width="70px" class="rounded-circle" alt=""></div>
            @endif
            @endif
                </div>

                <h5>{{$employee->fullName}}</h5>
                <p>{{$employee->designation}}(EmployeeId:-{{$employee->employeeId}})</p>
{{--                <span>{{$employee->branch->name}}</span>--}}

                <div class="d-flex align-self-stretch mg-t-30">
                    <a href="mailto:{{$employee->mailId}} " class="btn btn-brand-01 btn-sm btn-uppercase flex-fill">Email</a>
{{--                    <a href="mailto:{{$employee->mailId}}" class="btn btn-brand-01 btn-sm btn-uppercase flex-fill">Email</a>--}}
                    <a href=" " class="btn btn-white btn-sm btn-uppercase flex-fill mg-l-5">Edit Profile</a>
                </div>
            </div><!-- profile-sidebar-header -->
            <div class="profile-sidebar-body">

                <label class="content-label">Contact Information</label>
                <ul class="list-unstyled profile-info-list mg-b-0">
                    <li><i data-feather="briefcase"></i> <span class="tx-color-03">{{$employee->presentAddress}}</span></li>
                    <li><i data-feather="home"></i> <span class="tx-color-03">{{$employee->permanentAddress}}</span></li>
{{--                    <li><i data-feather="smartphone"></i> <a href="">(+1) 012 345 6789</a></li>--}}
                    <li><i data-feather="phone"></i> <a href="">{{$employee->phoneNumber}}</a></li>
                    <li><i data-feather="mail"></i> <a href="">{{$employee->mailId}}</a></li>
                </ul>

                <hr class="mg-y-25">

                <label class="content-label">Websites &amp; Other Detail</label>
                <ul class="list-unstyled profile-info-list">
                    <li><span class="tx-color-03">Citizenship No: {{$employee->citizenshipNumber}}</span></li>
                    <li><span class="tx-color-03">Joining Date: {{$employee->joiningDate}}</span></li>
                    <li><span class="tx-color-03">Date of Birth: {{$employee->dob}}</span></li>
                    <li><span class="tx-color-03">Salary: NRs.{{$employee->salary}}</span></li>
                    <li><span class="tx-color-03">Blood Group: {{$employee->bloodGroup}}</span></li>
                </ul>

                <hr class="mg-y-25">

            </div><!-- profile-sidebar-body -->
        </div><!-- profile-sidebar -->
        <div class="profile-body">
            <div class="profile-body-header">
                <div class="nav-wrapper">
                    <ul class="nav nav-line" id="profileTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="projects-tab" data-toggle="tab" href="#bills" role="tab" aria-controls="projects" aria-selected="false">Attandence</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="people-tab" data-toggle="tab" href="#receipts" role="tab" aria-controls="people" aria-selected="false">Payment History</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="people-tab" data-toggle="tab" href="#ledger" role="tab" aria-controls="people" aria-selected="false">Ledger</a>
                        </li>
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" id="permission-tab" data-toggle="tab" href="#permission" role="tab" aria-controls="people" aria-selected="false">Permission</a>--}}
{{--                        </li>--}}
                    </ul>
                </div><!-- nav-wrapper -->
            </div><!-- profile-body-header -->
            <div class="tab-content pd-15 pd-sm-20">
                <div id="overview" class="tab-pane active show">

                    <div class="stat-profile">
                        <div class="stat-profile-body">
                            <div class="row row-xs">
                                <div class="col">
                                    <div class="card card-body pd-10 pd-md-15 bd-0 shadow-none bg-primary-light">
                                        <h1 class="tx-light tx-sans tx-spacing--4 tx-primary mg-b-5">50,000</h1>
                                        <p class="tx-13 tx-lg-14 tx-color-02 mg-b-0">Total Payable Amount</p>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="card card-body pd-10 pd-md-15 bd-0 shadow-none bg-teal-light">
                                        <h1 class="mg-b-5 tx-sans tx-spacing--2 tx-light tx-teal">35 Month</h1>
                                        <p class="tx-13 tx-lg-14 tx-color-03 mg-b-0">Total Working Period</p>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="card card-body pd-10 pd-md-15 bd-0 shadow-none bg-pink-light">
                                        <h1 class="mg-b-5 tx-sans tx-spacing--2 tx-light tx-pink">1,00,500</h1>
                                        <p class="tx-13 tx-lg-14 tx-color-03 mg-b-0">Total Salary Paid</p>
                                    </div>
                                </div>
                            </div><!-- row -->
                        </div><!-- stat-profile-body -->
                    </div><!-- stat-profile -->

                    <hr class="mg-y-15 op-0">

                    <label class="content-label content-label-lg mg-b-15 tx-color-01">Biography</label>
                    <p class="tx-color-03">{{$employee->description}} </p>

                    <hr class="mg-y-15 op-0">

                    <label class="content-label content-label-lg mg-b-15 tx-color-01">Latest Transactions</label>
                    <ul class="list-unstyled media-list-profile">
                        <li class="media">
                            <div class="wd-40 ht-40 bg-teal op-5"></div>
                            <div class="media-body">
                                <h6 class="mg-b-5 tx-semibold">Sell for NRs. 20,000 and Paid NRs. 10,000  with Bill No. 0005</h6>
                                <p class="tx-color-03 tx-13">January 2016 - Present</p>
                            </div>
                        </li>
                        <li class="media">
                            <div class="wd-40 ht-40 bg-primary op-5"></div>
                            <div class="media-body">
                                <h6 class="mg-b-5 tx-semibold">Paid NRs. 10,000 with Receipt No. 006</h6>
                                <p class="tx-color-03 tx-13">December 2012 - November 2015</p>
                            </div>
                        </li>
                        <li class="media">
                            <div class="wd-40 ht-40 bg-pink op-5"></div>
                            <div class="media-body">
                                <h6 class="mg-b-5 tx-semibold">Sell for NRs. 20,000 and Paid NRs. 10,000  with Bill No. 0005</h6>
                                <p class="tx-color-03 tx-13">January 2016 - Present</p>
                            </div>
                        </li>
                    </ul><!-- media-list-profile -->

                    <label class="content-label content-label-lg mg-b-15 tx-color-01">Parents Detail</label>
                    <ul class="list-unstyled media-list-profile">
                        <li class="media">
                            <div class="wd-40 ht-40 bg-gray-400"></div>
                            <div class="media-body">
                                <h6 class="mg-b-5 tx-semibold">Father Name:-{{$employee->fatherName}}</h6>
                                <p class="tx-color-03 tx-13">{{$employee->fatherPhoneNumber}}</p>
                            </div>
                        </li>
                        <li class="media">
                            <div class="wd-40 ht-40 bg-gray-400"></div>
                            <div class="media-body">
                                <h6 class="mg-b-5 tx-semibold">Mother Name:-{{$employee->motherName}}</h6>
                                <p class="tx-color-03 tx-13">{{$employee->motherPhoneNumber}}</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="bills" role="tabpanel" aria-labelledby="contact-tab5">
                    <label class="content-label content-label-lg mg-b-0 tx-color-01"><h5>Attandence list</h5></label>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="wd-15p">Month</th>
                                <th class="wd-10">Month Days</th>
                                <th class="wd-10">Absent Days</th>
                                <th class="wd-10">late Days</th>
                                <th class="wd-10">Holi Days</th>
                                <th class="wd-10">Half Days</th>
                                <th class="wd-10">Present Days</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>2076-Jestha</td>
                                <td>31</td>
                                <td>5</td>
                                <td>2</td>
                                <td>5</td>
                                <td>2</td>
                                <td>22</td>
                            </tr>
                            <tr>
                            <tr>
                                <td>2076-Bishak</td>
                                <td>31</td>
                                <td>5</td>
                                <td>2</td>
                                <td>5</td>
                                <td>2</td>
                                <td>22</td>
                            <tr>
                            </tbody>
                        </table>
                    </div><!-- table-responsive -->
                </div><!---individual content-->
                <div class="tab-pane fade" id="receipts" role="tabpanel" aria-labelledby="contact-tab5">
                    <label class="content-label content-label-lg mg-b-0 tx-color-01"><h5>All Receipts</h5></label>
                    <div class="component-section" style="margin-top:0;">
                        <table  class="table">
                            <thead>
                            <tr>
                                <th class="wd-25p">Date</th>
                                <th class="wd-25p">Payment Id.</th>
                                <th class="wd-25p">Description</th>
                                <th class="wd-25p">Amount</th>
                                <th class="wd-25p">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($salary_payroll as $key=> $salary_payrolls)

                                @if($salary_payrolls->payingAmount)
                            <tr>
                                <td>{{$salary_payrolls->created_date}}</td>
                                <td>{{$salary_payrolls->id}}</td>
                                <td>
                                    Salary payroll
                                </td>
                                <td>
                                    @if($salary_payrolls->payingAmount){{$salary_payrolls->payingAmount}}@else - @endif
                                </td>
                                <td><a href="#">View</a></td>
                            </tr>
                            @endif

                                @if($salary_payrolls->advanceAmount)
                                    <tr>
                                        <td>{{$salary_payrolls->created_date}}</td>
                                        <td>{{$salary_payrolls->id}}</td>
                                        <td>
                                            Advance
                                        </td>

                                        <td>
                                            @if($salary_payrolls->advanceAmount){{$salary_payrolls->advanceAmount}}@else -@endif
                                        </td>
                                        <td><a href="#">View</a></td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div><!-- component-section -->
                </div><!--individual-content-->
                <div class="tab-pane fade" id="ledger" role="tabpanel" aria-labelledby="contact-tab5">
                    <div class="mail-navbar">
                        <label class="content-label content-label-lg mg-b-0 tx-color-01"><h5>Account Statement</h5></label>
                        <div class="d-none d-lg-flex mg-r-20">
                            <a href=""><i data-feather="rotate-cw"></i></a>
                            <a href=""><i data-feather="alert-circle"></i></a>
                            <a href=""><i data-feather="printer"></i></a>
                            <a href=""><i data-feather="trash"></i></a>
                            <a href=""><i data-feather="folder"></i></a>
                            <a href=""><i data-feather="star"></i></a>
                        </div>
                    </div>
                    <div class="row mg-b-10 mg-t-10">
                        <div class="col-3">
                            <input type="text" id="dateFrom" class="form-control form-control-sm" placeholder="From">
                        </div><!-- col -->
                        <div class="col-3">
                            <input type="text" id="dateTo" class="form-control form-control-sm" placeholder="To">
                        </div><!-- col -->
                    </div><!-- row -->
                    <div class="table-responsive">
                        <table class="table table-bordered mg-b-0">
                            <thead>
                            <tr>
                                <th class="wd-15p">Date</th>
                                <th class="wd-35p">Description</th>
                                <th class="wd-15p">Dr. Amount</th>
                                <th class="wd-15p">Cr. Amount</th>
                                <th class="wd-20p">Balance</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">{{$employee->joiningDate}}</th>
                                <td> Basic Salary</td>
                                <td>@if($employee->salaryDue1 > 0){{$employee->salaryDue1}} @else - @endif</td>
                                <td>@if($employee->salaryDue1 < 0){{$employee->salaryDue1 * -1}} @else - @endif</td>
                                <td>@if($employee->salaryDue1 > 0){{$employee->salaryDue1}} Cr. @else {{$employee->salaryDue1 * -1}} Dr. @endif</td>
                            </tr>
{{--                            @if(isset($ledger_employee->salary_payroll))--}}

                            @foreach($ledger_employee as $key=>$ledger_employees)
                                @if(($ledger_employees->salary_payroll_id))




                                    @if(isset($ledger_employees->salary_payroll->advanceAmount))

                                        <tr>
                                            <th scope="row"> {{$ledger_employees->salary_payroll->created_date}}</th>
{{--                                            <td>Advance </td>--}}
                                            <td>Advance @if(isset($ledger_employees->salary_payroll->employee->salary_master[0]->month))
                                                    {{$ledger_employees->salary_payroll->employee->salary_master[0]->month}} @endif</td>
                                            <td>-</td>
                                            <td>
                                                {{$ledger_employees->salary_payroll->advanceAmount}}
                                            </td>
                                            <td>@if($ledger_employees->balance >0)
                                                    {{$ledger_employees->balance}} Cr.
                                                @else {{$ledger_employees->balance * -1}} Dr.
                                                @endif

                                            </td>

                                        </tr>
                                    @endif

                                @if(isset($ledger_employees->salary_payroll->payingAmount))

                                    <tr>
                                <th scope="row"> {{$ledger_employees->salary_payroll->created_date}}</th>
{{--                                <td>salary payment of </td>--}}
                                <td>salary payment of @if(isset($ledger_employees->salary_payroll->employee->salary_master[0]->month))
                                        {{$ledger_employees->salary_payroll->employee->salary_master[0]->month}} @endif</td>
                                <td>-</td>
                                 <td>
                                    {{$ledger_employees->salary_payroll->payingAmount}}
                                </td>
                                <td>@if($ledger_employees->balance >0)
                                        {{$ledger_employees->balance}} Cr.
                                    @else {{$ledger_employees->balance * -1}} Dr.
                                    @endif

                                </td>
                                    </tr>

                                @endif

                            @endif
                            @endforeach
{{--                                @endif--}}
                            </tbody>
                        </table>
                    </div><!-- table-responsive -->
                </div><!--individual-content-->
                <div id="permission" class="tab-pane active show">

                </div>
            </div><!-- tab-content -->
        </div><!-- profile-body -->
    </div><!-- content-body -->
</div><!-- content -->

{{--<script src="../lib/jquery/jquery.min.js"></script>--}}
{{--<script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>--}}
{{--<script src="../lib/feather-icons/feather.min.js"></script>--}}
{{--<script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>--}}
{{--<script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>--}}
{{--<script src="../lib/js-cookie/js.cookie.js"></script>--}}

{{--<script src="../assets/js/cassie.js"></script>--}}
{{--<script>--}}
{{--    $(function(){--}}

{{--        'use strict'--}}

{{--        $('#profileMenu').on('click', function(e) {--}}
{{--            e.preventDefault();--}}

{{--            $('body').addClass('profile-menu-show');--}}
{{--            $('#mainMenu').removeClass('d-none');--}}
{{--            $(this).addClass('d-none');--}}
{{--        })--}}

{{--        $('#example1').DataTable({--}}
{{--            language: {--}}
{{--                searchPlaceholder: 'Search...',--}}
{{--                sSearch: '',--}}
{{--                lengthMenu: '_MENU_ items/page',--}}
{{--            }--}}
{{--        });--}}

{{--        $('#example2').DataTable({--}}
{{--            language: {--}}
{{--                searchPlaceholder: 'Search...',--}}
{{--                sSearch: '',--}}
{{--                lengthMenu: '_MENU_ items/page',--}}
{{--            }--}}
{{--        });--}}

{{--    })--}}
{{--</script>--}}
{{--</body>--}}
{{--</html>--}}
@endsection
