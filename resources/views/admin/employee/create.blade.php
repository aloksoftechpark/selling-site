@extends('admin.layouts.app')
@section('content')

    <div class="content-header justify-content-between">
        <div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Employee</a></li>
                    <li class="breadcrumb-item active" aria-current="page">New Employee</li>
                </ol>
            </nav>
            <h4 class="content-title content-title-xs">Add New Employee</h4>
        </div>
    </div><!-- content-header -->
    <div class="content-body content-body-calendar" style="margin-top: -50px;">
        <div class="col-md-8 mg t-30 mg-b-20">
            <div class="card card-body pd-sm-40 pd-md-30 pd-xl-y-35 pd-xl-x-40">
                <div class="tab-content">
                    <div id="paneProfile" class="tab-pane active show">
                        <form action="{{route('employee.store')}}" method="post" enctype="multipart/form-data">
                            @csrf

                        <div class="form-settings">
                            <div class="form-group">
                                <label class="form-label">Employee Image</label>
                                <div>
                                    <input type='file' id="image" name="image" onchange="readURL(this);" multiple=" "/>
                                    <img id="blah" src="http://placehold.it/180" alt="your image"  />
{{--                                    <img src="https://via.placeholder.com/640x426/637382/fff" class="img-thumbnail wd-150 ht-150" alt="Responsive image">--}}
                                </div>
                            </div><!-- component-section -->
                            <div class="form-group">
                                <label class="form-label">Employee ID</label>
                                <input type="text" name="employeeId" class="form-control form-control-sm{{ $errors->has('employeeId') ? 'has-error' : '' }}"  value="{{isset($employee) ? $employee->employeeId+1 : 1 }}" placeholder="Employee Id" >
                            </div><!-- form-group -->
                            <div class="form-group">
{{--                                <label class="form-label">Employee Name <span class="tx-danger">*</span></label>--}}
{{--                                <div class="d-flex">--}}
{{--                                    <select class="form-control form-control-sm select2-no-search wd-15p">--}}
{{--                                        <option label="Mr"></option>--}}
{{--                                        <option value="Chrome">Mrs</option>--}}
{{--                                        <option value="Opera">Other</option>--}}
{{--                                    </select>--}}
{{--                                    <input type="text" class="form-control form-control-sm" aria-label="Text input with dropdown button">--}}
{{--                                </div>--}}
                                <label class="form-label">Employee Name <span class="tx-danger">*</span></label>
{{--                                <input type="text" name="firstName" id="firstName" class="form-control form-control-sm {{ $errors->has('firstName') ? 'has-error' : '' }}" placeholder="Enter Employee first Name" required>--}}
{{--                                --}}{{--                                <label class="form-label">Customer Name <span class="tx-danger">*</span></label>--}}{{--<br>--}}
{{--                                <input type="text" name="MiddleName" id="MiddleName" class="form-control form-control-sm {{ $errors->has('MiddleName') ? 'has-error' : '' }}" placeholder="Enter Employee Middle Name" >--}}
{{--                                <br>--}}
{{--                                <input type="text" name="lastName" id="lastName" class="form-control form-control-sm {{ $errors->has('lastName') ? 'has-error' : '' }}" placeholder="Enter Employee Last Name" required>--}}
{{--                                @if ($errors->has('firstName'))--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $errors->first('firstName') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
                                <div class="form-group d-md-flex">
                                    <div class="mg-b-0">
                                        <label>First Name</label>
                                        <input type="text" name="firstName" class="form-control form-control-sm wd-md-200" placeholder="Enter first Name" required>
                                    </div><!-- form-group -->
                                    <div class="mg-b-0 mg-md-l-20 mg-t-20 mg-md-t-0">
                                        <label>Middle Name </label>
                                        <input type="text" name="middleName" class="form-control form-control-sm wd-md-200" placeholder="Enter Middle Name " >
                                    </div><!-- form-group -->
                                    <div class="mg-b-0 mg-md-l-20 mg-t-20 mg-md-t-0">
                                        <label>Last Name </label>
                                        <input type="text" name="lastName" class="form-control form-control-sm wd-md-200" placeholder="Enter Last Name " required>
                                    </div><!-- form-group -->
                                </div><!-- d-flex -->
                            </div><!-- form-group -->
                            <div class="form-group">
                                <label class="form-label">Temporary Address</label>
                                <input type="text" name="presentAddress" class="form-control form-control-sm" placeholder="Employee Temporary Address" value="">
                            </div><!-- form-group -->
                            <div class="form-group">
                                <label class="form-label">Parmanent Address</label>
                                <input type="text" name="permanentAddress" class="form-control form-control-sm" placeholder="Employee Parmanent Address" value="">
                            </div><!-- form-group -->
                            <div class="form-group">
                                <label class="form-label">Phone No.</label>
                                <input type="text" name="phoneNumber" class="form-control form-control-sm" placeholder="Enter Employee Phone No." value="">
                            </div><!-- form-group -->
                            <div class="form-group">
                                <label class="form-label">Employee Email</label>
                                <input type="text" name="email" class="form-control form-control-sm" placeholder="Enter Employee Email" value="">
                            </div><!-- form-group -->
                            <div class="form-group">
                                <label class="form-label">Citizenship No.</label>
                                <input type="text" name="citizenshipNumber" class="form-control form-control-sm" placeholder="Enter Employee Citizenship No." value="">
                            </div><!-- form-group -->
                            <div class="form-group d-md-flex">
                                <div class="mg-b-0">
                                    <label>Date of Birth</label>
                                    <br>
                                    <input type="text" name="dob" class="bod-picker" placeholder="YYYY/MM/DD" required>
                                </div><!-- form-group -->
                                <div class="mg-b-0 mg-md-l-20 mg-t-20 mg-md-t-0">
                                    <label>Blood Group</label>
                                    <!-- <label class="form-label">Employee Name <span class="tx-danger">*</span></label> -->
                                    <div class="d-flex">
                                        <select name="bloodGroup"  class="form-control form-control-sm select2-no-search wd-md-200">
                                            <option label="Unknown"></option>
                                            <option  value="B+">B+</option>
                                            <option value="A+">A+</option>
                                            <option  value="A-">A-</option>
                                            <option value="AB+">AB+</option>
                                            <option  value="Ab-">Ab-</option>
                                            <option value="O+">O+</option>
                                        </select>
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- d-flex -->
                            <div class="form-group d-md-flex">
                                <div class="mg-b-0">
                                    <label>Father Name</label>
                                    <input type="text" name="fatherName" class="form-control form-control-sm wd-md-200" placeholder="Enter Father Name" required>
                                </div><!-- form-group -->
                                <div class="mg-b-0 mg-md-l-20 mg-t-20 mg-md-t-0">
                                    <label>Father Mobile No. </label>
                                    <input type="text" name="fatherPhoneNumber" class="form-control form-control-sm wd-md-200" placeholder="Enter Mobile No. " >
                                </div><!-- form-group -->
                            </div><!-- d-flex -->
                            <div class="form-group d-md-flex">
                                <div class="mg-b-0">
                                    <label>Mother Name</label>
                                    <input type="text" name="motherName" class="form-control form-control-sm wd-md-200" placeholder="Enter Mother Name" required>
                                </div><!-- form-group -->
                                <div class="mg-b-0 mg-md-l-20 mg-t-20 mg-md-t-0">
                                    <label>Mother Mobile No. </label>
                                    <input type="text" name="motherPhoneNumber" class="form-control form-control-sm wd-md-200" placeholder="Enter Mobile No. " required>
                                </div><!-- form-group -->
                            </div><!-- d-flex -->
                            <div class="form-group d-md-flex">
                                <div class="mg-b-0">
                                    <label>Designation</label>
                                    <input type="text" name="designation" class="form-control form-control-sm wd-md-200" placeholder="Job Title" required>
                                </div><!-- form-group -->
                                <div class="mg-b-0 mg-md-l-20 mg-t-20 mg-md-t-0">
                                    <label>Joining Branch</label>
                                    <!-- <label class="form-label">Employee Name <span class="tx-danger">*</span></label> -->
                                    <div class="d-flex">
                                        <select class="form-control form-control-sm select2-no-search wd-md-200" name="branch_id">
                                            <option value=" ">Select branch</option>
                                            @forelse($branch as $branch)
                                                <option value="{{$branch->id}}">{{$branch->name}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- d-flex -->
                            <div class="form-group d-md-flex">
                                <div class="mg-b-0">
                                    <label>Joining Date</label>
                                    <br>
                                    <input type="text" name="joiningDate" class="bod-picker" placeholder="YYYY/MM/DD" required>
                                </div><!-- form-group -->
                                <div class="mg-b-0 mg-md-l-20 mg-t-20 mg-md-t-0">
                                    <label>Salary </label>
                                    <div class="input-group mg-b-10">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text form-control-sm" id="basic-addon1">NPR</span>
                                        </div>
                                        <input type="text" name="salary" class="form-control form-control-sm" placeholder="Salary" value="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- d-flex -->

                            <div class="form-group">
                                <label class="form-label">Document</label>
                                <input type="file" name="document" >
{{--                                <div class="custom-file">--}}
{{--                                    <input type="file" name="document" class="custom-file-input form-control-sm" id="customFile">--}}
{{--                                    <label class="custom-file-label form-control-sm" for="customFile">Choose file</label>--}}
{{--                                </div>--}}
                            </div>
                            <div class="form-group">
                                <label class="form-label">Description</label>
                                <textarea name="description" class="form-control form-control-sm" rows="3" placeholder="Write something about Employee"></textarea>
                            </div><!-- form-group -->


                            <div class="form-group tx-13 tx-color-04">
                                Name is Compulsary and All other fields on this page are optional. All fileds can be edited at any time, and by filling them out.
                            </div>

                            <hr class="op-0">

                            <button class="btn btn-brand-02">Update Profile</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- content-body -->
{{--        <div class="content-footer">--}}
{{--            &copy; 2019. All Rights Reserved. Created by <a href="http://softechpark.com" target="_blank">Softechpark</a>--}}
{{--        </div><!-- content-footer -->--}}
    </div><!-- content -->



{{--    <script src="../lib/jquery/jquery.min.js"></script>--}}
{{--    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>--}}
{{--    <script src="../lib/feather-icons/feather.min.js"></script>--}}
{{--    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>--}}
{{--    <script src="../lib/prismjs/prism.js"></script>--}}
{{--    <script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>--}}
{{--    <script src="../lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>--}}
{{--    <script src="../lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>--}}
{{--    <script src="../lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>--}}
{{--    <script src="../lib/select2/js/select2.min.js"></script>--}}
{{--    <script src="../lib/js-cookie/js.cookie.js"></script>--}}

{{--    <script src="../assets/js/cassie.js"></script>--}}
{{--    <script>--}}
{{--        $(function(){--}}
{{--            'use strict'--}}

{{--            $('#example1').DataTable({--}}
{{--                language: {--}}
{{--                    searchPlaceholder: 'Search...',--}}
{{--                    sSearch: '',--}}
{{--                    lengthMenu: '_MENU_ items/page',--}}
{{--                }--}}
{{--            });--}}

{{--            $('#example2').DataTable({--}}
{{--                responsive: true,--}}
{{--                language: {--}}
{{--                    searchPlaceholder: 'Search...',--}}
{{--                    sSearch: '',--}}
{{--                    lengthMenu: '_MENU_ items/page',--}}
{{--                }--}}
{{--            });--}}

{{--            $('#example8').DataTable({--}}
{{--                responsive: true,--}}
{{--                language: {--}}
{{--                    searchPlaceholder: 'Search...',--}}
{{--                    sSearch: '',--}}
{{--                    lengthMenu: '_MENU_ items/page',--}}
{{--                }--}}
{{--            });--}}



{{--            // Select2--}}
{{--            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });--}}

{{--        });--}}
{{--    </script>--}}
{{--</body>--}}
{{--</html>--}}
@endsection
