@extends('admin.layouts.app')
@section('content')

    <div class="d-md-flex">
        <div class="col-sm-5">
            <div class="content-header pd-l-0">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Human Resource</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Employee</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">All Employee</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            @if(Auth::guard('admin')->check())
                 <button type="button" class="btn btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">
                New Employee
            </button>
            @else
            @can('employee-create')
                 <button type="button" class="btn btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">
                New Employee
            </button>
            @endcan
            @endif


            <!-- Modal -->
            <div class="modal fade modal_cust employee_tab" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                    <form action="{{route('employee.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">NEW EMPLOYEE</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">PERSONAL INFO</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">PARENTAL INFO</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">COMPANY INFO</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="upload_type">
                                                <img id="blah" src="http://placehold.it/180" alt="your image">

                                            </div><!-- upload_type -->
                                        </div><!-- col -->
                                        <div class="col-md-9">
                                            <div class="mg-t-5">
                                                Employee ID: <input type="text" name="employeeId" class="form-control {{ $errors->has('employeeId') ? 'has-error' : '' }}"  value="{{isset($employee1) ? $employee1->employeeId+1 : 1 }}" placeholder="Employee Id" readonly>
                                            </div>
                                            <div class="mg-t-5">


                                                Employee Name: <input type="text" name="fullName" class="form-control " placeholder="Enter  Name " required>
                                            </div>


                                        </div><!-- col -->
                                        <div class="col-md-12">
                                            <div class="mg-t-5 if_function_input">
                                                <input type="file" id="image" name="image" onchange="readURL(this);">
                                            </div>
                                        </div><!-- col -->
                                    </div><!-- row -->
                                    <div class="row">
                                        <div class="col-md-6 mg-t-5">

                                            Temporary Address:  <input type="text" name="presentAddress" class="form-control " placeholder="Employee Temporary Address" value="">

                                        </div><!-- col -->
                                        <div class="col-md-6 mg-t-5">

                                            Permanent Address:<input type="text" name="permanentAddress" class="form-control " placeholder="Employee Parmanent Address" value="">

                                        </div><!-- col -->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mg-t-5">

                                            PAN / VAT Number: <input type="text" name="pan_number" value="" placeholder="20383">

                                        </div><!-- col -->
                                        <div class="col-md-6 mg-t-5">

                                            Phone Number:    <input type="text" name="phoneNumber" class="form-control" placeholder="Enter Employee Phone No." value="">

                                        </div><!-- col -->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mg-t-5">
                                            Email ID:  <input type="text" name="email" class="form-control" placeholder="Enter Employee Email" value="">

                                        </div><!-- col -->
                                        <div class="col-md-6 mg-t-5">


                                            Blood Group:
                                            <select name="bloodGroup"  class="form-control select2-no-search ">
                                                <option label="Unknown"></option>
                                                <option  value="B+">B+</option>
                                                <option value="A+">A+</option>
                                                <option  value="A-">A-</option>
                                                <option value="AB+">AB+</option>
                                                <option  value="Ab-">Ab-</option>
                                                <option value="O+">O+</option>
                                            </select>
                                        </div><!-- col -->

                                        <!-- col -->
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 mg-t-5">

                                            Citizenship Number:      <input type="text" name="citizenshipNumber" class="citizenshipNumber form-control" placeholder="citizenship number" required>

                                        </div><!-- col -->



                                        <div class="col-md-6 mg-t-5">

                                            Date of Birth:  <input type="text" name="dob" class="bod-picker dob" placeholder="YYYY/MM/DD" required>

                                        </div><!-- col -->



                                        <!-- col -->
                                    </div>

                                    <div class="input-group group-end">
                                        <div class="tab_nxt_prv">
                                            <a class="btn btn-success btnNext">Next</a>
                                        </div><!-- tab_nxt_prv -->
                                    </div>


                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="row">
                                        <div class="col-md-6 mg-t-5">

                                            Father's Name:   <input type="text" name="fatherName" class="form-control " placeholder="Enter Father Name" required>

                                        </div><!-- col -->
                                        <div class="col-md-6 mg-t-5">

                                            Father's Number:<input type="text" name="fatherPhoneNumber" class="form-control " placeholder="Enter Mobile No. " >

                                        </div><!-- col -->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mg-t-5">

                                            Mother's Namer:           <input type="text" name="motherName" class="form-control " placeholder="Enter Mother Name" required>

                                        </div><!-- col -->
                                        <div class="col-md-6 mg-t-5">

                                            Mother's Number:  <input type="text" name="motherPhoneNumber" class="form-control" placeholder="Enter Mobile No. " required>

                                        </div><!-- col -->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mg-t-5">

                                            Parent's Address: <input type="text" name="FirstName" value="" placeholder="Kathmandu">

                                        </div>
                                    </div>
                                    <div class="input-group group-end">
                                        <div class="tab_nxt_prv mg-t-26p">
                                            <a class="btn btn-primary btnPrevious">Prev</a>
                                            <a class="btn btn-success btnNext">Next</a>
                                        </div><!-- tab_nxt_prv -->
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <div class="row">
                                        <div class="col-md-6 mg-t-5">

                                            Designation:      <input type="text" name="designation" class="form-control " placeholder="Job Title" required>

                                        </div><!-- col -->
                                        <div class="col-md-6 mg-t-5">


                                            Joining Branch:
                                            <select class="form-control  select2-no-search " name="branch_id">
                                                <option value=" ">Select branch</option>
                                                @forelse($branch as $branches)
                                                    <option value="{{$branches->id}}">{{$branches->name}}</option>
                                                @empty
                                                @endforelse
                                            </select>

                                        </div><!-- col -->

                                        <!-- col -->
                                    </div>



                                    <div class="row">
                                        <div class="col-md-6 mg-t-5">

                                            Joining Date:  <input type="text" name="joiningDate" class="bod-picker" placeholder="YYYY/MM/DD" required>

                                        </div><!-- col -->
                                        <div class="col-md-6 mg-t-5 n">

                                            Salary: <div class="gropup-flex mg-b-0">
                                                <div class="input-group-prepend">
                                                    <button class="btn btn-outline-light btn_small" type="button" id="button-addon1">NPR</button>
                                                </div>
                                                <input type="text" name="salary" class="form-control " placeholder="Salary" value="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                            </div>

                                        </div><!-- col -->

                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 mg-t-5">
                                    <span class="mg-t-5 small">Document:</span>

                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="document" id="inputGroupFile01"
                                               aria-describedby="inputGroupFileAddon01">
                                        <label class="custom-file-label" for="inputGroupFile01">Upload bill image/pdf</label>
                                    </div>
                                        </div>

                                        <div class="col-md-6 mg-t-5 n">

                                            Opening Due: <div class="gropup-flex mg-b-0">
                                                <div class="input-group-prepend">
                                                    <button class="btn btn-outline-light btn_small" type="button" id="button-addon1">NPR</button>
                                                </div>
                                                <input type="text" name="opening_due" class="form-control " placeholder="Opening due" value="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                            </div>

                                        </div><!-- col -->

                                    </div>

                                    <div class="col-md-12 mg-t-5 textarea_custom no-pad">

                                        Description
                                        <textarea name="description" class="form-control " rows="3" placeholder="Write something about Employee"></textarea>


                                    </div>
                                    <div class="input-group group-end">
                                        <div class="tab_nxt_prv">
                                            <a class="btn btn-primary btnPrevious">Prev</a>
                                            <button class="btn btn-success btnSubmit">Submit</button>
{{--                                            <a class="btn btn-success btnSubmit">Submit</a>--}}
                                        </div><!-- tab_nxt_prv -->
                                    </div>
                                </div>
                            </div>




                        </div>

                    </div>
                    </form>
                </div>

            </div>






        </div><!-- col -->
    </div><!-- row -->


    <div class="content-body" style="margin-top: -30px;">
        <div class="component-section">

            <div class="d-lg-flex pd-l-0">
                <div class="pd-lg-r-20">
                    <select class="form-control form-control-sm select2-no-search">
                        <option label="All Branch"></option>
                        <option value="Firefox">Kathmandu</option>
                        <option value="Chrome">Pokhhara</option>
                        <option value="Opera">Butwal</option>
                    </select>
                </div>
                <div class="pd-lg-r-20">
                    <select class="form-control form-control-sm select2-no-search">
                        <option label="Designation"></option>
                        <option value="Firefox">Manager</option>
                        <option value="Chrome">Seller</option>
                        <option value="Opera">Designer</option>
                    </select>
                </div>
                <div class="pd-l-0 wd-20p">
                    <input type="search" class="form-control form-control-sm" placeholder="Search Employee">
                </div>
            </div>
        </div> <!--form-group-->
            <table id="example1" class="table  table table-sm table-bordered mg-b-0">
                <thead>
                <tr>
{{--                    <th class="wd-5p"><input type="checkbox" aria-label="Checkbox for following text input"></th>--}}
                    <th class="wd-10p">Employee Id</th>
                    <th class="wd-20p">Employee Name</th>
                    <th class="wd-10p">Designation</th>
                    <th class="wd-20p">Address</th>
                    <th class="wd-15p">Email</th>
                    <th class="wd-10p">Phone No</th>
                    <th class="wd-10p">Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($employees as $key =>$employee)
                    <tr>
                        {{--                    {{dd($employee )}}--}}
{{--                        <td><input type="checkbox" aria-label="Checkbox for following text input"></td>--}}
                        <td>{{$employee->employeeId}}</td>
                        <input type="hidden" class="employee_id123">
                        <td>{{$employee->fullName}}</td>
                        <td>{{$employee->designation}}</td>
                        <td>{{$employee->presentAddress}}</td>
                        <td>{{$employee->mailId}}</td>
                        <td>{{$employee->phoneNumber}}</td>
                        <td class="d-md-flex">
                            @if(Auth::guard('admin')->check())
                                 <div class="mg-r-20" title="View"><a href=" {{route('employee.show',$employee->id)}}"><i class="icon ion-clipboard text-success"></i></a></div>
                            @else
                            @can('employee-view')
                                 <div class="mg-r-20" title="View"><a href=" {{route('employee.show',$employee->id)}}"><i class="icon ion-clipboard text-success"></i></a></div>
                            @endcan
                            @endif

                            @if(Auth::guard('admin')->check())
                                 <div class="mg-r-20" title="Edit"><a href=" {{route('employee.edit',$employee->id)}}" class="edit" data-id="{{$employee->id}}" data-toggle="modal" data-target="#exampleModalCenter1"><i class="far fa-edit text-warning"></i></a></div>
                            @else
                            @can('employee-edit')
                                 <div class="mg-r-20" title="Edit"><a href=" {{route('employee.edit',$employee->id)}}" class="edit" data-id="{{$employee->id}}" data-toggle="modal" data-target="#exampleModalCenter1"><i class="far fa-edit text-warning"></i></a></div>
                            @endcan
                            @endif

                            @if(Auth::guard('admin')->check())
                                 <div class="mg-r-20" title="Delete"><a href="" data-id="{{$employee->id}}" class="delete_data"  data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                            @else
                            @can('employee-delete')
                                 <div class="mg-r-20" title="Delete"><a href="" data-id="{{$employee->id}}" class="delete_data" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                            @endcan
                            @endif



{{--                            <div class="mg-r-20" title="Delete"><a href="{{route('employee.destroy',$employee->id)}}"><i class="icon ion-trash-b text-danger"></i></a></div>--}}




                            {{--                            fot delete popup--}}

                            <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <div class="conform_del">
                                                <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                            </div><!-- conform_del -->

                                            <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                                <a class="final_delete btn btn-success mg-r-10" href=" ">Yes</a>
                                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-0 tx-right col-lg-7">
{{--                                <button type="button" class="btn btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal" data-target="#exampleModalCenter">--}}
{{--                                    New Employee--}}
{{--                                </button>--}}

                                <!-- Modal -->
                                <div class="modal fade modal_cust employee_tab" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                        <form action="{{route('employee.store')}}" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalCenterTitle">NEW EMPLOYEE</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                        <li class="nav-item">
                                                            <a class="nav-link active" id="home1-tab" data-toggle="tab" href="#home1" role="tab" aria-controls="home1" aria-selected="true">PERSONAL INFO</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="profile1-tab" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile1" aria-selected="false">PARENTAL INFO</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="contact1-tab" data-toggle="tab" href="#contact1" role="tab" aria-controls="contact1" aria-selected="false">COMPANY INFO</a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content" id="myTabContent">
                                                        <div class="tab-pane fade show active" id="home1" role="tabpanel" aria-labelledby="home1-tab">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <div class="image1234 upload_type">
{{--                                                                        <img id="blah" src="http://placehold.it/180" alt="your image">--}}

                                                                    </div><!-- upload_type -->
                                                                </div><!-- col -->
                                                                <div class="col-md-9">
                                                                    <div class="mg-t-5">
                                                                        Employee ID: <input type="text" name="employeeId" class="employeeId123 form-control {{ $errors->has('employeeId') ? 'has-error' : '' }}"  placeholder="Employee Id" readonly>
                                                                    </div>
                                                                    <div class="mg-t-5">


                                                                        Employee Name: <input type="text" name="fullName" class="name123 form-control " placeholder="Enter  Name " required>
                                                                    </div>


                                                                </div><!-- col -->
                                                                <div class="col-md-12">
                                                                    <div class="mg-t-5 if_function_input">
                                                                        <input type="file" id="image" class="image123" name="image" onchange="readURL(this);">
                                                                    </div>
                                                                </div><!-- col -->
                                                            </div><!-- row -->
                                                            <div class="row">
                                                                <div class="col-md-6 mg-t-5">

                                                                    Temporary Address:  <input type="text" name="presentAddress" class="temp_address123 form-control " placeholder="Employee Temporary Address" value="">

                                                                </div><!-- col -->
                                                                <div class="col-md-6 mg-t-5">

                                                                    Permanent Address:<input type="text" name="permanentAddress" class="per_address123 form-control " placeholder="Employee Parmanent Address" value="">

                                                                </div><!-- col -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6 mg-t-5">

                                                                    PAN / VAT Number: <input type="text" class="pan_number123" name="pan_number" value="" placeholder="20383">

                                                                </div><!-- col -->
                                                                <div class="col-md-6 mg-t-5">

                                                                    Phone Number:    <input type="text" name="phoneNumber" class="phone_number123 form-control" placeholder="Enter Employee Phone No." value="">

                                                                </div><!-- col -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6 mg-t-5">
                                                                    Email ID:  <input type="text" name="email" class="email123 form-control" placeholder="Enter Employee Email" value="">

                                                                </div><!-- col -->
                                                                <div class="col-md-6 mg-t-5">


                                                                    Blood Group:
                                                                    <select name="bloodGroup"  class="blood123 form-control select2-no-search ">
                                                                        <option label="Unknown"></option>
                                                                        <option  value="B+">B+</option>
                                                                        <option value="A+">A+</option>
                                                                        <option  value="A-">A-</option>
                                                                        <option value="AB+">AB+</option>
                                                                        <option  value="Ab-">Ab-</option>
                                                                        <option value="O+">O+</option>
                                                                    </select>
                                                                </div><!-- col -->

                                                                <!-- col -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6 mg-t-5">

                                                                    Citizenship Number:      <input type="text" name="citizenshipNumber" class="citizenshipNumber123 form-control" placeholder="citizenship number" required>

                                                                </div><!-- col -->



                                                                <div class="col-md-6 mg-t-5">

                                                                    Date of Birth:  <input type="text" name="dob" class="bod-picker dob123" placeholder="YYYY/MM/DD" required>

                                                                </div><!-- col -->



                                                                <!-- col -->
                                                            </div>

                                                            <div class="input-group group-end">
                                                                <div class="tab_nxt_prv">
                                                                    <a class="btn btn-success btnNext">Next</a>
                                                                </div><!-- tab_nxt_prv -->
                                                            </div>


                                                        </div>
                                                        <div class="tab-pane fade" id="profile1" role="tabpanel" aria-labelledby="profile1-tab">
                                                            <div class="row">
                                                                <div class="col-md-6 mg-t-5">

                                                                    Father's Name:   <input type="text" name="fatherName" class="father_name123 form-control " placeholder="Enter Father Name" required>

                                                                </div><!-- col -->
                                                                <div class="col-md-6 mg-t-5">

                                                                    Father's Number:<input type="text" name="fatherPhoneNumber" class="father_phone123 form-control " placeholder="Enter Mobile No. " >

                                                                </div><!-- col -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6 mg-t-5">

                                                                    Mother's Namer:           <input type="text" name="motherName" class="mother_name123 form-control " placeholder="Enter Mother Name" required>

                                                                </div><!-- col -->
                                                                <div class="col-md-6 mg-t-5">

                                                                    Mother's Number:  <input type="text" name="motherPhoneNumber" class="mother_phone123 form-control" placeholder="Enter Mobile No. " required>

                                                                </div><!-- col -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 mg-t-5">

                                                                    Parent's Address: <input type="text" class="parents_address123" name="FirstName" value="" placeholder="Kathmandu">

                                                                </div>
                                                            </div>
                                                            <div class="input-group group-end">
                                                                <div class="tab_nxt_prv mg-t-26p">
                                                                    <a class="btn btn-primary btnPrevious">Prev</a>
                                                                    <a class="btn btn-success btnNext">Next</a>
                                                                </div><!-- tab_nxt_prv -->
                                                            </div>

                                                        </div>
                                                        <div class="tab-pane fade" id="contact1" role="tabpanel" aria-labelledby="contact1-tab">
                                                            <div class="row">
                                                                <div class="col-md-6 mg-t-5">

                                                                    Designation:      <input type="text" name="designation" class="designation123 form-control " placeholder="Job Title" required>

                                                                </div><!-- col -->
                                                                <div class="col-md-6 mg-t-5">


                                                                    Joining Branch:
                                                                    <select class="branch_id123 form-control  select2-no-search " name="branch_id">
                                                                        <option value=" ">Select branch</option>
                                                                        @forelse($branch as $branches)
                                                                            <option value="{{$branches->id}}">{{$branches->name}}</option>
                                                                        @empty
                                                                        @endforelse
                                                                    </select>

                                                                </div><!-- col -->

                                                                <!-- col -->
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-6 mg-t-5">

                                                                    Joining Date:  <input type="text" name="joiningDate" class="joining_data123 bod-picker" placeholder="YYYY/MM/DD" required>

                                                                </div><!-- col -->
                                                                <div class="col-md-6 mg-t-5 n">

                                                                    Salary: <div class="gropup-flex mg-b-0">
                                                                        <div class="input-group-prepend">
                                                                            <button class="btn btn-outline-light btn_small" type="button" id="button-addon1">NPR</button>
                                                                        </div>
                                                                        <input type="text" name="salary" class="salary123 form-control " placeholder="Salary" value="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                    </div>

                                                                </div><!-- col -->

                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-6 mg-t-5">
                                                            <span class="mg-t-5 small">Document:</span>

                                                            <div class="custom-file">
                                                                <input type="file" class="document123 custom-file-input" name="document" id="inputGroupFile01"
                                                                       aria-describedby="inputGroupFileAddon01">
                                                                <label class="custom-file-label" for="inputGroupFile01">Upload bill image/pdf</label>
                                                            </div>

                                                                </div>


                                                                <div class="col-md-6 mg-t-5 n">

                                                                    Due: <div class="gropup-flex mg-b-0">
                                                                        <div class="input-group-prepend">
                                                                            <button class="btn btn-outline-light btn_small" type="button" id="button-addon1">NPR</button>
                                                                        </div>
                                                                        <input type="text" name="opening_due" class="opening_due123 form-control " placeholder="opening due" value="0.0" aria-label="Username" aria-describedby="basic-addon1">
                                                                    </div>

                                                                </div><!-- col -->


                                                            </div>

                                                            <div class="col-md-12 mg-t-5 textarea_custom no-pad">

                                                                Description
                                                                <textarea name="description" class="description123 form-control " rows="3" placeholder="Write something about Employee"></textarea>


                                                            </div>
                                                            <div class="input-group group-end">
                                                                <div class="tab_nxt_prv">
                                                                    <a class="btn btn-primary btnPrevious">Prev</a>
                                                                    <button class="update btn btn-success btnSubmit">Submit</button>
                                                                    {{--                                            <a class="btn btn-success btnSubmit">Submit</a>--}}
                                                                </div><!-- tab_nxt_prv -->
                                                            </div>
                                                        </div>
                                                    </div>




                                                </div>

                                            </div>
                                        </form>
                                    </div>

                                </div>






                            </div><!-- col -->
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->



@endsection


@push('scripts')
    <script>

        $(".edit").click(function (e) {
            e.preventDefault()

            var employee_id= $(this).data('id');
            // alert(employee_id);
            $.ajax({

                url: "{{url('/employee-edit')}}",
                type: "POST",
                data: {
                    _token : "{{csrf_token()}}",
                    employee_id : employee_id
                },
                success: function (data) {
                    console.log(data);

                    $('.employee_id123').append($('.employee_id123').val(employee_id));
                    $('.employeeId123').append($('.employeeId123').val(data.employee.employeeId));
                    $('.name123').append($('.name123').val(data.employee.fullName));
                    $('.temp_address123').append($('.temp_address123').val(data.employee.presentAddress));
                    $('.per_address123').append($('.per_address123').val(data.employee.permanentAddress));
                    $('.pan_number123').append($('.pan_number123').val(data.employee.panNumber));
                    $('.phone_number123').append($('.phone_number123').val(data.employee.phoneNumber));
                    $('.email123').append($('.email123').val(data.employee.mailId));
                    $('.blood123').append($('.blood123').val(data.employee.bloodGroup));

                    $('.father_name123').append($('.father_name123').val(data.employee.fatherName));
                    $('.father_phone123').append($('.father_phone123').val(data.employee.fatherPhoneNumber));
                    $('.mother_name123').append($('.mother_name123').val(data.employee.motherName));
                    $('.mother_phone123').append($('.mother_phone123').val(data.employee.motherPhoneNumber));
                    $('.parents_address123').append($('.parents_address123').val(data.employee.parentsAddress));

                    $('.designation123').append($('.designation123').val(data.employee.designation));
                    $('.branch_id123').append($('.branch_id123').val(data.employee.branch_id));
                    $('.joining_data123').append($('.joining_data123').val(data.employee.joiningDate));
                    $('.salary123').append($('.salary123').val(data.employee.salary));
                    $('.opening_due123').append($('.opening_due123').val(data.employee.salaryDue));
                    $('.description123').append($('.description123').val(data.employee.description));
                    $('.citizenshipNumber123').append($('.citizenshipNumber123').val(data.employee.citizenshipNumber));
                    $('.dob123').append($('.dob123').val(data.employee.dob));
                    $.each(data.employee.images, function(index, value) {
                        console.log(value['image'][0]);
                        $('.image1234').html('<img src="'+value['image']+'">');
                    });
                },

                error: function (error) {
                    console.log('error');
                }
            });
        });

        $(".update").click(function (e) {
            e.preventDefault()
            // var branch_id= $(this).data('id');
            var employee_id= $('.employee_id123').val();
            var employee_id= $('.citizenshipNumber123').val();
            var employee_id= $('.dob123').val();
            var employeeId= $('.employeeId123').val();
            var name = $('.name123').val();
            var temp_address = $('.temp_address123').val();
            var per_address = $('.per_address123').val();
            var pan_number = $('.pan_number123').val();
            var phone_number = $('.phone_number123').val();
            var email = $('.email123').val();
            var blood = $('.blood123').val();

            var image = $('.image123').val();
            var document = $('.document123').val();

            var father_name = $('.father_name123').val();
            var father_phone = $('.father_phone123').val();
            var mother_name = $('.mother_name123').val();
            var mother_phone = $('.mother_phone').val();
            var parents_address = $('.parents_address').val();

            var designation = $('.designation123').val();
            var branch_id = $('.branch_id123').val();
            var joining_date = $('.joining_data123').val();
            var salary = $('.salary123').val();
            var opening_due = $('.opening_due123').val();
            var description = $('.description123').val();



            // alert(employee_id);
            $.ajax({

                url: "{{url('/employees-update')}}",
                type: "POST",
                data: {
                    _token : "{{csrf_token()}}",

                    employee_id:employee_id,
                    citizenshipNumber:citizenshipNumber,
                    dob:dob,
                    employeeId:employeeId,
                    fullName:name,
                    presentAddress:temp_address,
                    permanentAddress:per_address,
                    pan_number:pan_number,
                    phoneNumber:phone_number,
                    email:email,
                    bloodGroup:blood,

                    fatherName:father_name,
                    fatherPhoneNumber:father_phone,
                    motherName:mother_name,
                    motherPhoneNumber:mother_phone,
                    parents_address:parents_address,

                    designation:designation,
                    joiningDate:joining_date,
                    branch_id:branch_id,
                    salary:salary,
                    opening_due:opening_due,
                    description:description,

                    image:image,
                    document:document,


                },
                success: function (data) {
                    console.log(data);


                    toastr.success(data.success);
                    // console.log('success');
                    window.location.reload();

                },

                error: function (error) {

                    console.log('error');

                }


            });

        });




        //for the delete data
        $(".delete_data").click(function(e){
            e.preventDefault()

            var employee_id = $(this).data('id');
            // alert(customer_id);


            $.ajax({
                type: 'POST',
                url: '{{url('/employee-edit')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    employee_id : employee_id,


                },
                success: function (data, status) {

                    if(data.error){

                        return;
                    }

                    // $('#show_modal').modal('hide');
                    // toastr.success(data.success);

                    // console.log(data.customer.fullName);
                    // window.location.reload();
                    // alert('test');

                    $('.employee_id123').append($('.employee_id123').val(data.employee.id));


                },
                error: function (xhr, status, error) {
                    // console.log('error');
                    // toastr.error(error.errors);
                }
            });

        });


        //for final delete
        $(".final_delete").click(function(e){
            e.preventDefault()

            // alert('ssax');
            var employee_id= $(".employee_id123").val();


            // alert(employee_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/employee-delete')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    employee_id : employee_id,


                },
                success: function (data, status) {

                    if(data.errors){
                        toastr.error(data.errors);
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);



                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // toastr.error(data.errors);
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });

    </script>
@endpush
