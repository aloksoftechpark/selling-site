@extends('admin.layouts.app')
@section('content')


    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Product Setup</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Plans and Pricing</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Plans and Pricing</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"
                    data-target="#exampleModalCenter2">Add Plan</button>
            <div class="modal fade modal_cust" id="exampleModalCenter2" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle1">ADD PLAN</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body salary_payroll">

                            <div class="row">
                                <div class="col-md-6">
                                    Plan Title
                                    <input type="text" name="name" value="" class="name form-control-sm">
                                    <p id="name_error" style="color: red"></p>
                                    <div class="mg-t-5">
                                        Package
                                        <select id="package_id" name="package_id" class="form-control form-control-sm modal_select_option_height bd bd-gray-900 package"
                                                aria-label="Example text with button addon" aria-describedby="button-addon1">
                                            <option value="" id="package_id">Select packages</option>

                                            @forelse($package as $packages)
                                                <option value="{{$packages->id}}">{{$packages->name}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                    <p id="package_error" style="color: red"></p>
                                    <div class="mg-t-5">
                                        No of branch
                                        <input type="text" name="number" value="" class="no_of_branch form-control-sm">
                                        <p id="no_of_branch_error" style="color: red"></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    Price
                                    <input type="text" name="number" value="" class="price form-control-sm">
                                    <p id="price_error" style="color: red"></p>
{{--                                    <div class="mg-t-5">--}}
{{--                                        Sub Package--}}
{{--                                        <select class="form-control form-control-sm modal_select_option_height bd bd-gray-900"--}}
{{--                                                aria-label="Example text with button addon" aria-describedby="button-addon1">--}}
{{--                                            <option value="volvo">Guest</option>--}}
{{--                                            <option value="saab">Ranjan</option>--}}
{{--                                            <option value="opel">Manoj</option>--}}
{{--                                            <option value="audi">Pawan</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
                                    <div class="mg-t-5">
                                        No of users
                                        <input type="text" name="number" value="" class="no_of_user form-control-sm">
                                        <p id="no_of_user_error" style="color: red"></p>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-30">
                                <button type="button" class="save_plan_pricing btn btn-success mg-r-10">Submit</button>
                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-3">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                </div>
            </div>
            <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5p">SN.</th>
                        <th class="wd-15p">Packages</th>
                        <th class="wd-15p">No. of Branch</th>
                        <th class="wd-15p">No. of User</th>
                        <th class="wd-20p">Plan Title</th>
                        <th class="wd-15p">Price</th>
                        <th class="wd-15p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($plan_pricing as $key=> $plan_pricing)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$plan_pricing->package->name}}</td>
                        <td>{{$plan_pricing->no_of_branch}}</td>
                        <td>{{$plan_pricing->no_of_user}}</td>
                        <td>{{$plan_pricing->name}}</td>
                        <td>{{$plan_pricing->price}}</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href="../extraPages/client-detail.html"><i
                                        class="icon ion-clipboard text-success"></i></a></div>
                            <div class="mg-r-20" title="Edit"><a href="" class="edit_plan_price" data-id="{{$plan_pricing->id}}" data-toggle="modal" data-target="#exampleModalCenter22"><i class="far fa-edit text-warning"></i></a></div>
                            <div class="mg-r-20" title="Cancle"><a href="" class="delete_data" data-id="{{$plan_pricing->id}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>


                        </td>


                        {{--for edit--}}
                        <div class="col-sm-0 tx-right col-lg-7">
                            {{--                                <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"--}}
                            {{--                                        data-target="#exampleModalCenter2">Add Plan</button>--}}
                            <div class="modal fade modal_cust" id="exampleModalCenter22" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle1">ADD PLAN</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body salary_payroll">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    Plan Title
                                                    <input type="text" name="name" value="" class="name123 form-control-sm">
                                                    <p id="name_error123" style="color: red"></p>
                                                    <div class="mg-t-5">
                                                        Package
{{--                                                        <div class="package123">--}}
                                                        <input type="hidden" class="plan_price_id123">
                                                        <select class="package123 form-control form-control-sm modal_select_option_height bd bd-gray-900"
                                                                aria-label="Example text with button addon" aria-describedby="button-addon1">

                                                        </select>
                                                    </div>
                                                    <p id="package_error123" style="color: red"></p>
                                                    <div class="mg-t-5">
                                                        No of branch
                                                        <input type="text" name="number" value="" class="no_of_branch123 form-control-sm">
                                                        <p id="no_of_branch_error123" style="color: red"></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Price
                                                    <input type="text" name="number" value="" class="price123 form-control-sm">
                                                    <p id="price_error123" style="color: red"></p>
                                                    {{--                                    <div class="mg-t-5">--}}
                                                    {{--                                        Sub Package--}}
                                                    {{--                                        <select class="form-control form-control-sm modal_select_option_height bd bd-gray-900"--}}
                                                    {{--                                                aria-label="Example text with button addon" aria-describedby="button-addon1">--}}
                                                    {{--                                            <option value="volvo">Guest</option>--}}
                                                    {{--                                            <option value="saab">Ranjan</option>--}}
                                                    {{--                                            <option value="opel">Manoj</option>--}}
                                                    {{--                                            <option value="audi">Pawan</option>--}}
                                                    {{--                                        </select>--}}
                                                    {{--                                    </div>--}}
                                                    <div class="mg-t-5">
                                                        No of users
                                                        <input type="text" name="number" value="" class="no_of_user123 form-control-sm">
                                                        <p id="no_of_user_error123" style="color: red"></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-30">
                                                <button type="button" class="update_plan_pricing btn btn-success mg-r-10">Submit</button>
                                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                              <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- col -->



                        {{--                            fot delete popup--}}

                        <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <div class="conform_del">
                                            <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                        </div><!-- conform_del -->

                                        <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                            <a class="final_delete btn btn-success mg-r-10" href=" ">Yes</a>
                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->


@endsection

@push('scripts')

<script>

    $(".edit_plan_price").click(function(e){
        e.preventDefault()
        var plan_price_id = $(this).data('id');

        // alert(contact_id);

        $.ajax({
            type: 'POST',
            url: '{{url('/edit-plan-pricing1')}}',
            data: {
                _token : "{{csrf_token()}}",
                plan_price_id : plan_price_id,

            },
            success: function (data, status) {

                if(data.error){
                    return;
                }

                // toastr.success(data.success);
                // window.location.reload();
                console.log(data);

                $('.name123').append($('.name123').val(data.plan_pricing.name));
                $('.price123').append($('.price123').val(data.plan_pricing.price));
                // $('.package123').append($('.package123').val(data.plan_pricing.package));
                $('.package123').empty();
                $('.package123').append('<option class="package1234" value="'+data.plan_pricing.package['id']+'" selected>'+data.plan_pricing.package['name']+'</option>');
                $.each(data.package, function(index, value) {

                    $('.package123').append('<option value="'+value['id']+'">'+value['name']+'</option>');

                });
                $('.no_of_branch123').append($('.no_of_branch123').val(data.plan_pricing.no_of_branch));
                $('.no_of_user123').append($('.no_of_user123').val(data.plan_pricing.no_of_user));
                $('.plan_price_id123').append($('.plan_price_id123').val(data.plan_pricing.id));

            },
            error: function (xhr, status, error) {
                console.log('error');
                // var err = JSON.parse(xhr.responseText);
                // $('#category_error').append(err.errors.title);
            }
        });

    });

    $(".update_plan_pricing").click(function(e){
        e.preventDefault()
        // var category_id = $(this).data('id');

        var plan_price_id= $(".plan_price_id123").val();
        var name= $(".name123").val();
        var price= $(".price123").val();
        var pac= $(".package123").val();
        var no_of_branch= $(".no_of_branch123").val();
        var no_of_user = $(".no_of_user123").val();
        // alert(pac);

        $.ajax({
            type: 'POST',
            url: '{{url('/update-plan-pricing')}}',
            data: {
                _token : "{{csrf_token()}}",
                name : name,
                price : price,
                plan_price_id : plan_price_id,
                package : pac,
                no_of_branch : no_of_branch,
                no_of_user : no_of_user,
            },
            success: function (data, status) {

                if(data.error){
                    return;
                }
                if(data.success){

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);
                }

            },
            error: function (xhr, status, error) {
                console.log('error');
                var err = JSON.parse(xhr.responseText);
                // $('#category_error').append(err.errors.title);
                $('#name_error123').html(err.errors.name);
                $('#price_error123').html(err.errors.price);
                $('#package_error123').html(err.errors.package);
                $('#no_of_branch_error123').html(err.errors.no_of_brach);
                $('#no_of_user_error123').html(err.errors.no_of_user);

                // if (err.errors.password) {
                //     toastr.error(err.errors.password);
                // }
            }
        });

    });

    $(".save_plan_pricing").click(function(e){
        e.preventDefault()
        // var category_id = $(this).data('id');
        var name= $(".name").val();
        var price= $(".price").val();
        var pac= $(".package").val();
        var no_of_branch= $(".no_of_branch").val();
        var no_of_user = $(".no_of_user").val();

        // alert(pac);

        $.ajax({
            type: 'POST',
            url: '{{url('/add-plan-pricing')}}',
            data: {
                _token : "{{csrf_token()}}",
                name : name,
                price : price,
                package : pac,
                no_of_branch : no_of_branch,
                no_of_user : no_of_user,

            },
            success: function (data, status) {

                if(data.error){
                    return;
                }

                if(data.success){

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);
                }

            },
            error: function (xhr, status, error) {
                console.log('error');
                var err = JSON.parse(xhr.responseText);
                // $('#category_error').append(err.errors.title);
                $('#name_error').html(err.errors.name);
                $('#price_error').html(err.errors.price);
                $('#package_error').html(err.errors.package);
                $('#no_of_branch_error').html(err.errors.no_of_branch);
                $('#no_of_user_error').html(err.errors.no_of_user);

                // if (err.errors.password) {
                //     toastr.error(err.errors.password);
                // }
            }
        });

    });




    //for the delete data
    $(".delete_data").click(function(e){
        e.preventDefault()

        var plan_price_id = $(this).data('id');
        // alert(customer_id);

        $.ajax({
            type: 'POST',
            url: '{{url('/edit-plan-pricing1')}}',
            data: {
                _token : "{{csrf_token()}}",
                plan_price_id : plan_price_id,

            },
            success: function (data, status) {

                if(data.error){

                    return;
                }

                // $('#show_modal').modal('hide');
                // toastr.success(data.success);

                console.log(data.package.id);
                // window.location.reload();
                // alert('test');
                $('.plan_price_id123').append($('.plan_price_id123').val(data.plan_pricing.id));

            },
            error: function (xhr, status, error) {
                // console.log('error');
                // toastr.error(error.errors);
            }
        });

    });


    //for final delete
    $(".final_delete").click(function(e){
        e.preventDefault()

        var plan_price_id= $(".plan_price_id123").val();

        // alert(category_id1234);

        $.ajax({
            type: 'POST',
            url: '{{url('/delete-plan-pricing')}}',
            data: {
                _token : "{{csrf_token()}}",
                plan_price_id : plan_price_id,


            },
            success: function (data, status) {

                if(data.errors){
                    toastr.error(data.errors);
                    return;
                }

                toastr.success(data.success);
                window.location.reload();
                console.log(data);

            },
            error: function (xhr, status, error) {
                console.log('error');
                // toastr.error(data.errors);
                // var err = JSON.parse(xhr.responseText);
                // $('#category_error').append(err.errors.title);
            }
        });

    });

</script>
@endpush
