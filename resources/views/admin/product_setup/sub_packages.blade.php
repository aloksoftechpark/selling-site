@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Product Setup</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Sub Packages</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Sub Packages</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"
                    data-target="#exampleModalCenter2">Add Sub Package</button>
            <div class="modal fade modal_cust" id="exampleModalCenter2" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle1">ADD SUB PACKAGES</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body salary_payroll">

                            <div class="row">
                                <div class="col-md-6">
                                    Packages
                                    <select class="form-control form-control-sm modal_select_option_height bd bd-gray-900"
                                            aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <option value="volvo">Guest</option>
                                        <option value="saab">Ranjan</option>
                                        <option value="opel">Manoj</option>
                                        <option value="audi">Pawan</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    Sub Package Title
                                    <input type="text" name="title value="" class=" form-control-sm">
                                </div>
                            </div>
                            <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-30">
                                <button type="button" class="btn btn-success mg-r-10">Update</button>
                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>





                        </div>
                    </div>
                </div>
            </div>
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-3">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                </div>
            </div>
            <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5p">SN.</th>
                        <th class="wd-30p">Packages</th>
                        <th class="wd-50p">Sub Title</th>
                        <th class="wd-15p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Service Oriented</td>
                        <td>akjjjjjjjjjjjj</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href="../extraPages/client-detail.html"><i
                                        class="icon ion-clipboard text-success"></i></a></div>
                            <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>
                            <div class="mg-r-20" title="Cancle"><a href=""><i class="icon ion-trash-b text-danger"></i></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Product Oriented</td>
                        <td>Product Oriented</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>
                            <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>
                            <div class="mg-r-20" title="Cancle"><a href=""><i class="icon ion-trash-b text-danger"></i></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Service Oriented</td>
                        <td>Product Oriented</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>
                            <div class="mg-r-20" title="Edit"><a href=""><i class="far fa-edit text-warning"></i></a></div>
                            <div class="mg-r-20" title="Cancle"><a href=""><i class="icon ion-trash-b text-danger"></i></a></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->


@endsection
