@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Product Setup</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Packages</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Packages</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"
                    data-target="#exampleModalCenter2">Add Package</button>
            <div class="modal fade modal_cust" id="exampleModalCenter2" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle1">ADD SUB PACKAGES</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body salary_payroll">

                            <div class="row">

                                <div class="col-md-12">
                                    Package Title
                                    <input type="text" name="title" class="name form-control-sm">
                                    <p id="name_error" style="color: red"></p>
                                </div>
                            </div>
                            <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-30">
                                <button class="save_package btn btn-success mg-r-10">Update</button>
                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-3">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                </div>
            </div> <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5p">SN.</th>
                        <th class="wd-80p">Title</th>
                        <th class="wd-15p">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($package as $key => $packages)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$packages->name}}</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href=""><i class="icon ion-clipboard text-success"></i></a></div>
                            <div class="mg-r-20" title="Edit"><a href="" class="edit_package" data-id="{{$packages->id}}" data-toggle="modal" data-target="#exampleModalCenter22"><i class="far fa-edit text-warning"></i></a></div>
                            <div class="mg-r-20" title="Cancle"><a href="" class="delete_data" data-id="{{$packages->id}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>
                        </td>


                        <div class="col-sm-0 tx-right col-lg-7">
{{--                            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"--}}
{{--                                    data-target="#exampleModalCenter2">Add Package</button>--}}
                            <div class="modal fade modal_cust" id="exampleModalCenter22" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle1">ADD SUB PACKAGES</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body salary_payroll">

                                            <div class="row">

                                                <div class="col-md-12">
                                                    Package Title
                                                    <input type="text" name="title" class="name123 form-control-sm">
                                                    <input type="hidden" name="title" class="package_id123 form-control-sm">
                                                    <p id="name_error123" style="color: red"></p>
                                                </div>
                                            </div>
                                            <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-30">
                                                <button  class="update_package btn btn-success mg-r-10">Update</button>
                                                <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>
                                                <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                               <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- col -->


                        {{--                            fot delete popup--}}

                        <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <div class="conform_del">
                                            <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                        </div><!-- conform_del -->

                                        <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                            <a class="final_delete btn btn-success mg-r-10" href=" ">Yes</a>
                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->




@endsection


@push('scripts')

    <script>

        $(".edit_package").click(function(e){
            e.preventDefault()
            var package_id = $(this).data('id');

            // alert(package_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/edit-package')}}',
                data: {

                    _token : "{{csrf_token()}}",
                    package_id : package_id,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // toastr.success(data.success);
                    // window.location.reload();
                    console.log(data);

                    $('.name123').append($('.name123').val(data.package.name));
                    $('.package_id123').append($('.package_id123').val(data.package.id));

                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });

        $(".update_package").click(function(e){
            e.preventDefault()

            var name= $(".name123").val();
            var package_id= $(".package_id123").val();

            // alert(category_id1234);

            $.ajax({
                type: 'POST',
                url: '{{url('/update-package')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    package_id : package_id,
                    name : name,


                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    if(data.success){

                        toastr.success(data.success);
                        window.location.reload();
                        console.log(data);
                    }


                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                    $('#name_error123').html(err.errors.name);

                    // if (err.errors.password) {
                    //     toastr.error(err.errors.password);
                    // }
                }
            });

        });

        $(".save_package").click(function(e){
            e.preventDefault()
            // var category_id = $(this).data('id');
            var name= $(".name").val();

            // alert(category_id1234);

            $.ajax({
                type: 'POST',
                url: '{{url('/add-package')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    name : name,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }
                    if(data.success){

                        toastr.success(data.success);
                        window.location.reload();
                        console.log(data);
                    }

                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                    $('#name_error').html(err.errors.name);

                    // if (err.errors.password) {
                    //     toastr.error(err.errors.password);
                    // }
                }
            });

        });


        //for the delete data
        $(".delete_data").click(function(e){
            e.preventDefault()

            var package_id = $(this).data('id');
            // alert(customer_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/edit-package')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    package_id : package_id,

                },
                success: function (data, status) {

                    if(data.error){

                        return;
                    }

                    // $('#show_modal').modal('hide');
                    // toastr.success(data.success);

                    console.log(data.package.id);
                    // window.location.reload();
                    // alert('test');

                    $('.package_id123').append($('.package_id123').val(data.package.id));

                },
                error: function (xhr, status, error) {
                    // console.log('error');
                    // toastr.error(error.errors);
                }
            });

        });


        //for final delete
        $(".final_delete").click(function(e){
            e.preventDefault()

            var package_id= $(".package_id123").val();

            // alert(category_id1234);

            $.ajax({
                type: 'POST',
                url: '{{url('/delete-package')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    package_id : package_id,


                },
                success: function (data, status) {

                    if(data.errors){
                        toastr.error(data.errors);
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);

                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // toastr.error(data.errors);
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });

    </script>

@endpush
