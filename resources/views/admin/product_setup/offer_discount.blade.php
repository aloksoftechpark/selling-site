@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Product Setup</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Offers and Discount</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Offers and Discount</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"
                    data-target="#exampleModalCenter2">Add Offer</button>
            <div class="modal fade modal_cust" id="exampleModalCenter2" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">ADD FEEDBACK</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body text-left salary_payroll">
                            <div class="row">
                                <div class="col-md-6">

                                    Promo Code
                                    <input type="text" name="number" value="" class="promo_code form-control-sm">
                                    <p id="promo_code_error" style="color: red"></p>
                                    <div class="mg-t-5">
                                        Valid Form
                                        <input type="text" name="number" value="" class="bod-picker valid_from form-control-sm">
                                    </div>
                                    <div class="mg-t-5">


                                        Valid For
                                        <div class="d-flex mg-t-5 mg-b-14">
                                            <div class="custom-control custom-radio mg-r-20">
                                                <input type="checkbox" id="customRadio1" value="cash bill" name="customRadio" class="cash_bill checkbox custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Cash Bill</label>
                                            </div>
                                            <div class="custom-control custom-radio mg-r-10">
                                                <input type="checkbox" id="customRadio2" value="due bill" name="customRadio" class="due_bill checkbox1 custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Due Bill</label>
                                            </div>
                                        </div>
                                        <div class="d-flex mg-t-5 mg-b-14">
                                            <div class="custom-control custom-radio mg-r-20">
                                                <input type="checkbox" id="customRadio3" value="old client" name="customRadio" class="old_client checkbox2 custom-control-input">
                                                <label class="custom-control-label" for="customRadio3">Old Client</label>
                                            </div>
                                            <div class="custom-control custom-radio mg-r-10">
                                                <input type="checkbox" id="customRadio4" name="customRadio" value="new client" class="new_client checkbox3 custom-control-input">
                                                <label class="custom-control-label" for="customRadio4">New Client</label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    Discount (%)
                                    <div class="input-group">
                                        <input type="text" class="discount form-control form-control-sm bd-r-0" placeholder="0.0"
                                               aria-label="Username" aria-describedby="basic-addon1">

                                        <div class="input-group-prepend wd-25p">
                                          <span class="input-group-text form-control wd-100p justify-content-center"
                                            id="basic-addon1">%</span>
                                        </div>

                                    </div>
                                    <p id="discount_error" style="color: red"></p>
                                    <div class="mg-t-5">
                                        Valid till
                                        <input type="text" name="date" class="bod-picker valid_till form-control form-control-sm wd-120" placeholder="">
                                    </div>
                                    <div class="mg-t-5">
                                        <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-30">
                                            <button class="save_plan_pricing btn btn-success mg-r-10">Update</button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                            <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                             <button type="button" class="btn btn-primary">Save changes</button> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-3">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                </div>
            </div>
            <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5p">SN.</th>
                        <th class="wd-15p">PromoCode</th>
                        <th class="wd-15p">Discount</th>
                        <th class="wd-25p">Validity</th>
                        <th class="wd-25p">Apply For</th>
                        <th class="wd-15p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($offer_discount as $key => $offer_discounts)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$offer_discounts->promo_code}}</td>
                        <td>{{$offer_discounts->discount}}%</td>
                        <td>{{$offer_discounts->valid_from}} -{{$offer_discounts->valid_till}}</td>
                        <td>
                                @if($offer_discounts->cash_bill)
                                {{$offer_discounts->cash_bill}},
                                @endif
                                @if($offer_discounts->due_bill)
                                    {{$offer_discounts->due_bill}},
                                @endif
                                @if($offer_discounts->new_client)
                                    {{$offer_discounts->new_client}},
                                @endif
                                    @if($offer_discounts->old_client)
                                        {{$offer_discounts->old_client}}
                                    @endif
                        </td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href="../extraPages/client-detail.html"><i
                                        class="icon ion-clipboard text-success"></i></a></div>
                            <div class="mg-r-20" title="Edit"><a href="" class="edit_offer_discount" data-id="{{$offer_discounts->id}}" data-toggle="modal" data-target="#exampleModalCenter22"><i class="far fa-edit text-warning"></i></a></div>
                            <div class="mg-r-20" title="Cancle"><a href="" class="delete_data" data-id="{{$offer_discounts->id}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>



                        </td>


                        <div class="col-sm-0 tx-right col-lg-7">
                            {{--                                <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"--}}
                            {{--                                        data-target="#exampleModalCenter2">Add Offer</button>--}}
                            <div class="modal fade modal_cust" id="exampleModalCenter22" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">ADD FEEDBACK</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-left salary_payroll">
                                            <div class="row">
                                                <div class="col-md-6">

                                                    Promo Code
                                                    <input type="hidden" class="offer_discount_id123">
                                                    <input type="text" name="number" value="" class="promo_code123 form-control-sm">
                                                    <p id="promo_code_error123" style="color: red"></p>
                                                    <div class="mg-t-5">
                                                        Valid Form
                                                        <input type="text" name="number" value="" class="bod-picker valid_from123 form-control-sm">
                                                    </div>
                                                    <div class="mg-t-5">


                                                        Valid For
                                                        <div class="d-flex mg-t-5 mg-b-14">
                                                            <div class="custom-control custom-radio mg-r-20">
                                                                <input type="checkbox" id="customRadio8" value="cash bill" name="customRadio" class="cash_bill4 checkbox4 custom-control-input"  @if(isset($offer_discounts) && $offer_discounts->cash_bill == 'cash bill') checked="" @endif>
                                                                <label class="custom-control-label" for="customRadio8">Cash Bill</label>
                                                            </div>
                                                            <div class="custom-control custom-radio mg-r-10">
                                                                <input type="checkbox" id="customRadio5" value="due bill" name="customRadio" class="due_bill5 checkbox5 custom-control-input" @if(isset($offer_discounts) && $offer_discounts->due_bill == 'due bill') checked="" @endif>
                                                                <label class="custom-control-label" for="customRadio5">Due Bill</label>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex mg-t-5 mg-b-14">
                                                            <div class="custom-control custom-radio mg-r-20">
                                                                <input type="checkbox" id="customRadio6" value="old client" name="customRadio" class="old_client6 checkbox6 custom-control-input" @if(isset($offer_discounts) && $offer_discounts->old_client == 'old client') checked="" @endif>
                                                                <label class="custom-control-label" for="customRadio6">Old Client</label>
                                                            </div>
                                                            <div class="custom-control custom-radio mg-r-10">
                                                                <input type="checkbox" id="customRadio7" name="customRadio" value="new client" class="new_client7 checkbox7 custom-control-input" @if(isset($offer_discounts) && $offer_discounts->new_client == 'new client') checked="" @endif>
                                                                <label class="custom-control-label" for="customRadio7">New Client</label>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    Discount (%)
                                                    <div class="input-group">
                                                        <input type="text" class="discount123 form-control form-control-sm bd-r-0" placeholder="0.0"
                                                               aria-label="Username" aria-describedby="basic-addon1">

                                                        <div class="input-group-prepend wd-25p">
                                                                  <span class="input-group-text form-control wd-100p justify-content-center"
                                                                        id="basic-addon1">%</span>
                                                        </div>
                                                        <p id="discount_error123" style="color: red"></p>
                                                    </div>
                                                    <div class="mg-t-5">
                                                        Valid till
                                                        <input type="text" name="date" class="bod-picker valid_till123 form-control form-control-sm wd-120" placeholder="">
                                                    </div>
                                                    <div class="mg-t-5">
                                                        <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-30">
                                                            <button  class="update_offer_discount btn btn-success mg-r-10">Update</button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                                            <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary">Save changes</button> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div><!-- col -->





                        {{--                            fot delete popup--}}

                        <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <div class="conform_del">
                                            <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                        </div><!-- conform_del -->

                                        <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                            <a class="final_delete btn btn-success mg-r-10" href=" ">Yes</a>
                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->


@endsection


@push('scripts')

    <script>

        $(".edit_offer_discount").click(function(e){
            e.preventDefault()
            var offer_discount_id = $(this).data('id');

            // alert(contact_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/edit-offer-discount1')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    offer_discount_id : offer_discount_id,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // toastr.success(data.success);
                    // window.location.reload();
                    console.log(data);

                    $('.promo_code123').append($('.promo_code123').val(data.offer_discount.promo_code));
                    $('.discount123').append($('.discount123').val(data.offer_discount.discount));
                    $('.valid_from123').append($('.valid_from123').val(data.offer_discount.valid_from));
                    $('.valid_till123').append($('.valid_till123').val(data.offer_discount.valid_till));
                    $('.offer_discount_id123').append($('.offer_discount_id123').val(data.offer_discount.id));

                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });

        $(".update_offer_discount").click(function(e){
            e.preventDefault()
            // var category_id = $(this).data('id');

            var offer_discount_id= $(".offer_discount_id123").val();
            var promo_code= $(".promo_code123").val();
            var discount= $(".discount123").val();
            var valid_from= $(".valid_from123").val();
            var valid_till= $(".valid_till123").val();

            // alert(category_id1234);

            if($('.checkbox4').is(":checked"))
            {
                var cash_bill= $(".cash_bill4").val();

            }

            if($('.checkbox5').is(":checked"))
            {

                var due_bill= $(".due_bill5").val();

            }
            if($('.checkbox6').is(":checked"))
            {

                var old_client= $(".old_client6").val();

            }
            if($('.checkbox7').is(":checked"))
            {

                var new_client= $(".new_client7").val();
            }

            $.ajax({
                type: 'POST',
                url: '{{url('/update-offer-discount')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    discount : discount,
                    promo_code : promo_code,
                    offer_discount_id : offer_discount_id,
                    valid_from : valid_from,
                    valid_till : valid_till,
                    cash_bill : cash_bill,
                    due_bill : due_bill,
                    old_client : old_client,
                    new_client : new_client,
                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    if(data.success){

                        toastr.success(data.success);
                        window.location.reload();
                        console.log(data);
                    }



                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                    $('#promo_code_error123').html(err.errors.promo_code);
                    $('#discount_error123').html(err.errors.discount);


                    // if (err.errors.password) {
                    //     toastr.error(err.errors.password);
                    // }
                }
            });

        });

        $(".save_plan_pricing").click(function(e){
            e.preventDefault()
            // var category_id = $(this).data('id');
            var promo_code= $(".promo_code").val();
            var discount= $(".discount").val();
            var valid_from= $(".valid_from").val();
            var valid_till= $(".valid_till").val();


            // alert(valid_till);

            if($('.checkbox').is(":checked"))
            {
                var cash_bill= $(".cash_bill").val();

            }

            if($('.checkbox1').is(":checked"))
            {

                var due_bill= $(".due_bill").val();

            }
            if($('.checkbox2').is(":checked"))
            {

                var old_client= $(".old_client").val();

            }
            if($('.checkbox3').is(":checked"))
            {

                var new_client= $(".new_client").val();
            }
            // alert(cash_bill);

            $.ajax({
                type: 'POST',
                url: '{{url('/add-offer-discount')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    discount : discount,
                    promo_code : promo_code,
                    valid_from : valid_from,
                    cash_bill : cash_bill,
                    due_bill : due_bill,
                    old_client : old_client,
                    new_client : new_client,
                    valid_till : valid_till,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    if(data.success){

                        toastr.success(data.success);
                        window.location.reload();
                        console.log(data);

                    }



                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                    $('#promo_code_error').html(err.errors.promo_code);
                    $('#discount_error').html(err.errors.discount);


                    // if (err.errors.password) {
                    //     toastr.error(err.errors.password);
                    // }
                }
            });

        });




        //for the delete data
        $(".delete_data").click(function(e){
            e.preventDefault()

            var offer_discount_id = $(this).data('id');
            // alert(customer_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/edit-offer-discount1')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    offer_discount_id : offer_discount_id,

                },
                success: function (data, status) {

                    if(data.error){

                        return;
                    }

                    // $('#show_modal').modal('hide');
                    // toastr.success(data.success);

                    console.log(data.package.id);
                    // window.location.reload();
                    // alert('test');

                    $('.offer_discount_id123').append($('.offer_discount_id123').val(data.offer_discount.id));

                },
                error: function (xhr, status, error) {
                    // console.log('error');
                    // toastr.error(error.errors);
                }
            });

        });


        //for final delete
        $(".final_delete").click(function(e){
            e.preventDefault()

            var offer_discount_id= $(".offer_discount_id123").val();

            // alert(category_id1234);

            $.ajax({
                type: 'POST',
                url: '{{url('/delete-offer-discount')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    offer_discount_id : offer_discount_id,


                },
                success: function (data, status) {

                    if(data.errors){
                        toastr.error(data.errors);
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);

                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // toastr.error(data.errors);
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });

    </script>
@endpush
