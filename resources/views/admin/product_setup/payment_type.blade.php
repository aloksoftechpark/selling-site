@extends('admin.layouts.app')
@section('content')

    <div class="row mg-0">
        <div class="col-sm-5">
            <div class="content-header pd-l-5">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Product Setup</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Payment Type</li>
                        </ol>
                    </nav>
                    <h4 class="content-title content-title-sm">Payment Types</h4>
                </div>
            </div><!-- content-header -->
        </div><!-- col -->
        <div class="col-sm-0 tx-right col-lg-7">
            <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"
                    data-target="#exampleModalCenter2">Add Payment Type</button>
            <div class="modal fade modal_cust" id="exampleModalCenter2" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle1">ADD PAYMENT TYPE</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body salary_payroll">

                            <div class="row">
                                <div class="col-md-6">
                                    Title
                                    <input type="text" name="name" value="" class="name form-control-sm">
                                    <p id="name_error" style="color: red"></p>
                                    <div class="mg-t-5">
                                        Discount (%)
                                        <div class="input-group">
                                            <input type="text" class="discount form-control form-control-sm bd-r-0" placeholder="0.0"
                                                   aria-label="Username" aria-describedby="basic-addon1">
                                            <div class="input-group-prepend wd-25p">
                                            <span class="input-group-text form-control wd-100p justify-content-center"
                                              id="basic-addon1">%</span>
                                            </div>
                                        </div>
                                        <p id="discount_error" style="color: red"></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    No of months
                                    <input type="text" name="month" value="" class="no_of_month form-control-sm">
                                    <p id="no_of_month_error" style="color: red"></p>
                                    <div class="mg-t-5">
                                        <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-14">
                                            <button class="save_payment_type btn btn-success mg-r-10">Submit</button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                            <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                           <button type="button" class="btn btn-primary">Save changes</button> -->
                                        </div>
                                    </div>
                                </div>
                            </div>





                        </div>
                    </div>
                </div>
            </div>
        </div><!-- col -->
    </div><!-- row -->
    <div class="content-body" style="margin-top: -50px;">
        <div class="component-section">
            <div class="form-group">
                <div class="row pd-l-0">
                    <div class="col-sm-3">
                        <input type="search" class="form-control form-control-sm" placeholder="Search">
                    </div>
                </div>
            </div>
            <!--form-group-->
            <div class="table-responsive">
                <table class="table table-sm table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th class="wd-5p">SN.</th>
                        <th class="wd-40p">Title</th>
                        <th class="wd-20p">No. Of Month</th>
                        <th class="wd-20p">Discount</th>
                        <th class="wd-15p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($payment_type as $key=> $payment_types)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$payment_types->name}}</td>
                        <td>{{$payment_types->no_of_month}}</td>
                        <td>{{$payment_types->discount}}%</td>
                        <td class="d-md-flex">
                            <div class="mg-r-20" title="View"><a href="../extraPages/client-detail.html"><i
                                        class="icon ion-clipboard text-success"></i></a></div>
                            <div class="mg-r-20" title="Edit"><a href="" class="edit_payment_type" data-id="{{$payment_types->id}}" data-toggle="modal" data-target="#exampleModalCenter22"><i class="far fa-edit text-warning"></i></a></div>
                            <div class="mg-r-20" title="Cancle"><a href="" class="delete_data" data-id="{{$payment_types->id}}" data-toggle="modal" data-target="#exampleModalCenter10"><i class="icon ion-trash-b text-danger"></i></a></div>



                        </td>


                        <div class="col-sm-0 tx-right col-lg-7">
                            {{--                                <button type="button" class="btn btn-sm btn-primary mg-t-30 mg-r-20 mg-b-10" data-toggle="modal"--}}
                            {{--                                        data-target="#exampleModalCenter2">Add Payment Type</button>--}}
                            <div class="modal fade modal_cust" id="exampleModalCenter22" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle1" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle1">ADD PAYMENT TYPE</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body salary_payroll">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    Title
                                                    <input type="hidden" class="payment_method_id123">
                                                    <input type="text" name="name" value="" class="name123 form-control-sm">
                                                    <p id="name_error123" style="color: red"></p>
                                                    <div class="mg-t-5">
                                                        Discount (%)
                                                        <div class="input-group">
                                                            <input type="text" class="discount123 form-control form-control-sm bd-r-0" placeholder="0.0"
                                                                   aria-label="Username" aria-describedby="basic-addon1">
                                                            <div class="input-group-prepend wd-25p">
                                                            <span class="input-group-text form-control wd-100p justify-content-center" id="basic-addon1">%</span>
                                                            </div>
                                                        </div>
                                                        <p id="discount_error123" style="color: red"></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    No of months
                                                    <input type="text" name="month" value="" class="no_of_month123 form-control-sm">
                                                    <p id="no_of_month_error123" style="color: red"></p>
                                                    <div class="mg-t-5">
                                                        <div class="modal-footer modal-footer_footer justify-content-center footer_inline pd-b-0 pd-t-14">
                                                            <button  class="update_payment_method btn btn-success mg-r-10">Submit</button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                                            <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary">Save changes</button> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- col -->



                        {{--                            fot delete popup--}}

                        <div class="modal fade modal_cust" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal_ac_head text-left" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">ARE YOU SURE YOU WANT TO DELETE THIS ??</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <div class="conform_del">
                                            <p>After Deletiing This You Will Not Able To Recover It Again. Be Sure Before Deleting.</p>
                                        </div><!-- conform_del -->

                                        <div class="modal-footer modal-footer_footer modal-footer-right text-center conform_del_btn">

                                            <a class="final_delete btn btn-success mg-r-10" href=" ">Yes</a>
                                            <button type="button" class="btn btn-danger mg-r-10" data-dismiss="modal">Cancel</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- component-section -->

    </div><!-- content-body -->
</div><!-- content -->

@endsection

@push('scripts')

    <script>

        $(".edit_payment_type").click(function(e){
            e.preventDefault()
            var payment_method_id = $(this).data('id');

            // alert(contact_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/edit-payment-type')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    payment_type_id : payment_method_id,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // toastr.success(data.success);
                    // window.location.reload();
                    console.log(data);

                    $('.name123').append($('.name123').val(data.payment_method.name));
                    $('.no_of_month123').append($('.no_of_month123').val(data.payment_method.no_of_month));
                    $('.discount123').append($('.discount123').val(data.payment_method.discount));
                    $('.payment_method_id123').append($('.payment_method_id123').val(data.payment_method.id));

                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });

        $(".update_payment_method").click(function(e){
            e.preventDefault()
            // var category_id = $(this).data('id');
            var name= $(".name123").val();
            var no_of_month= $(".no_of_month123").val();
            var discount= $(".discount123").val();
            var payment_method_id= $(".payment_method_id123").val();

            // alert(category_id1234);

            $.ajax({
                type: 'POST',
                url: '{{url('/update-payment-type')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    payment_type_id : payment_method_id,
                    name : name,
                    no_of_month : no_of_month,
                    discount : discount,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    if(data.success){

                        toastr.success(data.success);
                        window.location.reload();
                        console.log(data);
                    }


                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                    $('#name_error123').html(err.errors.name);
                    $('#no_of_month_error123').html(err.errors.no_of_month);
                    $('#discount_error123').html(err.errors.discount);

                    // if (err.errors.password) {
                    //     toastr.error(err.errors.password);
                    // }
                }
            });

        });

        $(".save_payment_type").click(function(e){
            e.preventDefault()
            // var category_id = $(this).data('id');
            var name= $(".name").val();
            var no_of_month= $(".no_of_month").val();
            var discount= $(".discount").val();

            // alert(category_id1234);

            $.ajax({
                type: 'POST',
                url: '{{url('/add-payment-type')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    name : name,
                    no_of_month : no_of_month,
                    discount : discount,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }
                    if(data.success){

                        toastr.success(data.success);
                        window.location.reload();
                        console.log(data);
                    }

                },
                error: function (xhr, status, error) {
                    console.log('error');
                    var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                    $('#name_error').html(err.errors.name);
                    $('#no_of_month_error').html(err.errors.no_of_month);
                    $('#discount_error').html(err.errors.discount);

                    // if (err.errors.password) {
                    //     toastr.error(err.errors.password);
                    // }
                }
            });

        });




        //for the delete data

        $(".delete_data").click(function(e){
            e.preventDefault()
            var payment_method_id = $(this).data('id');

            // alert(contact_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/edit-payment-type')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    payment_type_id : payment_method_id,

                },
                success: function (data, status) {

                    if(data.error){
                        return;
                    }

                    // toastr.success(data.success);
                    // window.location.reload();
                    console.log(data);

                    $('.payment_method_id123').append($('.payment_method_id123').val(data.payment_method.id));

                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });


        //for final delete
        $(".final_delete").click(function(e){
            e.preventDefault()

            var payment_method_id= $(".payment_method_id123").val();

            alert(payment_method_id);

            $.ajax({
                type: 'POST',
                url: '{{url('/delete-payment-type')}}',
                data: {
                    _token : "{{csrf_token()}}",
                    payment_type_id : payment_method_id,


                },
                success: function (data, status) {

                    if(data.errors){
                        toastr.error(data.errors);
                        return;
                    }

                    toastr.success(data.success);
                    window.location.reload();
                    console.log(data);

                },
                error: function (xhr, status, error) {
                    console.log('error');
                    // toastr.error(data.errors);
                    // var err = JSON.parse(xhr.responseText);
                    // $('#category_error').append(err.errors.title);
                }
            });

        });

    </script>
@endpush
