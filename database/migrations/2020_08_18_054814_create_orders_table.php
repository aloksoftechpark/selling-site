<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company');
            $table->string('address');
            $table->string('contact');
            $table->string('email');
            $table->string('pan');
            $table->unsignedBigInteger('plan_id');
            $table->unsignedBigInteger('paymentdetail_id');
            $table->integer('subscription')->default(1)->comment('1-Annual, 2- half annual, 3 quaterly');
            $table->string('promo_code')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->integer('status')->default(0)->comment('0-pending, 1-approved, 2-rejected, 3-suspended');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
