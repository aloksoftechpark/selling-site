<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->unsignedBigInteger('branch_id')->nullable();
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('leadId')->unique();
            $table->string('lead')->default('a');
            $table->bigInteger('phoneNumber');
            $table->string('mailId');
            $table->string('followUpDate')->nullable();
            $table->string('followUpTime')->nullable();
            $table->string('todo')->nullable();
            $table->longtext('comment')->nullable();
            $table->string('website')->nullable();
            $table->string('address')->nullable();
            $table->string('priority')->nullable();
            $table->boolean('status')->default(1);
            $table->string('status1')->nullable();
            $table->string('addedBy')->nullable();
            $table->string('source')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
