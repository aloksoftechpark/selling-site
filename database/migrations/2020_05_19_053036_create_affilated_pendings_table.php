<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAffilatedPendingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affilated_pendings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();

            $table->string('full_name')->nullable();
            $table->string('citizenship_number')->nullable();
            $table->string('nationality')->nullable();
            $table->string('passport_number')->nullable();
            $table->string('temporary_address')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('permanent_address')->nullable();
            $table->string('father_name')->nullable();
            $table->string('email')->nullable();
            $table->string('father_contact_number')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('sex')->nullable();
            $table->string('mother_contact_number')->nullable();


            $table->string('institute_name')->nullable();
            $table->string('website')->nullable();
            $table->string('school_address')->nullable();
            $table->string('acedemic_grade')->nullable();


            $table->string('bank_name')->nullable();
            $table->string('account_number')->nullable();
            $table->string('account_holder_name')->nullable();
            $table->string('account_issued_branch_name')->nullable();

            $table->string('citizenship_front')->nullable();
            $table->string('citizenship_back')->nullable();
            $table->string('pan_certificate')->nullable();
            $table->string('personal_photo')->nullable();
            $table->string('other_document')->nullable();



            $table->string('slug')->unique();
            $table->boolean('status')->default(1);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affilated_pendings');
    }
}
