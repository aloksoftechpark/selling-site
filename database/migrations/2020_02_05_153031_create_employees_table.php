<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->unsignedBigInteger('branch_id')->nullable();
            $table->string('employeeId');
            $table->string('firstName');
            $table->string('middleName')->nullable();
            $table->string('lastName');
            $table->string('fullName');
            $table->string('slug')->unique();
            $table->string('presentAddress')->nullable();
            $table->string('permanentAddress')->nullable();
            $table->string('fatherName')->nullable();
            $table->string('motherName')->nullable();
            $table->string('fatherPhoneNumber')->nullable();
            $table->string('motherPhoneNumber')->nullable();
            $table->string('phoneNumber')->nullable();
            $table->string('mailId')->nullable();
            $table->string('citizenshipNumber')->nullable();
            $table->string('gender')->nullable();
            $table->date('dob')->nullable();
            $table->string('panNumber')->nullable();
            $table->date('joiningDate')->nullable();
            $table->string('bloodGroup')->nullable();
            $table->double('salary')->nullable();
            $table->string('designation')->nullable();
            $table->string('advanceDue')->default(0.0);
            $table->string('salaryDue')->default(0.0);
            $table->string('salaryDue1')->nullable();

            $table->string('description')->nullable();
            $table->boolean('status')->default(1);

            $table->rememberToken();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
