<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_discounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
//            $table->unsignedBigInteger('package_id')->nullable();

            $table->string('promo_code')->nullable();
            $table->string('discount')->nullable();
            $table->string('valid_from')->nullable();
            $table->string('valid_till')->nullable();
            $table->string('cash_bill')->nullable();
            $table->string('due_bill')->nullable();
            $table->string('old_client')->nullable();
            $table->string('new_client')->nullable();
            $table->string('slug')->unique();

            $table->boolean('status')->default(1);

            $table->rememberToken();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
//            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_discounts');
    }
}
