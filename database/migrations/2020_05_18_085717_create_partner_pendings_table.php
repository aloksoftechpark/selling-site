<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnerPendingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_pendings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();



            $table->string('company_name')->nullable();
            $table->string('company_email')->nullable();
            $table->string('country')->nullable();
            $table->string('partnership')->nullable();
            $table->string('state')->nullable();
            $table->string('company_website')->nullable();
            $table->string('city')->nullable();
            $table->string('telephone_number')->nullable();
            $table->string('office_address')->nullable();
            $table->string('company_established_year')->nullable();
            $table->string('pan')->nullable();
            $table->string('form_of_business')->nullable();


            $table->string('full_name')->nullable();
            $table->string('citizenship_number')->nullable();
            $table->string('nationality')->nullable();
            $table->string('passport_number')->nullable();
            $table->string('temporary_address')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('permanent_address')->nullable();
            $table->string('father_name')->nullable();
            $table->string('email')->nullable();
            $table->string('father_contact_number')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('sex')->nullable();
            $table->string('mother_contact_number')->nullable();


            $table->string('nominee_full_name')->nullable();
            $table->string('nominee_telephone_number')->nullable();
            $table->string('nominee_relationship')->nullable();
            $table->string('nominee_mobile_number')->nullable();
            $table->string('nominee_date_of_birth')->nullable();
            $table->string('nominee_email')->nullable();
            $table->string('nominee_citizenship_number')->nullable();


            $table->string('bank_name')->nullable();
            $table->string('account_number')->nullable();
            $table->string('account_holder_name')->nullable();
            $table->string('account_issued_branch_name')->nullable();



            $table->string('partner_full_name')->nullable();
            $table->string('partner_citizenship_number')->nullable();
            $table->string('partner_nationality')->nullable();
            $table->string('partner_passport_number')->nullable();
            $table->string('partner_temporary_address')->nullable();
            $table->string('partner_marital_status')->nullable();
            $table->string('partner_permanent_address')->nullable();
            $table->string('partner_father_name')->nullable();
            $table->string('partner_email')->nullable();
            $table->string('partner_father_contact_number')->nullable();
            $table->string('partner_date_of_birth')->nullable();
            $table->string('partner_mother_name')->nullable();
            $table->string('partner_sex')->nullable();
            $table->string('partner_mother_contact_number')->nullable();



            $table->string('image')->nullable();
            $table->string('citizenship_front')->nullable();
            $table->string('citizenship_back')->nullable();
            $table->string('pan_certificate')->nullable();
            $table->string('personal_photo')->nullable();
            $table->string('other_document')->nullable();



            $table->string('slug')->unique();
            $table->boolean('status')->default(1);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_pendings');
    }
}
