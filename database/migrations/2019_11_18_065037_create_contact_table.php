<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('address');
            $table->string('phone');
            $table->string('email');
            $table->string('name1')->nullable();
            $table->string('image')->nullable();
            $table->longText('message')->nullable();
            $table->string('email_id')->nullable();
            $table->string('name')->nullable();
            $table->string('address1')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
