<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('company');
            $table->string('workspace');
            $table->string('address');
            $table->string('contact');
            $table->string('email');
            $table->string('pan');
            $table->unsignedBigInteger('plan_id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->integer('status')->default(0)->comment('0-pending, 1-approved, 2-rejected, 3-expired');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demos');
    }
}
