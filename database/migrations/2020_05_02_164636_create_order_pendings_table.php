<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderPendingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_pendings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('package_id')->nullable();
            $table->string('company_name');
            $table->string('company_address');
            $table->string('slug')->unique();
            $table->string('contact_number')->nullable();
            $table->double('pan')->nullable();
            $table->string('ird_verified')->nullable();
            $table->string('package')->nullable();
            $table->longtext('plan')->nullable();
            $table->longtext('payment_type')->nullable();
            $table->longtext('payment_status')->nullable();
            $table->longtext('payment_method')->nullable();
            $table->longtext('payment_id')->nullable();
            $table->longtext('referal_code')->nullable();
            $table->longtext('promo_code')->nullable();
            $table->boolean('status')->default(0)->comment('0-pending, 1-active , 2-rejected');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_pendings');
    }
}
