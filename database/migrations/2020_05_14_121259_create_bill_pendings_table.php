<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillPendingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_pendings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('package_id')->nullable();
//            $table->unsignedBigInteger('package_id')->nullable();
            $table->unsignedBigInteger('order_pending_id')->nullable();


            $table->string('company_name')->nullable();
            $table->string('package_name')->nullable();
            $table->string('amount')->nullable();
//            $table->string('amount')->nullable();
            $table->string('discount_amount')->nullable();
            $table->string('vat_amount')->nullable();
            $table->string('total')->nullable();


            $table->string('slug')->unique();

            $table->boolean('status')->default(1);

            $table->rememberToken();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('order_pending_id')->references('id')->on('order_pendings')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_pendings');
    }
}
