<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstimatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->unsignedBigInteger('branch_id')->nullable();
            $table->unsignedBigInteger('lead_id')->nullable();

            $table->string('estimate_date')->nullable();
            $table->string('valid_date')->nullable();
            $table->string('item')->nullable();
            $table->string('quantity')->nullable();

            $table->string('rate')->nullable();
            $table->string('total')->nullable();
            $table->string('discount_percent')->nullable();
            $table->string('discount_amount')->nullable();
            $table->string('description')->nullable();
            $table->string('final_note')->nullable();
            $table->string('sub_total')->nullable();
            $table->string('additional_charge')->nullable();
            $table->string('grand_total')->nullable();


            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('lead_id')->references('id')->on('leads')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimates');
    }
}
