<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillPaidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_paids', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('payment_method_id')->nullable();
//            $table->unsignedBigInteger('package_id')->nullable();
            $table->unsignedBigInteger('bill_pending_id')->nullable();


            $table->string('date')->nullable();
            $table->string('due_amount')->nullable();
            $table->string('discount')->nullable();
//            $table->string('amount')->nullable();
            $table->string('receivable_amount')->nullable();
            $table->string('promo_code')->nullable();
            $table->string('paid_amount')->nullable();
            $table->string('paymentId')->nullable();


            $table->string('slug')->unique();

            $table->boolean('status')->default(1);

            $table->rememberToken();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('bill_pending_id')->references('id')->on('bill_pendings')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_paids');
    }
}
