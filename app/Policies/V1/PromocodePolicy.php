<?php

namespace App\Policies\V1;

use App\Models\User;
use App\Models\V1\Promocode;
use Illuminate\Auth\Access\HandlesAuthorization;

class PromocodePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any promocodes.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the promocode.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Promocode  $promocode
     * @return mixed
     */
    public function view(User $user, Promocode $promocode)
    {
        //
    }

    /**
     * Determine whether the user can create promocodes.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the promocode.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Promocode  $promocode
     * @return mixed
     */
    public function update(User $user, Promocode $promocode)
    {
        //
    }

    /**
     * Determine whether the user can delete the promocode.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Promocode  $promocode
     * @return mixed
     */
    public function delete(User $user, Promocode $promocode)
    {
        //
    }

    /**
     * Determine whether the user can restore the promocode.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Promocode  $promocode
     * @return mixed
     */
    public function restore(User $user, Promocode $promocode)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the promocode.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Promocode  $promocode
     * @return mixed
     */
    public function forceDelete(User $user, Promocode $promocode)
    {
        //
    }
}
