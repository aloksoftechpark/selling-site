<?php

namespace App\Policies\V1;

use App\Models\User;
use App\Models\V1\Validity;
use Illuminate\Auth\Access\HandlesAuthorization;

class ValidityPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any validities.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the validity.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Validity  $validity
     * @return mixed
     */
    public function view(User $user, Validity $validity)
    {
        //
    }

    /**
     * Determine whether the user can create validities.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the validity.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Validity  $validity
     * @return mixed
     */
    public function update(User $user, Validity $validity)
    {
        //
    }

    /**
     * Determine whether the user can delete the validity.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Validity  $validity
     * @return mixed
     */
    public function delete(User $user, Validity $validity)
    {
        //
    }

    /**
     * Determine whether the user can restore the validity.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Validity  $validity
     * @return mixed
     */
    public function restore(User $user, Validity $validity)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the validity.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Validity  $validity
     * @return mixed
     */
    public function forceDelete(User $user, Validity $validity)
    {
        //
    }
}
