<?php

namespace App\Policies\V1;

use App\Models\User;
use App\Models\V1\Demo;
use Illuminate\Auth\Access\HandlesAuthorization;

class DemoPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any demos.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the demo.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Demo  $demo
     * @return mixed
     */
    public function view(User $user, Demo $demo)
    {
        //
    }

    /**
     * Determine whether the user can create demos.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the demo.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Demo  $demo
     * @return mixed
     */
    public function update(User $user, Demo $demo)
    {
        //
    }

    /**
     * Determine whether the user can delete the demo.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Demo  $demo
     * @return mixed
     */
    public function delete(User $user, Demo $demo)
    {
        //
    }

    /**
     * Determine whether the user can restore the demo.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Demo  $demo
     * @return mixed
     */
    public function restore(User $user, Demo $demo)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the demo.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\V1\Demo  $demo
     * @return mixed
     */
    public function forceDelete(User $user, Demo $demo)
    {
        //
    }
}
