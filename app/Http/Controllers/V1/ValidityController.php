<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\ValidityRequest;
use App\Models\V1\Plan;
use App\Models\V1\Validity;
use Illuminate\Http\Request;

class ValidityController extends Controller
{
    private $validity;

    public function __construct(Validity $validity)
    {
        $this->validity = $validity;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.validities.index')
            ->with('validities', $this->validity->paginate(15));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidityRequest $request)
    {
        $plan= Validity::create($request->all());
        if(empty($plan)){
            return back()->with('success','Something went wrong. Please try again later.');
        }
        return back()->with('success','Validity has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\V1\Validity  $validity
     * @return \Illuminate\Http\Response
     */
    public function show(Validity $validity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\V1\Validity  $validity
     * @return \Illuminate\Http\Response
     */
    public function edit(Validity $validity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\V1\Validity  $validity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Validity $validity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\V1\Validity  $validity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Validity $validity)
    {
        //
    }
}
