<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\V1\Demo;
use App\Models\V1\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DemoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('frontend.free-trial')->with('plans',Plan::select('id','title')->get());
    }

    /**
     * Show the form for creating a new resource.
     */
    public function checkWorkspace($workspace)
    {
        if(count(Demo::where('workspace',$workspace)->get())>0){
            return response()->json("$workspace already exist. please choose another workspace.");
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $demo = Demo::create([
            'name' => $request->name,
            'company' => $request->company,
            'workspace' => $request->workspace,
            'address' => $request->address,
            'contact' => $request->contact,
            'email' => $request->email,
            'pan' => $request->pan,
            'plan_id' => $request->plan_id,
            'user_id' => Auth::user()->id,
        ]);
        return response()->json('Your trial request has been successfully submitted.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Demo $demo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\V1\Demo  $demo
     * @return \Illuminate\Http\Response
     */
    public function edit(Demo $demo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\V1\Demo  $demo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Demo $demo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\V1\Demo  $demo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Demo $demo)
    {
        //
    }
}
