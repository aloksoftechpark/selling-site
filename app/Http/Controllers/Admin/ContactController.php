<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contact;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
//        dd('test');
        $contacts =Contact::all();
        $contact = Contact::all();
        return view('admin.contact.index')
            ->with('contacts',$contacts)
            ->with('contact',$contact);
    }
    public function index1()
    {
        $conatc=Contact::all();
        return view('admin.contact.index_frontend')
            ->with('contact',$conatc);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.contact.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[

//            'address'=> 'required',
            'address'=>'required',
            'phone'=>'required',
            'email'=>'required|email',


        ]);

        $contact = new Contact() ;
//        $contacts = new Contact() ;

        $contact->address =$request->get('address');
        $contact->phone =$request->get('phone');
        $contact->email =$request->get('email');
        $contact->name1=$request->get('title');

        if($request->hasFile('image')){
            $image=$request->file('image');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $contact->image = '/uploads/topwide/'.$image_new_name;
        }
//        $contact->office_hour=$request->get('office_hour');
//        $contacts->name =$request->get('name');
//        $contacts->email_id =$request->get('email_id');
////        $contact->phone =$request->get('phone');
//        $contacts->message =$request->get('message');
//        $contact->phone =$request->get('phone');
//        $contact->message =$request->get('message');
        $contact->save();
//        $contacts->save();
        return back()->with('success','Data submitted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //
        $contact = Contact::find($id);
        return view('admin.contact.edit')->with('contact',$contact);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[

            'address'=>'required',
            'phone'=>'required',
            'email'=>'required|email',
        ]);

        $contact = Contact::find($id) ;

        if ($id)
        {
            $contact->address =$request->get('address');
            $contact->phone =$request->get('phone');
            $contact->email =$request->get('email');
            $contact->name1=$request->get('title');

            if($request->hasFile('image')){
                $image=$request->file('image');
                $image_new_name=time().$image->getClientOriginalName();
                $destination='uploads/topwide';
                $image->move($destination,$image_new_name);
                $contact->image = '/uploads/topwide/'.$image_new_name;
            }
//        $contact->office_hour=$request->get('office_hour');
//            $contact->name =$request->get('name');
//            $contact->email_id =$request->get('email_id');
////        $contact->phone =$request->get('phone');
//            $contact->message =$request->get('message');
////        $contact->phone =$request->get('phone');
//        $contact->message =$request->get('message');

        }


        $contact->save();
        return back()->with('success','Data updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $contact = Contact::find($id);
        $contact->delete();
//        return back()->with('success','data has been deleted successfully');
        return '#contact'.$id;
    }

}
