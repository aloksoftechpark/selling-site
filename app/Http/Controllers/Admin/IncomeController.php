<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AccountHead;
use App\Models\Branch;
use App\Models\Expense;
use App\Models\Income;
use App\Models\Ledger;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class IncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['income'] = Income::latest()->get();
            $data['branch']  = Branch::where('status', true)->latest()->get();
            $data['account_head']  = AccountHead::where('type', 'income')->latest()->get();

            $data['payments'] = PaymentMethod::where('status', true)->get();
//            dd($data);
        }
        if (Auth::guard('web')->check())
        {
            // dd('test');
//            $data['payments'] = PaymentMethod::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->get();
//            $data['branch'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
//            $data['account_head'] = AccountHead::where('admin_id', Auth::guard('web')->user()->admin->id)->where('type', 'income')->latest()->get();
//            $data['income'] = Income::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
        }
        return view('admin.income.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.income.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request,[


//            'name'=>'required',
//            'address'=>'required',
//            'reg_no'=>'required',
//            'phone_no'=>'required',
//            'email'=>'required',
//            'website'=>'required',
//            'opening_due'=>'required',
//            'description'=>'required',

        ]);
        $income= new Income();
//        $branch= new \App\Models\Branch();

//        if (Auth::guard('admin')->check())
//        {
//            $income->admin_id=Auth::guard('admin')->user()->id;
////            $branchId = $branch->latest()->first();
////            $employeeId = $employee->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
//        }

//        else{
//            $income->user_id=Auth::guard('web')->user()->id;
//            $income->admin_id=Auth::guard('web')->user()->admin->id;
////            $employeeId = $employee->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//        }
//        $invoice = new Invoice();

//        $branchId = $branch->latest()->first();
//        dd($employeeId);
//        if ($employeeId)
//        {
////
//            $employee->employeeId= $employeeId->employeeId +1;
////            $branchId=0;
//        }else{
//
////            dd('test');
//            $employee->employeeId=1;
//        }

        $income->date=$request->date;
        $income->branch_id=$request->branch_id;

        $income->account_head_id= $request->account_head_id;
        $income->payment_method_id= $request->payment_method_id;
        $income->amount= $request->amount;
        $income->bank_name= $request->bank_name;
        $income->transaction_id= $request->transaction_id;
        $income->purpose= $request->description;
        $income->slug=str::slug($request->fullName).time();

        $income->save();

        if ($income)
        {
            $ledger= new Ledger();

            if (Auth::guard('admin')->check())
            {
                $ledger->admin_id=Auth::guard('admin')->user()->id;

            }

            else{
                $ledger->user_id=Auth::guard('web')->user()->id;
                $ledger->admin_id=Auth::guard('web')->user()->admin->id;

            }

            $ledger->account_head_id = $request->account_head_id;
            $ledger->branch_id = $request->branch_id;
            $ledger->income_id = $income->id;
            $ledger->save();
        }


        return back()->with('success','Data has been added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function income_edit(Request $request)
    {
//         dd('test');
        $data['income']= Income::with('account_head','payment_method')->find($request->income_id);
        return $data;
    }

    public function edit($id)
    {
        //
        $income=Income::find($id);
        return view('admin.income.index')->with('income',$income);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
//        dd($request->all());
        $this->validate($request,[


//            'name'=>'required',
//            'address'=>'required',
//            'reg_no'=>'required',
//            'phone_no'=>'required',
//            'email'=>'required',
//            'website'=>'required',
//            'opening_due'=>'required',
//            'description'=>'required',

        ]);
        $income= Income::find($request->income_id);
//        $branch= new \App\Models\Branch();

        if ($request->income_id)
        {
//            if (Auth::guard('admin')->check())
//            {
//                $income->admin_id=Auth::guard('admin')->user()->id;
////            $branchId = $branch->latest()->first();
////            $employeeId = $employee->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
//            }
//
//            else{
//                $income->user_id=Auth::guard('web')->user()->id;
//                $income->admin_id=Auth::guard('web')->user()->admin->id;
////            $employeeId = $employee->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//            }
//        $invoice = new Invoice();

//        $branchId = $branch->latest()->first();
//        dd($employeeId);
//        if ($employeeId)
//        {
////
//            $employee->employeeId= $employeeId->employeeId +1;
////            $branchId=0;
//        }else{
//
////            dd('test');
//            $employee->employeeId=1;
//        }

            $income->date=$request->date;
            $income->branch_id=$request->branch_id;

            $income->account_head_id= $request->account_head_id;
            $income->payment_method_id= $request->payment_method_id;
            $income->amount= $request->amount;
            $income->bank_name= $request->bank_name;
            $income->transaction_id= $request->transaction_id;
            $income->purpose= $request->description;
            $income->slug=str::slug($request->fullName).time();

            $income->save();


        }


        $data['success']='Data has been added successfully';
        return $data;
//        return back()->with('success','Data has been added successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $income_ledger=Ledger::where('income_id',$request->income_id)->first();
//        dd($customer_sell);
//        dd($customer_pay);
        if ($income_ledger == null )
        {
//            dd('test delete');
            $income = Income::find($request->income_id);
            $income->delete();

            $data['success']='Data has been deleted successfully';

            return $data;

        }
        else
        {
//            dd('test not delete');
            $data['errors']='You cant delete data due to transaction made';

            return $data;
        }
    }
}
