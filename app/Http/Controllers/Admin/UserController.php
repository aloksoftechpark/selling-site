<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\VerifyMail;
use App\Models\OrderPending;
use App\Models\User;
use App\Models\VerifyUser;
use Illuminate\Http\Request;

use Hash;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check()) {
            $data['users'] = User::all();
        }
        if (Auth::guard('web')->check()) {

            $data['users'] = User::where('id', Auth::guard('web')->user()->id)->latest()->get();
            //            dd($data);
        }

        return view('admin.user.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(count(User::where('email',$request->email)->get())>0){
            return response()->json('This email  already exist..');
        }
        if(empty($request->email)){
            return response()->json('Please enter your email.');
        }else if(empty($request->name)){
            return response()->json('Please enter your name.');
        }else if(empty($request->phone)){
            return response()->json('Please enter your phone.');
        }else if(empty($request->password)){
            return response()->json('Please enter password.');
        }else if($request->password != $request->confirm_password){
            return response()->json("Password and confirm password doesn't match.");
        }

        $data = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'phone' => $request->phone,
            'refral_code' => $request->refral_code,
        ]);

//        $verifyUser = VerifyUser::create([
//            'user_id' => $data->id,
//            'token' => str::random(40)
//        ]);
//        Mail::send(new VerifyMail($data));
        return response()->json('We successfully sent you an activation code. Check your email and click on the link to verify');
    }

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if (isset($verifyUser)) {
            $user = $verifyUser->user;
            if (!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->email_verified_at = date('Y-m-d h:i:s');
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login.";
            } else {
                $status = "Your e-mail is already verified. You can now login.";
            }
        } else {
            //            return redirect('/user-login')->with('warning', "Sorry your email cannot be identified.");
            return redirect('/')->with('warning', "Sorry your email cannot be identified.");
        }

        return redirect('/')->with('status', $status);
        //        return redirect('/user-login')->with('status', $status);
    }
    protected function registered(Request $request, $user)
    {
        $this->guard()->logout();
        return redirect('/')->with('status', 'We sent you an activation code. Check your email and click on the link to verify.');
    }

    public function user_edit(Request $request)
    {
        //          dd($request->all());
        $data['user'] = User::find($request->user_id);

        return $data;
    }
    public function update_user(Request $request)
    {

        //         dd($request->all());
        $this->validate($request, [
            'phone' => 'required|min:8|max:20',
            'name' => 'required|min:2|max:20',
            'email' => 'required|email',
            'password' => 'required|min:5|same:confirm_password',
            //            'checkbox' => 'accepted'
        ]);
        //dd('dcd');
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['slug'] = str::slug($request->name) . time();


        $user = User::find($request->user_id);
        //        dd($user);
        if ($user) {
            if ($request->user_id) {
                //                $data = [
                //
                //                    'name' => $input['name'],
                //                    'email' => $input['email'],
                //                    'password' => $input['password'],
                //                    'phone' => $input['phone'],
                //                    'refral_code' => $input['refral_code'],
                //                    'slug' => $input['slug'],
                //                ];
                //                User::updated($data);

                $user->name = $request->name;
                $user->email = $request->email;
                $user->phone = $request->phone;
                $user->refral_code = $request->refral_code;
                $user->password = Hash::make($input['password']);
                //                $user->slug=$request->str::slug($request->name).time();

            }
            $user->save();
        }

        $data['success'] = 'Successfully user updated';
        return $data;

        //        return redirect()->route('users.index')
        //            ->with('success','User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
