<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        if (Auth::guard('admin')->check())
        {
            $data['payment_method'] = PaymentMethod::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();

        }
        if (Auth::guard('web')->check())
        {
            // dd('test');
            $data['payment_method'] =PaymentMethod::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
        }
//        $branch=Branch::all();
        return view('admin.payment_method.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        if (Auth::guard('admin')->check())
        {
            $data['payment_method'] = PaymentMethod::where('admin_id', Auth::guard('admin')->user()->id)->latest()->first();
        }
        if (Auth::guard('web')->check())
        {
            $data['payment_method'] = PaymentMethod::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->first();
        }
//        $branch=Branch::all();
        return view('admin.payment_method.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[

//            'name'=>'required',



        ]);

        $payment_method= new PaymentMethod();

        if (Auth::guard('admin')->check())
        {
            $payment_method->admin_id=Auth::guard('admin')->user()->id;
//            $branchId = $branch->latest()->first();
//            $bankId = $bank->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
        }

        else{
            $payment_method->user_id=Auth::guard('web')->user()->id;
            $payment_method->admin_id=Auth::guard('web')->user()->admin->id;
//            $bankId = $bank->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
        }
//        $invoice = new Invoice();

//        $branchId = $branch->latest()->first();
//        dd($branchId);
//        if ($bankId)
//        {
////
//            $bank->bankId= $bankId->bankId +1;
////            $branchId=0;
//        }else{
//
//            $bank->bankID=1;
//        }

//        dd($branch->branchId);
//        dd($request->all());
        $payment_method->title= $request->name;
        $payment_method->slug=str::slug($request->slug).time();
//        $bank->account_no=$request->account_no;
//        $bank->balance=$request->balance;

        $payment_method->save();
//        session()->set('success','Item created successfully.');
//        Session::flash('success','Job Created Successfully.');
        return back()->with('success','Data has been added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $payment_method= PaymentMethod::find($id);
        return view('admin.payment_method.edit')->with('payment_method',$payment_method);
    }

    public function payment_method_edit(Request $request)
    {
//         dd($request);
        $data['payment_method']= PaymentMethod::find($request->payment_method_id);
        return $data;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request,[

//            'name'=>'required',



        ]);

        $payment_method= PaymentMethod::find($request->payment_method_id);
        if ($request->payment_method_id)
        {

            if (Auth::guard('admin')->check())
            {
                $payment_method->admin_id=Auth::guard('admin')->user()->id;
//            $branchId = $branch->latest()->first();
//            $bankId = $bank->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
            }

            else{
                $payment_method->user_id=Auth::guard('web')->user()->id;
                $payment_method->admin_id=Auth::guard('web')->user()->admin->id;
//            $bankId = $bank->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
            }
//        $invoice = new Invoice();

//        $branchId = $branch->latest()->first();
//        dd($branchId);
//        if ($bankId)
//        {
////
//            $bank->bankId= $bankId->bankId +1;
////            $branchId=0;
//        }else{
//
//            $bank->bankID=1;
//        }

//        dd($branch->branchId);
//        dd($request->all());
            $payment_method->title= $request->name;
            $payment_method->slug=str::slug($request->slug).time();
//        $bank->account_no=$request->account_no;
//        $bank->balance=$request->balance;

        }

        $payment_method->save();
//        session()->set('success','Item created successfully.');
//        Session::flash('success','Job Created Successfully.');
//        return back()->with('success','Data has been added successfully');
        $data['success']='Data has been updated successfully';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $payment_method= PaymentMethod::find($id);
        $payment_method->delete();
        return back()->with('success','data has been deleted successfully');
    }
}
