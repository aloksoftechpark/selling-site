<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\SalaryType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class SalaryTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        //
        if (Auth::guard('admin')->check())
        {
            $data['salary_type'] =SalaryType::latest()->get();
            $data['branch']  = Branch::where('status', true)->latest()->get();

//            dd($data);
        }
        if (Auth::guard('web')->check())
        {
            // dd('test');
//            $data['branch'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
//            $data['salary_type'] =SalaryType ::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
        }
//        dd($data);

//        $salary_type= SalaryType::all();
        return view('admin.salary.salary_type_list',$data);
//            ->with('salary_type',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request->all());

        $this->validate($request,[

        ]);

        $salary_type= new SalaryType();
//        if (Auth::guard('admin')->check())
//        {
//            $salary_type->admin_id=Auth::guard('admin')->user()->id;
////            $branchId = $branch->latest()->first();
////            $categoryId = $category->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
//        }
//
//        else{
//            $salary_type->user_id=Auth::guard('web')->user()->id;
//            $salary_type->admin_id=Auth::guard('web')->user()->admin->id;
////            $categoryId = $category->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//        }

        $salary_type->benificial=$request->benificial;
        $salary_type->slug=str::slug($request->benificial.time());
        $salary_type->type=$request->type;
        $salary_type->save();
        return back()->with('success','Data has been added successfully');




    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
//        dd($id);
        $salary_type= SalaryType::find($id);
        $salary_type->delete();
        return back()->with('success','data has been deleted successfully');
    }
}
