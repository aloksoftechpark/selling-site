<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\PaymentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PaymentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Auth::guard('admin')->check())
        {
//            $data['users']=User::all();
            $data['payment_type'] = PaymentType::latest()->get();

        }
        if (Auth::guard('web')->check())
        {
            $data['payment_type'] = PaymentType::where('user_id', Auth::guard('web')->user()->id)->latest()->get();
//            $data['users'] = User::where('id', Auth::guard('web')->user()->id)->latest()->get();
            //dd($data);
        }
//        dd($data);
        return view('admin.product_setup.payment_type',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[

            'name'=>'required',
            'no_of_month'=>'required',
//            'discount'=>'required',

//            'payment_status'=>'required',

        ]);

        $payment_type= new PaymentType();

        $payment_type->name= $request->name;
        $payment_type->no_of_month= $request->no_of_month;
        $payment_type->discount= $request->discount;


        $payment_type->slug=str::slug($request->name).time();
        $payment_type->save();
        $data['success']='Data has been submitted successfully';

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function edit_payment_type(Request $request)
    {
//         dd($request);
        $data['payment_method']= PaymentType::find($request->payment_type_id);
        $data['payment_method123']= PaymentType::where('id',$request->payment_type_id)->get();
        $payment_types=$data['payment_method123'];

        $dis=$payment_types[0]->discount;
        $no_of_month=$payment_types[0]->no_of_month;
        $data['dis']=$dis;
        //if no price

        $price=(($request->price) * ($no_of_month));
        $data['price']=$price;
//        dd($price);

        $discount_amount= ($dis * $price)/100;
//        dd($discount_amount);
        $data['discount_amount']=$discount_amount;



        $total= $price - $discount_amount;
        $data['total']= $total;
        $vat_percent=13;
        $vat_amount=($total * $vat_percent)/100;
        $data['vat_amount']= $vat_amount;
        $final_total=$vat_amount + $total;
        $data['final_total']= $final_total;

//        $data['vat_amount321']= $request->vat_amount;
//        $data['final_total321']= $request->total;
//        dd($discount_amount);
//dd($data);
        return $data;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request,[

            'name'=>'required',
            'no_of_month'=>'required',
//            'discount'=>'required',

//            'payment_status'=>'required',

        ]);

        $payment_type= PaymentType::find($request->payment_type_id);

        if ($request->payment_type_id)
        {
            $payment_type->name= $request->name;
            $payment_type->no_of_month= $request->no_of_month;
            $payment_type->discount= $request->discount;

            $payment_type->slug=str::slug($request->name).time();
        }

        $payment_type->save();
        $data['success']='Data has been submitted successfully';

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
//        dd($request->all());
//        $plans_pricing=Ledger::where('package_id',$request->package_id)->first();
//        $customer_pay=SupplierCustomerPayment::where('customer_id',$request->customer_id)->first();
//        dd($customer_sell);
//        dd($customer_pay);
//        if ($plans_pricing == null)
//        {
//            dd('test delete');
            $payment_type = PaymentType::find($request->payment_type_id);
            $payment_type->delete();

            $data['success']='Data has been deleted successfully';
            return $data;
//
//        }
//        else
//        {
//            dd('test not delete');
//            $data['errors']='You cant delete data due to transaction made';
//            return $data;
//        }
//
//    }
    }
}
