<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PartnerPending;
use App\Models\StudentPending;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PartnerPendingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['users']=User::all();
            $data['partner_pending'] = PartnerPending::with('user')->where('status',1)->latest()->get();

        }
        if (Auth::guard('web')->check())
        {

            $data['partner_pending'] = PartnerPending::with('user')->where('user_id', Auth::guard('web')->user()->id)->where('status',1)->latest()->get();
            $data['users'] = User::where('id', Auth::guard('web')->user()->id)->latest()->get();

//
        }
        return view('admin.partners.pending_partners',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['users']=User::all();
            $data['partner_pending'] = PartnerPending::with('user')->where('status',1)->latest()->get();

        }
        if (Auth::guard('web')->check())
        {

            $data['partner_pending'] = PartnerPending::with('user')->where('user_id', Auth::guard('web')->user()->id)->where('status',1)->latest()->get();
            $data['users'] = User::where('id', Auth::guard('web')->user()->id)->latest()->get();

//
        }
        return view('admin.partners.partners_form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request->all());

        $this->validate($request,[

//            'company_name'=>'required',
//            'pan'=>'required',
//            'package'=>'required',
//            'payment_status'=>'required',

        ]);

        $partner_pending= new PartnerPending();
        if (Auth::guard('web')->check())
        {
            $user_id=Auth::guard('web')->user()->id;
            $partner_pending->user_id= $user_id;
        }
        if (Auth::guard('admin')->check())
        {

            $partner_pending->user_id= $request->user_id;
        }

        $partner_pending->company_name= $request->company_name;
        $partner_pending->company_email= $request->company_email;
        $partner_pending->country= $request->country;
        $partner_pending->partnership= $request->partnership;
        $partner_pending->state= $request->state;
        $partner_pending->company_website= $request->company_website;
        $partner_pending->city= $request->city;
        $partner_pending->telephone_number= $request->telephone_number;
        $partner_pending->office_address= $request->office_address;
        $partner_pending->company_established_year= $request->company_established_year;
        $partner_pending->pan= $request->pan;
        $partner_pending->form_of_business= $request->form_of_business;


        $partner_pending->full_name= $request->full_name;
        $partner_pending->citizenship_number= $request->citizenship_number;
        $partner_pending->nationality= $request->nationality;
        $partner_pending->passport_number= $request->passport_number;
        $partner_pending->temporary_address= $request->temporary_address;
        $partner_pending->marital_status= $request->marital_status;
        $partner_pending->permanent_address= $request->permanent_address;
        $partner_pending->father_name= $request->father_name;
        $partner_pending->email= $request->email;
        $partner_pending->father_contact_number= $request->father_contact_number;
        $partner_pending->date_of_birth= $request->date_of_birth;
        $partner_pending->mother_name= $request->mother_name;
        $partner_pending->sex= $request->sex;
        $partner_pending->mother_contact_number= $request->mother_contact_number;
        $partner_pending->mobile_number= $request->mobile_number;




        $partner_pending->nominee_full_name= $request->nominee_full_name;
        $partner_pending->nominee_telephone_number= $request->nominee_telephone_number;
        $partner_pending->nominee_relationship= $request->nominee_relationship;
        $partner_pending->nominee_mobile_number= $request->nominee_mobile_number;
        $partner_pending->nominee_date_of_birth= $request->nominee_date_of_birth;
        $partner_pending->nominee_email= $request->nominee_email;
        $partner_pending->nominee_citizenship_number= $request->nominee_citizenship_number;


        $partner_pending->bank_name= $request->bank_name;
        $partner_pending->account_number= $request->account_number;
        $partner_pending->account_holder_name= $request->account_holder_name;
        $partner_pending->account_issued_branch_name= $request->account_issued_branch_name;



        $partner_pending->partner_full_name= $request->partner_full_name;
        $partner_pending->partner_citizenship_number= $request->partner_citizenship_number;
        $partner_pending->partner_nationality= $request->partner_nationality;
        $partner_pending->partner_passport_number= $request->partner_passport_number;
        $partner_pending->partner_temporary_address= $request->partner_temporary_address;
        $partner_pending->partner_marital_status= $request->partner_marital_status;
        $partner_pending->partner_permanent_address= $request->partner_permanent_address;
        $partner_pending->partner_father_name= $request->partner_father_name;
        $partner_pending->partner_email= $request->partner_email;
        $partner_pending->partner_father_contact_number= $request->partner_father_contact_number;
        $partner_pending->partner_date_of_birth= $request->partner_date_of_birth;
        $partner_pending->partner_mother_name= $request->partner_mother_name;
        $partner_pending->partner_sex= $request->partner_sex;
        $partner_pending->partner_mother_contact_number= $request->partner_mother_contact_number;



        $partner_pending->slug=str::slug($request->company_name).time();

        if($request->hasFile('image')){
            $image=$request->file('image');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $partner_pending->image = '/uploads/topwide/'.$image_new_name;
        }

        if($request->hasFile('citizenship_front')){
            $image=$request->file('citizenship_front');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $partner_pending->citizenship_front = '/uploads/topwide/'.$image_new_name;
        }
        if($request->hasFile('citizenship_back')){
            $image=$request->file('citizenship_back');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $partner_pending->citizenship_back = '/uploads/topwide/'.$image_new_name;
        }
        if($request->hasFile('pan_certificate')){
            $image=$request->file('pan_certificate');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $partner_pending->pan_certificate = '/uploads/topwide/'.$image_new_name;
        }
        if($request->hasFile('personal_photo')){
            $image=$request->file('personal_photo');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $partner_pending->personal_photo = '/uploads/topwide/'.$image_new_name;
        }
        if($request->hasFile('other_document')){
            $image=$request->file('other_document');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $partner_pending->other_document = '/uploads/topwide/'.$image_new_name;
        }


        $partner_pending->save();

        return back()->with('success','Data has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
