<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AffilatedPending;
use App\Models\StudentPending;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AffilatedPendingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['users']=User::all();
            $data['affilated_pending'] = AffilatedPending::with('user')->where('status',1)->latest()->get();

        }
        if (Auth::guard('web')->check())
        {

            $data['affilated_pending'] = AffilatedPending::with('user')->where('user_id', Auth::guard('web')->user()->id)->where('status',1)->latest()->get();
            $data['users'] = User::where('id', Auth::guard('web')->user()->id)->latest()->get();

//
        }
        return view('admin.affilated.pending_affilated',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['users']=User::all();
            $data['affilated_pending'] = AffilatedPending::with('user')->where('status',1)->latest()->get();

        }
        if (Auth::guard('web')->check())
        {

            $data['affilated_pending'] = AffilatedPending::with('user')->where('user_id', Auth::guard('web')->user()->id)->where('status',1)->latest()->get();
            $data['users'] = User::where('id', Auth::guard('web')->user()->id)->latest()->get();

//
        }
        return view('admin.affilated.affilated_form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $this->validate($request,[

//            'company_name'=>'required',
//            'pan'=>'required',
//            'package'=>'required',
//            'payment_status'=>'required',

        ]);

        $student_pending= new AffilatedPending();
        if (Auth::guard('web')->check())
        {
            $user_id=Auth::guard('web')->user()->id;
            $student_pending->user_id= $user_id;
        }
        if (Auth::guard('admin')->check())
        {

            $student_pending->user_id= $request->user_id;
        }

        $student_pending->full_name= $request->full_name;
        $student_pending->citizenship_number= $request->citizenship_number;
        $student_pending->nationality= $request->nationality;
        $student_pending->passport_number= $request->passport_number;
        $student_pending->temporary_address= $request->temporary_address;
        $student_pending->marital_status= $request->marital_status;
        $student_pending->permanent_address= $request->permanent_address;
        $student_pending->father_name= $request->father_name;
        $student_pending->email= $request->email;
        $student_pending->father_contact_number= $request->father_contact_number;
        $student_pending->date_of_birth= $request->date_of_birth;
        $student_pending->mother_name= $request->mother_name;
        $student_pending->sex= $request->sex;
        $student_pending->mother_contact_number= $request->mother_contact_number;
        $student_pending->mobile_number= $request->mobile_number;


        $student_pending->institute_name= $request->institute_name;
        $student_pending->website= $request->website;
        $student_pending->school_address= $request->school_address;
        $student_pending->acedemic_grade= $request->acedemic_grade;


        $student_pending->bank_name= $request->bank_name;
        $student_pending->account_number= $request->account_number;
        $student_pending->account_holder_name= $request->account_holder_name;
        $student_pending->account_issued_branch_name= $request->account_issued_branch_name;




        $student_pending->slug=str::slug($request->full_name).time();

        if($request->hasFile('citizenship_front')){
            $image=$request->file('citizenship_front');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $student_pending->citizenship_front = '/uploads/topwide/'.$image_new_name;
        }
        if($request->hasFile('citizenship_back')){
            $image=$request->file('citizenship_back');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $student_pending->citizenship_back = '/uploads/topwide/'.$image_new_name;
        }
        if($request->hasFile('pan_certificate')){
            $image=$request->file('pan_certificate');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $student_pending->pan_certificate = '/uploads/topwide/'.$image_new_name;
        }
        if($request->hasFile('personal_photo')){
            $image=$request->file('personal_photo');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $student_pending->personal_photo = '/uploads/topwide/'.$image_new_name;
        }
        if($request->hasFile('other_document')){
            $image=$request->file('other_document');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $student_pending->other_document = '/uploads/topwide/'.$image_new_name;
        }

        $student_pending->save();

        return back()->with('success','Data has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
