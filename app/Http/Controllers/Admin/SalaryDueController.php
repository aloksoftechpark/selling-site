<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Branch;
use App\Models\Employee;
use App\Models\PaymentMethod;
use App\Models\SalaryPayroll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SalaryDueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['branch']  = Branch::where('admin_id', Auth::guard('admin')->user()->id)->where('status', true)->latest()->get();
            $data['employee1'] =\App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['employee12'] =\App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->first();
            $data['payment_method'] = PaymentMethod::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['bank'] = Bank::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();

//            dd($data);
        }
        if (Auth::guard('web')->check())
        {
            // dd('test');
            $data['branch'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
            $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['employee1'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['payment_method'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
            $data['bank'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
        }
        return view('admin.salary.salary_due_list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        dd($request->all());


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function employee_edit(Request $request)
    {
//        dd($request);
        $salary_master= Employee::find($request->employee_id);
//        dd($salary_master);
        return $salary_master;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
//        dd('test');
//        dd($request->all());

        $branch_name=$request->branch_name;

        $employees= Employee::all();
        if (Auth::guard('admin')->check())
        {
            $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['branch']  = Branch::where('admin_id', Auth::guard('admin')->user()->id)->where('status', true)->latest()->get();
            $data['employee1'] =\App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['payment_method'] = PaymentMethod::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['bank'] = Bank::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();

//            dd($data);
        }
        if (Auth::guard('web')->check())
        {
            // dd('test');
            $data['branch'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
            $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['employee1'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['payment_method'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
            $data['bank'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
        }
        $salary_due=SalaryPayroll::Where('branch_id', 'LIKE', "%{$branch_name}%")
            ->get() ;
//            dd($lead_management);
        return view('admin.salary.search_due',$data)-> with('salary_due',$salary_due);
//        return response()->json($salary_payroll);

    }


}
