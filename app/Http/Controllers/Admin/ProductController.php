<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $category=Category::all();
        $product = Product::all();
        return view('admin.product.index')
            ->with('category',$category)
            ->with('product',$product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $category=Category::all();
        return view('admin.product.create')
            ->with('category',$category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request->all());
        $this->validate($request,[
            'title'=>'required',
            'description'=>'required',
            'image'=>'required',

        ]);


        $product= new Product() ;

        $product->title=$request->get('title');
        if($request->hasFile('image')){
            $image=$request->file('image');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $product->image = '/uploads/topwide/'.$image_new_name;
        }
        $product->description =$request->get('description');
//        $product->category_id=$request->category_id;






        $product->save();
        return back()->with('success','Data has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category=Category::all();
        $product = Product::find($id);
        return view('admin.product.edit')
            ->with('category',$category)
            ->with('product',$product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[

            'title'=>'required',
            'description'=>'required',
            'image'=>'required',

        ]);

        $product = Product::find($id) ;
        if($id)
        {   $product->title=$request->get('title');
            if($request->hasFile('image')){
                $image=$request->file('image');
                $image_new_name=time().$image->getClientOriginalName();
                $destination='uploads/topwide';
                $image->move($destination,$image_new_name);
                $product->image = '/uploads/topwide/'.$image_new_name;
            }
            $product->description =$request->get('description');

//            $product->category_id=$request->category_id;




        }


        $product->save();
        return back()->with('success','Data update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $product= Product::find($id);
        $product->delete();
//        return back()->with('success','data has been deleted successfully');
        return '#product'.$id;
    }

}
