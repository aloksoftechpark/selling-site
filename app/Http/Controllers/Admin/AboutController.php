<?php

namespace App\Http\Controllers\Admin;

use App\Models\About;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $about = About::all();
        return view('admin.about.index')
            ->with('about',$about);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.about.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
//            'title'=>'required',
            'description'=>'required',
//            'image'=>'required',

//            'service_title'=>'required',
//            'service_description'=>'required',
//            'service_image'=>'required',

//            'history'=>'required',
//            'team_image'=>'required',
        ]);


        $about = new About() ;

        $about->title=$request->get('title');
        $about->title_client=$request->get('title1');
//        if($request->hasFile('image')){
//            $image=$request->file('image');
//            $image_new_name=time().$image->getClientOriginalName();
//            $destination='uploads/topwide';
//            $image->move($destination,$image_new_name);
//            $about->image = '/uploads/topwide/'.$image_new_name;
//        }
        $about->description =$request->get('description');
        $about->description_client =$request->get('description1');



//        $about->service_title=$request->get('service_title');
//        if($request->hasFile('service_image')){
//            $service_image=$request->file('service_image');
//            $service_image_new_name=time().$service_image->getClientOriginalName();
//            $destination='uploads/topwide';
//            $service_image->move($destination,$service_image_new_name);
//            $about->service_image = '/uploads/topwide/'.$service_image_new_name;
//        }
//        $about->service_description =$request->get('service_description');
//

        $about->history =$request->get('history');
//        if($request->hasFile('team_image')){
//            $team_image=$request->file('team_image');
//            $team_image_new_name=time().$team_image->getClientOriginalName();
//            $destination='uploads/topwide';
//            $team_image->move($destination,$team_image_new_name);
//            $about->team_image = '/uploads/topwide/'.$team_image_new_name;
//        }




        $about->save();
        return back()->with('success','Data has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $about = About::find($id);
        return view('admin.about.edit')->with('about',$about);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'title'=>'required',
            'description'=>'required',
            'history'=>'required',
//
        ]);


        $about = About::find($id);
        if ($id)
        {

            $about->title=$request->get('title');
//
            $about->description =$request->get('description');


            $about->history =$request->get('history');
//
        }


        $about->save();
        return back()->with('success','Data has been added successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $about = About::find($id);
        $about->delete();

        return '#about'.$id;

    }
}
