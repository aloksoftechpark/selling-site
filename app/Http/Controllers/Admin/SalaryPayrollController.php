<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Branch;
use App\Models\Employee;
use App\Models\Ledger;
use App\Models\LedgerCustomer;
use App\Models\LedgerEmployee;
use App\Models\PaymentMethod;
use App\Models\SalaryPayroll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class SalaryPayrollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check())

        {
            $data['salary_payroll'] = SalaryPayroll::latest()->where('status',1)->get();
            $data['employees'] = \App\Models\Employee::latest()->get();
            $data['branch']  = Branch::where('status', true)->latest()->get();
            $data['employee1'] =\App\Models\Employee::latest()->get();
            $data['payment_method'] = PaymentMethod::latest()->get();
            $data['bank'] = Bank::latest()->get();

//            dd($data);
        }
        if (Auth::guard('web')->check())
        {
            // dd('test');

//            $data['salary_payroll'] = SalaryPayroll::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
//            $data['branch'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
//            $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
//            $data['employee1'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
//            $data['payment_method'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
//            $data['bank'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
        }
//        $employee= Employee::all();
        return view('admin.salary.salary_payroll_list', $data);
//            ->with('employee',$employee);


//            ->with('employees',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.salary.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request )
    {

//        dd($request->all());

        $this->validate($request,[


//            'name'=>'required',
//            'address'=>'required',
//            'reg_no'=>'required',
//            'phone_no'=>'required',
//            'email'=>'required',
//            'website'=>'required',
//            'opening_due'=>'required',
//            'description'=>'required',

        ]);

        //payroll if paying amount

        if ($request->paying_amount)
        {
            $salary_payroll= new  SalaryPayroll();


            if (Auth::guard('admin')->check())
            {
//                $salary_payroll->admin_id=Auth::guard('admin')->user()->id;
//            $branchId = $branch->latest()->first();
                $paymentId = $salary_payroll->latest()->first();
            }

//            else{
//                $salary_payroll->user_id=Auth::guard('web')->user()->id;
//                $salary_payroll->admin_id=Auth::guard('web')->user()->admin->id;
//                $paymentId = $salary_payroll->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//            }

//        dd($employeeId);
            if ($paymentId)
            {
                $salary_payroll->paymentId= $paymentId->paymentId +1;

            }else{

                $salary_payroll->paymentId=1;
            }

            $salary_payroll->branch_id=$request->branch_id;

            $salary_payroll->employee_id= $request->employee_id;


//            $salary_payroll->advanceAmount= $request->advance_amount;
            $salary_payroll->slug=str::slug('payroll').time();
            $salary_payroll->paymentType=$request->payment_type;
            $salary_payroll->bankName=$request->bank_name;
            $salary_payroll->payingAmount= $request->paying_amount;
            $salary_payroll->totalDue= $request->total_due;
            $salary_payroll->remainingDue= $request->remaining_due;
            $salary_payroll->created_date= $request->created_date;
//        $salary_payroll->advanceAmount= $request->advance_deduction;

            $salary_payroll->save();
            //for employee ledger

            if($salary_payroll)
            {

                if ($request->paying_amount)
                {
                    $ledger_employee= new LedgerEmployee();

                    if (Auth::guard('admin')->check())
                    {
                        $ledger_employee->admin_id=Auth::guard('admin')->user()->id;

                    }

                    else{
                        $ledger_employee->user_id=Auth::guard('web')->user()->id;
                        $ledger_employee->admin_id=Auth::guard('web')->user()->admin->id;

                    }

                    $ledger_employee->employee_id = $request->employee_id;
                    $ledger_employee->branch_id = $request->branch_id;
                    $ledger_employee->salary_payroll_id = $salary_payroll->id;

                    if (Auth::guard('admin')->check())
                    {

                        $ledger_employee1= LedgerEmployee::where('employee_id',$request->employee_id)->orderBy('id', 'desc')->latest()->first();
//                        dd($ledger_employee1);
                        if ($ledger_employee1)
                        {
                            if ($request->paying_amount)
                            {
//                        dd('test1');
                                $ledger_employee->balance=$ledger_employee1->balance  - $request->paying_amount;
//                                dd($ledger_employee);
                            }

                        }
                        else{

                            $employee123=Employee::find($request->employee_id);
                            $ledger_employee->balance=$employee123->salaryDue1 - $request->paying_amount;
                        }
                    }
                    if (Auth::guard('web')->check())
                    {

                        $ledger_employee1= LedgerEmployee::where('employee_id',$request->employee_id)->where('admin_id', Auth::guard('web')->user()->admin->id)->orderBy('id', 'desc')->latest()->first();

                        if ($ledger_employee1)
                        {
                            if ($request->paying_amount)
                            {
//                        dd('test1');
                                $ledger_employee->balance=$ledger_employee1->balance  - $request->paying_amount  ;
                            }

                        }
                        else{

                            $employee123=Employee::find($request->employee_id);
                            $ledger_employee->balance=$employee123->salaryDue1 - $request->paying_amount;
                        }
                    }

                    $ledger_employee->save();
                }
            }

            //for report in journal
            if($salary_payroll)
            {
                $ledger= new Ledger();

                if (Auth::guard('admin')->check())
                {
                    $ledger->admin_id=Auth::guard('admin')->user()->id;

                }

                else{
                    $ledger->user_id=Auth::guard('web')->user()->id;
                    $ledger->admin_id=Auth::guard('web')->user()->admin->id;

                }

//            $ledger->customer_id = $request->customerId;
                $ledger->branch_id = $request->branch_id;
                $ledger->salary_payroll_id = $salary_payroll->id;
                $ledger->save();
            }


            $employee= Employee::find($request->employee_id);
//        dd($employee);
            if ($request->employee_id)
            {
                if (Auth::guard('admin')->check())
                {
//                dd('test');
                    $employee->admin_id=Auth::guard('admin')->user()->id;
//            $branchId = $branch->latest()->first();
//                $employeeId = $employee->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
                }

                else{
                    $employee->user_id=Auth::guard('web')->user()->id;
                    $employee->admin_id=Auth::guard('web')->user()->admin->id;
//                $employeeId = $employee->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
                }

//                $employee->advanceDue = $request->advance_amount + $employee->advanceDue ;

                $employee->salaryDue= $request->total_due - $request->paying_amount ;
            }



            $employee->save();
        }


        //if salary payroll for advance amount case
        if ($request->advance_amount)
        {
            $salary_payroll= new  SalaryPayroll();


            if (Auth::guard('admin')->check())
            {
                $salary_payroll->admin_id=Auth::guard('admin')->user()->id;
//            $branchId = $branch->latest()->first();
                $paymentId = $salary_payroll->latest()->first();
            }

//            else{
//                $salary_payroll->user_id=Auth::guard('web')->user()->id;
//                $salary_payroll->admin_id=Auth::guard('web')->user()->admin->id;
//                $paymentId = $salary_payroll->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//            }


                if ($paymentId)
                {
                    $salary_payroll->paymentId= $paymentId->paymentId +1;

                }else{

                    $salary_payroll->paymentId=1;
                }


            $salary_payroll->branch_id=$request->branch_id;

            $salary_payroll->employee_id= $request->employee_id;


            $salary_payroll->advanceAmount= $request->advance_amount;
            $salary_payroll->slug=str::slug('payroll2').time();
            $salary_payroll->paymentType=$request->payment_type;
            $salary_payroll->bankName=$request->bank_name;
//            $salary_payroll->payingAmount= $request->paying_amount;
            $salary_payroll->totalDue= $request->total_due;
//            $salary_payroll->remainingDue= $request->remaining_due;
            $salary_payroll->created_date= $request->created_date;
//        $salary_payroll->advanceAmount= $request->advance_deduction;

            $salary_payroll->save();


//for employee ledger case of advance amount and due
            if($salary_payroll)
            {
                if ($request->advance_amount)
                {
//                dd('test2');
                    $ledger_employee= new LedgerEmployee();

                    if (Auth::guard('admin')->check())
                    {
                        $ledger_employee->admin_id=Auth::guard('admin')->user()->id;

                    }

                    else{
                        $ledger_employee->user_id=Auth::guard('web')->user()->id;
                        $ledger_employee->admin_id=Auth::guard('web')->user()->admin->id;

                    }

                    $ledger_employee->employee_id = $request->employee_id;
                    $ledger_employee->branch_id = $request->branch_id;
                    $ledger_employee->salary_payroll_id = $salary_payroll->id;

                    if (Auth::guard('admin')->check())
                    {

                        $ledger_employee1= LedgerEmployee::where('employee_id',$request->employee_id)->orderBy('id', 'desc')->latest()->first();

                        if ($ledger_employee1)
                        {

                            if ($request->advance_amount)
                            {
//                            dd('test2');
                                $ledger_employee->balance=$ledger_employee1->balance   - $request->advance_amount;
                            }

                        }
                        else{

                            $employee123=Employee::find($request->employee_id);

                            $ledger_employee->balance=$employee123->salaryDue1 - $request->advance_amount;
                        }
                    }
                    if (Auth::guard('web')->check())
                    {

                        $ledger_employee1= LedgerEmployee::where('employee_id',$request->employee_id)->where('admin_id', Auth::guard('web')->user()->admin->id)->orderBy('id', 'desc')->latest()->first();

                        if ($ledger_employee1)
                        {

                            if ($request->advance_amount)
                            {
//                            dd('test2');
                                $ledger_employee->balance=$ledger_employee1->balance   - $request->advance_amount;
                            }

                        }
                        else{

                            $employee123=Employee::find($request->employee_id);

                            $ledger_employee->balance=$employee123->salaryDue1 - $request->advance_amount;
                        }
                    }

                    $ledger_employee->save();
                }


            }

            //for report in journal
            if($salary_payroll)
            {
                $ledger= new Ledger();

                if (Auth::guard('admin')->check())
                {
                    $ledger->admin_id=Auth::guard('admin')->user()->id;

                }

                else{
                    $ledger->user_id=Auth::guard('web')->user()->id;
                    $ledger->admin_id=Auth::guard('web')->user()->admin->id;

                }

//            $ledger->customer_id = $request->customerId;
                $ledger->branch_id = $request->branch_id;
                $ledger->salary_payroll_id = $salary_payroll->id;
                $ledger->save();
            }


            $employee= Employee::find($request->employee_id);
//        dd($employee);
            if ($request->employee_id)
            {
                if (Auth::guard('admin')->check())
                {
//                dd('test');
                    $employee->admin_id=Auth::guard('admin')->user()->id;
//            $branchId = $branch->latest()->first();
//                $employeeId = $employee->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
                }

                else{
                    $employee->user_id=Auth::guard('web')->user()->id;
                    $employee->admin_id=Auth::guard('web')->user()->admin->id;
//                $employeeId = $employee->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
                }

                $employee->advanceDue = $request->advance_amount + $employee->advanceDue ;

//                $employee->salaryDue= $request->total_due - $request->paying_amount ;
            }



            $employee->save();
        }





        $data['success'] = 'Data has been Updated successfully';
        return $data;
//        return back()->with('success','Data has been Updated successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function employee_edit(Request $request)
    {
//        dd($request);
        $salary_master= Employee::find($request->employee_id);
//        dd($salary_master);
        return $salary_master;
    }
    public function payroll_edit(Request $request)
    {
//        dd($request);
        $salary_master= SalaryPayroll::find($request->salary_payrollId);
//        dd($salary_master);
        return $salary_master;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function search(Request $request)
    {
//        dd('test');
//        dd($request->all());

        $branch_name=$request->branch_name;

        $employees= Employee::all();
        if (Auth::guard('admin')->check())

        {
            $data['salary_payroll'] = SalaryPayroll::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['branch']  = Branch::where('admin_id', Auth::guard('admin')->user()->id)->where('status', true)->latest()->get();
            $data['employee1'] =\App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['payment_method'] = PaymentMethod::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['bank'] = Bank::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();

//            dd($data);
        }
        if (Auth::guard('web')->check())
        {
            // dd('test');

            $data['salary_payroll'] = SalaryPayroll::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['branch'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
            $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['employee1'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['payment_method'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
            $data['bank'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
        }

        $salary_payroll=SalaryPayroll::Where('branch_id', 'LIKE', "%{$branch_name}%")
            ->get() ;
//            dd($lead_management);
        return view('admin.salary.search',$data)-> with('salary_payroll',$salary_payroll);
//        return response()->json($salary_payroll);

    }

    public function ChangeStatus(Request $request)
    {

//        dd('ds');
        $salary_payroll = SalaryPayroll::findOrFail($request->salary_payroll_id);
        $salary_payroll->status = $request->status;
        $salary_payroll->save();
        $employee=Employee::find($request->employee_id);
        $employee->salaryDue =($employee->salaryDue + ($salary_payroll->totalDue -  $salary_payroll->remainingDue));
        $employee->save();

        $data['success'] =  "Status Changed Successfully";
        return $data;
    }


}
