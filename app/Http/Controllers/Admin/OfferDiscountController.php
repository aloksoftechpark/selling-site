<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OfferDiscount;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class OfferDiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check())
        {
//            $data['users']=User::all();
            $data['offer_discount'] = OfferDiscount::latest()->get();

        }
        if (Auth::guard('web')->check())
        {

            $data['offer_discount'] = OfferDiscount::where('user_id', Auth::guard('web')->user()->id)->latest()->get();
//            $data['users'] = User::where('id', Auth::guard('web')->user()->id)->latest()->get();
//dd($data);
        }
        return view('admin.product_setup.offer_discount',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[

            'promo_code'=>'required',
            'discount'=>'required',

//            'payment_status'=>'required',

        ]);

        $offer_discount= new OfferDiscount();

        $offer_discount->promo_code= $request->promo_code;
        $offer_discount->discount= $request->discount;
        $offer_discount->valid_from= $request->valid_from;
        $offer_discount->valid_till= $request->valid_till;
        $offer_discount->cash_bill= $request->cash_bill;
        $offer_discount->due_bill= $request->due_bill;
        $offer_discount->old_client= $request->old_client;
        $offer_discount->new_client= $request->new_client;

        $offer_discount->slug=str::slug('offer').time();
        $offer_discount->save();
        $data['success']='Data has been submitted successfully';

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
//        dd($request->all());
        $this->validate($request,[

            'promo_code'=>'required',
            'discount'=>'required',

//            'payment_status'=>'required',

        ]);

        $offer_discount= OfferDiscount::find($request->offer_discount_id);
        if($request->offer_discount_id)
        {
            $offer_discount->promo_code= $request->promo_code;
            $offer_discount->discount= $request->discount;
            $offer_discount->valid_from= $request->valid_from;
            $offer_discount->valid_till= $request->valid_till;
            $offer_discount->cash_bill= $request->cash_bill;
            $offer_discount->due_bill= $request->due_bill;
            $offer_discount->old_client= $request->old_client;
            $offer_discount->new_client= $request->new_client;

            $offer_discount->slug=str::slug('offer').time();
        }


        $offer_discount->save();
        $data['success']='Data has been submitted successfully';

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //


        $offer_discount = OfferDiscount::find($request->offer_discount_id);
        $offer_discount->delete();
        $data['success']='Data has been deleted successfully';
        return $data;
    }

    public function edit_offer_discount1(Request $request)
    {
//         dd($request->all());
//        $data['package'] = Package::latest()->get();
        $data['offer_discount']= OfferDiscount::find($request->offer_discount_id);


//        $data['offer_discount123']= OfferDiscount::where('promo_code',$request->promo_code)->get();
//        $offer_discount123=$data['offer_discount123'];
//
//
////        dd($dis);
//        if ($offer_discount123)
//        {
//            $dis=$offer_discount123[0]->discount;
//            $data['dis']=$dis;
//            $discount_amount123= ($dis * $request->price)/100;
//
//            $discount_amount= $discount_amount123 + $request->discount_amount;
//            $data['discount_amount']=$discount_amount;
//
//
//            $total= $request->price - $discount_amount;
//            $data['total']= $total;
//            $vat_percent=13;
//            $vat_amount=($total * $vat_percent)/100;
//            $data['vat_amount']= $vat_amount;
//            $final_total=$vat_amount + $total;
//            $data['final_total']= $final_total;
//        }

//        dd($data);
        return $data;
    }


    public function edit_offer_discount(Request $request)
    {
//         dd($request->all());
//        $data['package'] = Package::latest()->get();
        $data['offer_discount']= OfferDiscount::find($request->offer_discount_id);


        $data['offer_discount123']= OfferDiscount::where('promo_code',$request->promo_code)->get();
        $offer_discount123=$data['offer_discount123'];


//        dd($dis);
        if ($offer_discount123)
        {
            $dis=$offer_discount123[0]->discount;
            $data['dis']=$dis;
            $price=(($request->price) - ($request->discount_amount));
//            dd($price);
            $discount_amount12= ($dis * $price)/100;
            $discount_amount123=$discount_amount12 + $request->discount_amount;
//            dd($discount_amount123);
//            $discount_amount= $discount_amount123 + $request->discount_amount;
            $data['discount_amount']=$discount_amount123;
//           $discount_amount = $data['discount_amount'];


            $total= $price - $discount_amount12;
//            $total= $price;
            $data['total']= $total;
            $vat_percent=13;
            $vat_amount=($total * $vat_percent)/100;
            $data['vat_amount']= $vat_amount;
            $final_total=$vat_amount + $total;
            $data['final_total']= $final_total;
        }

//        dd($data);
        return $data;
    }


    public function edit_offer_discount2(Request $request)
    {
//         dd($request->all());

        $data['offer_discount123']= OfferDiscount::where('promo_code',$request->promo_code)->get();
        $offer_discount123=$data['offer_discount123'];


//        dd($dis);
        if ($offer_discount123)
        {
            $dis=$offer_discount123[0]->discount;
            $data['dis']=$dis;
            $discount_amount= ($dis * $request->price)/100;
            $data['discount_amount']=$discount_amount;

            $total= $request->price - $discount_amount;
            $data['total']= $total;

        }

//        dd($data);
        return $data;
    }
}
