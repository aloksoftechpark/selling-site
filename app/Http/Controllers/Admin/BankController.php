<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

//        dd($request->all());
        if (Auth::guard('admin')->check())
        {
            $data['bank1'] = Bank::latest()->first();
            $data['bank'] = Bank::latest()->get();

        }
        if (Auth::guard('web')->check())
        {
            // dd('test');
//            $data['bank1'] = Bank::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->first();
//            $data['bank'] = Bank::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
        }
//        $branch=Branch::all();
        return view('admin.bank.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['bank'] = Bank::latest()->first();
        }
        if (Auth::guard('web')->check())
        {
//            $data['bank'] = Bank::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->first();
        }

        return view('admin.bank.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request->all());
        $this->validate($request,[

//            'name'=>'required',




        ]);

        $bank= new Bank();

        if (Auth::guard('admin')->check())
        {
//            $bank->admin_id=Auth::guard('admin')->user()->id;
//            $branchId = $branch->latest()->first();
            $bankId = $bank->latest()->first();
        }

//        else{
//            $bank->user_id=Auth::guard('web')->user()->id;
//            $bank->admin_id=Auth::guard('web')->user()->admin->id;
//            $bankId = $bank->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//        }
//        $invoice = new Invoice();

//        $branchId = $branch->latest()->first();
//        dd($branchId);
        if ($bankId)
        {
//
            $bank->bankId= $bankId->bankId +1;
//            $branchId=0;
        }else{

            $bank->bankID=1;
        }

//        dd($branch->branchId);
//        dd($request->all());
        $bank->name= $request->name;
        $bank->slug=str::slug($request->name).time();
        $bank->account_no=$request->account_no;
        $bank->balance=$request->balance;

        $bank->save();
//        session()->set('success','Item created successfully.');
//        Session::flash('success','Job Created Successfully.');
//        return redirect()->route('bill-show');
        return back()->with('success','Data has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $bank= Bank::find($id);
        return view('admin.bank.edit')->with('bank',$bank);

    }

    public function bank_edit(Request $request)
    {
//         dd($request);
        $data['bank']= Bank::find($request->bank_id);
        return $data;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        //
        $this->validate($request,[

//            'name'=>'required',



        ]);

        $bank= Bank::find($request->bank_id);
        if ($request->bank_id)
        {

//            if (Auth::guard('admin')->check())
//            {
//                $bank->admin_id=Auth::guard('admin')->user()->id;
////            $branchId = $branch->latest()->first();
////                $bankId = $bank->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
//            }
//
//            else{
//                $bank->user_id=Auth::guard('web')->user()->id;
//                $bank->admin_id=Auth::guard('web')->user()->admin->id;
////                $bankId = $bank->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//            }
//        $invoice = new Invoice();

//        $branchId = $branch->latest()->first();
//        dd($branchId);
//            if ($bankId)
//            {
////
//                $bank->bankId= $bankId->bankId +1;
////            $branchId=0;
//            }else{
//
//                $bank->bankID=1;
//            }

//        dd($branch->branchId);
//        dd($request->all());
            $bank->bankId=$request->bankId;
            $bank->name= $request->name;
            $bank->slug=str::slug($request->name).time();
            $bank->account_no=$request->account_no;
            $bank->balance=$request->balance;

        }

        $bank->save();
//        session()->set('success','Item created successfully.');
//        Session::flash('success','Job Created Successfully.');
        return back()->with('success','Data has been updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $bank = Bank::find($id);
        $bank->delete();
        return back()->with('success','data has been deleted successfully');
    }
}
