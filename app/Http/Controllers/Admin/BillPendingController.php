<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BillPaid;
use App\Models\BillPending;
use App\Models\OrderPending;
use App\Models\PaymentMethod;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BillPendingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['payment_method'] = PaymentMethod::latest()->get();
        if (Auth::guard('admin')->check())
        {
            $data['users']=User::all();
            $data['bill_pending'] = BillPending::with('user','package','order_pending')->where('status',1)->latest()->get();

        }
        if (Auth::guard('web')->check())
        {

            $data['bill_pending'] = BillPending::with('user','package','order_pending')->where('user_id', Auth::guard('web')->user()->id)->where('status',1)->latest()->get();
            $data['users'] = User::where('id', Auth::guard('web')->user()->id)->latest()->get();

//
        }
        return view('admin.bill.pending_bill',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data['bill_pending']=BillPending::with('package','order_pending','payment_type','package.plan_pricing')->find($id);
        $data['bill_paid']=BillPaid::with('payment_method')->where('bill_pending_id',$id)->get();
        $bill_paid=BillPaid::with('payment_method')->where('bill_pending_id',$id)->get();
//        dd($bill_paid[0]->payment_method->title);
        return view('admin.bill.show_bill',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function bill_pending_edit(Request $request)
    {
//         dd($request);

        $data['bill_pending']= BillPending::find($request->bill_pending_id);

        return $data;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
//        dd($request->all());
        $this->validate($request,[

            'cancel_reason'=>'required',
        ]);
//        dd('test');
        $bill_pending = BillPending::findOrFail($request->bill_pending_id);
//        dd($bill_pending);
        if ($bill_pending)
        {
//            dd('test');
            $bill_pending->status = $request->status;
            $bill_pending->cancel_reason = $request->cancel_reason;
            $bill_pending->save();
        }

        $data['success'] =  "Bill Canceled Successfully";
        return $data;
    }
}
