<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
//use App\Product;
use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
//        $product = Product::all();
        $service = Service::all();
        return view('admin.service.index')
//            ->with('product',$product)
            ->with('service',$service);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'title'=>'required',
            'description'=>'required',
            'image'=>'required',

        ]);


        $service = new Service() ;

        $service->title=$request->get('title');
        if($request->hasFile('image')){
            $image=$request->file('image');
            $image_new_name=time().$image->getClientOriginalName();
            $destination='uploads/topwide';
            $image->move($destination,$image_new_name);
            $service->image = '/uploads/topwide/'.$image_new_name;
        }
        $service->description =$request->get('description');






        $service->save();
        return back()->with('success','Data has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $service= Service::find($id);
        return view('admin.service.edit')->with('service',$service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'title'=>'required',
            'description'=>'required',
            'image'=>'required',

        ]);


        $service =Service::find($id) ;
        if ($id)
        {
            $service->title=$request->get('title');
            if($request->hasFile('image')){
                $image=$request->file('image');
                $image_new_name=time().$image->getClientOriginalName();
                $destination='uploads/topwide';
                $image->move($destination,$image_new_name);
                $service->image = '/uploads/topwide/'.$image_new_name;
            }
            $service->description =$request->get('description');


        }


        $service->save();
        return back()->with('success','Data has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $service = Service::find($id);
        $service->delete();
//        return back()->with('success','data has been deleted successfully');
        return '#service'.$id;
    }
}
