<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BillPending;
use App\Models\OrderPending;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class OrderPendingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::guard('admin')->check()) {
            $users = User::all();
            $order_pending = OrderPending::with('user', 'package')->where('status', 0)->latest()->get();
        }
        if (Auth::guard('web')->check()) {
            $order_pending = OrderPending::with('user', 'package')->where(['user_id' => Auth::guard('web')->user()->id, 'status' => 0])->latest()->get();
            $users = User::where('id', Auth::guard('web')->user()->id)->latest()->get();
        }
        return view('admin.order.pending_order', compact(['users', 'order_pending']));
    }
    public function getApprovedOrder()
    {
        if (Auth::guard('admin')->check()) {
            $users = User::all();
            $order_approved = OrderPending::with('user', 'package')->where('status', 1)->latest()->get();
        }
        if (Auth::guard('web')->check()) {
            $order_approved = OrderPending::with('user', 'package')->where(['user_id' => Auth::guard('web')->user()->id, 'status' => 1])->latest()->get();
            $users = User::where('id', Auth::guard('web')->user()->id)->latest()->get();
        }
        return view('admin.order.approved_order', compact(['users', 'order_approved']));
    }
    public function getRejectedOrder()
    {
        if (Auth::guard('admin')->check()) {
            $users = User::all();
            $order_rejected = OrderPending::with('user', 'package')->where('status', 2)->latest()->get();
        }
        if (Auth::guard('web')->check()) {
            $order_rejected = OrderPending::with('user', 'package')->where(['user_id' => Auth::guard('web')->user()->id, 'status' => 2])->latest()->get();
            $users = User::where('id', Auth::guard('web')->user()->id)->latest()->get();
        }
        return view('admin.order.rejected_order', compact(['users', 'order_rejected']));
    }
    public function getSuspendedOrder()
    {
        if (Auth::guard('admin')->check()) {
            $users = User::all();
            $order_suspended = OrderPending::with('user', 'package')->where('status', 3)->latest()->get();
        }
        if (Auth::guard('web')->check()) {
            $order_suspended = OrderPending::with('user', 'package')->where(['user_id' => Auth::guard('web')->user()->id, 'status' => 3])->latest()->get();
            $users = User::where('id', Auth::guard('web')->user()->id)->latest()->get();
        }
        return view('admin.order.suspended_order', compact(['users', 'order_suspended']));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function saveOrder(Request $request)
    {
        $this->validate($request, [
            'company_name' => 'required',
            'company_address' => 'required',
            'contact_number' => 'required',
            'pan' => 'required',
            'package' => 'required',
        ]);
        $order_pending = OrderPending::create([
            'user_id' => Auth::guard('web')->user()->id,
            'package_id' => $request->package,
            'company_name' => $request->company_name,
            'company_address' => $request->company_address,
            'slug' => str::slug($request->company_name) . time(),
            'contact_number' => $request->contact_number,
            'pan' => $request->pan,
            'ird_verified' => $request->ird_yes ? $request->ird_yes : $request->ird_no,
            'plan' => $request->plan,
            'payment_type' => $request->plan_type,
            'payment_status' => $request->payment_yes ? $request->payment_yes : $request->payment_no,
            'payment_method' => $request->payment_method,
            'payment_id' => $request->payment_id,
            'referal_code' => $request->referral_code,
            'promo_code' => $request->promo_code,
        ]);
        return response()->json('Data has been submitted successfully');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'company_name' => 'required',
            'pan' => 'required',
            'package' => 'required',
        ]);
        OrderPending::create([
            'user_id' => $request->user_id,
            'package' => $request->package,
            'company_name' => $request->company_name,
            'company_address' => $request->company_address,
            'slug' => str::slug($request->company_name) . time(),
            'contact_number' => $request->contact_number,
            'pan' => $request->pan,
            'ird_verified' => $request->ird_yes ? $request->ird_yes : $request->ird_no,
            'plan' => $request->plan,
            'payment_type' => $request->payment_type,
            'payment_status' => $request->payment_yes ? $request->payment_yes : $request->payment_no,
            'payment_method' => $request->payment_method,
            'payment_id' => $request->payment_id,
            'referal_code' => $request->referral_code,
            'promo_code' => $request->promo_code,
        ]);
        return back()->with('success', 'Data has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function order_pending_edit(Request $request)
    {
        $data['order_pending'] = OrderPending::with('user')->find($request->order_pending_id);
        return $data;
    }
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'company_name' => 'required',
            'company_address' => 'required',
            'contact_number' => 'required',
            'pan' => 'required',
            'package' => 'required',
        ]);
        OrderPending::where('id', $id)->update([
            'user_id' => $request->user_id,
            'package' => $request->package,
            'company_name' => $request->company_name,
            'company_address' => $request->company_address,
            'slug' => str::slug($request->company_name) . time(),
            'contact_number' => $request->contact_number,
            'pan' => $request->pan,
            'plan' => $request->plan,
            'payment_type' => $request->payment_type,
        ]);
        return response()->json('Order details updated successfully');
    }
    public function setOrderStatus(Request $request, $id)
    {
        $order = OrderPending::find($id);
        if (!empty($order)) {
            $order->status = $request->status;
            $order->save();
            return response()->json($request->status == 1 ? 'Activated successfully' : 'Rejected successfully');
        }
        return response()->json('Trying to updated unknown order.');
    }
    public function generateBill($order)
    {
        BillPending::create([
            'user_id' => Auth::guard('web')->user()->id,
            'package_id' => $order->package,
            'order_pending_id' => $order->id,
            'company_name' => $order->company_name,
            'package_name' => $order->package_name,
            'amount' => $order->amount,
            'discount_amount' => $order->discount_amount,
            'vat_amount' => $order->vat_amount,
            'total' => $order->total,
            'slug' => str::slug($order->company_name) . time(),
        ]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
