<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DemoPending;
use App\Models\OrderPending;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class DemoPendingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (Auth::guard('admin')->check())
        {
            $users=User::all();
            $demo_pending = DemoPending::with('user')->where('status',0)->latest()->get();
        }
        if (Auth::guard('web')->check())
        {
            $demo_pending = DemoPending::with('user')->where(['user_id'=> Auth::guard('web')->user()->id,'status'=>0])->latest()->get();
            $users = User::where('id', Auth::guard('web')->user()->id)->latest()->get();
        }
        return view('admin.demo.pending_request',compact(['demo_pending','users']));
    }
    public function getApprovedDemo()
    {
        if (Auth::guard('admin')->check())
        {
            $users=User::all();
            $demo_approved = DemoPending::with('user')->where('status',1)->latest()->get();
        }
        if (Auth::guard('web')->check())
        {
            $users = User::where('id', Auth::guard('web')->user()->id)->latest()->get();
            $demo_approved = DemoPending::with('user')->where(['user_id'=> Auth::guard('web')->user()->id,'status'=>1])->latest()->get();
        }
        return view('admin.demo.approved_request',compact(['demo_approved','users']));
    }
    public function getRejectedDemo()
    {
        if (Auth::guard('admin')->check())
        {
            $users=User::all();
            $demo_rejected = DemoPending::with('user')->where('status',2)->latest()->get();
        }
        if (Auth::guard('web')->check())
        {
            $users = User::where('id', Auth::guard('web')->user()->id)->latest()->get();
            $demo_rejected = DemoPending::with('user')->where(['user_id'=> Auth::guard('web')->user()->id,'status'=>2])->latest()->get();
        }
        return view('admin.demo.rejected_request',compact(['demo_rejected','users']));
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */

    public function store_free_trial(Request $request)
    {
        $this->validate($request,[
            'company_name'=>'required',
            'pan'=>'required',
            'package'=>'required',
        ]);

        DemoPending::create([
            'user_id'=>Auth::guard('web')->user()->id,
            'company_name'=>$request->company_name,
            'company_address'=>$request->company_address,
            'slug'=>str::slug($request->company_name).time(),
            'contact_number'=>$request->contact_number,
            'pan'=>$request->pan,
            'package'=>$request->package
        ]);
        return response()->json('Demo request has been submitted successfully');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'company_name'=>'required',
            'pan'=>'required',
            'package'=>'required',
        ]);

        DemoPending::create([
            'user_id'=>$request->user_id,
            'company_name'=>$request->company_name,
            'company_address'=>$request->company_address,
            'slug'=>str::slug($request->company_name).time(),
            'contact_number'=>$request->contact_number,
            'pan'=>$request->pan,
            'package'=>$request->package
        ]);
        return back()->with('success','Demo request has been submitted successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //
    }

    public function demo_pending_edit(Request $request)
    {
//         dd($request);
        $data['demo_pending']= DemoPending::with('user')->find($request->demo_pending_id);

//        $demo_pending_id=$request->demo_pending_id;
//        $data['demo_pending']=DemoPending::with(['user' => function($query) use($demo_pending_id){
//
//            $query->where('id', $demo_pending_id);
//
//        }])->get();
        return $data;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        DemoPending::where('id',$id)->update([
            'company_address'=>$request->company_address,
            'company_name'=>$request->company_name,
            'contact_number'=>$request->contact_number,
            'package'=>$request->package,
            'pan'=>$request->pan,
            'user_id'=>$request->user_id,
        ]);
        return response()->json('Data updated successfully.');
    }
    public function setDemoStatus(Request $request, $id)
    {
        $demo=DemoPending::find($id);
        if(!empty($demo)){
            $demo->status=$request->status;
            $demo->save();
            return response()->json($request->status==1?'Activated successfully':'Rejected successfully');
        }
        return response()->json('Trying to updated unknown request.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
