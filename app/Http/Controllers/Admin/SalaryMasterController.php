<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Branch;
use App\Models\Employee;
use App\Models\Month;
use App\Models\PaymentMethod;
use App\Models\SalaryMaster;
use App\Models\SalaryPayroll;
use App\Models\SalaryType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class SalaryMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $currentMonth = SalaryMaster::latest()->pluck('month')->first();



        if (Auth::guard('admin')->check())
        {
            $currentMonth = SalaryMaster::where('admin_id', Auth::guard('admin')->user()->id)->latest()->pluck('month')->first();
            $parts = explode("-", $currentMonth);
//            $data['month'] = $parts[1];

            $data['salary_masters'] = SalaryMaster::where('admin_id', Auth::guard('admin')->user()->id)->where('month',$currentMonth)->latest()->get();
            $data['salary_types'] =SalaryType::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['salary_types1'] =SalaryType::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['branch']  = Branch::where('admin_id', Auth::guard('admin')->user()->id)->where('status', true)->latest()->get();
            $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['branch']  = Branch::where('admin_id', Auth::guard('admin')->user()->id)->where('status', true)->latest()->get();
            $data['employee1'] =\App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->first();
        }
        if (Auth::guard('web')->check())
        {
            $currentMonth = SalaryMaster::where('admin_id', Auth::guard('admin')->user()->admin->id)->latest()->pluck('month')->first();
            $parts = explode("-", $currentMonth);
            $data['month'] = $parts[1];
            $data['salary_masters'] = SalaryMaster::where('admin_id', Auth::guard('web')->user()->admin->id)->where('month', $currentMonth)->latest()->get();
            $data['branch'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
            $data['salary_types'] =SalaryType::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['salary_types1'] =SalaryType::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['employee1'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->first();
        }
        return view('admin.salary.salary_master_list',$data);
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        // dd($request->all());


        $this->validate($request,[


//            'name'=>'required',
//            'address'=>'required',
//            'reg_no'=>'required',
//            'phone_no'=>'required',
//            'email'=>'required',
//            'website'=>'required',
//            'opening_due'=>'required',
//            'description'=>'required',

        ]);
        $salary_master= new  SalaryMaster();


        if (Auth::guard('admin')->check())
        {
            $salary_master->admin_id=Auth::guard('admin')->user()->id;
//            $branchId = $branch->latest()->first();
//                $employeeId = $employee->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
        }

        else{
            $salary_master->user_id=Auth::guard('web')->user()->id;
            $salary_master->admin_id=Auth::guard('web')->user()->admin->id;
//                $employeeId = $employee->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
        }


        $salary_master->employee_id= $request->employee_id;


        $salary_master->basicSalary= $request->basic_salary;
        $salary_master->slug=str::slug('master').time();
        $salary_master->advanceDue=$request->advance_due;
        $salary_master->advanceDeduction=$request->advance_deduction;
        $salary_master->netSalary= $request->net_salary;
        $salary_master->deduction= $request->deduction;
        $salary_master->addation= $request->addation;
        $salary_master->month=$request->month;
//        $salary_payroll->advanceAmount= $request->advance_deduction;





        $salary_master->save();

        $employee= Employee::find($request->employee_id);
//        dd($employee);
        if ($request->employee_id)
        {
            if (Auth::guard('admin')->check())
            {
//                dd('test');
                $employee->admin_id=Auth::guard('admin')->user()->id;
//            $branchId = $branch->latest()->first();
//                $employeeId = $employee->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
            }

            else{
                $employee->user_id=Auth::guard('web')->user()->id;
                $employee->admin_id=Auth::guard('web')->user()->admin->id;
//                $employeeId = $employee->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
            }

            $employee->advanceDue = $request->advance_due - $request->advance_deduction;

            $employee->salaryDue= $request->net_salary + $employee->salaryDue;
            $employee->month=$request->month;
        }



        $employee->save();

        $data['success'] = 'Data has been Updated successfully';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $salary_master= SalaryMaster::find($id);
        return $salary_master;
    }

    public function employee_edit(Request $request)
    {
       // dd($request);
        $data['salary_master']= Employee::find($request->employee_id);
        $data['month'] = $request->month;
        $data['masterId'] = $request->masterId;
//        dd($salary_master);
        return $data;
    }

    public function update(Request $request, $id)
    {
        dd($request->all());
        $this->validate($request,[


        ]);
        $employee= \App\Models\Employee::find($id);
        if ($id)
        {
            if (Auth::guard('admin')->check())
            {
                $employee->admin_id=Auth::guard('admin')->user()->id;
//            $branchId = $branch->latest()->first();
                $employeeId = $employee->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
            }

            else{
                $employee->user_id=Auth::guard('web')->user()->id;
                $employee->admin_id=Auth::guard('web')->user()->admin->id;
                $employeeId = $employee->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
            }
//        $invoice = new Invoice();

//        $branchId = $branch->latest()->first();
//        dd($employeeId);
            if ($employeeId)
            {
//
                $employee->employeeId= $employeeId->employeeId +1;
//            $branchId=0;
            }else{

//            dd('test');
                $employee->employeeId=1;
            }
//        $invoice = new Invoice();
//        $lastInvoiceID = $invoice->orderBy('id', DESC)->pluck('id')->first();
            $employee->branch_id=$request->branch_id;

            $employee->employeeId= $request->employeeId;


            $employee->fullName= $request->fullName;
            $employee->slug=str::slug($request->fullName).time();
//        $product->product_category_id= $request->category_id;

            $employee->presentAddress= $request->presentAddress;
            $employee->permanentAddress= $request->permanentAddress;
            $employee->mailId= $request->email;
            $employee->phoneNumber= $request->phoneNumber;
            $employee->citizenshipNumber= $request->citizenshipNumber;
            $employee->dob= $request->dob;
            $employee->panNumber= $request->pan_number;
            $employee->bloodGroup= $request->bloodGroup;
//        dd($request->bloodGroup);
            $employee->fatherName= $request->fatherName;
            $employee->fatherPhoneNumber= $request->fatherPhoneNumber;
            $employee->motherName= $request->motherName;
            $employee->motherPhoneNumber= $request->motherPhoneNumber;
            $employee->joiningDate= $request->joiningDate;
            $employee->salary= $request->salary;
            $employee->designation= $request->designation;
            $employee->description= $request->description;

//            if ($employee->advanceDetuction!)
            $employee->advanceDue = $request->advance_due+ $employee->advanceDue ;

            $employee->salaryDue= $request->salary_due + $employee->salaryDue;
        }



        $employee->save();


        return back()->with('success','Data has been Updated successfully');
    }


    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
        // dd($request->all());


        $branch_name=$request->branch_name;
        $date=$request->academicYear.'-'.$request->month;


        if (Auth::guard('admin')->check())
        {
            if ($request->branch_id) {
            $data['salary_masters'] = SalaryMaster::where('admin_id', Auth::guard('admin')->user()->id)->where('month', $date)->where('branch_id', $request->branch_id)->latest()->get();
            // // $data['salary_masters'] = SalaryMaster::where('admin_id', Auth::guard('admin')->user()->id)->whereYear('month', '=', $request->date('Y'))->whereMonth('month', '=', $request->date('M'))->where('branch_id', $request->branch_id)->latest()->get();
            // dd('fhdska');
            $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->where('branch_id', $request->branch_id)->latest()->get();

            // dd($data);
            }else{
               $data['salary_masters'] = SalaryMaster::where('admin_id', Auth::guard('admin')->user()->id)->where('month', $date)->latest()->get();
               // $data['salary_masters'] = SalaryMaster::where('admin_id', Auth::guard('admin')->user()->id)->whereYear('month', '=', $request->date('Y'))->whereMonth('month', '=', $request->date('M'))->latest()->get();
               $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            }


            $data['salary_types'] =SalaryType::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['salary_types1'] =SalaryType::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['branch']  = Branch::where('admin_id', Auth::guard('admin')->user()->id)->where('status', true)->latest()->get();
            $data['employee1'] =\App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->first();
            $data['payment_method'] = PaymentMethod::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
           $data['bank'] = Bank::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();


        }
        if (Auth::guard('web')->check())
        {
            if ($request->branch_id) {
                $data['salary_masters'] = SalaryMaster::where('user_id', Auth::guard('web')->user()->admin->id)->where('month', $date)->where('branch_id', $request->branch_id)->latest()->get();
                $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->where('branch_id', $request->branch_id)->latest()->get();
            }else{
                $data['salary_masters'] = SalaryMaster::where('user_id', Auth::guard('web')->user()->admin->id)->where('month', $date)->latest()->get();
                $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->where('branch_id', $request->branch_id)->latest()->get();
            }

            $data['branch'] = Branch::where('user_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
            $data['employee1'] = \App\Models\Employee::where('user_id', Auth::guard('web')->user()->admin->id)->latest()->first();
           $data['payment_method'] = Branch::where('user_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
           $data['bank'] = Branch::where('user_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();

        }

        if ($request->branch_id) {
            $data['selectedBranch'] = Branch::findOrFail($request->branch_id);
        }
        if ($request->month) {
            $data['month'] = $request->month;
        }


        // dd($data);

//         $data['employees']=Employee::Where('branch_id', 'LIKE', "%{$branch_name}%")
// //            ->Where('month', 'LIKE', "%{$month}%")
//             ->get() ;

        return view('admin.salary.salary_master_list',$data);
        // return view('admin.salary.search_master',$data)-> with('employees',$employees);

    }


    public function month (Request $request)
    {

        //
//        dd($request->all());

        $this->validate($request,[

            'month' => 'required'

        ]);
        $month= new SalaryMaster();


        if (Auth::guard('admin')->check())
        {
            $month->admin_id=Auth::guard('admin')->user()->id;
        }

        else{
            $month->user_id=Auth::guard('web')->user()->id;
            $month->admin_id=Auth::guard('web')->user()->admin->id;
        }

        $employees =Employee::all();


        foreach ($employees as $employee)
        {
            $month->employee_id= $employee->id;
            $month->month=$request->month;
            $month->save();

        }



        $data['success'] = 'Data has been Updated successfully';
        return $data;

       }

       //store to salary master
    public function storeEmployee(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'month' => 'required',

        ]);
        $employees = Employee::all();
        $salaryMaster = new SalaryMaster();
        $data = [];
        if (Auth::guard('admin')->check()) {
            for($i = 0; $i < count($employees); $i++){
            $data = [
                    'month' => $request->academicYear.'-'.$request->month,
                    // 'branch_id' => $request->branch_id,
                    'admin_id' => Auth::guard('admin')->user()->id,
                    'employee_id' => $employees[$i]->id,
                    'branch_id' => $employees[$i]->branch_id,
                ];
                SalaryMaster::create($data);
            }
        }

        if(Auth::guard('web')->check())
        {
            for($i = 0; $i < count($employees); $i++){
            $data = [
                    'month' => $request->academicYear.'-'.$request->month,
                    'user_id' => Auth::guard('web')->user()->id,
                    'employee_id' => $employees[$i]->id,
                    'branch_id' => $employees[$i]->branch_id,
                ];
                SalaryMaster::create($data);
            }
        }



        $employee = SalaryMaster::where('employee_id', $request->employee_id)->latest()->first();
        // dd($employee);

        if ($employee) {
            // dd($employee);
           $employee->branch_id = $request->branch_id;
            $employee->basicSalary = $request->basic_salary;
            $employee->advanceDue = $request->advance_due;
            $employee->netSalary = $request->net_salary;
            $employee->advanceDeduction = $request->advance_deduction;
            $employee->deduction = $request->deduction;
            $employee->addation = $request->addation;
            $employee->save();
        }


        $data['success'] = 'Updated Successfully';

        return $data;
    }


    public function updateMasterSalary(Request $request)
    {
        // dd('fhdsklahfdnslkjdjfndcxdsfddddd');

        $salary_master= SalaryMaster::findOrFail($request->salaryMasterId);

        if (!$salary_master) {
            return 'Not Found Record';
        }


        if (Auth::guard('admin')->check())
        {
            $salary_master->admin_id=Auth::guard('admin')->user()->id;
//            $branchId = $branch->latest()->first();
//                $employeeId = $employee->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
        }

        else{
            $salary_master->user_id=Auth::guard('web')->user()->id;
            $salary_master->admin_id=Auth::guard('web')->user()->admin->id;
//                $employeeId = $employee->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
        }


        $salary_master->basicSalary= $request->basic_salary;
        $salary_master->slug=str::slug('master').time();
        $salary_master->advanceDue=$request->advance_due;
        $salary_master->advanceDeduction=$request->advance_deduction;
        $salary_master->netSalary= $request->net_salary;
        $salary_master->deduction= $request->deduction;
        $salary_master->addation= $request->addation;





        $salary_master->save();

        $employee= Employee::find($request->employee_id);

        if (!$employee) {
            return 'not found';
        }

        $employee->advanceDue = $request->advance_due - $request->advance_deduction;

//            $employee->salaryDue= $request->net_salary;
        $employee->salaryDue= $request->net_salary + $employee->salaryDue;



        $employee->save();

        $data['success'] = 'Data has been Updated successfully';
        return $data;
    }
}
