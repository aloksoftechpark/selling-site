<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\PlanPricing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PlanPricingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check())
        {
//            $data['users']=User::all();
            $data['plan_pricing'] = PlanPricing::latest()->get();
            $data['package'] = Package::latest()->get();

        }
        if (Auth::guard('web')->check())
        {

            $data['package'] = Package::where('user_id', Auth::guard('web')->user()->id)->latest()->get();
            $data['plan_pricing'] = PlanPricing::where('user_id', Auth::guard('web')->user()->id)->latest()->get();
//            $data['users'] = User::where('id', Auth::guard('web')->user()->id)->latest()->get();
//dd($data);
        }
        return view('admin.product_setup.plan_pricing',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[

            'name'=>'required',
            'price'=>'required',
            'no_of_branch'=>'required',
            'no_of_user'=>'required',
            'package'=>'required',

//            'payment_status'=>'required',

        ]);

        $plan_pricing= new PlanPricing();

        $plan_pricing->name= $request->name;
        $plan_pricing->price= $request->price;
        $plan_pricing->package_id= $request->package;
        $plan_pricing->no_of_user= $request->no_of_user;
        $plan_pricing->no_of_branch= $request->no_of_branch;
        $plan_pricing->slug=str::slug($request->name).time();
        $plan_pricing->save();
        $data['success']='Data has been submitted successfully';

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function edit_plan_pricing1(Request $request)
    {
//         dd($request);
        $data['package'] = Package::latest()->get();
        $data['plan_pricing']= PlanPricing::with('package')->find($request->plan_price_id);
//        $data['plan_pricing123']= PlanPricing::with('package')->where('package_id',$request->package_id)->get();
//        $plan_pricing123=$data['plan_pricing123'];
//        $total=$plan_pricing123[0]->price;
//        $data['total']= $total;
//        $vat_percent=13;
//        $vat_amount=($total * $vat_percent)/100;
//        $data['vat_amount']= $vat_amount;
//        $final_total=$vat_amount + $total;
//        $data['final_total']= $final_total;
//        dd($data);
        return $data;
    }

    public function edit_plan_pricing(Request $request)
    {
//         dd($request);
        $data['package'] = Package::latest()->get();
        $data['plan_pricing']= PlanPricing::with('package')->find($request->plan_price_id);
        $data['plan_pricing123']= PlanPricing::with('package')->where('package_id',$request->package_id)->get();
        $plan_pricing123=$data['plan_pricing123'];
        $total=$plan_pricing123[0]->price;
        $data['total']= $total;
        $vat_percent=13;
        $vat_amount=($total * $vat_percent)/100;
        $data['vat_amount']= $vat_amount;
        $final_total=$vat_amount + $total;
        $data['final_total']= $final_total;
//        dd($data);
        return $data;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request,[

            'name'=>'required',
            'price'=>'required',
            'no_of_branch'=>'required',
            'no_of_user'=>'required',

//            'payment_status'=>'required',

        ]);

        $plan_pricing= PlanPricing::find($request->plan_price_id);

        if ($request->plan_price_id)
        {
            $plan_pricing->name= $request->name;
            $plan_pricing->price= $request->price;
            $plan_pricing->package_id= $request->package;
            $plan_pricing->no_of_user= $request->no_of_user;
            $plan_pricing->no_of_branch= $request->no_of_branch;
            $plan_pricing->slug=str::slug($request->name).time();
        }

        $plan_pricing->save();
        $data['success']='Data has been submitted successfully';

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {


        $plan_pricing = PlanPricing::find($request->plan_price_id);
        $plan_pricing->delete();
        $data['success']='Data has been deleted successfully';
        return $data;
    }
}
