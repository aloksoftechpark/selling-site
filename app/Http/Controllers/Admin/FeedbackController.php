<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DemoPending;
use App\Models\Feedback;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['users']=User::all();
            $data['feedback'] = Feedback::latest()->get();

        }
        if (Auth::guard('web')->check())
        {

            $data['feedback'] = Feedback::where('user_id', Auth::guard('web')->user()->id)->latest()->get();
            $data['users'] = User::where('id', Auth::guard('web')->user()->id)->latest()->get();
//dd($data);
        }
        return view('admin.feedback.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[

            'email'=>'required',
            'message'=>'required',
            'full_name'=>'required',
//            'user_id'=>'required',

        ]);

//        dd('sdcs');
        $feedback= new Feedback();

//        $user_id=Auth::guard('web')->user()->id;
        $feedback->user_id= $request->user_id;
        $feedback->date= $request->date;
        $feedback->email= $request->email;
        $feedback->source= $request->source;
        $feedback->full_name= $request->full_name;
        $feedback->message= $request->message;

        $feedback->slug=str::slug($request->company_name).time();
        $feedback->save();
        $data['success']='Data has been submitted successfully';

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
