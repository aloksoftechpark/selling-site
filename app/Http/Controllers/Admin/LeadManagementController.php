<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AddFollow;
use App\Models\Comment;
use App\Models\Company;
use App\Models\ContactPerson;
use App\Models\Customer;
use App\Models\Estimate;
use App\Models\Lead;
use App\Models\LeadSetup;
use App\Models\Purchase;
use App\Models\PurchaseProduct;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class LeadManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
//        dd('test');
         $user=User::all();
        if (Auth::guard('admin')->check())
        {
            $data['lead_setups'] = LeadSetup::latest()->get();
            $data['lead_management'] = Lead::latest()->get();


        }
        if (Auth::guard('web')->check())
        {
            $data['lead_setups'] = LeadSetup::where('admin_id', Auth::guard('web')->user()->id)->latest()->get();
            $data['lead_management'] = Lead::where('admin_id', Auth::guard('web')->user()->id)->latest()->get();
        }
//        dd($data);
//        $customer= Customer::all();

        return view('admin.lead_management.index',$data)->with('user',$user);
//        ->with('customers',$data);
    }

    public function show_details()
    {
        if (Auth::guard('admin')->check())
        {
            $data['lead_setups'] = LeadSetup::latest()->get();

        }
        if (Auth::guard('web')->check())
        {
            $data['lead_setups'] = LeadSetup::where('admin_id', Auth::guard('web')->user()->id)->latest()->get();

        }
        return view('admin.lead_management.show',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

//        dd('test2');
        if (Auth::guard('admin')->check())
        {
            $data['lead_management'] = Lead::where('admin_id', Auth::guard('admin')->user()->id)->latest()->first();
        }
        if (Auth::guard('web')->check())
        {
            $data['lead_management'] = Lead::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->first();
        }

        return view('admin.lead_management.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function estimate_destroy($id)
    {
        //
//        dd('ada');
//        dd($id);
//        $estimate = Estimate::find($id);
        $estimate = Estimate::where('lead_id',$id);
        $estimate->delete();
        return back()->with('success','data has been deleted successfully');
    }

    public function estimate_show($id)
    {
        if (Auth::guard('admin')->check())
        {

            $data['billing']=Estimate::where('admin_id', Auth::guard('admin')->user()->id)->count();
            $data['estimate'] = Estimate::with('lead')->where('admin_id', Auth::guard('admin')->user()->id)->find($id);
            $estimate= Estimate::with('lead')->where('admin_id', Auth::guard('admin')->user()->id)->find($id);
//            dd($estimate);
            $estimateId123=$estimate->lead_id;


//            dd($purchaseId123);
            $data['estimate_product'] = Estimate::with('lead')->where('lead_id',$estimateId123)->get();
            $total = Estimate::with('lead')->where('lead_id',$estimateId123)->sum('total');
            $sub_total = Estimate::with('lead')->where('lead_id',$estimateId123)->sum('sub_total');
            $grand_total = Estimate::with('lead')->where('lead_id',$estimateId123)->sum('grand_total');
            $additional_charges = Estimate::with('lead')->where('lead_id',$estimateId123)->sum('additional_charge');
            $data['generalSetting'] = Company::where('admin_id', Auth::guard('admin')->user()->id)->latest()->first();

//        dd($total);

        }
        if (Auth::guard('web')->check())
        {

            $data['billing']=Estimate::where('admin_id', Auth::guard('web')->user()->admin->id)->count();
            $data['estimate'] = Estimate::with('lead')->where('admin_id', Auth::guard('web')->user()->admin->id)->find($id);
            $estimate= Estimate::with('lead')->where('admin_id', Auth::guard('web')->user()->admin->id)->find($id);
//            dd($estimate);
            $estimateId123=$estimate->id;


//            dd($purchaseId123);
            $data['estimate_product'] = Estimate::with('lead')->where('lead_id',$estimateId123)->get();
            $total = Estimate::with('product')->where('lead_id',$estimateId123)->sum('total');
            $sub_total = Estimate::with('product')->where('lead_id',$estimateId123)->sum('sub_total');
            $grand_total = Estimate::with('product')->where('lead_id',$estimateId123)->sum('grand_total');
            $additional_charges = Estimate::with('product')->where('lead_id',$estimateId123)->sum('additional_charge');

            $data['generalSetting'] =Company::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->first();

        }



        $estimate=Estimate::find($id);
        return view('admin.lead_management.estimate_bill',$data)
            ->with('estimate',$estimate)
            ->with('total',$total)
            ->with('sub_total',$sub_total)
            ->with('grand_total',$grand_total)
            ->with('additional_charges',$additional_charges);

    }

    public function add_estimate(Request $request)
    {

//        dd($request->all());

        $this->validate($request,[

//            'valid_date'=>'required',
//            'valid_date.*' => 'required'
//            'item.*' => 'required'

        ]);

//        if (Auth::guard('admin')->check())
//        {
//            $admin_id=Auth::guard('admin')->user()->id;
////
//        }
//
//        else{
//            $user_id=Auth::guard('web')->user()->id;
//
//        }
        $input = $request->all();

//        dd($estimate_number);
        for($i=0; $i< count($input['item']); $i++) {

            $estimate_number = Estimate::where('lead_id',$request->lead_id)->latest()->first();
            $lead = Lead::where('id',$request->lead_id)->latest()->first();

            $admin_id=$lead->admin_id;
            $user_id=$lead->user_id;
            if ($estimate_number)
            {
//
                $estimate_number= $estimate_number->estimate_number +1;
//            $branchId=0;
            }else{

                $estimate_number=1;
            }
            $data = [
                // dd($input['branch_id']),
                'admin_id' => $admin_id,
                'user_id' => $user_id,
                'lead_id' => $input['lead_id'],
                'estimate_number' => $estimate_number,
                'estimate_date' => $input['estimate_date'],
                'valid_date' => $input['valid_date'],
                'item' => $input['item'][$i],
                'quantity' => $input['quantity'][$i],
                'rate' => $input['rate'][$i],
                'total' => $input['total'][$i],
                'discount_percent' => $input['discount_percent'][$i],
                'discount_amount' => $input['discount_amount'][$i],
                'final_note' => $input['final_note'],
                'sub_total' => $input['sub_total'],
                'additional_charge' => $input['additional_charge'],
                'grand_total' => $input['grand_total'],
            ];
            Estimate::create($data);
        }
        $data['success']='Data has been added successfully';
        return $data;


    }

    public function store(Request $request)
    {
//        dd($request->all());
        $this->validate($request,[


        ]);
        $lead_management = new Lead();

//        if (Auth::guard('admin')->check())
//        {
//            $lead_management->admin_id=Auth::guard('admin')->user()->id;
////            $leadId = $lead_management->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
//        }

//        else{
//            $lead_management->user_id=Auth::guard('web')->user()->id;
//            $lead_management->admin_id=Auth::guard('web')->user()->admin->id;
////            $leadId= $lead_management->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//        }
//        $invoice = new Invoice();

//        $branchId = $branch->orderBy('id')->pluck('id')->first();
//        if ($branchId==null)
//        {
////            dd('zckjzs');
//            $branchId=001;
//        }

//        if ($leadId)
//        {
////
//            $lead_management->leadId= $leadId->leadId +1;
////            $branchId=0;
//        }else{
//
//            $lead_management->leadId=1;
//        }
//        $customer->customerId= $request->customerId;

        $lead_management->name= $request->name;
//        $customer->lastName= $request->lastName;
//        $customer->middleName= $request->middleName;
//        $customer->fullName= $request->firstName.' '.$request->middleName.' '.$request->lastName;
        $lead_management->slug=str::slug($request->name).time();
//        $lead_management->companyName=$request->companyName;
        $lead_management->address=$request->address;

//        $lead_management->registrationNumber= $request->reg_no;
        $lead_management->phoneNumber= $request->phone_no;
        $lead_management->mailid= $request->email;
//        $customer->website= $request->website;
        $lead_management->website= $request->website;
        $lead_management->comment= $request->description;

        $lead_management->followUpDate= $request->followUpDate;
        $lead_management->todo= $request->todo;
        $lead_management->priority= $request->priority;
        $lead_management->addedBy= $request->added_by;
        $lead_management->source= $request->source;
        $lead_management->status1= $request->status;
        $lead_management->save();

        if ($lead_management) {
//            dd('testr');
            if ($request->hasFile('image')) {
//                dd($request->all());
//                dd($request->hasFile('image'));
//                foreach ($request->file('image') as $image)
//                {
//                    dd('sd');
//                    dd($image->getClientOriginalName());
                $image = $request->file('image');
                $image_new_name = time() . $image->getClientOriginalName();
                $destination = 'upload/topwide';
                $image->move($destination, $image_new_name);
//                        $product->images()->image='/upload/topwide/'.$image_new_name;
                $lead_management->images()->create(['image' => '/upload/topwide/' . $image_new_name]);

//                }
//
            }
        }


            return back()->with('success','Data has been added successfully');
    }

    public function contact_store(Request $request)
    {
//        dd($request->all());
        $this->validate($request,[


        ]);
        $contact_person = new ContactPerson();

        if (Auth::guard('admin')->check())
        {
            $contact_person->admin_id=Auth::guard('admin')->user()->id;
//            $leadId = $lead_management->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
        }

        else{
            $contact_person->user_id=Auth::guard('web')->user()->id;
            $contact_person->admin_id=Auth::guard('web')->user()->admin->id;
//            $leadId= $lead_management->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
        }



        $contact_person->lead_id= $request->lead_id;
        $contact_person->name=$request->name;
        $contact_person->jobTitle= $request->job_title;
        $contact_person->phoneNumber= $request->phone_number;
        $contact_person->email= $request->email;
//        dd('ts');
        $contact_person->save();


//        return back()->with('success','Data has been added successfully');
        $data['success']='Data has been added successfully';
        return $data;
    }


    public function contact_edit(Request $request)
    {
//         dd($request);
        $data['contact_person']= ContactPerson::find($request->contact_id);
        return $data;
    }

    public function contact_update(Request $request)
    {
//        dd($request->all());
        $this->validate($request,[


        ]);
        $contact_person = ContactPerson::find($request->contact_id);
//    dd($contact_person);

        if ($request->contact_id)
        {
            if (Auth::guard('admin')->check())
            {
                $contact_person->admin_id=Auth::guard('admin')->user()->id;
//            $leadId = $lead_management->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
            }

            else{
                $contact_person->user_id=Auth::guard('web')->user()->id;
                $contact_person->admin_id=Auth::guard('web')->user()->admin->id;
//            $leadId= $lead_management->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
            }



            $contact_person->lead_id= $request->lead_id;
            $contact_person->name=$request->name;
            $contact_person->jobTitle= $request->job_title;
            $contact_person->phoneNumber= $request->phone_no;
            $contact_person->email= $request->email;
//            dd($contact_person->email);

        }

//        dd('ts');
        $contact_person->save();


//        return back()->with('success','Data has been added successfully');
        $data['success']='Data has been added successfully';
        return $data;
    }


    public function add_follow_edit(Request $request)
    {
//         dd($request->all());
        $data['add_follow']= AddFollow::find($request->follow_id);
        return $data;
    }


    public function add_follow_store(Request $request)
    {
//        dd($request->all());
        $this->validate($request,[


        ]);
        $add_follow = new AddFollow();

        if (Auth::guard('admin')->check())
        {
            $add_follow->admin_id=Auth::guard('admin')->user()->id;
//            $leadId = $lead_management->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
        }

        else{
            $add_follow->user_id=Auth::guard('web')->user()->id;
            $add_follow->admin_id=Auth::guard('web')->user()->admin->id;
//            $leadId= $lead_management->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
        }

        $add_follow->lead_id= $request->lead_id;
        $add_follow->followUpDate= $request->follow_up_date;
        $add_follow->nextFollowUpDate=$request->next_follow_up_date;
        $add_follow->toDo= $request->todo;
        $add_follow->followUpReport= $request->follow_up_report;
        $add_follow->save();

       $lead= Lead::find($request->lead_id);
        $lead->followUpDate=$request->next_follow_up_date;
        $lead->todo=$request->todo;
        $lead->save();

        $data['success']='Data has been added successfully';

//        return back()->with('success','Data has been added successfully');
        return $data;
    }

    public function add_follow_update(Request $request)
    {
//        dd($request->all());
        $this->validate($request,[


        ]);
        $add_follow = AddFollow::find($request->follow_id);

        if ($request->follow_id)
        {
            if (Auth::guard('admin')->check())
            {
                $add_follow->admin_id=Auth::guard('admin')->user()->id;
//            $leadId = $lead_management->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
            }

            else{
                $add_follow->user_id=Auth::guard('web')->user()->id;
                $add_follow->admin_id=Auth::guard('web')->user()->admin->id;
//            $leadId= $lead_management->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
            }

            $add_follow->lead_id= $request->lead_id;
            $add_follow->followUpDate= $request->follow_up_date;
            $add_follow->nextFollowUpDate=$request->next_follow_up_date;
            $add_follow->toDo= $request->todo;
            $add_follow->followUpReport= $request->follow_up_report;
        }


        $add_follow->save();

        $data['success']='Data has been added successfully';

//        return back()->with('success','Data has been added successfully');
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        //
        if (Auth::guard('admin')->check())
        {
            $data['lead_setups'] = LeadSetup::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();

        }
        if (Auth::guard('web')->check())
        {
            $data['lead_setups'] = LeadSetup::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();

        }

        $estimate=Estimate::where('lead_id',$id)->latest()->get();
        $user=User::all();
        $lead_management=Lead::find($id);
        $contact_person= ContactPerson::where('lead_id',$id)->latest()->get();

        $add_follow= AddFollow::where('lead_id',$id)->latest()->get();
//        dd($add_follow);
//        dd($contact_person);
//        $lead_management = Lead::all();
        return view('admin.lead_management.show',$data)
            ->with('user',$user)->with('lead_management',$lead_management)
            ->with('estimate',$estimate)
            ->with('add_follow',$add_follow)
            ->with('contact_person',$contact_person);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $lead_management= Lead::find($id);
        return view('admin.lead_management.edit')->with('lead_management',$lead_management);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

//        dd($request->all());
        $this->validate($request,[


        ]);
        $lead_management = Lead::find($id);

        if (Auth::guard('admin')->check())
        {
            $lead_management->admin_id=Auth::guard('admin')->user()->id;
//            $leadId = $lead_management->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
        }

        else{
            $lead_management->user_id=Auth::guard('web')->user()->id;
            $lead_management->admin_id=Auth::guard('web')->user()->admin->id;
//            $leadId= $lead_management->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
        }
//        $invoice = new Invoice();

//        $branchId = $branch->orderBy('id')->pluck('id')->first();
//        if ($branchId==null)
//        {
////            dd('zckjzs');
//            $branchId=001;
//        }

//        if ($leadId)
//        {
////
//            $lead_management->leadId= $leadId->leadId +1;
////            $branchId=0;
//        }else{
//
//            $lead_management->leadId=1;
//        }
//        $customer->customerId= $request->customerId;
        if ($id)
        {
            $lead_management->name= $request->name;
//        $customer->lastName= $request->lastName;
//        $customer->middleName= $request->middleName;
//        $customer->fullName= $request->firstName.' '.$request->middleName.' '.$request->lastName;
            $lead_management->slug=str::slug($request->name).time();
//        $lead_management->companyName=$request->companyName;
            $lead_management->address=$request->address;

//        $lead_management->registrationNumber= $request->reg_no;
            $lead_management->phoneNumber= $request->phone_no;
            $lead_management->mailid= $request->email;
//        $customer->website= $request->website;
            $lead_management->website= $request->website;
            $lead_management->comment= $request->description;

            $lead_management->followUpDate= $request->followUpDate;
            $lead_management->todo= $request->todo;
            $lead_management->priority= $request->priority;
            $lead_management->addedBy= $request->added_by;
            $lead_management->source= $request->source;
            $lead_management->status1= $request->status;
            $lead_management->save();

            if ($lead_management) {
//            dd('testr');
                if ($request->hasFile('image')) {
//                dd($request->all());
//                dd($request->hasFile('image'));
//                foreach ($request->file('image') as $image)
//                {
//                    dd('sd');
//                    dd($image->getClientOriginalName());
                    $image = $request->file('image');
                    $image_new_name = time() . $image->getClientOriginalName();
                    $destination = 'upload/topwide';
                    $image->move($destination, $image_new_name);
//                        $product->images()->image='/upload/topwide/'.$image_new_name;
                    $lead_management->images()->update(['image' => '/upload/topwide/' . $image_new_name]);


//                }
//
                }
            }

        }


        return back()->with('success','Data has been added successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lead_edit(Request $request)
    {

        $data['lead']= Lead::find($request->lead_id);

        return $data;
    }

    public function destroy(Request $request)
    {


        $lead_follow=AddFollow::where('lead_id',$request->lead_id)->first();
        $lead_estimate=Estimate::where('lead_id',$request->lead_id)->first();
//        dd($customer_sell);
//        dd($customer_pay);
        if ($lead_follow == null && $lead_estimate == null)
        {
//            dd('test delete');
            $lead_management = Lead::find($request->lead_id);
            $lead_management->delete();

            $data['success']='Data has been deleted successfully';

            return $data;

        }
        else
        {
//            dd('test not delete');
            $data['errors']='You cant delete data due to transaction made';

            return $data;
        }
    }

    public function search(Request $request)
    {
//        dd('test');
//        dd($request->all());

            $date=$request->date;
            $priority=$request->priority;
            $lead=$request->lead;

            $lead_management=Lead::Where('followUpDate', 'LIKE', "%{$date}%")
                ->Where('priority', 'LIKE', "%{$priority}%")
                ->Where('lead', 'LIKE', "%{$lead}%")->get() ;
//            dd($lead_management);
            return view('admin.lead_management.search')-> with('lead_management',$lead_management);

    }

    public function lead_assignment()
    {




        $data['user']=User::all();
        $data['users']=User::all();
//        dd($data);
//        dd('test');
        if (Auth::guard('admin')->check())
        {
            $data['lead_management'] = Lead::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['lead_assignment'] = Lead::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['lead_assign'] = Lead::with('users')->where('admin_id', Auth::guard('admin')->user()->id)->where('lead_assigned','as')->latest()->get();
//            dd($data);
        }
        if (Auth::guard('web')->check())
        {
            $data['lead_management'] = Lead::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['lead_assignment'] = Lead::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['lead_assign'] = Lead::where('admin_id', Auth::guard('web')->user()->admin->id)->where('lead_assigned','as')->latest()->get();
        }
//        dd($data);
//        $customer= Customer::all();

        return view('admin.lead_management.lead_assignment',$data);
    }

    public function lead_assignment_store(Request $request, $id)
    {

       $lead_assign=Lead::find($id);
       if ($id)
       {

           $lead_assign->lead_assigned='as';
            $lead_assign->assign_to=$request->user_id;
       }

       $lead_assign->save();
       return back()->with('success','Data has been added successfully');

    }

    public function  lead_search(Request $request)
    {
//        dd('test');
//        dd($request->all());

//        $lead=$request->lead;
        $users=User::all();
        $user =User::all();
        $user_id=$request->user_id;
        $lead=$request->lead;

        $lead_management=Lead::Where('lead_assigned', 'LIKE', "%{$lead}%")
            ->Where('assign_to', 'LIKE', "%{$user_id}%")
            ->get() ;
//            dd($lead_management);
        return view('admin.lead_management.lead_assignment_search')
            ->with('user',$user)
            ->with('users',$users)
            -> with('lead_management',$lead_management);

    }


    public function lead_followup()
    {
        $data['comment']=Comment::all();
        if (Auth::guard('admin')->check())
        {
            $data['lead_management'] = Lead::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['lead_followup'] = Lead::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();


        }
        if (Auth::guard('web')->check())
        {
            $data['lead_management'] = Lead::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['lead_followup'] = Lead::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
        }
//        dd($data);
//        $customer= Customer::all();

        return view('admin.lead_management.lead_follow_up',$data);
    }

    public function followup_search(Request $request)
    {
//        dd('test');
//        dd($request->all());

        $date=$request->date;
        $priority=$request->priority;
//        $lead=$request->lead;

        $lead_management=Lead::Where('followUpDate', 'LIKE', "%{$date}%")
            ->Where('priority', 'LIKE', "%{$priority}%")
            ->get() ;
//            dd($lead_management);
        return view('admin.lead_management.lead_follow_up_search')-> with('lead_management',$lead_management);

    }

    public function lead_followup_store(Request $request)
    {


        $this->validate($request,[


//            'name'=>'required',
//            'address'=>'required',
//            'reg_no'=>'required',
//            'phone_no'=>'required',
//            'email'=>'required',
//            'website'=>'required',
//            'opening_due'=>'required',
//            'description'=>'required',

    ]);

        $lead_folowup=new Comment();

        if (Auth::guard('admin')->check())
        {
            $lead_folowup->admin_id=Auth::guard('admin')->user()->id;
//            $customerId = $customer->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
        }

        else{
            $lead_folowup->user_id=Auth::guard('web')->user()->id;
            $lead_folowup->admin_id=Auth::guard('web')->user()->admin->id;
//            $customerId= $customer->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
        }
        $lead_folowup->followup_report=$request->followup_report;
        $lead_folowup->slug=str::slug($request->name).time();
        $lead_folowup->nextFollowUpDate=$request->next_followup_date;
        $lead_folowup->nextTodo=$request->todo;
        $lead_folowup->save();
        return back()->with('success','Data has been added successfully');

    }

}
