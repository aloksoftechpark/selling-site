<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AccountHead;
use App\Models\Branch;
use App\Models\Expense;
use App\Models\Ledger;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['expense'] = Expense::latest()->get();
//            $data['expenses'] = Expense::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['branch']  = Branch::where('status', true)->latest()->get();
            $data['account_head']  = AccountHead::where('type', 'expense')->latest()->get();

            $data['payments'] = PaymentMethod::where('status', true)->get();
//            dd($data);
        }
        if (Auth::guard('web')->check())
        {
            // dd('test');
//            $data['payments'] = PaymentMethod::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->get();
//            $data['branch'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
//            $data['account_head'] = AccountHead::where('admin_id', Auth::guard('web')->user()->admin->id)->where('type', 'expense')->latest()->get();
//            $data['expense'] = Expense::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
        }
//        dd($data);
        return view('admin.expense.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.expense.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request->all());


        $this->validate($request,[


//            'name'=>'required',
//            'address'=>'required',
//            'reg_no'=>'required',
//            'phone_no'=>'required',
//            'email'=>'required',
//            'website'=>'required',
//            'opening_due'=>'required',
//            'description'=>'required',

        ]);
        $expense= new Expense();
//        $branch= new \App\Models\Branch();
//
//        if (Auth::guard('admin')->check())
//        {
//            $expense->admin_id=Auth::guard('admin')->user()->id;
////            $branchId = $branch->latest()->first();
////            $employeeId = $employee->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
//        }
//
//        else{
//            $expense->user_id=Auth::guard('web')->user()->id;
//            $expense->admin_id=Auth::guard('web')->user()->admin->id;
////            $employeeId = $employee->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//        }
//        $invoice = new Invoice();

//        $branchId = $branch->latest()->first();
//        dd($employeeId);
//        if ($employeeId)
//        {
////
//            $employee->employeeId= $employeeId->employeeId +1;
////            $branchId=0;
//        }else{
//
////            dd('test');
//            $employee->employeeId=1;
//        }

        $expense->date=$request->date;
        $expense->branch_id=$request->branch_id;

        $expense->expense_head_id= $request->account_head_id;
        $expense->payment_method_id= $request->payment_method_id;
        $expense->amount= $request->amount;
        $expense->bank_name= $request->bank_name;
        $expense->transaction_id= $request->transaction_id;
        $expense->purpose= $request->description;
        $expense->slug=str::slug($request->fullName).time();

        $expense->save();

        if ($expense)
        {
            $ledger= new Ledger();

            if (Auth::guard('admin')->check())
            {
                $ledger->admin_id=Auth::guard('admin')->user()->id;

            }

            else{
                $ledger->user_id=Auth::guard('web')->user()->id;
                $ledger->admin_id=Auth::guard('web')->user()->admin->id;

            }

            $ledger->expense_head_id = $request->account_head_id;
            $ledger->branch_id = $request->branch_id;
            $ledger->expense_id = $expense->id;
            $ledger->save();
        }

        return back()->with('success','Data has been added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function expense_edit(Request $request)
    {
//         dd('test');
        $data['expense']= Expense::with('account_head','payment_method')->find($request->expense_id);
        return $data;
    }

    public function edit($id)
    {
        //
        $expense=Expense::find($id);
        return view('admin.expense.index')->with('expense',$expense);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //

//        dd($request->all());
        $this->validate($request,[


//            'name'=>'required',
//            'address'=>'required',
//            'reg_no'=>'required',
//            'phone_no'=>'required',
//            'email'=>'required',
//            'website'=>'required',
//            'opening_due'=>'required',
//            'description'=>'required',


//

        ]);
        $expense= Expense::find($request->expense_id);
//        $branch= new \App\Models\Branch();

        if ($request->expense_id)
        {

//            if (Auth::guard('admin')->check())
//            {
//                $expense->admin_id=Auth::guard('admin')->user()->id;
////            $branchId = $branch->latest()->first();
////            $employeeId = $employee->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
//            }
//
//            else{
//                $expense->user_id=Auth::guard('web')->user()->id;
//                $expense->admin_id=Auth::guard('web')->user()->admin->id;
////            $employeeId = $employee->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//            }
//        $invoice = new Invoice();

//        $branchId = $branch->latest()->first();
//        dd($employeeId);
//        if ($employeeId)
//        {
////
//            $employee->employeeId= $employeeId->employeeId +1;
////            $branchId=0;
//        }else{
//
////            dd('test');
//            $employee->employeeId=1;
//        }

            $expense->date=$request->date;
            $expense->branch_id=$request->branch_id;

            $expense->expense_head_id= $request->account_head_id;
            $expense->payment_method_id= $request->payment_method_id;
            $expense->amount= $request->amount;
            $expense->bank_name= $request->bank_name;
            $expense->transaction_id= $request->transaction_id;
            $expense->purpose= $request->description;
            $expense->slug=str::slug('expense').time();

            $expense->save();

        }

        $data['success']='Data has been added successfully';
        return $data;
//        return back()->with('success','Data has been added successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $expense_ledger=Ledger::where('expense_id',$request->expense_id)->first();
//        $customer_pay=SupplierCustomerPayment::where('customer_id',$request->customer_id)->first();
//        dd($customer_sell);
//        dd($customer_pay);
        if ($expense_ledger == null)
        {
//            dd('test delete');
            $expense = Expense::find($request->expense_id);
            $expense->delete();

            $data['success']='Data has been deleted successfully';

            return $data;

        }
        else
        {
//            dd('test not delete');
            $data['errors']='You cant delete data due to transaction made';

            return $data;
        }
    }
}
