<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DemoPending;
use App\Models\Package;
use App\Models\PlanPricing;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check())
        {
//            $data['users']=User::all();
            $data['package'] = Package::latest()->get();

        }
        if (Auth::guard('web')->check())
        {

            $data['package'] = Package::where('user_id', Auth::guard('web')->user()->id)->latest()->get();
//            $data['users'] = User::where('id', Auth::guard('web')->user()->id)->latest()->get();
//dd($data);
        }
        return view('admin.product_setup.packages',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[

            'name'=>'required',

//            'payment_status'=>'required',

        ]);

        $package= new Package();

        $package->name= $request->name;

        $package->slug=str::slug($request->name).time();
        $package->save();
        $data['success']='Data has been submitted successfully';

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function edit_package(Request $request)
    {
//         dd($request);
//        $data['package'] = Package::latest()->get();
        $data['package']= Package::find($request->package_id);

        return $data;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //

        $this->validate($request,[

            'name'=>'required',

        ]);

        $package= Package::find($request->package_id);

        if($request->package_id)
        {
            $package->name= $request->name;
            $package->slug=str::slug($request->name).time();
        }

        $package->save();
        $data['success']='Data has been submitted successfully';

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //

        $plans_pricing=PlanPricing::where('package_id',$request->package_id)->first();
//        $customer_pay=SupplierCustomerPayment::where('customer_id',$request->customer_id)->first();
//        dd($customer_sell);
//        dd($customer_pay);
        if ($plans_pricing == null)
        {
//            dd('test delete');
            $package = Package::find($request->package_id);
            $package->delete();

            $data['success']='Data has been deleted successfully';
            return $data;

        }
        else
        {
//            dd('test not delete');
            $data['errors']='You cant delete data due to transaction made';
            return $data;
        }

    }
}
