<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\BillPaid;
use App\Models\BillPending;
use App\Models\OrderPending;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class BillPaidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['users']=User::all();
            $data['bill_paid'] = BillPaid::with('user','bill_pending','bill_pending.user','bill_pending.order_pending')->where('status',1)->latest()->get();

        }
        if (Auth::guard('web')->check())
        {

            $data['bill_paid'] = BillPaid::with('user','bill_pending','bill_pending.user','bill_pending.package','bill_pending.order_pending')->where('user_id', Auth::guard('web')->user()->id)->where('status',1)->latest()->get();
            $data['users'] = User::where('id', Auth::guard('web')->user()->id)->latest()->get();

//
        }
        return view('admin.bill.paid_bill',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request->all());
        $this->validate($request,[

            'paid_amount'=>'required',

//            'payment_status'=>'required',

        ]);

        $bill_paid= new BillPaid();

        if (Auth::guard('web')->check())
        {
            $user_id=Auth::guard('web')->user()->id;
            $bill_paid->user_id= $user_id;
        }

        $bill_paid->bill_pending_id= $request->bill_pending_id;
        $bill_paid->date= $request->date;
        $bill_paid->due_amount= $request->due_amount;
        $bill_paid->discount= $request->discount;
        $bill_paid->receivable_amount= $request->receivable_amount;

        $bill_paid->promo_code= $request->promo_code;
        $bill_paid->paid_amount= $request->paid_amount;
        $bill_paid->payment_method_id= $request->payment_method_id;
        $bill_paid->paymentId= $request->paymentId;

        $bill_paid->slug=str::slug('bill').time();

        $bill_paid->save();

        if ($bill_paid)
        {

            $bill_pending= BillPending::find($request->bill_pending_id);

            if($request->bill_pending_id)
            {
                $bill_pending->status=$request->status;
            }

            $bill_pending->save();

        }

        $data['success']='Data has been submitted successfully';

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
