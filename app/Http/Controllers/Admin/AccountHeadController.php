<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AccountHead;
use App\Models\Attendance;
use App\Models\Branch;
use App\Models\DamangeProduct;
use App\Models\Employee;
use App\Models\Expense;
use App\Models\Income;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AccountHeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['account_head1'] =AccountHead::latest()->first();
            $data['account_head'] = AccountHead::latest()->get();


//            dd($data);
        }
        if (Auth::guard('web')->check())
        {
            // dd('test');
//            $data['account_head'] =AccountHead ::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
        }
//        return view('admin.attendance.index',$data);
        return view('admin.account_head.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['account_head'] =AccountHead::latest()->first();
            $data['branch']  = Branch::where('status', true)->latest()->get();
//            $data['employee']  = Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
        }
        if (Auth::guard('web')->check())
        {
//            // dd('test');
//            $data['account_head'] =AccountHead::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->first();
//            $data['branch'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
//            $data['employee'] = Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
        }
//        return view('admin.attendance.edit',$data);
        return view('admin.account_head.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        //
        $this->validate($request,[

//            'name'=>'required',



        ]);

        $account_head= new AccountHead();

        if (Auth::guard('admin')->check())
        {
//            $account_head->admin_id=Auth::guard('admin')->user()->id;
//            $branchId = $branch->latest()->first();
            $account_headId= $account_head->latest()->first();
        }

//        else{
//            $account_head->user_id=Auth::guard('web')->user()->id;
//            $account_head->admin_id=Auth::guard('web')->user()->admin->id;
//            $account_headId = $account_head->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//        }
//        $invoice = new Invoice();

//        $branchId = $branch->latest()->first();
//        dd($branchId);
        if ($account_headId)
        {
//
            $account_head->account_headId= $account_headId->account_headId +1;
//            $branchId=0;
        }else{

            $account_head->account_headId=1;
        }

//        dd($branch->branchId);
//        dd($request->all());

//        $account_head->product_id=$request->product_id;
//        $damage->branch_id=$request->branch_id;
//        $damage->name= $request->name;
//        $account_head->account_headId=$request->account_headId;
        $account_head->slug=str::slug('account_head').time();
        $account_head->title=$request->title;
        $account_head->type=$request->type;
        $account_head->opening_due=$request->opening_due;
//        $damage->comment=$request->description;
        $account_head->save();
        return back()->with('success','Data has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function account_head_edit(Request $request)
    {
//         dd('test');
        $data['account_head']= AccountHead::find($request->account_head_id);
        return $data;
    }
    public function edit($id)
    {
        //
        $account_head = AccountHead::find($id);
//        dd($account_head);
        return view('admin.account_head.edit')->with('account_head',$account_head);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

//        dd($request->all());
        $this->validate($request,[

//            'name'=>'required',

        ]);

        $account_head= AccountHead::find($request->account_head_id);
//        dd($account_head);
        if ($request->account_head_id)
        {
            if (Auth::guard('admin')->check())
            {
//                $account_head->admin_id=Auth::guard('admin')->user()->id;
//            $branchId = $branch->latest()->first();
                $account_headId= $account_head->latest()->first();
            }

//            else{
//                $account_head->user_id=Auth::guard('web')->user()->id;
//                $account_head->admin_id=Auth::guard('web')->user()->admin->id;
//                $account_headId = $account_head->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//            }
//        $invoice = new Invoice();

//        $branchId = $branch->latest()->first();
//        dd($branchId);
            if ($account_headId)
            {
//
                $account_head->account_headId= $account_headId->account_headId +1;
//            $branchId=0;
            }else{

                $account_head->account_headId=1;
            }

//        dd($branch->branchId);
//        dd($request->all());

//        $account_head->product_id=$request->product_id;
//        $damage->branch_id=$request->branch_id;
//        $damage->name= $request->name;
            $account_head->slug=str::slug('account_head').time();
            $account_head->title=$request->title;
            $account_head->type=$request->type;
            $account_head->opening_due=$request->opening_due;
//        $damage->comment=$request->description;
        }


        $account_head->save();
//        return back()->with('success','Data has been added successfully');
        $data['success']='Data has been added successfully';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $income_head=Income::where('account_head_id',$request->account_head_id)->first();
        $expence_head=Expense::where('expense_head_id',$request->account_head_id)->first();
//        dd($customer_sell);
//        dd($customer_pay);
        if ($expence_head == null && $income_head == null)
        {
//            dd('test delete');
            $account_head = AccountHead::find($request->account_head_id);
            $account_head->delete();

            $data['success']='Data has been deleted successfully';

            return $data;

        }
        else
        {
//            dd('test not delete');
            $data['errors']='You cant delete data due to transaction made';

            return $data;
        }
    }
}
