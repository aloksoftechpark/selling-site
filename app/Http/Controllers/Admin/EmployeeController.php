<?php

namespace App\Http\Controllers\Admin;

use App\Models\Bank;
use App\Models\Employee;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\LedgerEmployee;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Models\SalaryMaster;
use App\Models\SalaryPayroll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['employee1'] =\App\Models\Employee::latest()->first();
            $data['employees'] = \App\Models\Employee::latest()->get();
            $data['branch']  = Branch::where('status', true)->latest()->get();

//            dd($data);
        }
        if (Auth::guard('web')->check())
        {
            // dd('test');
            $data['employee1'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->first();
            $data['branch'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
            $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
        }
//        $product=Product::all();

//        $employee =Employee::all();
        return view('admin.employee.index',$data);
//            ->with('employee',$employee);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Auth::guard('admin')->check())
        {
            $data['employee'] =\App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->first();
            $data['branch']  = Branch::where('admin_id', Auth::guard('admin')->user()->id)->where('status', true)->latest()->get();
        }
        if (Auth::guard('web')->check())
        {
            // dd('test');
//            $data['employee'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->first();
//            $data['branch'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
        }
//        $product=Product::all();
//        return view('admin.products.index',$data);
        return view('admin.employee.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request->all());

        $this->validate($request,[


//            'name'=>'required',
//            'address'=>'required',
//            'reg_no'=>'required',
//            'phone_no'=>'required',
//            'email'=>'required',
//            'website'=>'required',
//            'opening_due'=>'required',
//            'description'=>'required',

        ]);
        $employee= new \App\Models\Employee();
//        $branch= new \App\Models\Branch();

//        if (Auth::guard('admin')->check())
//        {
////            $employee->admin_id=Auth::guard('admin')->user()->id;
////            $branchId = $branch->latest()->first();
////            $employeeId = $employee->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
//        }
//
//        else{
//            $employee->user_id=Auth::guard('web')->user()->id;
//            $employee->admin_id=Auth::guard('web')->user()->admin->id;
////            $employeeId = $employee->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//        }
////        $invoice = new Invoice();

//        $branchId = $branch->latest()->first();
//        dd($employeeId);
//        if ($employeeId)
//        {
////
//            $employee->employeeId= $employeeId->employeeId +1;
////            $branchId=0;
//        }else{
//
////            dd('test');
//            $employee->employeeId=1;
//        }
//        $invoice = new Invoice();
//        $lastInvoiceID = $invoice->orderBy('id', DESC)->pluck('id')->first();
        $employee->employeeId=$request->employeeId;
        $employee->branch_id=$request->branch_id;

        $employee->employeeId= $request->employeeId;

//        $employee->firstName= $request->firstName;
//        $employee->lastName= $request->lastName;
//        $employee->middleName= $request->middleName;
//        $employee->fullName= $request->firstName.' '.$request->middleName.' '.$request->lastName;
        $employee->fullName= $request->fullName;
        $employee->slug=str::slug($request->fullName).time();
//        $product->product_category_id= $request->category_id;

        $employee->presentAddress= $request->presentAddress;
        $employee->permanentAddress= $request->permanentAddress;
        $employee->mailId= $request->email;
        $employee->phoneNumber= $request->phoneNumber;
        $employee->citizenshipNumber= $request->citizenshipNumber;
        $employee->panNumber= $request->pan_number;
        $employee->dob= $request->dob;
        $employee->bloodGroup= $request->bloodGroup;
//        dd($request->bloodGroup);
        $employee->fatherName= $request->fatherName;
        $employee->fatherPhoneNumber= $request->fatherPhoneNumber;
        $employee->motherName= $request->motherName;
        $employee->motherPhoneNumber= $request->motherPhoneNumber;
        $employee->joiningDate= $request->joiningDate;
        $employee->salary= $request->salary;
        $employee->salary1= $request->salary;
        $employee->designation= $request->designation;
        $employee->description= $request->description;

        $employee->advanceDue = $request->advance_due+ $employee->advanceDue ;
//        $employee->salaryDue= $request->salary_due + $employee->salaryDue;
        $employee->salaryDue= $request->opening_due;
        $employee->salaryDue1= $request->opening_due;


//        $employee->document= $request->document;
//dd($request->all());

        $employee->save();



        if ($employee)
        {
//            dd('testr');
            if ($request->hasFile('image'))
            {
//                dd($request->all());
//                dd($request->hasFile('image'));
//                foreach ($request->file('image') as $image)
//                {
//                    dd('sd');
//                    dd($image->getClientOriginalName());
                $image=$request->file('image');
                $image_new_name=time().$image->getClientOriginalName();
                $destination='upload/topwide';
                $image->move($destination,$image_new_name);
//                        $product->images()->image='/upload/topwide/'.$image_new_name;
                $employee->images()->create(['image' => '/upload/topwide/'.$image_new_name]);

//                }
//
            }

            if ($request->hasFile('document'))
            {
//                dd($request->all());
//                dd($request->hasFile('image'));
//                foreach ($request->file('image') as $image)
//                {
//                    dd('sd');
//                    dd($image->getClientOriginalName());
                $document=$request->file('document');
                $image_new_name=time().$document->getClientOriginalName();
                $destination='upload/topwide';
                $document->move($destination,$image_new_name);
//                        $product->images()->image='/upload/topwide/'.$image_new_name;
                $employee->images()->create(['document' => '/upload/topwide/'.$image_new_name]);

//                }
//
            }
        }





        return back()->with('success','Data has been added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (Auth::guard('admin')->check())

        {
            $data['salary_payroll'] = SalaryPayroll::where('employee_id',$id)->where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['branch']  = Branch::where('admin_id', Auth::guard('admin')->user()->id)->where('status', true)->latest()->get();
            $data['employee1'] =\App\Models\Employee::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['payment_method'] = PaymentMethod::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $data['bank'] = Bank::where('admin_id', Auth::guard('admin')->user()->id)->latest()->get();
            $ledger_employee=LedgerEmployee::where('employee_id',$id)->with('salary_payroll')->get();
//            dd($ledger_employee);

        }
        if (Auth::guard('web')->check())
        {
            // dd('test');

            $ledger_employee=LedgerEmployee::where('employee_id',$id)->with('salary_payroll')->get();
            $data['salary_payroll'] = SalaryPayroll::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['branch'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
            $data['employees'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['employee1'] = \App\Models\Employee::where('admin_id', Auth::guard('web')->user()->admin->id)->latest()->get();
            $data['payment_method'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
            $data['bank'] = Branch::where('admin_id', Auth::guard('web')->user()->admin->id)->where('status', true)->latest()->get();
        }
        $employee= \App\Models\Employee::find($id);
//        dd($employee);
        return view('admin.employee.employee_details',$data)
            ->with('ledger_employee',$ledger_employee)
            ->with('employee',$employee);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function employee_edit(Request $request)
    {
//         dd('test');
        $data['employee']= Employee::with('images')->find($request->employee_id);
        return $data;
    }


    public function edit($id)
    {
        //
        $branch=Branch::all();
        $employee= \App\Models\Employee::find($id);
        return view('admin.employee.edit')
            ->with('branch',$branch)->with('employee',$employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        //
//        dd($request->all());

        $this->validate($request,[


//            'name'=>'required',
//            'address'=>'required',
//            'reg_no'=>'required',
//            'phone_no'=>'required',
//            'email'=>'required',
//            'website'=>'required',
//            'opening_due'=>'required',
//            'description'=>'required',

        ]);
        $employee= \App\Models\Employee::find($request->employee_id);
        if ($request->employee_id)
        {
//            if (Auth::guard('admin')->check())
//            {
//                $employee->admin_id=Auth::guard('admin')->user()->id;
////            $branchId = $branch->latest()->first();
////                $employeeId = $employee->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
//            }
//
//            else{
//                $employee->user_id=Auth::guard('web')->user()->id;
//                $employee->admin_id=Auth::guard('web')->user()->admin->id;
////                $employeeId = $employee->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//            }

            $employee->branch_id=$request->branch_id;

            $employee->employeeId= $request->employeeId;

            $employee->fullName= $request->fullName;
            $employee->slug=str::slug($request->fullName).time();
//        $product->product_category_id= $request->category_id;

            $employee->presentAddress= $request->presentAddress;
            $employee->permanentAddress= $request->permanentAddress;
            $employee->mailId= $request->email;
            $employee->phoneNumber= $request->phoneNumber;
            $employee->citizenshipNumber= $request->citizenshipNumber;
            $employee->dob= $request->dob;
            $employee->panNumber= $request->pan_number;
            $employee->bloodGroup= $request->bloodGroup;
//        dd($request->bloodGroup);
            $employee->fatherName= $request->fatherName;
            $employee->fatherPhoneNumber= $request->fatherPhoneNumber;
            $employee->motherName= $request->motherName;
            $employee->motherPhoneNumber= $request->motherPhoneNumber;
            $employee->joiningDate= $request->joiningDate;
            $employee->salary= $request->salary;
            $employee->salary1= $request->salary;
            $employee->designation= $request->designation;
            $employee->description= $request->description;

//            if ($employee->advanceDetuction!)
//            $employee->advanceDue = $request->advance_due+ $employee->advanceDue ;
            $employee->salary = $request->salary ;
            $employee->salaryDue = $request->opening_due ;
            $employee->salaryDue1 = $request->opening_due ;

//            $employee->salaryDue= $request->salary_due + $employee->salaryDue;
        }



        $employee->save();

        if ($employee)
        {

            if ($request->hasFile('image'))
            {
//                dd($request->all());
//                dd($request->hasFile('image'));
//                foreach ($request->file('image') as $image)
//                {
//                    dd('sd');
//                    dd($image->getClientOriginalName());
                $image=$request->file('image');
                $image_new_name=time().$image->getClientOriginalName();
                $destination='upload/topwide';
                $image->move($destination,$image_new_name);
//                        $product->images()->image='/upload/topwide/'.$image_new_name;
                $employee->images()->update(['image' => '/upload/topwide/'.$image_new_name]);

//                }
//
            }

            if ($request->hasFile('document'))
            {
//                dd($request->all());
//                dd($request->hasFile('image'));
//                foreach ($request->file('image') as $image)
//                {

//                    dd($image->getClientOriginalName());
                $document=$request->file('document');
                $image_new_name=time().$document->getClientOriginalName();
                $destination='upload/topwide';
                $document->move($destination,$image_new_name);
//                        $product->images()->image='/upload/topwide/'.$image_new_name;
                $employee->images()->update(['document' => '/upload/topwide/'.$image_new_name]);

//                }
//
            }
        }



        $data['success']='Data has been added successfully';
        return $data;

//        return back()->with('success','Data has been Updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {



        $employee_payroll=SalaryPayroll::where('employee_id',$request->employee_id)->first();
//        $employee_pLedger=LedgerEmployee::where('employee_id',$request->employee_id)->first();
        $employee_master=SalaryMaster::where('employee_id',$request->employee_id)->first();
//        dd($customer_sell);
//        dd($customer_pay);
        if ($employee_payroll == null && $employee_master == null)
        {
//            dd('test delete');
            $employee = Employee::find($request->employee_id);
            $employee->delete();

            $data['success']='Data has been deleted successfully';

            return $data;

        }
        else
        {
//            dd('test not delete');
            $data['errors']='You cant delete data due to transaction made';

            return $data;
        }
    }
    public function  update1(Request $request)
    {
//        dd($request->all());

        $this->validate($request,[


//            'name'=>'required',
//            'address'=>'required',
//            'reg_no'=>'required',
//            'phone_no'=>'required',
//            'email'=>'required',
//            'website'=>'required',
//            'opening_due'=>'required',
//            'description'=>'required',

        ]);
        $employee= Employee::find($request->employee_id);
//        dd($employee);
        if ($request->employee_id)
        {
//            if (Auth::guard('admin')->check())
//            {
////                dd('test');
//                $employee->admin_id=Auth::guard('admin')->user()->id;
////            $branchId = $branch->latest()->first();
////                $employeeId = $employee->where('admin_id',Auth::guard('admin')->user()->id)->latest()->first();
//            }
//
//            else{
//                $employee->user_id=Auth::guard('web')->user()->id;
//                $employee->admin_id=Auth::guard('web')->user()->admin->id;
////                $employeeId = $employee->where('admin_id',Auth::guard('web')->user()->admin->id)->latest()->first();
//            }

            $employee->advanceDue = $request->advanceDue + $employee->advanceDue ;

//            $employee->salaryDue= $request->salary_due + $employee->salaryDue;
        }



        $employee->save();

        $data['success'] = 'Data has been Updated successfully';
        return $data;
    }
}
