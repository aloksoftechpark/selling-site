<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        'App\Console\Commands\BillGenerate',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('bill:generate')
            ->everyMinute();
//        $schedule->call(function () {
//            //code
//            DB::table('bill_pendings')->get();
//        })->everyMinute();
//
//        $schedule->call(function () {
//            //code
//        })->yearly();
//
//        //this following code will execute the task on last day of every month
//        $schedule->call(function () {
//            //code
//        })->when(function () {
//            return \Carbon\Carbon::now()->endOfMonth()->isToday();
//        });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
