<?php

namespace App\Console\Commands;

use App\Models\BillPending;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class BillGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bill:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate bill';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $totalUsers = \DB::table('bill_pendings')
            ->whereRaw('Date(created_at) = CURDATE()')
            ->first();

//        $date = \Carbon\Carbon::today()->subDays(30);
//        $users = BillPending::where('created_at','>=',$date)->get();
//        dd($users);

//        $current = $totalUsers->created_at;
//        $current = Carbon::now();
//        $trialExpires = $current->addDays(30);
//        dd($trialExpires);
//        $bill_generate=BillPending::
//        whereDate('created_at','>', Carbon::now()->subMinutes(10))

//            whereMonth('created_at', Carbon::now()->month)->count();


//        dd($bill_generate);

        $bill_pending= new BillPending();

        $bill_pending->user_id= $totalUsers->user_id;
        $bill_pending->package_id= $totalUsers->package_id;
        $bill_pending->order_pending_id= $totalUsers->id;
        $bill_pending->amount= $totalUsers->amount;
        $bill_pending->discount_amount= $totalUsers->discount_amount;
        $bill_pending->vat_amount= $totalUsers->vat_amount;
        $bill_pending->total= $totalUsers->total;
        $bill_pending->slug=str::slug('bill_generate').time();

        $bill_pending->save();
        dd($totalUsers);
    }
}
