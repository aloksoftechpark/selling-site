<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    //
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $hidden= [
        'password', 'remember_token',
    ];

    public function lead()
    {
        return$this->hasMany('App\Models\Lead','admin_id');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
//    public function bill()
//    {
//        return $this->hasMany('App\Models\Billing');
//    }

    public function attendances()
    {
        return $this->hasMany('App\Models\Attendance', 'attended_by');
    }

//    public function stock()
//    {
//        return $this->hasMany('App\Models\Stock');
//    }
//    public function product()
//    {
//        return $this->hasMany('App\Models\Product');
//    }

    public function employee()
    {
        return $this->hasMany('App\Models\Employee');
    }
}
