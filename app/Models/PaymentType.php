<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    //

    public function order_pending()
    {
        return $this->hasMany('App\Models\OrderPending');
    }
    public function bill_pending()
    {
        return $this->hasMany('App\Models\BillPending');
    }
}
