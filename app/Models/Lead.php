<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    //
    public function user()
    {
        return$this->belongsTo('App\Models\User');
    }
    public function estimate()
    {
        return$this->hasMany('App\Models\Estimate');
    }

    public function user1()
    {
        return$this->belongsTo('App\Models\User','addedBy');
    }

    public function users()
    {
        return$this->belongsTo('App\Models\User','assign_to');
    }
    public function admin()
    {
        return$this->belongsTo('App\Models\Admin','admin_id');
    }
    Public function images()
    {
        return $this->morphMany('App\Models\Image','imageable');
    }
}
