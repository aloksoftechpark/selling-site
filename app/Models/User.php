<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $guard='web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','slug','refral_code', 'password','phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function partner_pending()
    {
        return $this->hasMany('App\Models\PartnerPending');
    }

    public function student_pending()
    {
        return $this->hasMany('App\Models\StudentPending');
    }
    public function affilated_pending()
    {
        return $this->hasMany('App\Models\AffilatedPending');
    }


    public function demo_pending()
    {
        return $this->hasMany('App\Models\DemoPending');
    }
    public function order_pending()
    {
        return $this->hasMany('App\Models\OrderPending');
    }

    public function bill_pending()
    {
        return $this->hasMany('App\Models\BillPending');
    }

    public function bill_paid()
    {
        return $this->hasMany('App\Models\BillPaid');
    }
    public function verifyUser()
    {
        return $this->hasOne('App\Models\VerifyUser');
    }


    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }
    public function branch()
    {
        return $this->hasMany('App\Models\Branch');
    }
    public function lead()
    {
        return$this->hasMany('App\Models\Lead','addedBy');
    }
    public function lead_assign()
    {
        return$this->hasMany('App\Models\Lead','assign_to');
    }
    public function employee()
    {
        return $this->hasMany('App\Models\Employee');
    }
}
