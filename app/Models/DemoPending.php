<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DemoPending extends Model
{
    protected  $fillable=['user_id','company_name','company_address','slug','contact_number','pan','package','status','remember_token'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
