<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //
    public function plan_pricing()
    {
        return $this->hasMany('App\Models\PlanPricing');
    }

    public function bill_pending()
    {
        return $this->hasMany('App\Models\BillPending');
    }

    public function order_pending()
    {
        return $this->hasMany('App\Models\OrderPending');
    }
}
