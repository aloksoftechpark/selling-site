<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryType extends Model
{
    //
//    protected $fillable= ['benificial'];
    protected $table = 'salary_types';
    public function salary_master()
    {
        return $this->belongsTo('App\Models\SalaryMaster');
    }
}
