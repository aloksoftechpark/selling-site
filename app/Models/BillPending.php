<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillPending extends Model
{
    //
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function payment_type()
    {
        return $this->belongsTo('App\Models\PaymentType');
    }

    public function package()
    {
        return $this->belongsTo('App\Models\Package');
    }

    public function order_pending()
    {
        return $this->belongsTo('App\Models\OrderPending');
    }
    public function bill_paid()
    {
        return $this->hasMany('App\Models\BillPaid');
    }

}
