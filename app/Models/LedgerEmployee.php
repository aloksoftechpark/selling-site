<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LedgerEmployee extends Model
{
    //
    public function salary_payroll()
    {

        return $this->belongsTo('App\Models\SalaryPayroll','salary_payroll_id');
    }
}
