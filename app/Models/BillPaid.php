<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillPaid extends Model
{
    //
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function bill_pending()
    {
        return $this->belongsTo('App\Models\BillPending');
    }
    public function payment_method()
    {
        return $this->belongsTo('App\Models\PaymentMethod','payment_method_id');
    }
}
