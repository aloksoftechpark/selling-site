<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    //

    public function bill_paid()
    {
        return $this->hasMany('App\Models\BillPaid');
    }

    public function expense()
    {
        return $this->hasMany('App\Models\Expense');
    }
    public function purchase()
    {
        return $this->hasMany('App\Models\Purchase');
    }
    public function sale()
    {
        return $this->hasMany('App\Models\Billing');
    }

    public function income()
    {
        return $this->hasMany('App\Models\Income');
    }
}
