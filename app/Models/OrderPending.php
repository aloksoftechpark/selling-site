<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPending extends Model
{
    protected $fillable=['user_id',	'package_id',	'company_name',	'company_address',	'slug',	'contact_number',	'pan',	'ird_verified',	'package',	'plan',	'payment_type',	'payment_status',	'payment_method',	'payment_id',	'referal_code',	'promo_code',	'status',	'remember_token'];
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function package()
    {
        return $this->belongsTo('App\Models\Package');
    }


    public function bill_pending()
    {
        return $this->hasMany('App\Models\BillPending','package_id');
    }

    public function payment_type()
    {
        return $this->belongsTo('App\Models\PaymentType');
    }
}
