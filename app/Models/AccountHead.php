<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountHead extends Model
{
    //
    public function expense()
    {
      return  $this->hasMany('App\Models\Expense','expense_head_id');
    }
    public function income()
    {
        return  $this->hasMany('App\Models\Income');
    }
}

