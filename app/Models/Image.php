<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $fillable = [ 'image' , 'document'] ;
    Public function imageable ()
    {
        return $this->morphTo();
    }

    Public function imageables ()
    {
        return $this->morphTo();
    }

}
