<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estimate extends Model
{
    //
    protected $fillable=['lead_id','admin_id','user_id','estimate_number','estimate_date','valid_date','item','quantity','rate','total','discount_percent','discount_amount','final_note','sub_total','additional_charge','grand_total'];

    public function lead()
    {
        return$this->belongsTo('App\Models\Lead');
    }
}
