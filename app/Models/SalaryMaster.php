<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryMaster extends Model
{

    protected $fillable = ['month', 'employee_id','admin_id', 'branch_id'];
    //
    public function salary_type()
    {
        return $this->hasMany('App\Models\SalaryType');
    }

    public function employee()
    {

        return$this->belongsTo('App\Models\Employee','employee_id');
    }
    public function month()
    {

        return$this->hasMany('App\Models\Month');
    }
}
