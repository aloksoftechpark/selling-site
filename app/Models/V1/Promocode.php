<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    protected $fillable = [
        'title', 'code', 'discount', 'from', 'to', 'description'
    ];
}
