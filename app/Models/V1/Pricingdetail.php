<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Pricingdetail extends Model
{
    protected $fillable = [
        'discount', 'vatable_amount', 'vat_amount'
    ];
}
