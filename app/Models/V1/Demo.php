<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{
    protected $fillable = [
        'name', 'company','workspace', 'address', 'contact', 'email', 'pan', 'plan_id', 'user_id', 'status'
    ];
}
