<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Paymentdetail extends Model
{
    protected $fillable = [
        'paymentmethod_id', 'bank', 'bill_id'
    ];
}
