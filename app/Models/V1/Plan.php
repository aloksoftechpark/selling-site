<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'title', 'branches', 'users', 'price', 'featured'
    ];
}
