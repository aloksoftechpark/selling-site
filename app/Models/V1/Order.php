<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'company', 'address', 'contact', 'email', 'pan', 'plan_id', 'paymentdetail_id', 'subscription', 'promo_code', 'user_id', 'status'
    ];
}