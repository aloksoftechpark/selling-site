<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Validity extends Model
{
    protected $fillable = [
        'title', 'months', 'discount'
    ];
}
