<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryPayroll extends Model
{
    //
    public function employee()
    {

        return$this->belongsTo('App\Models\Employee');
    }
    public function ledger()
    {

        return $this->hasMany('App\Models\Ledger');
    }
    public function ledger_employee()
    {

        return $this->hasMany('App\Models\LedgerEmployee');
    }

}
