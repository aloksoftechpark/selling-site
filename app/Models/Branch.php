<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    //
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function damage()
    {
        return$this->hasMany('App\Models\DamageProduct');
    }
    public function stock()
    {
        return$this->hasMany('App\Models\Stock');
    }

    public function employee()
    {

        return$this->hasMany('App\Models\Employee');
    }

    public function purchase()
    {

        return$this->hasMany('App\Models\Purchase');
    }
    public function purchaseProduct()
    {

        return$this->hasMany('App\Models\PurchaseProduct');
    }


}
