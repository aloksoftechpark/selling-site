<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ledger extends Model
{
    //
    public function supplier()
    {

        return $this->hasMany('App\Models\Supplier');
    }

    public function salary_payroll()
    {

        return $this->belongsTo('App\Models\SalaryPayroll','salary_payroll_id');
    }

    public function purchase()
    {

        return $this->belongsTo('App\Models\Purchase','purchase_id');
    }
    public function billing()
    {

        return $this->belongsTo('App\Models\Billing','billing_id');
    }
    public function expense()
    {

        return $this->belongsTo('App\Models\Expense','expense_id');
    }
    public function income()
    {

        return $this->belongsTo('App\Models\Income','income_id');
    }
    public function sale_return()
    {

        return $this->belongsTo('App\Models\SaleReturn','sale_return_id');
    }
    public function purchase_return()
    {

        return $this->belongsTo('App\Models\PurchaseReturn','purchase_return_id');
    }
    public function supplier_customer_payment()
    {

        return $this->belongsTo('App\Models\SupplierCustomerPayment','supplier_customer_payment_id');
    }
    public function supplier_customer_payment1()
    {

        return $this->belongsTo('App\Models\SupplierCustomerPayment','supplier_customer_payment_id1');
    }
}
