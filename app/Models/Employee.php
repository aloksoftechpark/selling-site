<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //

    Public function images ()
    {
        return $this->morphMany('App\Models\Image','imageable');
    }

    public function branch()
    {

        return$this->belongsTo('App\Models\Branch');
    }
    public function attendance()
    {

        return$this->hasMany('App\Models\Attendance');
    }

    public function salary_payroll()
    {

        return$this->hasMany('App\Models\SalaryPayroll');
    }
    public function salary_master()
    {

        return$this->hasMany('App\Models\SalaryMaster','employee_id');
    }


    public function month()
    {

        return$this->hasMany('App\Models\Month');
    }

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
