<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    //
    protected $table = 'expenses';
    public function account_head()
    {
       return $this->belongsTo('App\Models\AccountHead','expense_head_id');
    }

    public function ledger()
    {
        return $this->hasMany('App\Models\Ledger');
    }
    public function payment_method()
    {
        return $this->belongsTo('App\Models\PaymentMethod','payment_method_id');
    }
}
