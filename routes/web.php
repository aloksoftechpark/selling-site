    <?php

    Route::get('design', function () {  return view('new-design'); });
    Route::get('academic-year', function () {  return view('academic-year'); });
    Route::get('about-us', function () {   return view('frontend.about');});
    Route::get('contact-us', function () {   return view('frontend.contact'); });
    Route::get('blog', function () { return view('frontend.blog'); });
    Route::get('plan-price', function () {  return view('frontend.plan-price'); });
    Route::get('earn', function () { return view('frontend.earn'); });
    Route::get('blog/detail', function () {  return view('frontend.blog-detail'); });
    Route::get('order-placement', function () { return view('frontend.order-placement'); });
    Route::get('feature', function () {  return view('frontend.feature'); });
    Route::get('agent', function () {  return view('frontend.agent'); });
    Route::get('affiliate', function () {  return view('frontend.affiliate'); });
    Route::get('partner', function () {  return view('frontend.partner'); });
    Route::resource('/', 'Frontend\IndexController');

    Route::get('/', 'Frontend\IndexController@index')->name('frontend');

    Route::post('/save-user', 'Admin\UserController@store');
    Route::post('/users-login', 'User\Auth\LoginController@login')->name('user.login.store');
    // Route::post('/free-trial-demo', 'Admin\DemoPendingController@store_free_trial');
    Route::post('/save/order', 'Admin\OrderPendingController@saveOrder');
    Route::post('/save/inquiry', 'Admin\InquiryController@store');


    Auth::routes(['verify' => true]);
    Route::get('/user/verify/{token}', 'Admin\UserController@verifyUser');


    //for admin dashboard

    //Route::group(['middleware' => ['auth:admin,web'],['verify' => true]], function() {
    Route::group(['middleware' => ['auth:admin,web']], function () {
        Route::resource('/free-trial', 'V1\DemoController');
        Route::get('check/workspace/{workspace}', 'V1\DemoController@checkWorkspace');
        Route::resource('plans', 'V1\PlanController');
        Route::resource('validities', 'V1\ValidityController');





        //Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/dashboard', 'HomeController@index')->name('home');
        Route::resource('/about', 'Admin\AboutController');
        //    Route::get('/about/delete/{id}','Admin\AboutController@destroy')->name('about.destroy');
        Route::resource('/slider', 'Admin\SliderController');
        Route::resource('/service', 'Admin\ServiceController');
        Route::resource('/social', 'Admin\SocialController');


        Route::resource('/contact', 'Admin\ContactController');
        Route::get('/contact_view', 'Admin\ContactController@index1')->name('contact-view');
        Route::resource('/inquiry', 'Admin\InquiryController')->except(['store']);

        Route::resource('/product', 'Admin\ProductController');


        Route::resource('themes', 'Admin\ThemesController');


        //start from demo
        Route::resource('/demo-pending', 'Admin\DemoPendingController');
        Route::post('/demo-pending-edit', 'Admin\DemoPendingController@demo_pending_edit');
        Route::get('/demo-approved', 'Admin\DemoPendingController@getApprovedDemo');
        Route::post('/demo-status/{id}', 'Admin\DemoPendingController@setDemoStatus');
        Route::get('/demo-rejected', 'Admin\DemoPendingController@getRejectedDemo');

        Route::resource('/order-pending', 'Admin\OrderPendingController');
        Route::post('/order-pending-edit', 'Admin\OrderPendingController@order_pending_edit');

        Route::get('/order-approved', 'Admin\OrderPendingController@getApprovedOrder');
        Route::post('/order-status/{id}', 'Admin\OrderPendingController@setOrderStatus');
        Route::get('/order-rejected', 'Admin\OrderPendingController@getRejectedOrder');
        Route::get('/order-suspended', 'Admin\OrderPendingController@getSuspendedOrder');

        Route::resource('/bill-pending', 'Admin\BillPendingController');
        Route::post('/edit-bill-pending', 'Admin\BillPendingController@bill_pending_edit');
        Route::post('/delete-bill-pending', 'Admin\BillPendingController@destroy');

        Route::resource('/bill-paid', 'Admin\BillPaidController');
        Route::post('/add-bill-paid', 'Admin\BillPaidController@store');
        Route::resource('/bill-canceled', 'Admin\BillCanceledController');

        Route::resource('/client', 'Admin\ClientController');


        Route::resource('/student-pending', 'Admin\StudentPendingController');
        Route::resource('/student-approved', 'Admin\StudentApprovedController');
        Route::resource('/student-rejected', 'Admin\StudentRejectedController');
        Route::resource('/student-suspended', 'Admin\StudentSuspendedController');


        Route::resource('/affilated-pending', 'Admin\AffilatedPendingController');
        Route::resource('/affilated-approved', 'Admin\AffilatedApprovedController');
        Route::resource('/affilated-rejected', 'Admin\AffilatedRejectedController');
        Route::resource('/affilated-suspended', 'Admin\AffilatedSuspendedController');

        Route::resource('/partner-pending', 'Admin\PartnerPendingController');
        Route::resource('/partner-approved', 'Admin\PartnerApprovedController');
        Route::resource('/partner-rejected', 'Admin\PartnerRejectedController');
        Route::resource('/partner-suspended', 'Admin\PartnerSuspendedController');

        Route::resource('/user', 'Admin\UserController');
        Route::post('/add-user', 'Admin\UserController@store');
        Route::post('/edit-user', 'Admin\UserController@user_edit');
        Route::post('/update-user', 'Admin\UserController@update_user');

        Route::resource('/feedback', 'Admin\FeedbackController');
        Route::post('/add-feedback', 'Admin\FeedbackController@store');

        Route::resource('/package', 'Admin\PackageController');
        Route::post('/add-package', 'Admin\PackageController@store');
        Route::post('/edit-package', 'Admin\PackageController@edit_package');
        Route::post('/delete-package', 'Admin\PackageController@destroy');
        Route::post('/update-package', 'Admin\PackageController@update');

        Route::resource('/sub-package', 'Admin\SubPackageController');

        Route::resource('/plan-pricing', 'Admin\PlanPricingController');
        Route::post('/add-plan-pricing', 'Admin\PlanPricingController@store');
        Route::post('/edit-plan-pricing', 'Admin\PlanPricingController@edit_plan_pricing');
        Route::post('/edit-plan-pricing1', 'Admin\PlanPricingController@edit_plan_pricing1');
        Route::post('/delete-plan-pricing', 'Admin\PlanPricingController@destroy');
        Route::post('/update-plan-pricing', 'Admin\PlanPricingController@update');

        Route::resource('/offer-discount', 'Admin\OfferDiscountController');
        Route::post('/add-offer-discount', 'Admin\OfferDiscountController@store');
        Route::post('/edit-offer-discount', 'Admin\OfferDiscountController@edit_offer_discount');
        Route::post('/edit-offer-discount2', 'Admin\OfferDiscountController@edit_offer_discount2');
        Route::post('/edit-offer-discount1', 'Admin\OfferDiscountController@edit_offer_discount1');
        Route::post('/delete-offer-discount', 'Admin\OfferDiscountController@destroy');
        Route::post('/update-offer-discount', 'Admin\OfferDiscountController@update');

        Route::resource('/payment-type', 'Admin\PaymentTypeController');
        Route::post('/add-payment-type', 'Admin\PaymentTypeController@store');
        Route::post('/edit-payment-type', 'Admin\PaymentTypeController@edit_payment_type');
        Route::post('/delete-payment-type', 'Admin\PaymentTypeController@destroy');
        Route::post('/update-payment-type', 'Admin\PaymentTypeController@update');



        //from e-billing
        Route::resource('bank', 'Admin\BankController');
        Route::post('bank-edit', 'Admin\BankController@bank_edit')->name('bank-edit');
        Route::post('bank-update', 'Admin\BankController@update')->name('bank-update');
        Route::get('/bank/delete/{id}', 'Admin\BankController@destroy')->name('bank.destroy');

        Route::resource('payment-method', 'Admin\PaymentMethodController');
        Route::post('payment-method-edit', 'Admin\PaymentMethodController@payment_method_edit');
        Route::post('payment-method-update', 'Admin\PaymentMethodController@update');
        Route::get('/payment-method/delete/{id}', 'Admin\PaymentMethodController@destroy')->name('payment-method.destroy');

        Route::resource('account-head', 'Admin\AccountHeadController');

        Route::post('account-head-edit', 'Admin\AccountHeadController@account_head_edit');
        Route::post('account-head-delete', 'Admin\AccountHeadController@destroy');
        Route::post('account-head-update', 'Admin\AccountHeadController@update');

        Route::get('/account-head/delete/{id}', 'Admin\AccountHeadController@destroy')->name('account-head.destroy');


        Route::resource('expense', 'Admin\ExpenseController');
        Route::post('expense-edit', 'Admin\ExpenseController@expense_edit');
        Route::post('expense-delete', 'Admin\ExpenseController@destroy');
        Route::post('expense-update', 'Admin\ExpenseController@update');
        Route::resource('income', 'Admin\IncomeController');
        Route::post('income-edit', 'Admin\IncomeController@income_edit');
        Route::post('income-delete', 'Admin\IncomeController@destroy');
        Route::post('income-update', 'Admin\IncomeController@update');


        Route::resource('employee', 'Admin\EmployeeController');
        Route::post('employee-update', 'Admin\EmployeeController@update1')->name('employee.update');
        Route::post('employees-update', 'Admin\EmployeeController@update');
        Route::post('employee-edit', 'Admin\EmployeeController@employee_edit');
        Route::post('employee-delete', 'Admin\EmployeeController@destroy');
        Route::get('/employee/delete/{id}', 'Admin\EmployeeController@destroy')->name('employee.destroy');



        Route::resource('salary-payroll', 'Admin\SalaryPayrollController');

        Route::post('change-status-payroll', 'Admin\SalaryPayrollController@ChangeStatus')->name('change.status.payroll');

        Route::post('search', 'Admin\SalaryPayrollController@search')->name('salary-payroll.search');
        Route::post('search-due', 'Admin\SalaryDueController@search')->name('salary-due.search');

        Route::post('search-master', 'Admin\SalaryMasterController@search')->name('salary-master.search');
        Route::post('salary-master-update', 'Admin\SalaryMasterController@updateMasterSalary')->name('salary-master.update');
        // Route::post('salary-master-store','Admin\SalaryMasterController@storeEmployee')->name('admin.store.employee.master');

        Route::post('store_salary_master', 'Admin\SalaryMasterController@storeEmployee')->name('store.employee.salary.master');
        Route::post('salary-payroll-edit', 'Admin\SalaryPayrollController@employee_edit');
        Route::post('salary-payroll-edit2', 'Admin\SalaryPayrollController@payroll_edit');
        Route::resource('salary-type', 'Admin\SalaryTypeController');
        Route::get('salary-type-delete/{id}', 'Admin\SalaryTypeController@destroy')->name('salary-type.delete');
        Route::resource('salary-master', 'Admin\SalaryMasterController');

        Route::post('salary-master-month', 'Admin\SalaryMasterController@month')->name('salary-master-month');

        Route::post('salary-master-edit', 'Admin\SalaryMasterController@employee_edit');
        Route::resource('salary-due', 'Admin\SalaryDueController');
        Route::post('salary-due-edit', 'Admin\SalaryDueController@employee_edit');




        Route::resource('lead-management', 'Admin\LeadManagementController');
        Route::post('lead-delete', 'Admin\LeadManagementController@destroy');
        Route::post('lead-edit', 'Admin\LeadManagementController@lead_edit');
        Route::get('lead-management-delete/{id}', 'Admin\LeadManagementController@destroy')->name('lead-management.destroy');
        Route::post('add-estimate', 'Admin\LeadManagementController@add_estimate')->name('add-estimate.store');
        Route::get('estimate-destroy/{id}', 'Admin\LeadManagementController@estimate_destroy')->name('add-estimate.destroy');
        Route::get('estimate-show/{id}', 'Admin\LeadManagementController@estimate_show')->name('add-estimate.show');
        Route::post('add-follow-store', 'Admin\LeadManagementController@add_follow_store')->name('add_follow.store');
        Route::post('add-follow-edit', 'Admin\LeadManagementController@add_follow_edit');
        Route::post('add-follow-update', 'Admin\LeadManagementController@add_follow_update')->name('add_follow.update');

        Route::post('contact-person-store', 'Admin\LeadManagementController@contact_store')->name('contact.store');
        Route::post('contact-person-edit', 'Admin\LeadManagementController@contact_edit');
        Route::post('contact-person-update', 'Admin\LeadManagementController@contact_update')->name('contact.update');

        Route::post('lead-management-search', 'Admin\LeadManagementController@search')->name('lead-management.search');
        Route::get('lead-assignment', 'Admin\LeadManagementController@lead_assignment')->name('lead-assignment');

        Route::post('lead-assignment/{id}', 'Admin\LeadManagementController@lead_assignment_store')->name('lead-management.lead_assignment_store');
        Route::post('lead-assignment-search', 'Admin\LeadManagementController@lead_search')->name('lead-assignment.search');
        Route::get('lead-followup', 'Admin\LeadManagementController@lead_followup')->name('lead-followup');
        Route::post('lead-follow-up-search', 'Admin\LeadManagementController@followup_search')->name('lead-followup.search');
        Route::post('lead-followup', 'Admin\LeadManagementController@lead_followup_store')->name('lead-management.lead_followup_store');
    });




    Route::namespace('Admin\Auth')->group(function () {

        //login route
        Route::get('/login', 'LoginController@showLoginForm')->name('login');
        Route::post('/login', 'LoginController@login')->name('login.store');
        Route::get('/logout', 'LoginController@logout')->name('logout');

        //forget password
        Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::get('/password/email', 'ForgotPasswordController@sendLinkRequestForm')->name('password.email');


        //reset password
        Route::get('/password/reset{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        Route::get('/password/reset', 'ResetPasswordController@reset')->name('password.update');
    });


    //    Route::namespace('User\Auth',(['verify' => true]))->group(function (){
    Route::namespace('User\Auth')->group(function () {


        //    Route::get('/','LoginController@userShowLoginForm')->name('user-login');
        //        Route::get('/user-login','LoginController@userLoginForm')->name('user.login.form');
        //        Route::post('user-login','LoginController@userLogin')->name('user.login.store');
        //        Route::get('/user-logout','LoginController@userLogout')->name('user.logout');
        //        Route::get('/user-logout/{slug}/logout','LoginController@userLogoutUser')->name('user.logout.user');

        Route::post('/user-login', 'LoginController@login')->name('user.login.store');
        Route::get('/user-logout', 'LoginController@logout')->name('user.logout');
    });
