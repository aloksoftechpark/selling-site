$(document).ready(function () {
    var maxField = 10000000000000000000000000000000; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="field_mar"><div class="appended_wrapper"><hr><div class="row"><div class="col-md-3">Item<div class="d-md-flex mg-t-5"><div class="input-group-prepend"><span class="input-group-text form-control-sm wd-100p brief_icon" id="basic-addon1"><i class="icon ion-briefcase"></i></span></div><input type="text" class="form-control form-control-sm" aria-label="item" aria-describedby="basic-addon1"></div></div><div class="col-md-3"><div class="mg-t-5">Valid Date <input type="text" name="valid date" value=""></div></div> <div class="col-md-3"><div class="mg-t-5">Valid Date <input type="text" name="valid date" value=""></div> </div><div class="col-md-3"><div class="mg-t-5">Valid Date <input type="text" name="valid date" value=""></div></div></div><div class="row"><div class="col-md-3"><div class="mg-t-5">Discount<div class="d-md-flex pd-0"> <input type="text" name="lastname" class="form-control form-control-sm wd-40" required=""><div class="input-group-append"><span class="input-group-text form-control-sm pd-5 border_r_0" id="basic-addon2">%</span></div><input type="text" name="lastname" class="form-control form-control-sm wd-80" required=""></div></div></div><div class="col-md-9"> <div class="mg-t-5"> Description<input type="text" name="description"></div></div></div><a href="javascript:void(0);" class="remove_button"><img src="../assets/img/remove-icon.png"/></a></div></div>'; //New input field html
    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addButton).click(function () {
        //Check maximum number of input fields
        if (x < maxField) {
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function (e) {
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});



/*...............click table a href.............*/
$(document).ready(function ($) {
    $(".table-row").click(function () {
        window.document.location = $(this).data("href");
    });
});


$(document).ready(function () {
    $('.btnNext').click(function () {
        $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
    });

    $('.btnPrevious').click(function () {
        $('.nav-tabs .active').parent().prev('li').find('a').trigger('click');
    });
});




$(function () {

    $('#chkveg').multiselect({

        includeSelectAllOption: true

    });

    $('#btnget').click(function () {

        alert($('#chkveg').val());

    })

});




/*select-option with tabs*/



//hide all tabs first
$('.select-content').hide();
//show the first tab content
$('#tab-1').show();

$('#select-box').change(function () {
    dropdown = $('#select-box').val();
    //first hide all tabs again when a new option is selected
    $('.option-content').hide();
    //then show the tab content of whatever option value was selected
    $('#' + "tab-" + dropdown).show();
});



$('.select-content').hide();
//show the first tab content
$('#mtab-3').show();

$('#select-box2').change(function () {
    dropdown = $('#select-box2').val();
    //first hide all tabs again when a new option is selected
    $('.option-content2').hide();
    //then show the tab content of whatever option value was selected
    $('#' + "mtab-" + dropdown).show();
});


/*
$(document).ready(function () {
  $('.btnNext').click(function () {
    $('.nav-tabs > .active').next('li').find('a').trigger('click');
  });

  $('.btnPrevious').click(function () {
    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
  });
});
 */


/* .............highlight if click function............. */
$(document).ready(function () {

    $('#mylinks a').click(function () {
        $('#mylinks a').removeClass('highlight');
        $(this).addClass('highlight');
    });

});






var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
    // This function will display the specified tab of the form...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    //... and fix the Previous/Next buttons:
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Submit";
    } else {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
    //... and run a function that will display the correct step indicator:
    fixStepIndicator(n)
}

function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form...
    if (currentTab >= x.length) {
        // ... the form gets submitted:
        document.getElementById("regForm").submit();
        return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
}

function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
        // If a field is empty...
        if (y[i].value == "") {
            // add an "invalid" class to the field:
            y[i].className += " invalid";
            // and set the current valid status to false
            valid = false;
        }
    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
}

function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class on the current step:
    x[n].className += " active";
}


//for lead-detail popup & close
var el = document.getElementById('lead-detail');
el.addEventListener('click', function () {
    document.querySelector('.lead-detail').style.display = 'flex';
});

var el = document.getElementById('lead-detail-close');
el.addEventListener('click', function () {
    document.querySelector('.lead-detail').style.display = 'none';
});


//for lead-assignment popup & close
var assign = document.getElementById('popup-container');
assign.addEventListener('click', function () {
    document.querySelector('.popup-container').style.display = 'flex';
});

var assign = document.getElementById('popup-close');
assign.addEventListener('click', function () {
    document.querySelector('.popup-container').style.display = 'none';
});



























$(document).ready(function () {
    $('#dtVerticalScrollExample').DataTable({
        "scrollY": "200px",
        "scrollCollapse": true,
    });
    $('.dataTables_length').addClass('bs-select');
});



function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#reseller')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}



















/*......search wih find.......*/
function myFunction() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function myFunction1() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput1");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable1");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function myFunction2() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput2");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable2");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function myFunction3() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput3");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable3");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
